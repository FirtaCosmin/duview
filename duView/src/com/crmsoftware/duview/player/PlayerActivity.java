package com.crmsoftware.duview.player;


import android.app.KeyguardManager;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnSystemUiVisibilityChangeListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.IMvpActivity;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.slidemenu.MvpMenu;
import com.crmsoftware.duview.ui.MvpFragment;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;

public class PlayerActivity extends FragmentActivity implements 
IMvpActivity, 
IPlayerView, 
OnClickListener, 
OnSystemUiVisibilityChangeListener
{
	public static boolean DEBUG = Log.DEBUG; 
    private static final String TAG = PlayerActivity.class.getSimpleName();
    
    public static final String LAYOUT_ID = "layout_id";

	PlayerView viewPlayer;
	MvpViewHolder holder;
	
	
	/**
	 * @desc here the padding details of the controls will be saved and will be applied 
	 * when systemUI is visible 
	 */
	private Rect mControlsPaddingWithUI = new Rect(0,0,0,0);
	

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onCreate(Bundle data) 
	{
		if (DEBUG) Log.m(TAG,this,"onCreate");
		
		super.onCreate(data);
		
		  //Remove title bar
	    this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    
		Bundle extrasData  = getIntent().getExtras();
		
		int layoutId = extrasData.getInt(LAYOUT_ID, 0);
		setContentView(layoutId);
		
		holder = new MvpViewHolder(findViewById(android.R.id.content));
		
		viewPlayer = (PlayerView)findViewById(R.id.player_view);
		viewPlayer.showBorder(false);
		viewPlayer.setPlayerListener(this);
		holder.setClickListener(this, 
				R.id.player_play, 
				R.id.player_pause, 
				R.id.player_close,
				R.id.player_fullscreen,
				R.id.player_body_view);

		//viewPlayer.getViewTreeObserver().addOnGlobalLayoutListener(this);
		viewPlayer.updateView();
		
		viewPlayer.setVisibility(View.VISIBLE);
		viewPlayer.setOnSystemUiVisibilityChangeListener(this);
		hideSystemUI();
		
		FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
		viewPlayer.setLayoutParams(params);
		
		/*crea the rectangle that defines the paddings for the navigation panel*/
		createPaddingRect();
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
	        super.onWindowFocusChanged(hasFocus);
	    if (hasFocus) {
	    	/*commented out because on some devices after the swipe the view 
	    	 * gets focus thus hiding the UI after a very short time*/
//	    	hideSystemUI();
	    }
	}
	

	/**
	 * @desc will flip the System UI and the controller bar visibility 
	 */
	private void flipSystemUI(){

		if (viewPlayer.isControlBarVisible()){
			viewPlayer.showControlBar(false);
			hideSystemUI();	
		}else{

			viewPlayer.showControlBar(true, mControlsPaddingWithUI);
			showSystemUI();
		}
		
		
	}
	
	// This method hides the system bars.
	private void hideSystemUI() {
	    // Set the IMMERSIVE flag.
	    // Set the content to appear under the system bars so that the content
	    // doesn't resize when the system bars hide and show.
		viewPlayer.setSystemUiVisibility(
	            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
	            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
	            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
	            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
	            | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
	            | View.SYSTEM_UI_FLAG_IMMERSIVE);
	}

	// This methods shows the system bars. It does this by removing all the flags
	// except for the ones that make the content appear under the system bars.
	private void showSystemUI() {
		viewPlayer.setSystemUiVisibility(
	            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
	            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
	            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
	}
	
/*	@SuppressLint("NewApi")
    public void onGlobalLayout() 
    {
		if (DEBUG) Log.m(TAG,this,"onGlobalLayout");
    
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) 
        {
        	viewPlayer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        } 
        else 
        {
        	viewPlayer.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        }
        
		
		viewPlayer.attachPlayer();
    }*/

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	protected void onResume() {
		if (DEBUG) Log.m(TAG,this,"onResume");

		if ( !isSreenLocked() ){
			if (DEBUG) Log.d(TAG,"OnResume:: do not resume the player because screen lock");
			/*if screen is locked do nothing*/
			viewPlayer.onParentResume();
		}

		
		super.onResume();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	protected void onPause() {
		if (DEBUG) Log.m(TAG,this,"onPause");
		
		viewPlayer.onParentPaused();
		
		super.onPause();
	}
	
	@Override
	protected void onDestroy() {
		if (DEBUG) Log.m(TAG,this,"onDestroy");
		
		
		super.onDestroy();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onBackPressed() 
	{
		if (DEBUG) Log.m(TAG,this,"onBackPressed");
		
		Utils.clickView(holder.getView(R.id.player_fullscreen),true);
	}


	@Override
	public void setActionBarVisibility(int visibility) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void setMenuEnabled(boolean bState) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void setTitle(String title) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void goTo(Class fragmentType, String fragmentTag,
			Bundle fragmentData, boolean addToBackStack) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public MvpMenu getMenu() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void clearStack() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void showLoadingDialog() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void hideLoadingDialog() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public MvpFragment getCurrentScreen() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void setHomeButtonActionBar(boolean isVisible) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void show(Class classType, String fragmentTag, Bundle fragmentData) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void hide(String fragmentTag) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onVideoSizeChanged(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onVideoCompletion() 
	{
		finish();
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onClick(View v) 
	{
		if (DEBUG) Log.m(TAG,this,"onClick");		
		
		switch (v.getId()) 
		{
			
		case R.id.player_close:
		{
			viewPlayer.destroyPlayer();
			finish();
		}
		break;
		
		case R.id.player_body_view:
		{
//			viewPlayer.showControlBar(!viewPlayer.isControlBarVisible());
//			hideSystemUI();
			flipSystemUI();
		}
		break;
		
		case R.id.player_play:
		{
			viewPlayer.play();
		}
		break;
		
		case R.id.player_pause: 
		{
			viewPlayer.pause();
		}
		break;
			
		case R.id.player_fullscreen:
		{	
			finish();
		}
		break;
		
		default:
		break;
		}	
	}

	
	/**
	 * @desc method for testing if the device screen is locked or not. It does this by testing if 
	 * the keyboard is in restricted mode.
	 * @return true if device locked, false if not
	 */
	private boolean isSreenLocked(){

	    KeyguardManager myKM = (KeyguardManager) MainApp.getContext().getSystemService(Context.KEYGUARD_SERVICE);
	    if( myKM.inKeyguardRestrictedInputMode()) {
	     return true;
	    } else {
	    	return false;
	    }
	}

	@Override
	public void onSystemUiVisibilityChange(int visibility) {
		// TODO Auto-generated method stub
		// Note that system bars will only be "visible" if none of the
        // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
        if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
            // TODO: The system bars are visible. Make any desired
            // adjustments to your UI, such as showing the action bar or
            // other navigational controls.
        } else {
            // TODO: The system bars are NOT visible. Make any desired
            // adjustments to your UI, such as hiding the action bar or
            // other navigational controls.
        }
		
	}
	
	
	
	/**
	 * @desc will add the padding values to mControlsPaddingWithUI depending on 
	 * the screen density and device type 
	 */
	private void createPaddingRect(){
		/*set the top padding*/
		int topPad = (int) getResources().getDimension(R.dimen.full_screen_control_top_padding);
		mControlsPaddingWithUI.top = Utils.dpToPx(topPad);
		if ( !Utils.hasHwKeys() ){
			/*if device has sw navigation keys then apply padding on the keys part*/
			int navPad = (int)getResources().getDimension(R.dimen.full_screen_control_nav_btn_padding);
			if (  MainApp.isTablet() ){
				/*tablets have the button bar in the bottom of the screen*/
				mControlsPaddingWithUI.bottom = Utils.dpToPx(navPad);
			}else{
				/*phones have the button bar in the right part of the screen*/
				mControlsPaddingWithUI.right = Utils.dpToPx(navPad);
			}
		}
	}


}
