package com.crmsoftware.duview.player;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.drm.DrmErrorEvent;
import android.drm.DrmManagerClient;
import android.drm.DrmManagerClient.OnErrorListener;
import android.media.AudioManager;
import android.os.Handler;
import android.view.SurfaceHolder;

import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.core.common.HttpReqParams;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.core.common.TaskSingleReqParse;
import com.crmsoftware.duview.data.MvpContent;
import com.crmsoftware.duview.data.MvpError;
import com.crmsoftware.duview.data.MvpMedia;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.data.MvpServerError;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.player.drm.DRMContentInfo;
import com.crmsoftware.duview.player.drm.TaskCheckLicense;
import com.crmsoftware.duview.player.smil.model.MpxSmil;
import com.crmsoftware.duview.player.smil.model.MpxSmilConcurrency;
import com.crmsoftware.duview.player.smil.model.MpxUnlockConcurrency;
import com.crmsoftware.duview.player.smil.model.MpxUpdateConcurrecy;
import com.crmsoftware.duview.player.smil.model.MpxUpdateConcurrecy.UpdateLockResponse;
import com.crmsoftware.duview.ui.AuthFragment;
import com.crmsoftware.duview.utils.IHttpConst;
import com.crmsoftware.duview.utils.Log;
import com.insidesecure.drmagent.v2.AuthenTecMediaPlayer;
import com.insidesecure.drmagent.v2.DRMContent;
import com.insidesecure.drmagent.v2.mediaplayer.MediaPlayer;
import com.insidesecure.drmagent.v2.mediaplayer.MediaPlayer.OnBufferingUpdateListener;
import com.insidesecure.drmagent.v2.mediaplayer.MediaPlayer.OnCompletionListener;
import com.insidesecure.drmagent.v2.mediaplayer.MediaPlayer.OnPreparedListener;
import com.insidesecure.drmagent.v2.mediaplayer.MediaPlayer.OnSeekCompleteListener;
import com.insidesecure.drmagent.v2.mediaplayer.MediaPlayer.OnVideoSizeChangedListener;

public class MvpPlayer implements ITaskReturn, OnBufferingUpdateListener,
OnCompletionListener, OnPreparedListener, OnSeekCompleteListener,OnVideoSizeChangedListener,
OnErrorListener
{
	public static boolean DEBUG = Log.DEBUG; 
	public static boolean VERBOUSE = Log.VERBOUSE ; 
	public static boolean IS_TRACKING = false;
	public final static String TAG = MvpPlayer.class.getSimpleName();
	
	public final static String PLAYER_STATE = "PLAYER_STATE";
	public enum MvpPlayerState {DESTROYED, UNLOCK, SMIL, LOCK, LICENSE, PREPARED, START, PLAYING, PAUSED};
	public enum MvpPlayerError {TOKEN, LOCK, UPDATELOCK, UNLOCK, SMIL, LICENSE, MEDIAPLAYER, NOCONNECTION};

	// 
	public final static String BLACKSCREEN_MODELS = "SM-T535";
	
	public static MvpPlayer instance;
	
	private TaskSmilReqParse  taskSmilReqParse = null;

	private TaskCheckLicense taskCheckLicense = null;
	
	private TaskSingleReqParse  taskUpdateLock = null;
	private TaskSingleReqParse taskUnlock = null;
	private MpxSmil smil;
	
    private MvpPlayerState currentState = MvpPlayerState.DESTROYED;
    
	private MvpMedia mvpMedia;
	private MediaPlayer mMediaPlayer;

    private MpxSmilConcurrency smilConcurency;
    private MpxUpdateConcurrecy smilUpdateConcurency;
    
    private DRMContentInfo mDRMContentInfo;
    
	private PlayerView playerView;
	
	private boolean isPrepared = false;
	
	private long updateLockExpireTime = 0;	
	private int timerUpdateSecs = 2;			
	private int currentPosition = 0;			
	/**
	 * @desc flag to tell the player if it will use the allready created media player when going 
	 * 		 to full screen or it will create a new one.
	 * 		 true - it will use the already created media player
	 * 		 false - it will create a new media player
	 * 		 constant linked with the MvpUserData.fastPlayer flag
	 */
	public static boolean FAST_SWITCHING = true;		
	
	private static MvpPlayerInfo playerInfo = new MvpPlayerInfo();

	private Timer timer;
	private boolean userPausedPlayer = false;
    
	//tells handler to send a message
		class ProgressTask extends TimerTask 
		{
	        @Override
	        public void run() 
	        {
	        	if (DEBUG) Log.d(TAG,"ProgressTask::run ");
	        	int updateLockTime = getUpdateHeartBeat();
	        	if (updateLockTime > 0)
	        	{
	        		long currenTime = Calendar.getInstance().getTimeInMillis();
	        		
	        		if (updateLockExpireTime == 0)
	        		{
	        			updateLockExpireTime = currenTime + updateLockTime *1000;
	        			return;
	        		}
	        		
	            	if (currenTime >= updateLockExpireTime)
	            	{
	            		performUpdateLockTask();
	            		
	            		updateLockExpireTime = currenTime + updateLockTime * 1000;
	            	}
	        	}
	        	
	        	PlayerView view  = getPlayerView();
	        	if (view == null)
	        	{
	        		return;
	        	}
	        	
    			if (isPlaying())
    			{
        			view.updateProgressBar(mMediaPlayer.getCurrentPosition(), mMediaPlayer.getDuration());
    			}
    		}
        };
    
    /**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
    public static MvpPlayer getInstance()
    {
		if (DEBUG) Log.d(TAG,"getInstance");
    	if (instance  == null)
    	{

    		setFastSwitchingMode();
    			
    		instance  = new MvpPlayer();
    	}
    	return instance;
    }
    
    
    /**
 	 * 
 	 *
 	 * @param  
 	 * @return      
 	 * @see         
 	 */	
     public static void setFastSwitchingMode()
     {
 		if (DEBUG) Log.d(TAG,"setFastSwitchingMode");
   	
		MvpObject mvpObjAuthData = MvpObject.getReference(AuthFragment.MVP_AUTH_DATA);
		MvpUserData authData = (MvpUserData)mvpObjAuthData.getData();
		
		MvpPlayer.FAST_SWITCHING = authData.isFastPlayer();
		
     }
    
    
    /**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
    public static void removeInstance()
    {
 		if (DEBUG) Log.d(TAG,"removeInstance");
    	instance = null;
    } 
    
    
    /**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
    public MvpMedia getMedia()
    {
    	return mvpMedia;    
    } 
    
    
    
	
  	/**
  	 * 
  	 *
  	 * @param  
  	 * @return      
  	 * @see         
  	 */		
  	private void startTimer() 
  	{
		if (DEBUG) Log.m(TAG,this,"startTimer");
		
  		if (timer != null)
  		{
  			stopTimer();	
  		}

  		timer = new Timer();
  		timer.schedule(new ProgressTask(), 0, timerUpdateSecs*1000);		
  	}
  	
  	
  	/**
  	 * 
  	 *
  	 * @param  
  	 * @return      
  	 * @see         
  	 */		
  	private PlayerView getPlayerView() 
  	{
  		return playerView;
  	}
    
    /**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
    private void onError(MvpPlayerError error, Object data)
    {
 		if (DEBUG) Log.d(TAG,"onError");
    	if (playerView != null)
    	{
    		playerView.onError(error, data);
    	}
    }
  
    
    
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
    public MvpPlayerState getState()
    {
    	return currentState;
    }
    
    
    /**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
    public void setPlayerView(PlayerView playerView)
    {
		if (DEBUG) Log.m(TAG,this,"attachToView");
		
    	this.playerView = playerView;
    }
    
    public boolean isPrepared(){
    	return isPrepared;
    }
    
    /**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
    public void attachToView(PlayerView playerView)
    {
		if (DEBUG) Log.m(TAG,this,"attachToView");
			
		setPlayerView(playerView);
    	
    	if (null == mMediaPlayer)
    	{
    		return;
    	}

    	Runnable run = new Runnable() {
			
			@Override
			public void run() {

				if (DEBUG) Log.m(TAG,MvpPlayer.this,"attachToView::run");
				
				if (false == isPrepared)	// not prepared no attachement
				{
					if (DEBUG) Log.d(TAG,"\t\t player not prepared");
					return;
				}
				
				MediaPlayer player = MvpPlayer.getInstance().getPlayer();
				if (DEBUG) Log.d(TAG,"\t\t mediaplayer=" + player);
		    	if (null == player)
		    	{
		    		return;
		    	}
		    	
		    	PlayerView view  = MvpPlayer.getInstance().getPlayerView();
				if (DEBUG) Log.d(TAG,"\t\t playerView=" + view);

		    	if (null == view)
		    	{
		    		return;
		    	}
		    	
		    	SurfaceHolder holder = view.getSurfaceHolder();
		    	
		    	if (null == holder)
		    	{
		    		if (DEBUG) Log.d(TAG,"\t\t no SurfaceHolder" );
		    		new Handler().postDelayed(this, 800);
		    		return;
		    	}
		    	
		    	if (holder.isCreating())
		    	{
		    		if (DEBUG) Log.d(TAG,"\t\t no SurfaceHolder is creating" );
		    		new Handler().postDelayed(this, 800);
		    		return;		    		
		    	}
		    	
		    	view.showPlayerLoading(false);
		    	try 
		    	{   		
		    		player.setDisplay(holder);
				} 
		    	catch (Throwable e) 
				{
					e.printStackTrace();
					return;
				}
		    	
		    	player.setScreenOnWhilePlaying(true);
		    	
		    	try 
		    	{   
		    		String model = android.os.Build.MODEL;
		    		if (DEBUG) Log.m(TAG,this,"device model: " + android.os.Build.MODEL);
		    		if (DEBUG) Log.m(TAG,this,"device product: " + android.os.Build.PRODUCT);		    		
		    		if (DEBUG) Log.m(TAG,this,"device device: " + android.os.Build.DEVICE);		 
		    		if (DEBUG) Log.m(TAG,this,"device brand: " + android.os.Build.BRAND);			    		
		    		
		    		if (userPausedPlayer == false)
		    		{
		    			setPlayerState(MvpPlayerState.START);
		    			player.start();
		    		}
				} 
		    	catch (Exception e) 
				{
					e.printStackTrace();
				}
				
			}
		};
		
		new Handler().post(run);
    }
 
    /**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
    public boolean isRunning()
    {
 		if (DEBUG) Log.d(TAG,"isRunning");
    	return playerView == null && mMediaPlayer != null;
    }
    
    
    /**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
    public boolean isPlaying()
    {
 		if (DEBUG) Log.d(TAG,"isPlaying");
    	if (null == mMediaPlayer )
    	{
    		return false;
    	}
    	
    	
    	try 
    	{
    		if (getState() == MvpPlayerState.PLAYING || getState() == MvpPlayerState.PAUSED || getState() == MvpPlayerState.PREPARED)
    		{
    			try{
    				return mMediaPlayer.isPlaying();
    				}
    			catch(Exception e)
    			{
    				return false;
    			}
    		}
    		
    		return false;
    	} 
    	catch (Exception e) 
    	{
			e.printStackTrace();
		}
    	
    	return false;

    }
    
    /**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
    @SuppressLint("NewApi")
	public void detachFromView()
    {
		if (DEBUG) Log.m(TAG,this,"detachFromView");
    	
    	playerView = null;
    	
    	if  (null == mMediaPlayer)
    	{
    		return;
    	}
    	
    	if (false == isPrepared)
    	{
    		return;
    	}
    	
    	try 
    	{
        	currentPosition = mMediaPlayer.getCurrentPosition();
		} 
    	catch (Exception e) 
    	{
			// TODO: handle exception
		}
    	
    	try 
    	{
        	pause(false);
		} 
    	catch (Exception e) 
    	{
			// TODO: handle exception
		}
    	

    }
    
    /**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	    
    public void release(boolean removeDRMContent)
    {
		if (DEBUG) Log.m(TAG,this,"release");
		
		if (null == mMediaPlayer)
		{
			return;
		}
	
		try 
		{
			mMediaPlayer.stop();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		try 
		{
			mMediaPlayer.setDisplay(null);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		mMediaPlayer.setOnBufferingUpdateListener(null);
		mMediaPlayer.setOnCompletionListener(null);
		mMediaPlayer.setOnPreparedListener(null);
		mMediaPlayer.setOnSeekCompleteListener(null);
		mMediaPlayer.setOnErrorListener(null);
		mMediaPlayer.setOnVideoSizeChangedListener(null);

		
		TaskReleasePlayer taskReleasePlayer;
		
    	if (removeDRMContent)
    	{
    		taskReleasePlayer = new TaskReleasePlayer(mMediaPlayer, mDRMContentInfo);

    	}
    	else
    	{
    		taskReleasePlayer = new TaskReleasePlayer(mMediaPlayer, null);  		
    	}
    	
		taskReleasePlayer.execute();
    }
    
    /**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	  
    protected void play(boolean fromUser)
    {
		if (DEBUG) Log.m(TAG,this,"play");
		
		if (fromUser)
		{
			userPausedPlayer = false;
		}
		
    	try
    	{
    		setPlayerState(MvpPlayerState.START);
    		mMediaPlayer.start();
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    }
    
    
    /**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	  
    protected void pause(boolean fromUser)
    {
		if (DEBUG) Log.m(TAG,this,"pause");
		
		if (fromUser)
		{
			userPausedPlayer = true;
		}
		
    	if (null == mMediaPlayer)
    	{
    		return;
    	}
    	
    	try
    	{
    		setPlayerState(MvpPlayerState.PAUSED);
    		mMediaPlayer.pause();
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	

    }
    
    
    /**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	  
    public void seekTo(int msecs)
    {
		if (DEBUG) Log.m(TAG,this,"seekTo");
		
    	if (null == mMediaPlayer)
    	{
    		return;
    	}
    	
		if (DEBUG) Log.d(TAG,"newPosition = " + msecs);
    	
    	try
    	{
    		mMediaPlayer.seekTo(msecs);
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    }
    
    /**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
    public int getUpdateHeartBeat()
    {
 		if (DEBUG) Log.d(TAG,"getUpdateHeartBeat");
    	if (null != smilConcurency)
    	{
    		return smilConcurency.heartbeatSeconds();
    	}
    	
    	return 0;
    }
    
    
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void play(MvpMedia mvpMedia)
	{
		if (DEBUG) Log.m(TAG,this,"play");
		
		if (IS_TRACKING) MainApp.getInstance().writetoLogFile("play: " + mvpMedia.getTitle() + " \n id: " + mvpMedia.getId());
		
		if (null != this.mvpMedia)	// to avoid multiple taps on watch
		{
			if (this.mvpMedia.getId().equals(mvpMedia.getId()))
			{
				return;	//is playing
			}
			
			destroy();			
		}

		setPlayerState(MvpPlayerState.SMIL);	
		
		this.mvpMedia = mvpMedia;
		MvpContent content = mvpMedia.getContent();
		 
		taskSmilReqParse = new TaskSmilReqParse(this, content.getUrl());
		taskSmilReqParse.execute();		
		
		isPrepared = false;
	}
	
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void replay()
	{
		if (DEBUG) Log.m(TAG,this,"replay");
		
		if (IS_TRACKING) MainApp.getInstance().writetoLogFile("play: " + mvpMedia.getTitle() + " \n id: " + mvpMedia.getId());

		if (null == smil)	// w8
		{
			return;
		}

		isPrepared = false;
		taskCheckLicense = new TaskCheckLicense(this, MainApp.getContext(), smil.getUrl(), mvpMedia.getContent());
		taskCheckLicense.execute();
	}
    
    
    /**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
    public MediaPlayer getPlayer()
    {
    	return mMediaPlayer;
    }
    
    
    
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected void startDRMPlayer()
	{
		if (DEBUG) Log.m(TAG,this,"startDRMPlayer");
		
		if (null == mDRMContentInfo)
		{
			return;
		}
		
		final DRMContent drmContent = mDRMContentInfo.getDRMContent(MainApp.getContext());
		
		new Handler().post(new Runnable() {
			
			@Override
			public void run() 
			{
		 		if (DEBUG) Log.d(TAG,"startDRMPlayer::run");
				MediaPlayer player = (AuthenTecMediaPlayer) (drmContent.retrieveMediaPlayer(MainApp.getContext()));
				PlayerView view = MvpPlayer.getInstance().getPlayerView();
				if (view != null)
				{
					view.startPlayer(player);
				}
				else
				{
					if (MvpPlayer.getInstance().getState() == MvpPlayerState.DESTROYED)
					{
						return;
					}
					
					if (DEBUG) Log.d(TAG,"playerView not ready");
					new Handler().postDelayed(this, 500);
				}
			}
		});
	}
    
    /**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
    public void startPlayer(MediaPlayer player)
    {
		if (DEBUG) Log.m(TAG,this,"startPlayer");
		
    	mMediaPlayer = player;
		
    	mMediaPlayer.setOnBufferingUpdateListener(this);
    	mMediaPlayer.setOnCompletionListener(this);
    	mMediaPlayer.setOnPreparedListener(this);
    	mMediaPlayer.setOnSeekCompleteListener(this);
//    	mMediaPlayer.setOnErrorListener(this);
    	mMediaPlayer.setOnVideoSizeChangedListener(this);

		
		mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

		setPlayerState(MvpPlayerState.PREPARED);
		
		try 
		{			
			mMediaPlayer.prepareAsync();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();

		}

    }
    
    
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
    public void destroy()
    {
		if (DEBUG) Log.m(TAG,this,"destroy");
		
		setPlayerState(MvpPlayerState.DESTROYED);
		
		userPausedPlayer = false;
    	cancelTasks();

    	release(true);

		mMediaPlayer = null;    
		mDRMContentInfo = null;
    }
    
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void setPlayerState (MvpPlayerState state) 
	{
		Log.d(TAG,"player state: " + state);
		
		currentState = state;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	

	public void cancelTasks() 
	{
		if (DEBUG) Log.m(TAG,this,"cancelTasks");
		
		if (timer != null)
		{
			stopTimer();
		}
		
		if (taskUpdateLock != null)
		{
			taskUpdateLock.cancel(true);
			taskUpdateLock = null;
		}	
		
		if (taskSmilReqParse != null)
		{
			 taskSmilReqParse.cancel(false);
			 taskSmilReqParse = null;			 
		}

        performUnlockTask();
		 
		if (taskCheckLicense != null)
		{
			 taskCheckLicense.cancel(true);
			 taskCheckLicense = null;
		}

	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void stopTimer() 
	{
 		if (DEBUG) Log.d(TAG,"stopTimer");
		if (null == timer)
		{
			return;
		}
		
		timer.cancel();
		timer.purge();
		timer = null;
	}
    

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onReturnFromTask(Task task, boolean canceled) 
	{
		if (DEBUG) Log.m(TAG,this,"onReturnFromTask");
		
		//-----------------------------
		
		//-----------------------------		
		if(task == taskUpdateLock)
		{
			if (taskUpdateLock.isError())
			{
	
				if (IS_TRACKING) MainApp.getInstance().writetoLogFile("error UpdateLock: " + taskUpdateLock.getError().toString());
				
				MvpError error = taskUpdateLock.getError();
				if (error instanceof MvpServerError)
				{
					if (((MvpServerError)error).getResponseCode() != -1) // everythign outside of internet
					{
						onError(MvpPlayerError.UPDATELOCK, "Connection keepalive error. (concurrency)");
						return;
					}
				}

				onError(MvpPlayerError.NOCONNECTION, "Please check your internet connection !");
				

				return;
			}
			
			smilUpdateConcurency = (MpxUpdateConcurrecy) taskUpdateLock.getReturnData();
			
			return;
		}
		
		//-----------------------------		
		if(task == taskUnlock)
		{
			smilConcurency = null;
			
			if (taskUnlock.isError())
			{
				
				if (IS_TRACKING) MainApp.getInstance().writetoLogFile("error Unlock: " + taskUnlock.getError().toString());
				
				onError(MvpPlayerError.UNLOCK, "Connection release error. (concurrency)" );
				return;
			}
			
			
			return;
		}
		
		//-----------------------------
		if (task instanceof TaskSmilReqParse)
		{
			if (IS_TRACKING) MainApp.getInstance().writetoLogFile("\n\nsmil Received ");
			
			if (task.isError())
			{
				onError(MvpPlayerError.SMIL, "Error getting media. (SMIL)");
				return;
			}
			

			smil = (MpxSmil)task.getReturnData();
			taskSmilReqParse = null;
			
			if (null == smil)
			{
				onError(MvpPlayerError.SMIL, "Invalid media. (SMIL)");
				return;
			}
			
			if (smil.isError())			
			{
				
				if (IS_TRACKING) MainApp.getInstance().writetoLogFile("error: " + smil.getTitle() +"\n"+smil.getDescription());
				
				String exceptionType = smil.getParamValue(MpxSmil.PARAM_EXCEPTION);
				
				if (smil.getDescription().contains("token"))
				{			
					onError(MvpPlayerError.TOKEN, "Your session has expired.");
					return;
				}
				
				//title="Concurrency Restriction" abstract="Unable to find valid or current authentication token.">			<param name="isException" value="true"/>			<param name="exception" value="ConcurrencyLimitViolationAuth"/>			<param name="responseCode" value="403"/>		
				//title="License Does Not Grant Access" abstract="Subscription license check failed: Release is restricted. Entitlement check failed: There are no entitlements for the current user and device that allow access to the release.">			<param name="isException" value="true"/>			<param name="exception" value="LicenseNotGranted"/>			<param name="responseCode" value="403"/>
				
				if (exceptionType.equals(MpxSmil.EXCEPTION_CONCURRENCY_LIMIT_VIOLATION_AUTH))
				{
					onError(MvpPlayerError.SMIL, "Number of active players reached. Please wait.");
					return;
				}
				
				if (exceptionType.equals(MpxSmil.EXCEPTION_LICENSE_NOT_GRANTED))
				{
					
				}	
				
				onError(MvpPlayerError.SMIL, "Media retrieve error: " + smil.getDescription());
				return;
			}
			
			if (IS_TRACKING) MainApp.getInstance().writetoLogFile("smil OK: " + smil.getDescription());
			
			
			smilConcurency = smil.getSmilConcurrency();
			smilConcurency.setClientId(MainApp.getInstance().getDeviceId().toString());
			
			smilUpdateConcurency = new MpxUpdateConcurrecy();
			UpdateLockResponse updateData = smilUpdateConcurency.getUpdateLock();
			
			updateData.setEncryptedLock(smilConcurency.LockEncr());
			updateData.setId(smilConcurency.getId());
			updateData.setSequenceToken(smilConcurency.sequenceToken());
			
			if (canceled)
			{
				performUnlockTask();
				return;
			}
			
			taskCheckLicense = new TaskCheckLicense(this, MainApp.getContext(), smil.getUrl(), mvpMedia.getContent());
			taskCheckLicense.execute();
			
			return;
		}	
		
		//-----------------------------
		if (task == taskCheckLicense)
		{
			if (taskCheckLicense.isError())
			{
				
				if (IS_TRACKING) MainApp.getInstance().writetoLogFile("error: " + taskCheckLicense.getError().toString());
				
				onError(MvpPlayerError.LICENSE, "License error: " + taskCheckLicense.getError().toString());
				
				taskCheckLicense = null;
				return;
			}
			
			mDRMContentInfo = (DRMContentInfo) taskCheckLicense.getReturnData();
			
			startDRMPlayer();

			taskCheckLicense = null;
		}
	}
	
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void performUpdateLockTask()
	{
		if (DEBUG) Log.m(TAG,this,"performUpdateLockTask");
		if (IS_TRACKING)  MainApp.getInstance().writetoLogFile("\n\nperform UpdateLockTask");
		
		if (null == smilUpdateConcurency)
		{
			return;
		}
		
		HttpReqParams httpReqParams = new HttpReqParams(
				IHttpConst.VALUE_SCHEMA_1_0, 
				IHttpConst.VALUE_JSON);
		
		httpReqParams.setParam(IHttpConst.CLIENT_ID, smilConcurency.getClientId());
		httpReqParams.setParam(IHttpConst.LOCK_ID, smilUpdateConcurency.getUpdateLock().getId());
		httpReqParams.setParam(IHttpConst.LOCK_SEQUENCE_TOKEN, smilUpdateConcurency.getUpdateLock().getSequenceToken());
		httpReqParams.setParam(IHttpConst.LOCK, smilUpdateConcurency.getUpdateLock().getEncryptedLock());	
		
		if (DEBUG) Log.m(TAG,this,"player state: UPDATE LOCK");
		
		taskUpdateLock = new TaskSingleReqParse<MpxUpdateConcurrecy>
						(this,
						MpxUpdateConcurrecy.class,
						smilConcurency.updateURL(),
						httpReqParams);
		
		taskUpdateLock.execute();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void performUnlockTask()
	{
		if (DEBUG) Log.m(TAG,this,"performUnlockTask");
		if (IS_TRACKING)  MainApp.getInstance().writetoLogFile("\n\nperform unLockTask");
		if (null == smilUpdateConcurency)
		{
			return;
		}
		
		HttpReqParams httpReqParams = new HttpReqParams(
				IHttpConst.VALUE_SCHEMA_1_0, 
				IHttpConst.VALUE_JSON);
		
		httpReqParams.setParam(IHttpConst.CLIENT_ID, smilConcurency.getClientId());
		httpReqParams.setParam(IHttpConst.LOCK_ID, smilConcurency.getId());
		httpReqParams.setParam(IHttpConst.LOCK_SEQUENCE_TOKEN, smilConcurency.sequenceToken());
		httpReqParams.setParam(IHttpConst.LOCK, smilConcurency.LockEncr());	
		
		setPlayerState(MvpPlayerState.UNLOCK);
		taskUnlock = new TaskSingleReqParse<MpxUpdateConcurrecy>
						(this,
						MpxUnlockConcurrency.class,
						smilConcurency.unlockURL(),
						httpReqParams);
		
		taskUnlock.execute();
		
		smilUpdateConcurency = null;
	}
	
	
	


	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onBufferingUpdate(MediaPlayer mp, int percent) {
		if (DEBUG) Log.m(TAG,this,"onBufferingUpdate");
		
		if (DEBUG) Log.d(TAG,"percent: " + percent);
		
		if (null == playerView)
		{
			return;
		}
		
		playerView.showPlayerLoading(percent<100);
	}
	



	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
//	@Override
//	public boolean onError(MediaPlayer mp, int what, int extra) 
//	{
//		if (DEBUG) Log.m(TAG,this,"onError");
//		
//		switch(what)
//		{
//
//		case MediaPlayer.MEDIA_ERROR_UNKNOWN:
//			onError(MvpPlayerError.MEDIAPLAYER,"Player error (MEDIA_ERROR_UNKNOWN)");
//			break;
//			
//		case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
//			onError(MvpPlayerError.MEDIAPLAYER,"Player error (MEDIA_ERROR_SERVER_DIED)");
//			break;
//
//		}
//		
//		switch (extra) 
//		{
//		case MediaPlayer.MEDIA_ERROR_IO:
//			onError(MvpPlayerError.MEDIAPLAYER,"Player error (MEDIA_ERROR_IO)");
//			break;
//			
//		case MediaPlayer.MEDIA_ERROR_MALFORMED:
//			onError(MvpPlayerError.MEDIAPLAYER,"Player error (MEDIA_ERROR_MALFORMED)");
//			break;	
//			
//		case MediaPlayer.MEDIA_ERROR_UNSUPPORTED:
//			onError(MvpPlayerError.MEDIAPLAYER,"Player error (MEDIA_ERROR_UNSUPPORTED)");
//			break;		
//			
//		case MediaPlayer.MEDIA_ERROR_TIMED_OUT:
//			onError(MvpPlayerError.MEDIAPLAYER,"Player error (MEDIA_ERROR_TIMED_OUT)");
//			break;	
//
//		default:
//			break;
//		}
//		return false;
//	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onVideoSizeChanged(MediaPlayer mp, int width, int height) 
	{
		if (DEBUG) Log.m(TAG,this,"onVideoSizeChanged");		
		if (DEBUG) Log.d(TAG,"width = " + width + "  height = " + height);
		
		setPlayerState(MvpPlayerState.PLAYING);	
		if (null != playerView)
		{
			playerView.onVideoSizeChanged(mp, width, height);
		}
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onSeekComplete(MediaPlayer mp) 
	{
		if (DEBUG) Log.m(TAG,this,"onSeekComplete");
		
		startTimer();
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onPrepared(MediaPlayer mp) {
		if (DEBUG) Log.m(TAG,this,"onPrepared");
	
		isPrepared = true;

		try 
		{
			int durartion = mp.getDuration();
			if (durartion > 0)
			{
				mp.seekTo(currentPosition);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		try  
		{
			attachToView(playerView);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		startTimer();
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onCompletion(MediaPlayer mp) 
	{
		if (DEBUG) Log.m(TAG,this,"onCompletion");
		if (null == playerView)
		{
			return;
		}
		playerView.onCompletion(mp);
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public boolean isCompleted() 
	{
		if (DEBUG) Log.m(TAG,this,"isCompleted");
		
		if (null == mMediaPlayer)
		{
			return false;
		}
		
		return mMediaPlayer.getCurrentPosition() >= mMediaPlayer.getDuration() && mMediaPlayer.getDuration() > 0;
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	    
	public static MvpPlayerInfo getPlayerInfo()
	{
		return playerInfo;
	}


	@Override
	public void onError(DrmManagerClient client, DrmErrorEvent event) {
		// TODO Auto-generated method stub
		
	}

    
}
