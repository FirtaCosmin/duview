package com.crmsoftware.duview.player;
//******************************************************************************************
//Description:  file created for duView - CRM project
// Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************


import java.io.FileDescriptor;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.FragmentChangeActivity;
import com.crmsoftware.duview.data.MvpMedia;
import com.crmsoftware.duview.player.MvpPlayer.MvpPlayerError;
import com.crmsoftware.duview.player.MvpPlayer.MvpPlayerState;
import com.crmsoftware.duview.ui.common.MvpImageView;
import com.crmsoftware.duview.ui.watchnow.LiveProgressBarView;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;
import com.insidesecure.drmagent.v2.mediaplayer.MediaPlayer;
import com.insidesecure.drmagent.v2.mediaplayer.MediaPlayer.OnBufferingUpdateListener;
import com.insidesecure.drmagent.v2.mediaplayer.MediaPlayer.OnCompletionListener;
import com.insidesecure.drmagent.v2.mediaplayer.MediaPlayer.OnErrorListener;
import com.insidesecure.drmagent.v2.mediaplayer.MediaPlayer.OnInfoListener;
import com.insidesecure.drmagent.v2.mediaplayer.MediaPlayer.OnPreparedListener;
import com.insidesecure.drmagent.v2.mediaplayer.MediaPlayer.OnSeekCompleteListener;
import com.insidesecure.drmagent.v2.mediaplayer.MediaPlayer.OnTimedTextListener;
import com.insidesecure.drmagent.v2.mediaplayer.MediaPlayer.OnVideoSizeChangedListener;
import com.insidesecure.drmagent.v2.mediaplayer.MediaPlayer.TrackInfo;


public class PlayerView extends RelativeLayout implements OnSeekBarChangeListener,  SurfaceHolder.Callback, Observer  {

	public static boolean DEBUG = Log.DEBUG; 
	public static boolean VERBOUSE = Log.VERBOUSE ; 
	public static boolean IS_TRACKING = false;
	public final static String TAG = PlayerView.class.getSimpleName();
	
	public final static String SMIL_CONCURENCY = "SMIL_CONCURENCY";
	public final static String UPDATE_SMIL_CONCURENCY = "UPDATE_SMIL_CONCURENCY";
	public final static String SMIL_URL = "SMIL_URL";
	

	private MvpPlayer mvpPlayer = MvpPlayer.getInstance();

    
    private SurfaceView mSurfaceView;
    private View mPlayerLoading;
	private Bundle data = new Bundle();
	
    private Rect border = new Rect();
    private Drawable borderDrawable; 
    private View viewBackground;


	public final static SimpleDateFormat min_sec_Format = new SimpleDateFormat("mm:ss");
	public final static SimpleDateFormat hour_min_sec_Format = new SimpleDateFormat("hh:mm:ss");
		
    private boolean destroyPlayerOnDetach = false;
	
	private IPlayerView 	onPlayerViewCallback;
	private ViewGroup 		viewBody;
	private View 			viewControls;
	
	private View viewProgressBar;
	private TextView textViewRemainingTime;	
	private SurfaceHolder surfaceHolder;
	
	private int thumbVisibleMovementDist = 0;	// at 2 pixels is observable the move
	private boolean isThumbDragging = false;
	
	private Handler handlerUI = new Handler();
	
	private long startTime;
	private long endTime;
	
	private static final long FAST_SWITCH_WARNING_TOAST_DELAY = 10 * 1000;
	/**
	 * @desc the time when the toast with the fast switch will be displayed
	 */
	private long fastSwitchToastDelay;
	/**
	 * @desc flag set if the toast with the fast switch is displayed
	 */
	private boolean fastSwitchToastDisplayed = false;
	
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	private void showProgressBar(final int visibility) 
	{
		if (DEBUG) Log.d(TAG,"showProgressBar");
		if (null != mvpPlayer)
		{
			try
			{
				manageButtonsState(mvpPlayer.isPlaying());
			}
			catch(Exception e)
			{
				return;
			}
					
		}
		else
		{
			manageButtonsState(false);
		}
		
		viewProgressBar.setVisibility(visibility);
		viewProgressBar.bringToFront();
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void updateProgressBar(final int cursor, final int total) 
	{	
		if (DEBUG) Log.d(TAG,"updateProgressBar : progress: " + cursor + " / " + total);
		
		if (null == viewProgressBar)
		{
			return;
		}
		
		if (isThumbDragging)
		{
			return;
		}
		
		handlerUI.post(new Runnable() {
			
			@Override
			public void run() 
			{	
				if(DEBUG) Log.d(TAG,"updateProgressBar run method");
				
				/*check if the time to display the fast switch error should be displayed*/
//				long currentTime = Calendar.getInstance().getTimeInMillis();
//				if (currentTime >= fastSwitchToastDelay && !
//						fastSwitchToastDisplayed){
//					fastSwitchToastDisplayed = true;
//					showToast(getResources().getString(R.string.fast_switch_worning_toast));
//				}
				
				int position = cursor;
				int duration = total;
				
				if (viewProgressBar instanceof SeekBar)
				{
					((SeekBar)viewProgressBar).setMax(duration);
					((SeekBar)viewProgressBar).setProgress(position);
					((SeekBar)viewProgressBar).setOnSeekBarChangeListener(PlayerView.this);
				}
				else
				{
					if (startTime > 0 && endTime > 0)	// programs 
					{
						position = (int) (Calendar.getInstance().getTimeInMillis() - startTime);
						duration = (int) (endTime - startTime);
					}
					
					if (position>duration)
					{
						position = duration;
					}
					((LiveProgressBarView)viewProgressBar).setData(startTime, endTime);
				}
				
				long diff = (long) duration - (long)position;
				showProgressBar(diff > 0 ? View.VISIBLE : View.INVISIBLE);
				
				if (textViewRemainingTime == null)
				{		
					return;
				}
				
				if (diff == 0)
				{
					textViewRemainingTime.setVisibility(View.INVISIBLE);
					return;
				}
						
				long totalhMsec = 1000*3600;
				int hours = (int)((diff)/totalhMsec);
				int min = (int)((diff - hours * totalhMsec)/(1000*60));
				int sec = (int)(diff - hours * totalhMsec - min * 1000*60)/1000;
				
				String text = "";
				
				if (hours > 0) 
				{	
					text = String.format("%1d:%02d:%02d",hours,min,sec);
				}
				else
				{
					text = String.format("%02d:%02d", min,sec);
				}
				
				Utils.dateToString(diff, hours > 0 ? hour_min_sec_Format: min_sec_Format);
				
				text = "-" + text;
				
				textViewRemainingTime.setVisibility(View.VISIBLE);
				textViewRemainingTime.setText(text);

			}
				
		});	
	}
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public PlayerView(Context context,  AttributeSet attrs) {
		
		super(context, attrs);
		if (isInEditMode())
		{
			return;
		}
		if (DEBUG) Log.m(TAG,this,"ctor");
		
		init(context);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public PlayerView(Context context) 
	{
		super(context);
		if (DEBUG) Log.m(TAG,this,"ctor");
		
		init(context);

	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void init(Context context) 
	{
		if (DEBUG) Log.m(TAG,this,"init");
		
		thumbVisibleMovementDist = (int)context.getResources().getDimension(R.dimen.player_controls_thumb_sensitive);
		
		viewBody = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.player_view, this);
		
		mSurfaceView = (SurfaceView) viewBody.findViewById(R.id.player_surface);
		mSurfaceView.getHolder().addCallback(this);
		
		if (DEBUG) Log.d(TAG,"\t\tsurfaceCreated: init playerView=" + this);
		
		mPlayerLoading = (View) viewBody.findViewById(R.id.player_loading);

		viewBackground = viewBody.findViewById(R.id.player_body_view);
				
		border.left = viewBackground.getPaddingLeft();
		border.right = viewBackground.getPaddingRight();
		border.top = viewBackground.getPaddingTop();
		border.bottom = viewBackground.getPaddingBottom();
			
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void setPlayerListener(IPlayerView onPlayerViewCallback)
	{
		if (DEBUG) Log.d(TAG,"setPlayerListener");
		this.onPlayerViewCallback = onPlayerViewCallback;
		
		viewProgressBar = findViewById(R.id.media_seekbar);
		viewProgressBar.setVisibility(View.INVISIBLE);
		
		textViewRemainingTime = (TextView)findViewById(R.id.media_remaining_duration);
		textViewRemainingTime.setVisibility(View.INVISIBLE);
		
		viewControls = findViewById(R.id.player_controls);
		viewControls.setVisibility(View.INVISIBLE);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void manageButtonsState(boolean playing)
	{
		if (DEBUG) Log.d(TAG,"manageButtonsState");
		MvpPlayerState state = MvpPlayer.getInstance().getState();
		
		View viewPause = findViewById(R.id.player_pause);
		View viewPlay = findViewById(R.id.player_play);		
		if (viewPause != null)
		{
			viewPause.setVisibility(playing ? View.VISIBLE : View.GONE);
		}
		
		if (viewPlay != null)
		{
			viewPlay.setVisibility(playing ? View.GONE : View.VISIBLE);			
		}	
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void setStartEnd(long start, long end) 
	{
		if (DEBUG) Log.m(TAG,this,"ctor");
		
		this.startTime = start;
		this.endTime = end;

	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void setPlayerListener(IPlayerView onPlayerViewCallback, View viewControls)
	{
		Log.d(TAG,"setPlayerListener");
		this.viewControls = viewControls;
		
		setPlayerListener(onPlayerViewCallback);
	}
	
	/**
	 * @desc will show the control bar adding to it a margin with the 
	 * values described in the margin parameter  
	 * @param show
	 * @param margin
	 */
	public void showControlBar(boolean show, Rect margin){
		if (DEBUG) Log.d(TAG,"showControlBar");
		if (null == mvpPlayer)
		{
			return;
		}
		
		if (null == mvpPlayer.getPlayer())
		{
			return;
		}
		if ( !mvpPlayer.isPrepared() ){
			return;
		}
		
		/*set the padding from the passed rect*/
		LayoutParams params = new LayoutParams(
		        LayoutParams.WRAP_CONTENT,      
		        LayoutParams.WRAP_CONTENT
		);
		params.setMargins(margin.left, margin.top, margin.right, margin.bottom);
		viewControls.setLayoutParams(params);
		
		/*change the visibility of the view*/
		setControlBarVisibility(show);
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void showControlBar(boolean show) 
	{
		if (DEBUG) Log.d(TAG,"showControlBar");
		if (null == mvpPlayer)
		{
			return;
		}
		
		if (null == mvpPlayer.getPlayer())
		{
			return;
		}
		if ( !mvpPlayer.isPrepared() ){
			return;
		}
		setControlBarVisibility(show);
		
	}
	
	/**
	 * @desc will change the visibility of the control bars without checking for errors.
	 * @param show
	 */
	private void setControlBarVisibility(boolean show){
		 
		if (show)
		{
			int position = 0;
			int duration = 0;
			
			try 
			{
				position = mvpPlayer.getPlayer().getCurrentPosition();
				duration = mvpPlayer.getPlayer().getDuration();
			} 
			catch (Exception e) 
			{
				// TODO: handle exception
				Log.d(TAG,"get position exception");
			}
			
			updateProgressBar(position,duration);
		}
		
		int visibility = show ? View.VISIBLE : View.INVISIBLE;		
		viewControls.setVisibility(visibility);
		viewControls.bringToFront();
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public boolean isControlBarVisible() 
	{
		if (DEBUG) Log.d(TAG,"isControlBarVisible");
		return viewControls.getVisibility() == View.VISIBLE;
	}
	
	 
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void showToast(String text)
	{
		if (DEBUG) Log.d(TAG,"showToast");
		Toast toast = Toast.makeText(getContext(), text, Toast.LENGTH_LONG);
		toast.show();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void onError(final MvpPlayerError error, final Object data) 
	{
		if (DEBUG) Log.m(TAG,this,"onError");
		
		handlerUI.post(new Runnable() {
			
			@Override
			public void run() {

				if (DEBUG) Log.d(TAG,"onError::run");
				switch (error) 
				{
				case TOKEN:
					
					destroyPlayer();
					
//					FragmentChangeActivity activity = (FragmentChangeActivity)getContext();
//					activity.getMenu().performLogOut();
					
					break;

				case SMIL:
				case LOCK:
				case LICENSE:
				case NOCONNECTION:
					
					showToast(((String)data));	
					destroyPlayer(); 
					break;
					
				case MEDIAPLAYER:
					showToast(((String)data));
					/*
					 * display a toast to notify the user that the error night be because 
					 * of the fast play configuration.
					 * */
//					fastSwitchToastDisplayed = false;
//					handlerUI.postDelayed(new Runnable() {
//						
//						@Override
//						public void run() {
//							showToast(getResources().getString(R.string.fast_switch_worning_toast));							
//						}
//					},2000);
					destroyPlayer();
					break;
					
				default:
					break;
				}
				
			}
		});
		
	
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void attachPlayer() 
	{
		if (DEBUG) Log.m(TAG,this,"attachPlayer");
		
		MvpPlayer.getPlayerInfo().addObserver(this);
		
		mvpPlayer = MvpPlayer.getInstance();
		
		
		
		if (mvpPlayer.isRunning())
		{

			/*save the time when the toast with the fast switch warning will be displayed*/
//		    Calendar calendar = Calendar.getInstance();
//		    
//			fastSwitchToastDelay = calendar.getTimeInMillis() + FAST_SWITCH_WARNING_TOAST_DELAY;
//			fastSwitchToastDisplayed = false;
			
			
			setVisibility(View.VISIBLE);
			mSurfaceView.setVisibility(View.VISIBLE);
			
			if (false == MvpPlayer.FAST_SWITCHING)
			{
				showPlayerLoading(true);
				mvpPlayer.setPlayerView(this);
				mvpPlayer.replay();
				return;
			}
		}
		
		showPlayerLoading(mvpPlayer.getState() != MvpPlayerState.DESTROYED);
		
		new Handler().post(new Runnable() {
			
			@Override
			public void run() 
			{
				if (DEBUG) Log.m(TAG,PlayerView.this,"attachPlayer::run");
				try 
				{
					if (null == mvpPlayer)
					{
						return;
					}
					
					mvpPlayer.attachToView(PlayerView.this);
				} 
				catch (Exception e) 
				{
					
					e.printStackTrace();	
				}
			}
		});
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void dettachPlayer() 
	{
		if (DEBUG) Log.m(TAG,this,"dettachPlayer");
		
		MvpPlayer.getPlayerInfo().deleteObserver(this);
		if (null != mvpPlayer)
		{
			mvpPlayer.detachFromView();				
		
			if (false == MvpPlayer.FAST_SWITCHING)
			{
				mvpPlayer.release(false);
			}
		}

	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public SurfaceHolder getSurfaceHolder()
	{
		if (DEBUG) Log.m(TAG,this,"getSurfaceHolder");
		
		return surfaceHolder;
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void play(MvpMedia mvpMedia)
	{
		if (DEBUG) Log.m(TAG,this,"play");

		
		if (null == mvpMedia)
		{
			return;
		}
		
		bringToFront();
		setVisibility(View.VISIBLE);
		mSurfaceView.setVisibility(View.VISIBLE);
		showPlayerLoading(true);
		viewControls.setVisibility(View.INVISIBLE);
		
		MvpPlayer.getPlayerInfo().addObserver(this);
		
		mvpPlayer = MvpPlayer.getInstance();
		
		mvpPlayer.play(mvpMedia);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void showPlayerLoading(boolean state) 
	{
		if (DEBUG) Log.m(TAG,this,"showPlayerLoading");
		
		if (null == mPlayerLoading)
		{
			return;
		}
		
		mPlayerLoading.bringToFront();
		if (state)
		{	
			mPlayerLoading.setVisibility(View.VISIBLE);	
		}
		else
		{	
			mPlayerLoading.setVisibility(View.INVISIBLE);		
		}
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void destroyPlayer() 
	{
		if (DEBUG) Log.m(TAG,this,"destroyPlayer");

		if (mvpPlayer != null)
		{
			mvpPlayer.destroy();
			mvpPlayer = null;
		}
		
		setVisibility(View.INVISIBLE);
		mSurfaceView.setVisibility(View.INVISIBLE);
		
		MvpPlayer.getPlayerInfo().deleteObserver(this);
		MvpPlayer.removeInstance();	
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void play(String file)
	{
		if (DEBUG) Log.m(TAG,this,"play");	
		
		try 
		{
			bringToFront();
			setVisibility(View.VISIBLE);
			mSurfaceView.setVisibility(View.VISIBLE);
			showPlayerLoading(true);
			viewControls.setVisibility(View.INVISIBLE);
			
			if (DEBUG) Log.d(TAG,"file: " + file);
			MediaPlayer mediaPlayer = new MediaPlayer() {
				
				@Override
				public void stop() throws IllegalStateException {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void start() throws IllegalStateException {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setWakeMode(Context arg0, int arg1) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setVolume(float arg0, float arg1) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setVideoScalingMode(int arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setSurface(Surface arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setScreenOnWhilePlaying(boolean arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setOnVideoSizeChangedListener(OnVideoSizeChangedListener arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setOnTimedTextListener(OnTimedTextListener arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setOnSeekCompleteListener(OnSeekCompleteListener arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setOnPreparedListener(OnPreparedListener arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setOnInfoListener(OnInfoListener arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setOnErrorListener(OnErrorListener arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setOnCompletionListener(OnCompletionListener arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setOnBufferingUpdateListener(OnBufferingUpdateListener arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setNextMediaPlayer(MediaPlayer arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setLooping(boolean arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setDisplay(SurfaceHolder arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setDataSource(FileDescriptor arg0, long arg1, long arg2)
						throws IOException, IllegalArgumentException, IllegalStateException {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setDataSource(Context arg0, Uri arg1, Map<String, String> arg2)
						throws IOException, IllegalArgumentException, SecurityException,
						IllegalStateException {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setDataSource(Context arg0, Uri arg1) throws IOException,
						IllegalArgumentException, SecurityException, IllegalStateException {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setDataSource(FileDescriptor arg0) throws IOException,
						IllegalArgumentException, IllegalStateException {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setDataSource(String arg0) throws IOException,
						IllegalArgumentException, SecurityException, IllegalStateException {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setAuxEffectSendLevel(float arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setAudioStreamType(int arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setAudioSessionId(int arg0) throws IllegalArgumentException,
						IllegalStateException {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void selectTrack(int arg0) throws IllegalStateException {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void seekTo(int arg0) throws IllegalStateException {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public android.media.MediaPlayer retrieveMediaPlayer() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public void reset() {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void release() {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void prepareAsync() throws IllegalStateException {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void prepare() throws IOException, IllegalStateException {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void pause() throws IllegalStateException {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public boolean isPlaying() {
					// TODO Auto-generated method stub
					return false;
				}
				
				@Override
				public boolean isLooping() {
					// TODO Auto-generated method stub
					return false;
				}
				
				@Override
				public int getVideoWidth() {
					// TODO Auto-generated method stub
					return 0;
				}
				
				@Override
				public int getVideoHeight() {
					// TODO Auto-generated method stub
					return 0;
				}
				
				@Override
				public TrackInfo[] getTrackInfo() throws IllegalStateException {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public int getDuration() {
					// TODO Auto-generated method stub
					return 0;
				}
				
				@Override
				public int getCurrentPosition() {
					// TODO Auto-generated method stub
					return 0;
				}
				
				@Override
				public int getAudioSessionId() {
					// TODO Auto-generated method stub
					return 0;
				}
				
				@Override
				public void deselectTrack(int arg0) throws IllegalStateException {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void attachAuxEffect(int arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void addTimedTextSource(FileDescriptor arg0, long arg1, long arg2,
						String arg3) throws IllegalArgumentException, IllegalStateException {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void addTimedTextSource(Context arg0, Uri arg1, String arg2)
						throws IOException, IllegalArgumentException, IllegalStateException {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void addTimedTextSource(FileDescriptor arg0, String arg1)
						throws IllegalArgumentException, IllegalStateException {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void addTimedTextSource(String arg0, String arg1)
						throws IOException, IllegalArgumentException, IllegalStateException {
					// TODO Auto-generated method stub
					
				}
			};
			mediaPlayer.setDataSource(file);
		
			mvpPlayer = MvpPlayer.getInstance();
			startPlayer(mediaPlayer);
			
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected void startPlayer(final com.insidesecure.drmagent.v2.mediaplayer.MediaPlayer player)
    {
		if (DEBUG) Log.m(TAG,this,"startPlayer");
		
		if (null == player)
		{
			return;
		}

		
		new Handler().post(new Runnable() {
			
			@Override
			public void run() {
				if (DEBUG) Log.d(TAG,"startPlayer::run");
				
				if (null == mvpPlayer)
				{
					return;
				}
				
				setVisibility(View.VISIBLE);
				mSurfaceView.setVisibility(View.VISIBLE);
				
				manageButtonsState(true);
				
				mvpPlayer.startPlayer(player);
			} 
		});

    }

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void onVideoSizeChanged(com.insidesecure.drmagent.v2.mediaplayer.MediaPlayer mp, int width, int height) 
	{
		if (DEBUG) Log.m(TAG,this,"onVideoSizeChanged");
		
		showPlayerLoading(false);

		

		
		
		fitTheVideo(width, height);
		

		
		if (null != onPlayerViewCallback)
		{
			//onPlayerViewCallback.onVideoSizeChanged(width,height);
		}
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void fitTheVideo()
	{
		if (DEBUG) Log.d(TAG,"fitTheVideo");
		fitTheVideo(mSurfaceView.getWidth(), mSurfaceView.getHeight());
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void fitTheVideo(int width, int height)
	{
		if (DEBUG) Log.d(TAG,"fitTheVideo");
		View view = (View)mSurfaceView.getParent();
		
		double videoAspect = (double)width/(double)height;
		
		int newHeight = view.getHeight();
		int newWidth = (int)((double)newHeight * videoAspect);


		if (newWidth > view.getWidth())
		{
			newWidth = view.getWidth();
			newHeight = (int)((double)newWidth * videoAspect);
		}
		
		
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(newWidth,newHeight);
		params.addRule(RelativeLayout.CENTER_IN_PARENT);
		
		mSurfaceView.setLayoutParams(params);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void onCompletion(MediaPlayer mp) 
	{
		if (DEBUG) Log.m(TAG,this,"onCompletion");

		destroyPlayer();
		if (null != onPlayerViewCallback)
		{
			onPlayerViewCallback.onVideoCompletion();
		}
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void showBorder(boolean state) 
	{
		if (DEBUG) Log.d(TAG,"showBorder");
		if (state)
		{
			if (viewBackground.getPaddingLeft() == 0)
			{
				viewBackground.setPadding(border.left, border.top, border.right,border.bottom);
			}
		}
		else
		{
			if (viewBackground.getPaddingLeft() > 0)
			{
				viewBackground.setPadding(0,0,0,0);
			}
		}
		
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		if (DEBUG) Log.d(TAG,"onProgressChanged");
		
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) 
	{
		if (DEBUG) Log.d(TAG,"onStartTrackingTouch");
		isThumbDragging = true;
		mvpPlayer.stopTimer();
		
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) 
	{
		if (DEBUG) Log.d(TAG,"onStopTrackingTouch");
		mvpPlayer.seekTo(seekBar.getProgress());
		isThumbDragging = false;		
	}
	
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void destroyPlayerOnDetach (boolean state) 
	{
		if (DEBUG) Log.m(TAG,this,"destroyPlayerOnDetach");
		
		destroyPlayerOnDetach = state;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */
	public void onParentPaused()
	{
		if (DEBUG) Log.m(TAG,this,"onParentPaused");
		dettachPlayer();
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void onParentResume()
	{
		if (DEBUG) Log.m(TAG,this,"onParentResume");
		
		attachPlayer();
		
		
		new Handler().post(new Runnable() {
			
			@Override
			public void run() 
			{
				if (DEBUG) Log.m(TAG,this,"onParentResume::run");
				try 
				{
					if (mvpPlayer.isCompleted())
					{
						destroyPlayer();
					}
				} 
				catch (Exception e) 
				{
					
					e.printStackTrace();	
				}
			}
		});
	}
	
	
	
	  
    /**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	  
    public void play()
    {
		if (DEBUG) Log.m(TAG,this,"play");
		
		mvpPlayer.play(true);
		manageButtonsState(true);
    }
    
    
    /**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	  
    public void pause()
    {
		if (DEBUG) Log.m(TAG,this,"pause");
		
		mvpPlayer.pause(true);
		manageButtonsState(false);  	

    }
    
    
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		if (DEBUG) Log.m(TAG,this,"surfaceDestroyed");
		surfaceHolder = null;
		if (DEBUG) Log.d(TAG,"\t\t surfaceDestroyed: playerView=" + this);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		if (DEBUG) Log.m(TAG,this,"surfaceCreated");
		
		surfaceHolder = holder;
		
		if (DEBUG) Log.d(TAG,"\t\tsurfaceCreated: holder=" + surfaceHolder);
		if (DEBUG) Log.d(TAG,"\t\tsurfaceCreated: playerView=" + this);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) 
	{
		if (DEBUG) Log.m(TAG,this,"surfaceChanged");
		if (DEBUG) Log.m(TAG,this,"width=" + width + " height=" + height);
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void updateView()
	{
		if (DEBUG) Log.m(TAG,this,"updateView");
		MvpPlayerInfo info = MvpPlayer.getPlayerInfo();
		
		TextView title = (TextView) findViewById(R.id.player_item_title);
		TextView desc = (TextView) findViewById(R.id.player_item_description);
		MvpImageView image = (MvpImageView) findViewById(R.id.player_item_image);
		
		title.setText( info.getTitle());	
		desc.setText( info.getShortDesc());	
		
		
		if (null != image)
		{
			image.setData(info.getChannelImage());
		}
		
		setStartEnd(info.getStart(), info.getEnd());
	
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void update(Observable observable, Object data) {

		if (DEBUG) Log.m(TAG,this,"update");
		updateView();
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void setVisibility(int visibility) {
		if (DEBUG) Log.m(TAG,this,"setVisibility");
		if (null != mSurfaceView)
		{
			mSurfaceView.setVisibility(visibility);
		}
		super.setVisibility(visibility);
	}
	
	
	
	
}
