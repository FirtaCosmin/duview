package com.crmsoftware.duview.player.smil.model;

import org.json.JSONException;
import org.json.JSONObject;

import com.crmsoftware.duview.data.IJsonParser;

public class MpxUnlockConcurrency implements IJsonParser {

	public static final String UNLOCK_RESPONSE 		    = "unlockResponse";


	@Override
	public void fromJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException 
	{
		
		JSONObject obj = jsonObj.getJSONObject(UNLOCK_RESPONSE);
	}

	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException 
	{
		JSONObject updateResponse = new JSONObject();
		jsonObj.put(UNLOCK_RESPONSE, updateResponse);

	}

	public String toJsonString() {
		JSONObject jsonObj = new JSONObject();
		
		try 
		{
			toJson(jsonObj, null);
			
		} catch (Exception e) {
			
			return "{error}";
		}
		
		return jsonObj.toString();
	}
}
