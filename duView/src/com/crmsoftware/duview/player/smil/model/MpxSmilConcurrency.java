package com.crmsoftware.duview.player.smil.model;

import org.json.JSONException;
import org.json.JSONObject;

import com.crmsoftware.duview.data.IJsonParser;
import com.crmsoftware.duview.utils.Utils;

public class MpxSmilConcurrency implements IJsonParser{
	
	public static final String HEAR_BEAT_SECONDS 		    = "heartbeatSeconds";
	public static final String UPDATE_URL 	 		    	= "updateURL";
	public static final String UNLOCK_URL 	 				= "unlockURL";
	public static final String ID 	 						= "id";
	public static final String SEQUENCE_TOKEN 			    = "sequenceToken";
	public static final String LOCK_ENCRIPTION 			    = "lockEncr";
	public static final String CLIENT_ID 				    = "clientId";
	
	private int heartbeatSeconds;
	private String updateURL;
	private String unlockURL;
	private String id;
	private String sequenceToken;
	private String lockEncr;
	private String clientId;
	
	
	public void heartbeatSeconds(String heartbeatSeconds) 
	{
		this.heartbeatSeconds = Integer.parseInt(heartbeatSeconds);;
	}
	
	
	public int heartbeatSeconds() {
		return heartbeatSeconds;
	}

	public void updateURL(String updateURL) {
		this.updateURL = updateURL;
	}
	public String updateURL() {
		return updateURL;
	}
	
	public void unlockURL(String unlockURL) {
		this.unlockURL = unlockURL;
	}
	public String unlockURL() {
		return unlockURL;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return id;
	}
	
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getClientId() {
		return clientId;
	}
	
	public void sequenceToken(String sequenceToken) {
		this.sequenceToken = sequenceToken;
	}
	public String sequenceToken() {
		return sequenceToken;
	}
	
	public void LockEncr(String lockEncr) {
		this.lockEncr = lockEncr;
	}
	
	public String LockEncr() {
		return lockEncr;
	}
	
	@Override
	public String toString() {
		String sText = "";
		sText += super.toString();
		
		sText += "\nheartBeat: " + heartbeatSeconds;
		sText += "\nupdateURL: " + updateURL;
		sText += "\nid: " + id;		
		sText += "\nsequenceToken: " + sequenceToken;		
		sText += "\nlockEncr: " + lockEncr;		
		
		return sText;
	}
	
	@Override
	public void fromJson(JSONObject jsonObj, String tag) throws JSONException,
	InstantiationException, IllegalAccessException {
		heartbeatSeconds = Utils.getJSONAsInt(jsonObj, HEAR_BEAT_SECONDS);
		updateURL		 = Utils.getJSONAsString(jsonObj, UPDATE_URL);
		unlockURL		 = Utils.getJSONAsString(jsonObj, UNLOCK_URL);
		id 				 = Utils.getJSONAsString(jsonObj, ID);
		sequenceToken 	 = Utils.getJSONAsString(jsonObj, SEQUENCE_TOKEN);
		lockEncr		 = Utils.getJSONAsString(jsonObj, LOCK_ENCRIPTION);
		clientId		 = Utils.getJSONAsString(jsonObj, CLIENT_ID);
	}
	
	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
	InstantiationException, IllegalAccessException {
		Utils.setJSONAsInt(jsonObj, HEAR_BEAT_SECONDS, heartbeatSeconds);
		Utils.setJSONAsString(jsonObj, UPDATE_URL, updateURL);
		Utils.setJSONAsString(jsonObj, UNLOCK_URL, unlockURL);
		Utils.setJSONAsString(jsonObj, ID, id);
		Utils.setJSONAsString(jsonObj, SEQUENCE_TOKEN, sequenceToken);
		Utils.setJSONAsString(jsonObj, LOCK_ENCRIPTION, lockEncr);
		Utils.setJSONAsString(jsonObj, CLIENT_ID, clientId);
	}
	
	@Override
	public String toJsonString() {
		JSONObject jsonObj = new JSONObject();
		
		try 
		{
			toJson(jsonObj, null);
			
		} catch (Exception e) {
			
			return "{error}";
		}
		
		return jsonObj.toString();
	}
	
	
	public static MpxSmilConcurrency fromJsonString(String sJson) {

		if (sJson.isEmpty())
		{
			return null;
		}
		try 
		{
			MpxSmilConcurrency data = new MpxSmilConcurrency();
			data.fromJson(new JSONObject(sJson),null);
			
			return data;
			
		} catch (Exception e) {
			
		}
		
		return null;
	}
	
}
