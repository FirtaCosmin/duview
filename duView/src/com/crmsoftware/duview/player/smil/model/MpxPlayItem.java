package com.crmsoftware.duview.player.smil.model;
import java.io.Serializable;

import com.crmsoftware.duview.ui.settings.parentalcontrol.controls.ParentalItemLevelsController;

public class MpxPlayItem implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5861177112278083469L;
	public static final String INSTYPE_PREROLL 				= "PREROLL";
	public static final String INSTYPE_POSTROLL 			= "POSTROLL";
	public static final String INSTYPE_ORIGINAL 			= "CONTENT";
	private static MpxPlayItem instance = new MpxPlayItem();
	
	protected String url;		// vast url
	protected String type;	// type of insertion
	protected boolean noSkip;	// let the user to skip or not
	protected boolean playModeAsync = true;
	
//	public static boolean isMediaSupported(VastMediaFileItem vastMediaFileItem)
//	{
//		String url = vastMediaFileItem.url.toLowerCase().trim();
//		if (url.endsWith("m4v"))
//		{
//			return true;
//		}
//		
//		if (url.endsWith("mp4"))
//		{
//			return true;
//		}
//		
//		if (url.endsWith("3gp"))
//		{
//			return true;
//		}
//		
//	return false;
//	}	
	
	public static MpxPlayItem getInstance() {
		return instance ;
	}
	
	public boolean getPlayMode()
	{
		return playModeAsync;		
	}	
	
	public static boolean isSSUrl(String url)
	{
		if (null == url)
		{
			return false;
		}

		return url.toLowerCase().endsWith("manifest");		
	}
	
	public static boolean isHLSUrl(String url)
	{
		if (null == url)
		{
			return false;
		}

		return url.toLowerCase().endsWith("m3u8");
	}
	
	public static boolean isUrlValid(String url)
	{
		if (null == url)
		{
			return false;
		}
		if (url.isEmpty())
		{
			return false;
		}	
		
		return true;
	}
	
	public  boolean hasValidUrl()
	{		
		return isUrlValid(url);
	}
	
	public  boolean hasSSUrl()
	{		
		return isSSUrl(url);
	}
	
	public  boolean hasHLSUrl()
	{		
		return isHLSUrl(url);
	}
	
	public  boolean hasSmilUrl()
	{		
		return isSmilUrl(url);
	}
	
	public static boolean isSmilUrl(String url)
	{
		if (null == url)
		{
			return false;
		}
		
		return url.toLowerCase().contains(".smil");
	}
	
	
	public MpxPlayItem(){ 
		noSkip = true;
		url = "";
		type= "CONTENT";
	}
	
	public MpxPlayItem(String url){ 
		noSkip = true;
		setUrl(url);
		type= "CONTENT";
	}
	
	public void setType(String type)
	{
		this.type = type.toUpperCase();
	}
	
	public String getType()
	{
		return type;
	}
	
	public void setUrl(String url)
	{
		this.url = url;
		
		if (url.endsWith(".m4v"))
		{
			playModeAsync = false;
			return;
		}
		
		if (url.endsWith(".mp4"))
		{
			playModeAsync = false;
			return;
		}	
		
		if (url.endsWith(".3gp"))
		{
			playModeAsync = true;
			return;
		}	
	}
	
	public String getUrl()
	{
		return url;
	}
	
	public void setNoSkip(String noSkip)
	{
		this.noSkip = noSkip.equals("true");
	}
	
	public void setNoSkip(boolean playmode)
	{
		this.noSkip = playmode;
	}	
	
	public boolean getNoSkip()
	{
		return noSkip;
	}
}
