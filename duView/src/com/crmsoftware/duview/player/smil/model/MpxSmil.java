package com.crmsoftware.duview.player.smil.model;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.text.TextUtils;
import com.crmsoftware.duview.utils.IConst;
import com.crmsoftware.duview.utils.Log;

public class MpxSmil extends DefaultHandler implements IConst 
{
	public static boolean DEBUG = Log.DEBUG; 
	public static boolean VERBOUSE = Log.VERBOUSE; 
	
	public final static String TAG = MpxSmil.class.getSimpleName();
	
	
	public static final String SMIL_SRC 			= "src";
	public static final String SMIL_TITLE 			= "title";
	public static final String SMIL_ABSTRACT 		= "abstract";
	public static final String SMIL_COPYRIGHT 		= "copyright";
	public static final String SMIL_DURATION 		= "dur";
	public static final String SMIL_GUID 			= "guid";
	public static final String SMIL_CATEGORIES 		= "categories";
	public static final String SMIL_TYPE 			= "type";
	public static final String SMIL_HEIGHT 			= "height";
	public static final String SMIL_WIDTH 			= "width";
	public static final String SMIL_PARAM 			= "param";
	public static final String SMIL_PARAM_NAME		= "name";
	public static final String SMIL_PARAM_VALUE		= "value";
	public static final String SMIL_REF				= "ref";
	public static final String SMIL_SEQ				= "seq";
	
	private static final String VIDEO 				= "video";
	
	public static final String SMIL_META									= "meta";
	public static final String SMIL_META_NAME								= "name";
	public static final String SMIL_META_CONTENT							= "content";
	public static final String SMIL_META_UPDATE_LOCK_INTERVAL				= "updateLockInterval";
	public static final String SMIL_META_CONCURENCY_URL						= "concurrencyServiceUrl";
	public static final String SMIL_META_LOCK_ID							= "lockId";
	public static final String SMIL_META_LOCK_SEQ_TOKEN						= "lockSequenceToken";
	public static final String SMIL_META_LOCK								= "lock";
	
	public static final String SMIL_TAGS 			= "tags";
	public static final String SMIL_NOSKIP 			= "no-skip";
	public static final String SMIL_TYPE_VAST 		= "application/vast+xml";
	
	
	public static final String PARAM_ISEXCEPTIOPN = "isException";
	public static final String PARAM_EXCEPTION = "exception";	
	public static final String PARAM_RESPONSECODE = "exception";	
	
	public static final String EXCEPTION_LICENSE_NOT_GRANTED = "LicenseNotGranted";
	public static final String EXCEPTION_CONCURRENCY_LIMIT_VIOLATION_AUTH = "ConcurrencyLimitViolationAuth";	

	
	private String url			= EMPTY_STRING;	

	private String title		= EMPTY_STRING;
	private String description	= EMPTY_STRING;
	private String copyright	= EMPTY_STRING;
	private String duration		= EMPTY_STRING;
	private String guid 		= EMPTY_STRING;
	private String categories	= EMPTY_STRING;
	private String type			= EMPTY_STRING;
	private int height			= Integer.MIN_VALUE;
	private int width			= Integer.MIN_VALUE;

	
	private int screenWidth;
	private int screenHeight;
	private int bitrate;

	private MpxSmilParam smilErrorParam;
	private MpxSmilConcurrency smilConcurency =  new MpxSmilConcurrency();
	private List<MpxSmilParam> smilParams = new ArrayList<MpxSmilParam>();
	private List<MpxPlayItem> playerMediaItems = new ArrayList<MpxPlayItem>();	
	private StringBuilder procText = new StringBuilder("");

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		procText.setLength(0);

		if(SMIL_SEQ.equals(localName)) 
		{

			return;
		} 

		if (SMIL_META.equals(localName)) 
		{
			setAttributes(smilConcurency, attributes);
			return;
		} 

		if (VIDEO.equals(localName)) {
			setAttributes(attributes);
			return;
		}

		if(SMIL_REF.equals(localName)) {
			//for all other cases we consider ref error
			setAttributes(attributes);
			return;
				
		}

		if(SMIL_PARAM.equals(localName)) {
			smilErrorParam =  new MpxSmilParam();
			setAttributes(smilErrorParam, attributes);
			return;
		}
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void setAttributes(MpxSmilParam smilErrorParam, Attributes attributes) 
	{
		
		String nameParam = attributes.getValue(SMIL_PARAM_NAME);
		if(!TextUtils.isEmpty(nameParam)) {
			smilErrorParam.setName(nameParam);
		}
		String valueParam = attributes.getValue(SMIL_PARAM_VALUE);
		if(!TextUtils.isEmpty(valueParam)) {
			smilErrorParam.setValue(valueParam);
		}
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void setAttributes(MpxSmilConcurrency smilLockControl, Attributes attributes) 
	{
		String nameParam = attributes.getValue(SMIL_META_NAME);
		if(TextUtils.isEmpty(nameParam)) 
		{
			return;
		}

		String content = attributes.getValue(SMIL_META_CONTENT);
		if( nameParam.equals(SMIL_META_UPDATE_LOCK_INTERVAL) && !TextUtils.isEmpty(content))
		{
			smilLockControl.heartbeatSeconds(content.toString());
			return;
		} 
		
		if(nameParam.equals(SMIL_META_CONCURENCY_URL) && !TextUtils.isEmpty(content))
		{
			smilLockControl.updateURL(content.toString() + "/web/Concurrency/update");
			smilLockControl.unlockURL(content.toString() + "/web/Concurrency/unlock");
			return;
		} 
		
		if( nameParam.equals(SMIL_META_LOCK_ID) && !TextUtils.isEmpty(content))
		{
			smilLockControl.setId(content.toString());
			return;
		} 

		if( nameParam.equals(SMIL_META_LOCK_SEQ_TOKEN) && !TextUtils.isEmpty(content))
		{
			smilLockControl.sequenceToken(content.toString());
			return;
		}

		if( nameParam.equals(SMIL_META_LOCK) && !TextUtils.isEmpty(content))
		{
			smilLockControl.LockEncr(content.toString());
			return;
		}
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void setAttributes( Attributes attributes) {
		String src = attributes.getValue(SMIL_SRC);
		if (!TextUtils.isEmpty(src)) {
			setUrl(src);
		}
		
		String title = attributes.getValue(SMIL_TITLE);
		if (!TextUtils.isEmpty(title)) {
			setTitle(title);
		}
		
		String description = attributes.getValue(SMIL_ABSTRACT);
		if (!TextUtils.isEmpty(description)) {
			setDescription(description);
		}
		
		String copyright = attributes.getValue(SMIL_COPYRIGHT);
		if (!TextUtils.isEmpty(copyright)) {
			setCopyright(copyright);
		}
		
		String duration = attributes.getValue(SMIL_DURATION);
		if (!TextUtils.isEmpty(duration)) {
			setDuration(duration);
		}
		
		String guid = attributes.getValue(SMIL_GUID);
		if (!TextUtils.isEmpty(guid)) {
			setGuid(guid);
		}
		
		String categories = attributes.getValue(SMIL_CATEGORIES);
		if (!TextUtils.isEmpty(categories)) {
			setCategories(categories);
		}
		
		String type = attributes.getValue(SMIL_TYPE);
		if (!TextUtils.isEmpty(type)) {
			setType(type);
		}
		
		String height = attributes.getValue(SMIL_HEIGHT);
		if (!TextUtils.isEmpty(height)) {
			setHeight(Integer.parseInt(height));
		}
		
		String width = attributes.getValue(SMIL_WIDTH);
		if (!TextUtils.isEmpty(width)) {
			setWidth(Integer.parseInt(width));
		}
		
	}
	
	
	/**
	 * SAX event - Append characters to mText. Override this if different behavior desired.
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void characters(char ch[], int start, int length) throws SAXException 
	{
		procText.append(new String(ch, start, length).trim());
		super.characters(ch, start, length);
	}
	
	/**
	 *  
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException 
	{
		super.endElement(uri, localName, qName);
		
		if (VERBOUSE) Log.d(TAG,"smil group: " + procText);
		
		if(SMIL_SEQ.equals(localName)) 
		{
			return;
		}
		
		if(SMIL_PARAM.equals(localName)) 
		{
			if (smilParams != null){
				smilParams.add(smilErrorParam);
			}
			return;
		} 
		
		if(SMIL_REF.equals(localName)) 
		{
			setSmilErrorParamList(smilParams);
			return;
		} 
	}
	
	
	public String getTitle() {
		return title;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getCopyright() {
		return copyright;
	}
	
	public void setCopyright(String copyright) {
		this.copyright = copyright;
	}
	
	public String getDuration() {
		return duration;
	}
	
	public void setDuration(String duration) {
		this.duration = duration;
	}
	
	public String getGuid() {
		return guid;
	}
	
	public void setGuid(String guid) {
		this.guid = guid;
	}
	
	public String getCategories() {
		return categories;
	}
	
	public void setCategories(String categories) {
		this.categories = categories;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	public int getWidth() {
		return width;
	}
	
	public void setWidth(int width) {
		this.width = width;
	}

	public void setSmilErrorParamList(List<MpxSmilParam> smilErrorParamList) {
		this.smilParams = smilErrorParamList;
	}

	public List<MpxSmilParam> getSmilErrorParamList() {
		return smilParams;
	}
	
	public boolean isError() 
	{
		return smilParams.size()>0;
	}

	public void setSmilConcurrency( MpxSmilConcurrency slc ) {
		smilConcurency = slc;
	}
	
	public MpxSmilConcurrency getSmilConcurrency(){
		return smilConcurency;
	}

	public String getUrl() {
		return url;
	}
	
	public String getParamValue(String name) 
	{
		for (MpxSmilParam param : smilParams)
		{
			if (param.getName().equals(name))
			{
				return param.getValue();
			}
		}
		return "";
	}
	
}
