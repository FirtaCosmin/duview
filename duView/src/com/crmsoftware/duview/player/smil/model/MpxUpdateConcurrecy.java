package com.crmsoftware.duview.player.smil.model;

import org.json.JSONException;
import org.json.JSONObject;

import com.crmsoftware.duview.data.IJsonParser;
import com.crmsoftware.duview.utils.Utils;

public class MpxUpdateConcurrecy implements IJsonParser {
	public static final String UPDATE_LOCK 		    = "updateResponse";

	private UpdateLockResponse updateLock = new UpdateLockResponse();

	@Override
	public void fromJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException 
	{
		
		JSONObject updateResponse = jsonObj.getJSONObject(UPDATE_LOCK);
		updateLock.fromJson(updateResponse, null);	
	}

	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException 
	{
		JSONObject updateResponse = new JSONObject();
		jsonObj.put(UPDATE_LOCK, updateResponse);
		
		updateLock.toJson(updateResponse, UPDATE_LOCK);
	}

	@Override
	public String toJsonString() {
		JSONObject jsonObj = new JSONObject();
		
		try 
		{
			toJson(jsonObj, null);
			
		} catch (Exception e) {
			
			return "{error}";
		}
		
		return jsonObj.toString();
	}

	public UpdateLockResponse getUpdateLock() {
		return updateLock;
	}

	public void setUpdateLock(UpdateLockResponse updateLock) {
		this.updateLock = updateLock;
	}
	
	public static MpxUpdateConcurrecy fromJsonString(String sJson) {

		if (sJson.isEmpty())
		{
			return null;
		}
		
		try 
		{
			MpxUpdateConcurrecy data = new MpxUpdateConcurrecy();
			data.fromJson(new JSONObject(sJson),null);
			
			return data;
			
		} catch (Exception e) {
			
		}
		
		return null;
	}
	
	public class UpdateLockResponse implements IJsonParser {

		public static final String ID 		    = "id";
		public static final String SEQUENCE_TOKEN 		    = "sequenceToken";
		public static final String ENCRYPTED_LOCK 		    = "encryptedLock";
		
		private String id;
		private String sequenceToken;
		private String encryptedLock;
		
		@Override
		public void fromJson(JSONObject jsonObj, String tag)
				throws JSONException, InstantiationException,
				IllegalAccessException {
			
			id = Utils.getJSONAsString(jsonObj, ID);
			sequenceToken = Utils.getJSONAsString(jsonObj, SEQUENCE_TOKEN);
			encryptedLock = Utils.getJSONAsString(jsonObj, ENCRYPTED_LOCK);
		}

		@Override
		public void toJson(JSONObject jsonObj, String tag)
				throws JSONException, InstantiationException,
				IllegalAccessException {
			Utils.setJSONAsString(jsonObj, ID, id);
			Utils.setJSONAsString(jsonObj, SEQUENCE_TOKEN, sequenceToken);
			Utils.setJSONAsString(jsonObj, ENCRYPTED_LOCK, encryptedLock);
		}

		@Override
		public String toJsonString() {
			return null;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getSequenceToken() {
			return sequenceToken;
		}

		public void setSequenceToken(String sequenceToken) {
			this.sequenceToken = sequenceToken;
		}

		public String getEncryptedLock() {
			return encryptedLock;
		}

		public void setEncryptedLock(String encryptedLock) {
			this.encryptedLock = encryptedLock;
		}
		
	}

}
