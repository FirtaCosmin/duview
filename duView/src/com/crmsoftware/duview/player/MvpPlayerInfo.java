package com.crmsoftware.duview.player;

import java.util.Observable;

import com.crmsoftware.duview.data.MvpChannelPrograms;

import android.os.Bundle;

public class MvpPlayerInfo extends Observable{

	public static final String CHANNEL_ID = "CHANNEL_ID";
	public static final String PROGRAM_ID = "PROGRAM_ID";
	public static final String CHANNEL_IMAGE = "CHANNEL_IMAGE";
	public static final String PROGRAM_IMAGE = "PROGRAM_IMAGE";
	public static final String GENRES = "GENRES";
	public static final String TITLE = "TITLE";
	public static final String SHORTDESC = "SHORTDESC";
	public static final String LONGDESC = "LONGDESC";
	public static final String START = "START";
	public static final String END = "END";
	public static final String DURATION = "DURATION";
	
	private String channelId = "";
	private String programId = "";	

	private String channelImage = "";
	private String programImage = "";
	private String parentalRatings = "";
	private String title = "...loading...";
	private String longDesc = "";
	private String shortDesc = "";	
	private long start = 0;
	private long end = 0;
	private String duration;

	
	public void loadFrom(Bundle data) 
	{
		channelId = data.getString(CHANNEL_ID,"");
		programId = data.getString(PROGRAM_ID,"");
		
		channelImage = data.getString(CHANNEL_IMAGE,"");
		programImage = data.getString(PROGRAM_IMAGE,"");
		parentalRatings = data.getString(GENRES,"");
		title = data.getString(TITLE,MvpChannelPrograms.NO_PROG_INFO);
		longDesc = data.getString(LONGDESC,"");
		shortDesc = data.getString(SHORTDESC,"");
		start = data.getLong(START,0);
		end = data.getLong(END,0);
		duration = data.getString(DURATION,MvpChannelPrograms.NO_PROG_INFO);
		
		setChanged();
		notifyObservers();
	}
	
	public void copyFrom(MvpPlayerInfo data) 
	{
		channelId = data.channelId;
		programId = data.programId;
		
		channelImage = data.channelImage;
		programImage = data.programImage;
		parentalRatings = data.parentalRatings;
		title = data.title;
		longDesc = data.longDesc;
		shortDesc = data.shortDesc;
		start = data.start;
		end = data.end;
		duration = data.duration;
		
		setChanged();
		notifyObservers();
	}

	
	public String getChannelImage() {
		return channelImage;
	}

	public void setChannelImage(String channelImage) {
		this.channelImage = channelImage;
	}

	public String getProgramImage() {
		return programImage;
	}

	public void setProgramImage(String programImage) {
		this.programImage = programImage;
	}

	public String getParentalRatings() {
		return parentalRatings;
	}

	public void setParentalRatings(String parentalRatings) {
		this.parentalRatings = parentalRatings;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}

	public long getEnd() {
		return end;
	}

	public void setEnd(long end) {
		this.end = end;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public void saveTo(Bundle data) 
	{
		data.putString(CHANNEL_ID,channelId);
		data.putString(PROGRAM_ID,programId);
		
		data.putString(CHANNEL_IMAGE,channelImage);
		data.putString(PROGRAM_IMAGE,programImage);
		data.putString(GENRES,parentalRatings);
		data.putString(TITLE,title);
		data.putString(SHORTDESC,shortDesc);
		data.putString(LONGDESC,longDesc);
		data.putLong(START,start);
		data.putLong(END,end);
		data.putString(DURATION,duration);
	}

	
	public String getLongDesc() {
		return longDesc;
	}


	public void setLongDesc(String longDesc) {
		this.longDesc = longDesc;
	}
	
	public void notifyObservers() 
	{
		setChanged();
		super.notifyObservers();
	}
	
	
	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getProgramId() {
		return programId;
	}

	public void setProgramId(String programId) {
		this.programId = programId;
	}
	
	public void resetData()
	{
		setChannelId("");
		setProgramId("");
		setChannelImage("");
		setProgramImage("");
		setParentalRatings("");
		setTitle("");
		setShortDesc("");
		setStart(0);
		setEnd(0);
		setDuration("");
	}


}
