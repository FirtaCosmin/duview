/**
 * $URL$
 * DAAND
 * Copyright (C) 2004 - 2013, INSIDE Secure BV.  All rights reserved.
 */
package com.crmsoftware.duview.player.drm;

import android.content.Context;

import com.crmsoftware.duview.utils.Log;
import com.insidesecure.drmagent.v2.DRMContent;
import com.insidesecure.drmagent.v2.DRMContentFormat;
import com.insidesecure.drmagent.v2.DRMScheme;

import java.io.Serializable;
import java.net.*;
import java.util.concurrent.locks.*;

public class DRMContentInfo implements Serializable, Comparable<DRMContentInfo>
{
    private static final String TAG = "DRMContentInfo";

    private Lock mDRMContentLock = new ReentrantLock();
    private DRMContent mDRMContent;

    public String mContentDescriptorLocation;
    public URI mContentLocation;
    public String mTitle;
    public URI mSubtitleFileLocation;
    public URL mLaURLOverride;
    public URL mWebInitiatorURL;

    public boolean mStreamLive;
    public String mAuthor;
    public String mGenres;
    public String mLanguage;
    public String mShortDescription;
    public String mLongDescription;
    public String mContainer;
    public String mAudioCodec;
    public String mAudioChannels;
    public String mVideoCodec;
    public String mWidth;
    public String mHeight;
    public String mBitDepth;
    public String mBitRate;
    public String mFramerate;
    public String mRuntime;
    public String mPicture;
    public String mThumbnail;
    public String mLocalFileServerPath;
    public String mContentFormatFromCdesc;
    public String mCustomDataType;
    public String mCustomDataParameters;
    private DRMScheme mDRMScheme;
    private DRMContentFormat mDRMContentFormat;

    @Override
    public int hashCode()
    {
        int result = mDRMContent != null ? mDRMContent.hashCode() : 0;
        result = 31 * result + (mContentDescriptorLocation != null ? mContentDescriptorLocation.hashCode() : 0);
        result = 31 * result + (mContentLocation != null ? mContentLocation.hashCode() : 0);
        result = 31 * result + (mTitle != null ? mTitle.hashCode() : 0);
        result = 31 * result + (mLaURLOverride != null ? mLaURLOverride.hashCode() : 0);
        result = 31 * result + (mStreamLive ? 1 : 0);
        result = 31 * result + (mAuthor != null ? mAuthor.hashCode() : 0);
        result = 31 * result + (mGenres != null ? mGenres.hashCode() : 0);
        result = 31 * result + (mLanguage != null ? mLanguage.hashCode() : 0);
        result = 31 * result + (mShortDescription != null ? mShortDescription.hashCode() : 0);
        result = 31 * result + (mLongDescription != null ? mLongDescription.hashCode() : 0);
        result = 31 * result + (mContainer != null ? mContainer.hashCode() : 0);
        result = 31 * result + (mAudioCodec != null ? mAudioCodec.hashCode() : 0);
        result = 31 * result + (mAudioChannels != null ? mAudioChannels.hashCode() : 0);
        result = 31 * result + (mVideoCodec != null ? mVideoCodec.hashCode() : 0);
        result = 31 * result + (mWidth != null ? mWidth.hashCode() : 0);
        result = 31 * result + (mHeight != null ? mHeight.hashCode() : 0);
        result = 31 * result + (mBitDepth != null ? mBitDepth.hashCode() : 0);
        result = 31 * result + (mBitRate != null ? mBitRate.hashCode() : 0);
        result = 31 * result + (mFramerate != null ? mFramerate.hashCode() : 0);
        result = 31 * result + (mRuntime != null ? mRuntime.hashCode() : 0);
        result = 31 * result + (mPicture != null ? mPicture.hashCode() : 0);
        result = 31 * result + (mThumbnail != null ? mThumbnail.hashCode() : 0);
        result = 31 * result + (mLocalFileServerPath != null ? mLocalFileServerPath.hashCode() : 0);
        result = 31 * result + (mContentFormatFromCdesc != null ? mContentFormatFromCdesc.hashCode() : 0);
        result = 31 * result + (mCustomDataType != null ? mCustomDataType.hashCode() : 0);
        result = 31 * result + (mCustomDataParameters != null ? mCustomDataParameters.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (!(o instanceof DRMContentInfo))
        {
            return false;
        }

        final DRMContentInfo that = (DRMContentInfo) o;

        if (mStreamLive != that.mStreamLive)
        {
            return false;
        }
        if (mAudioChannels != null ? !mAudioChannels.equals(that.mAudioChannels) : that.mAudioChannels != null)
        {
            return false;
        }
        if (mAudioCodec != null ? !mAudioCodec.equals(that.mAudioCodec) : that.mAudioCodec != null)
        {
            return false;
        }
        if (mAuthor != null ? !mAuthor.equals(that.mAuthor) : that.mAuthor != null)
        {
            return false;
        }
        if (mBitDepth != null ? !mBitDepth.equals(that.mBitDepth) : that.mBitDepth != null)
        {
            return false;
        }
        if (mBitRate != null ? !mBitRate.equals(that.mBitRate) : that.mBitRate != null)
        {
            return false;
        }
        if (mContainer != null ? !mContainer.equals(that.mContainer) : that.mContainer != null)
        {
            return false;
        }
        if (mContentDescriptorLocation != null ? !mContentDescriptorLocation.equals(that.mContentDescriptorLocation) : that.mContentDescriptorLocation != null)
        {
            return false;
        }
        if (mContentLocation != null ? !mContentLocation.equals(that.mContentLocation) : that.mContentLocation != null)
        {
            return false;
        }
        if (mDRMContent != null ? !mDRMContent.equals(that.mDRMContent) : that.mDRMContent != null)
        {
            return false;
        }
        if (mFramerate != null ? !mFramerate.equals(that.mFramerate) : that.mFramerate != null)
        {
            return false;
        }
        if (mGenres != null ? !mGenres.equals(that.mGenres) : that.mGenres != null)
        {
            return false;
        }
        if (mHeight != null ? !mHeight.equals(that.mHeight) : that.mHeight != null)
        {
            return false;
        }
        if (mLaURLOverride != null ? !mLaURLOverride.equals(that.mLaURLOverride) : that.mLaURLOverride != null)
        {
            return false;
        }
        if (mLanguage != null ? !mLanguage.equals(that.mLanguage) : that.mLanguage != null)
        {
            return false;
        }
        if (mLocalFileServerPath != null ? !mLocalFileServerPath.equals(that.mLocalFileServerPath) : that.mLocalFileServerPath != null)
        {
            return false;
        }
        if (mContentFormatFromCdesc != null ? !mContentFormatFromCdesc.equals(that.mContentFormatFromCdesc) : that.mContentFormatFromCdesc != null)
        {
            return false;
        }
        if (mLongDescription != null ? !mLongDescription.equals(that.mLongDescription) : that.mLongDescription != null)
        {
            return false;
        }
        if (mPicture != null ? !mPicture.equals(that.mPicture) : that.mPicture != null)
        {
            return false;
        }
        if (mRuntime != null ? !mRuntime.equals(that.mRuntime) : that.mRuntime != null)
        {
            return false;
        }
        if (mShortDescription != null ? !mShortDescription.equals(that.mShortDescription) : that.mShortDescription != null)
        {
            return false;
        }
        if (mThumbnail != null ? !mThumbnail.equals(that.mThumbnail) : that.mThumbnail != null)
        {
            return false;
        }
        if (mTitle != null ? !mTitle.equals(that.mTitle) : that.mTitle != null)
        {
            return false;
        }
        if (mVideoCodec != null ? !mVideoCodec.equals(that.mVideoCodec) : that.mVideoCodec != null)
        {
            return false;
        }
        if (mWidth != null ? !mWidth.equals(that.mWidth) : that.mWidth != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public String toString()
    {
        return "DRMContentInfo{" +
                "mDRMContent=" + mDRMContent +
                ", mContentDescriptorLocation='" + mContentDescriptorLocation + '\'' +
                ", mContentLocation=" + mContentLocation +
                ", mTitle='" + mTitle + '\'' +
                ", mLaURLOverride='" + mLaURLOverride + '\'' +
                ", mStreamLive=" + mStreamLive +
                ", mAuthor='" + mAuthor + '\'' +
                ", mGenres='" + mGenres + '\'' +
                ", mLanguage='" + mLanguage + '\'' +
                ", mShortDescription='" + mShortDescription + '\'' +
                ", mLongDescription='" + mLongDescription + '\'' +
                ", mContainer='" + mContainer + '\'' +
                ", mAudioCodec='" + mAudioCodec + '\'' +
                ", mAudioChannels='" + mAudioChannels + '\'' +
                ", mVideoCodec='" + mVideoCodec + '\'' +
                ", mWidth='" + mWidth + '\'' +
                ", mHeight='" + mHeight + '\'' +
                ", mBitDepth='" + mBitDepth + '\'' +
                ", mBitRate='" + mBitRate + '\'' +
                ", mFramerate='" + mFramerate + '\'' +
                ", mRuntime='" + mRuntime + '\'' +
                ", mPicture='" + mPicture + '\'' +
                ", mThumbnail='" + mThumbnail + '\'' +
                ", mLocalFileServerPath='" + mLocalFileServerPath + '\'' +
                ", mContentFormatFromCdesc='" + mContentFormatFromCdesc + '\'' +
                ", mCustomDataType='" + mCustomDataType + '\'' +
                ", mCustomDataParameters='" + mCustomDataParameters + '\'' +
                '}';
    }

    public int compareTo(final DRMContentInfo drmContentInfo)
    {
        return mTitle.compareTo(drmContentInfo.mTitle);
    }

    public void releaseDRMContent()
    {
        mDRMContentLock.lock();
        try
        {
            if (mDRMContent == null)
            {
                Log.d(TAG,"DRMContent instance is not available, will simply return");
                return;
            }
            Log.d(TAG,"Releasing DRMContent instance");
            mDRMContent.release();
            mDRMContent = null;
            Log.d(TAG,"DRMContent instance released");
        }
        finally
        {
            mDRMContentLock.unlock();
        }
    }

    public DRMContent getDRMContent(Context context)
    {
        mDRMContentLock.lock();
        try
        {
            if (mDRMContent == null)
            {
                Log.d(TAG, "DRMContent instance is null, will instantiate new instance");
                mDRMContent = DRMAgentDelegate.getDRMContent(context, mContentLocation, mDRMContentFormat, mDRMScheme);
                Log.d(TAG, "DRMContent instance instantiated");
            }
            return mDRMContent;
        }
        finally
        {
            mDRMContentLock.unlock();
        }
    }

    public void setDRMContent(final DRMContent drmContent)
    {
        mDRMContentLock.lock();
        try
        {
            if (mDRMContent != null)
            {
                Log.e(TAG, "DRMContent instance is not null as expected, is this really what you want to do?");
                throw new IllegalStateException("DRMContent instance is not null as expected");
            }

            Log.d(TAG, "Setting DRMContent instance: " + drmContent.getOriginalContentURI());
            mDRMContentFormat = drmContent.getDRMContentFormat();
            mDRMScheme = drmContent.getDRMScheme();
            mDRMContent = drmContent;
        }
        finally
        {
            mDRMContentLock.unlock();
        }
    }
}
