/**
 * $URL$
 * DAAND
 * Copyright (C) 2004 - 2013, INSIDE Secure BV.  All rights reserved.
 */
package com.crmsoftware.duview.player.drm;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.content.Context;
import android.content.res.AssetManager;

import com.crmsoftware.duview.utils.Log;
import com.insidesecure.drmagent.v2.DRMAgentException;
import com.insidesecure.drmagent.v2.DRMContentFormat;
import com.insidesecure.drmagent.v2.DRMScheme;

public class ContentHandler
{
    public static final String TAG = "ContentHandler";

    private static XmlPullParserFactory mParserFactory;

    private static Set<String> mLastLoadedListFileNames = new HashSet<String>();
    private static List<DRMContentInfo> mLastLoadedDRMContentInfos = new ArrayList<DRMContentInfo>();

    /**
     *
     */
    public static void initialize()
    {
        try
        {
            final File f = new File(Constants.CDESC_PATH);

            if (!f.exists())
            {
                if (!f.mkdir())
                {
                    Log.w(TAG, "Could not create content directory: " + Constants.CDESC_PATH);
                }
                else
                {
                    Log.d(TAG, "Created content directory: " + Constants.CDESC_PATH);
                }
            }
            else
            {
                // Verfify it is a directory!
                if (!f.isDirectory())
                {
                    Log.e(TAG, "Content directory is actually a file!");
                }
            }

        }
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage(), e);
        }
    }

    /**
     *
     * @param context
     */
    public static void addDefaultContent(final Context context)
    {
        try
        {
            final AssetManager assetManager = context.getAssets();

            String[] assets = assetManager.list("cdesc");

            for (String asset : assets)
            {
                Log.d(TAG,"Opening asset: " + asset);
                FileOutputStream outputStream = null;
                InputStream inputStream = null;
                try
                {
                    inputStream = assetManager.open("cdesc/" + asset);
                    outputStream = new FileOutputStream(Constants.CDESC_PATH + asset);
                    Tools.copyStream(inputStream, outputStream);
                }
                catch (Exception e)
                {
                    Log.e(TAG, "Error while opening asset: " + asset,e);
                }
                finally
                {
                    if (inputStream != null)
                        inputStream.close();
                    if (outputStream != null)
                    {
                        outputStream.flush();
                        outputStream.close();
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.e(TAG,"Error while listing assets: " + e.getMessage(),e);
        }

    }

    public static void clearCache()
    {
        mLastLoadedListFileNames.clear();
        mLastLoadedDRMContentInfos.clear();
    }

    public static void clearCachedContent(final Context context, final DRMContentInfo drmContentInfo)
    {
        drmContentInfo.getDRMContent(context).clearCachedData();
    }

    /** Listener API which is called as valid descriptor files are found.
     */
    public static interface ContentRetrieverListener
    {
        /** Method invoked whenever a valid content descriptor has been found.
         *
         * @param drmContentInfo A valid content descriptor
         */
        public void drmContentInfoRetrieved(final DRMContentInfo drmContentInfo);

        /** Method invoked whenever a error occured while processing a descriptor
         *
         * @param name The name of the file being processed
         * @param url The URL (may be null if could not be read) of the content.
         */
        public void drmContentInfoRetrievalFailed(final File name, final URL url);
    }

    /** Retrieves the list of content information found.  This will attempt to load all the descriptors available and
     * may impose some network I/O.
     *
     * @param context The context to use for resolving information as need be
     * @param validateLocalFiles Whether the content descriptors found should be validated if pointing to local media files
     * @param contentRetrieverListener Listener which is invoked for each valid descriptor found.  Allows for progressive
     * updates of content lists for example
     * @return The fully loaded list of valid content descriptors
     */
    public static List<DRMContentInfo> retrieveDRMContentInfos(final Context context, boolean validateLocalFiles, final boolean loadDRMInformation, final ContentRetrieverListener contentRetrieverListener)
    {
        initialize();

        Log.d(TAG, "Retrieving content");

        final List<DRMContentInfo> drmContentInfos = new ArrayList<DRMContentInfo>();

        final File[] files = retrieveContentDescriptorFiles();

        final Set<String> loadedFileNames = convertToNameSet(files);

        if (loadedFileNames.size() == mLastLoadedListFileNames.size() && mLastLoadedListFileNames.containsAll(loadedFileNames))
        {
            Log.d(TAG, "No changes in descriptor set: will use cached content info");

            if (contentRetrieverListener != null)
            {
                for (DRMContentInfo drmContentInfo : mLastLoadedDRMContentInfos)
                {
                    contentRetrieverListener.drmContentInfoRetrieved(drmContentInfo);
                }
            }
            // Return cached list
            return mLastLoadedDRMContentInfos;
        }

        Log.d(TAG, "Changes in descriptor set: cannot use cached content info");
        loadedFileNames.clear();

        if (files != null)
        {
            for (final File file : files)
            {
                try
                {
                    final DRMContentInfo drmContentInfo = processContentDescriptor(context, file, validateLocalFiles, loadDRMInformation);
                    if (drmContentInfo != null)
                    {
                        if (contentRetrieverListener != null)
                        {
                            contentRetrieverListener.drmContentInfoRetrieved(drmContentInfo);
                        }
                        loadedFileNames.add(file.getAbsolutePath());
                        drmContentInfos.add(drmContentInfo);
                    }
                    else
                    {
                        // Error loading the descriptor in question
                        if (contentRetrieverListener != null)
                        {
                            contentRetrieverListener.drmContentInfoRetrievalFailed(file,null);
                        }
                    }
                }
                catch (DescriptorLoadException e)
                {
                    // Error loading the descriptor in question
                    if (contentRetrieverListener != null)
                    {
                        contentRetrieverListener.drmContentInfoRetrievalFailed(e.mDescriptorFile, e.mURl);
                    }
                }

            }

        }

        mLastLoadedListFileNames = loadedFileNames;
        mLastLoadedDRMContentInfos = drmContentInfos;

        return drmContentInfos;
    }

    private static Set<String> convertToNameSet(final File[] files)
    {
        Set<String> names = new HashSet<String>();
        if (files != null)
        {
            for (File file : files)
            {
                names.add(file.getAbsolutePath());
            }
        }
        return names;
    }

    private static File[] retrieveContentDescriptorFiles()
    {
        final File baseDir = new File(Constants.CDESC_PATH);
        return baseDir.listFiles(new FileFilter()
        {
            public boolean accept(final File file)
            {
                return (file.isFile() && file.getName().endsWith(".cdesc"));
            }
        });
    }

    private static DRMContentInfo processContentDescriptor(final Context context, final File file, final boolean validateLocalFiles, final boolean loadDRMInformation)
    {
        try
        {
            Log.d(TAG,"Processing descriptor: " + file);
            // First try to parse it as XML
            DRMContentInfo drmContentInfo = parseAsXML(context, file,validateLocalFiles, loadDRMInformation);

            if (validateLocalFiles && drmContentInfo != null)
            {
                // Perform some sanity checking of the content - this will check if the content is actually
                // DRM protected (if the descriptors indicate they are).  Prevents errors further down the
                // road
                try
                {
                    if (drmContentInfo.getDRMContent(context).isProtected())
                    {
                        // calling the ContentHandler wrapper here wouldn't detect inconsistencies
                        drmContentInfo.getDRMContent(context).getDRMRights();
                    }
                }
                catch (DRMAgentException e)
                {
                    // If this happens in the above call, there is some inconsistency with the content!
                    Log.d(TAG, "Peeking at content caused error: " + e.getMessage());
                    Log.d(TAG, "This indicates the content located at " +
                            "'" + drmContentInfo.mContentLocation + "' " +
                            "is marked as DRM protected and the DRM Agent detected the content to be CLEARTEXT.  " +
                            "Please verify your content descriptor");
                    throw new DescriptorLoadException(file, drmContentInfo.mContentLocation.toURL(), e.getMessage());
                }
            }

            // The try property
            return drmContentInfo;
        }
        catch (DescriptorLoadException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            Log.e(TAG, "Error while parsing XML: " + e.getMessage(), e);
            throw new DescriptorLoadException(file, null, e.getMessage());
        }
    }

    private static URI performValidationAndRewrite(final boolean validateLocalFiles, final String uriStr, final boolean useContentFromUS) throws URISyntaxException
    {
        String uriToUse;
        if (uriStr == null)
        {
            throw new NullPointerException("No URI specified");
        }

        Log.d(TAG,"Resolving URI: " + uriStr);

        if(useContentFromUS && (uriStr.startsWith(Constants.EU_HOST_NAME) || uriStr.startsWith(Constants.AMS_HOST_NAME)))
        {
            uriToUse = uriStr.replace(Constants.EU_HOST_NAME, Constants.US_HOST_NAME);
            uriToUse = uriStr.replace(Constants.AMS_HOST_NAME, Constants.US_HOST_NAME);
            Log.d(TAG,"Updating host-name to stream content from US, new url: " + uriToUse);
        }
        else
        {
            uriToUse = uriStr;
        }

        URI uri;
        if (uriToUse.startsWith("file://") || uriToUse.startsWith("http://")|| uriToUse.startsWith("https://") )
        {
            //encode white spaces
            uri = new URI(uriToUse.replaceAll(" ", "%20"));
        }
        else
        {
            File f = new File(Constants.CDESC_PATH,uriToUse);
            uri = f.toURI();
        }

        Log.d(TAG,"URI resolved to: " + uri);

        if (validateLocalFiles && (uri.getScheme() == null || "file".equals(uri.getScheme())))
        {
            Log.d(TAG,"Verifying local URI: " + uri );
            // Verify the file exists as well!
            File f = new File(uri.getPath());
            if (!f.exists())
            {
                throw new RuntimeException("Local file specified by '" + uriToUse + "' could not be found");
            }
        }
        return uri;
    }

    public static DRMContentInfo parseAsXML(final Context context, final File file, final boolean validateLocalFiles, final boolean attemptLoadDRMInformation)
    {
        URL contentLocation = null;

        boolean useContentFromUS = context.getSharedPreferences(Constants.PREFERENCES_FILENAME, Context.MODE_PRIVATE).getBoolean(Constants.PREFERENCES_USE_CONTENT_FROM_US, false);

        try
        {
            BufferedReader br = new BufferedReader(new FileReader(file), 8192);
            try
            {
                Log.d(TAG, "Attempting to parse " + file + " as XML content descriptor");

                long t1 = System.currentTimeMillis();

                if (mParserFactory == null)
                {
                    mParserFactory = XmlPullParserFactory.newInstance();
                }

                XmlPullParser parser = mParserFactory.newPullParser();
                parser.setInput(br);

                long t2 = System.currentTimeMillis();

                Log.v(TAG, "Parser setup and initialized in " + (t2 - t1) + " millisecond(s)");


                t1 = System.currentTimeMillis();

                int parserEvent = parser.getEventType();
                String tag = null;
                DRMContentInfo drmContentInfo = null;
                DRMScheme drmScheme = null;
                DRMContentFormat drmContentFormat = null;

                while (parserEvent != XmlPullParser.END_DOCUMENT)
                {
                    switch (parserEvent)
                    {
                        case XmlPullParser.START_TAG:
                            tag = parser.getName();
                            if ("Descriptor".equals(tag))
                            {
                                drmContentInfo = new DRMContentInfo();
                            }
                            else if ("CustomDataParameters".equals(tag))
                            {
                                // read the contents of this tag, including the XML tags
                                final StringBuffer buffer = new StringBuffer("<" + tag + ">");
                                parserEvent = parser.next();
                                boolean copy = true;
                                while (copy)
                                {
                                    String innerTag;
                                    switch (parserEvent)
                                    {
                                        case XmlPullParser.START_TAG:
                                            innerTag = parser.getName();
                                            buffer.append("<" + innerTag + ">");
                                            break;
                                        case XmlPullParser.END_TAG:
                                            innerTag = parser.getName();
                                            buffer.append("</" + innerTag + ">");
                                            if (innerTag == tag)
                                            {
                                                copy = false;
                                            }
                                            break;
                                        case XmlPullParser.CDSECT:
                                            final String cdata = parser.getText();
                                            if (cdata.trim().length() != 0)
                                            {
                                                buffer.append("<![CDATA[");
                                                buffer.append(cdata);
                                                buffer.append("]]>");
                                            }
                                            break;
                                        case XmlPullParser.TEXT:
                                            final String innerText = parser.getText();
                                            if (innerText.trim().length() != 0)
                                            {
                                                buffer.append(innerText);
                                            }
                                            break;
                                    }

                                    parserEvent = parser.nextToken();
                                }
                                drmContentInfo.mCustomDataParameters = buffer.toString();
                            }
                            break;
                        case XmlPullParser.END_TAG:
                            break;
                        case XmlPullParser.TEXT:
                            String text = parser.getText();
                            if (text.trim().length() == 0)
                            {
                                break;
                            }
                            if (drmContentInfo != null)
                            {
                                if (text.trim().length() == 0) break;
                                else if (tag.compareTo("Title") == 0)
                                {
                                    drmContentInfo.mTitle = text;
                                }
                                else if (tag.compareTo("URL") == 0)
                                {
                                    drmContentInfo.mContentLocation = performValidationAndRewrite(validateLocalFiles, text, useContentFromUS);
                                    contentLocation = drmContentInfo.mContentLocation.toURL();
                                }
                                else if (tag.compareTo("LA_URL_OVERRIDE") == 0)
                                {
                                    drmContentInfo.mLaURLOverride = verifyAndCreateURL(text);
                                }
                                else if (tag.equals("SubtitleFileLocation"))
                                {
                                    drmContentInfo.mSubtitleFileLocation = performValidationAndRewrite(validateLocalFiles, text, useContentFromUS);
                                }
                                else if (tag.equals("WebInitiatorURL"))
                                {
                                    drmContentInfo.mWebInitiatorURL = verifyAndCreateURL(text);
                                }
                                else if (tag.compareTo("Protection") == 0)
                                {
                                    drmScheme = DRMScheme.valueOf(text);
                                }
                                else if (tag.compareTo("LocalFile_ServerPath") == 0)
                                {
                                    // What now?
                                    drmContentInfo.mLocalFileServerPath = text;
                                }
                                else if (tag.compareTo("StreamType") == 0)
                                {
                                    drmContentFormat = DRMContentFormat.valueOf(text);
                                    drmContentInfo.mContentFormatFromCdesc = text;
                                }
                                else if (tag.compareTo("StreamLive") == 0)
                                {
                                    drmContentInfo.mStreamLive = ("1".equals(text));
                                }
                                else if (tag.compareTo("Author") == 0)
                                {
                                    drmContentInfo.mAuthor = text;
                                }
                                else if (tag.compareTo("Genres") == 0)
                                {
                                    drmContentInfo.mGenres = text;
                                }
                                else if (tag.compareTo("Language") == 0)
                                {
                                    drmContentInfo.mLanguage= text;
                                }
                                else if (tag.compareTo("ShortDescription") == 0)
                                {
                                    drmContentInfo.mShortDescription= text;
                                }
                                else if (tag.compareTo("LongDescription") == 0)
                                {
                                    drmContentInfo.mLongDescription= text;
                                }
                                else if (tag.compareTo("Container") == 0)
                                {
                                    drmContentInfo.mContainer= text;
                                }
                                else if (tag.compareTo("AudioCodec") == 0)
                                {
                                    drmContentInfo.mAudioCodec= text;
                                }
                                else if (tag.compareTo("AudioChannels") == 0)
                                {
                                    drmContentInfo.mAudioChannels= text;
                                }
                                else if (tag.compareTo("VideoCodec") == 0)
                                {
                                    drmContentInfo.mVideoCodec= text;
                                }
                                else if (tag.compareTo("Width") == 0)
                                {
                                    drmContentInfo.mWidth= text;
                                }
                                else if (tag.compareTo("Height") == 0)
                                {
                                    drmContentInfo.mHeight= text;
                                }
                                else if (tag.compareTo("BitDepth") == 0)
                                {
                                    drmContentInfo.mBitDepth= text;
                                }
                                else if (tag.compareTo("BitRate") == 0)
                                {
                                    drmContentInfo.mBitRate= text;
                                }
                                else if (tag.compareTo("Framerate") == 0)
                                {
                                    drmContentInfo.mFramerate= text;
                                }
                                else if (tag.compareTo("Runtime") == 0)
                                {
                                    drmContentInfo.mRuntime= text;
                                }
                                else if (tag.compareTo("Picture") == 0)
                                {
                                    drmContentInfo.mPicture= text;
                                }
                                else if (tag.compareTo("Thumbnail") == 0)
                                {
                                    drmContentInfo.mThumbnail= text;
                                }
                                else if (tag.compareTo("CustomDataType") == 0)
                                {
                                    drmContentInfo.mCustomDataType = text;
                                }
                              break;
                            }
                            break;
                    }
                    parserEvent = parser.next();
                }
                t2 = System.currentTimeMillis();
                Log.d(TAG, "Content descriptor parsed in " + (t2 - t1) + " millisecond(s)");

                Log.d(TAG, "DRMContentInfo: " + drmContentInfo);
                Log.d(TAG, "DRMScheme: " + drmScheme);
                Log.d(TAG, "DRM Content Format: " + drmContentFormat);

                if (drmContentInfo != null && drmScheme != null && drmContentFormat != null)
                {
                    Log.d(TAG, "Have sufficient information from parsing the XML, will return DRM content info");
                    if (attemptLoadDRMInformation)
                    {
                        drmContentInfo.setDRMContent(DRMAgentDelegate.getDRMContent(context, drmContentInfo.mContentLocation, drmContentFormat, drmScheme));
                    }
                    drmContentInfo.mContentDescriptorLocation = file.getAbsolutePath();

                    return drmContentInfo;
                }
                else
                {
                    Log.d(TAG, "Not enough information retrieved during parsing");
                    return null;
                }
            }
            finally
            {
                br.close();
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, "Error while parsing XML descriptor '" + file.getAbsolutePath() + "' : " + e.getMessage(), e);
            throw new DescriptorLoadException(file,contentLocation, e.getMessage());
        }
    }

    private static URL verifyAndCreateURL(final String urlStr)
    {
        try
        {
            return new URL(urlStr);
        }
        catch (MalformedURLException e)
        {
            Log.e(TAG,"Error while validating URL: " + urlStr);
            throw new RuntimeException("Error while validating URL: " + urlStr, e);
        }
    }

    public static void deleteContent(DRMContentInfo drmContentInfo, boolean removeFromList)
    {
        initialize();
        final File f = new File(drmContentInfo.mContentDescriptorLocation);
        Log.d(TAG, "Deleting " + f);
        if (!f.delete())
        {
            Log.d(TAG, "Deleted file, but for some reason it was not properly deleted");
        }
        //delete local file (if needed)
        if(drmContentInfo.mContentLocation.getScheme().compareToIgnoreCase("file")==0)
        {
            //delete local file
            final File localFile = new File(drmContentInfo.mContentLocation);
            Log.d(TAG, "Deleting local file " + localFile);
            if (!localFile.delete())
            {
                Log.d(TAG, "Deleted local file, but for some reason it was not properly deleted");
            }
        }

        if(removeFromList)
        {
            mLastLoadedDRMContentInfos.remove(drmContentInfo);
            mLastLoadedListFileNames.remove(f.getAbsolutePath());
        }
    }
    public static void deleteContent(DRMContentInfo drmContentInfo)
    {
        deleteContent(drmContentInfo,  true);
    }

    public static void deleteAllContent()
    {
        initialize();

        Log.d(TAG, "Deleting all content (including local files)");

        for (DRMContentInfo drmContentInfo : mLastLoadedDRMContentInfos)
        {
            deleteContent(drmContentInfo, false);
        }

        mLastLoadedDRMContentInfos.clear();
        mLastLoadedListFileNames.clear();
    }

    private static class DescriptorLoadException extends RuntimeException
    {
        File mDescriptorFile;
        URL mURl;
        String mErrorMessage;

        private DescriptorLoadException(final File mDescriptorFile, final URL mURl, final String mErrorMessage)
        {
            super("Error loading descriptor at " + mDescriptorFile + " (" + mURl + ") with error message: " + mErrorMessage);
            this.mDescriptorFile = mDescriptorFile;
            this.mURl = mURl;
            this.mErrorMessage = mErrorMessage;
        }
    }
    
    /**
    *
    * @param context
    * @param uri
    * @return
    */
   public static DRMContentInfo createDRMContentInfoForUnknownURI(final Context context, final URI uri, boolean isProtected)
   {
       DRMContentInfo drmContentInfo;
       DRMContentFormat drmContentFormat;

       if (uri.getScheme() != null && uri.getScheme().startsWith("http"))
       {
           if(uri.toString().endsWith("m3u8") || uri.toString().endsWith("m3u"))
           {
               drmContentFormat = DRMContentFormat.HTTP_LIVE_STREAMING;
           }
           else if(uri.toString().endsWith("manifest") || uri.toString().endsWith("Manifest"))
           {
               drmContentFormat = DRMContentFormat.SMOOTH_STREAMING;
           }
           else if(uri.toString().endsWith(".wmv") || uri.toString().endsWith(".wma"))
           {
               drmContentFormat = DRMContentFormat.WINDOWS_MEDIA;
           }
           else if(uri.toString().endsWith(".pyv") || uri.toString().endsWith(".pya") || uri.toString().endsWith(".asf"))
           {
               drmContentFormat = DRMContentFormat.PLAYREADY_ASF;
           }
           else if(uri.toString().endsWith(".piff"))
           {
               drmContentFormat = DRMContentFormat.PIFF;
           }
           else
           {
               //format not recognized
               String errMsgFormatNotRecognized = "Format not recognized, double-check the extension of the file to play; known extensions are: m3u8, m3u, manifest/Manifest, wmv, wma, pyv, pya, asf, piff.";
//               if(DEBUG) Log.e(TAG,errMsgFormatNotRecognized);
               throw new RuntimeException(errMsgFormatNotRecognized);
           }
       }
       else
       {
           //schema not recognized
           String errMsgSchemaNotRecognized = "Schema not recognized for url:" + uri.toString();
//           if(DEBUG) Log.e(TAG,errMsgSchemaNotRecognized);
           throw new RuntimeException(errMsgSchemaNotRecognized);
       }
//       if (DEBUG) Log.d(TAG,"Sufficient information available, drmContentFormat=" + drmContentFormat.toString() + "; will populate an object instance for " + uri);
       drmContentInfo = new DRMContentInfo();
       drmContentInfo.mContentFormatFromCdesc = drmContentFormat.toString();
       drmContentInfo.mContentLocation = uri;
       if (isProtected) 
       {
       	drmContentInfo.setDRMContent(DRMAgentDelegate.getDRMAgentInstance(context).getDRMContent(uri,drmContentFormat));
       } 
       else 
       {
       	DRMScheme scheme = DRMScheme.valueOf("CLEARTEXT");
       	drmContentInfo.setDRMContent(DRMAgentDelegate.getDRMContent(context, drmContentInfo.mContentLocation, drmContentFormat, scheme));
       }
       return drmContentInfo;
   }
   
}
