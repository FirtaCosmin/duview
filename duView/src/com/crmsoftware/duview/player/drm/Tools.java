/**
 * $URL$
 * DAAND
 * Copyright (C) 2004 - 2013, INSIDE Secure BV.  All rights reserved.
 */
package com.crmsoftware.duview.player.drm;

import java.io.*;
import java.net.URI;
import java.text.DecimalFormat;

import com.crmsoftware.duview.utils.Log;

import android.app.*;
import android.content.*;
import android.net.http.AndroidHttpClient;


public class Tools
{
    public static final String TAG = "Tools";
    private static final String defaultSubfileName = Constants.CDESC_PATH + "subtitles";
    private static final String dot = ".";
    
    /**
     * Creates a new <code>AndroidHttpClient</code> instance.
     *
     * @return The newly created HTTP client
     */
    public static AndroidHttpClient createHttpClient()
    {
        return AndroidHttpClient.newInstance(Constants.DRM_AGENT_USER_STRING);
    }

    /**
     * Copies the specified input stream onto the given output stream.
     *
     * @param inputStream  Input stream to read from
     * @param outputStream Output stream to write to
     * @return The number of bytes copied
     * @throws IOException If an error occurs while copying the data
     */
    public static long copyStream(final InputStream inputStream, final OutputStream outputStream) throws IOException
    {
        final int size = 16192;
        final byte[] buffer = new byte[size];

        int len;

        long totalCount = 0;
        while ((len = inputStream.read(buffer, 0, size)) != -1)
        {
            outputStream.write(buffer, 0, len);
            totalCount += len;

        }
        outputStream.flush();

        return totalCount;

    }

    /**
     * Method will check if the specified object is null and if so, generate and throw a <code>NullPointerException</code>.
     *
     * @param name The display name to use in the exception
     * @param obj  The object to check for null.
     */
    public static void DEFENSE(String name, Object obj)
    {
        if (obj == null)
        {
            throw new NullPointerException(name);
        }
    }

    public static ProgressDialog createProgressDialog(final Context owner, final CharSequence title, final CharSequence text, boolean cancelable,
                                              final CharSequence positiveText, final DialogInterface.OnClickListener positiveListener,
                                              final CharSequence negativeText, final DialogInterface.OnClickListener negativeListener)
    {
        ProgressDialog dialog = new ProgressDialog(owner);
        dialog.setTitle(title);
        dialog.setMessage(text);
        if (positiveText != null && positiveListener != null)
        {
            dialog.setButton(DialogInterface.BUTTON_POSITIVE, positiveText, positiveListener);
        }

        if (negativeText != null && negativeListener != null)
        {
            dialog.setButton(DialogInterface.BUTTON_NEGATIVE, negativeText, negativeListener);
        }

        dialog.setCancelable(cancelable);

        return dialog;
    }


    public static Dialog createAlertDialog(final Context owner, final CharSequence title, final CharSequence text, final boolean cancelable,
                                           final CharSequence positiveText, final DialogInterface.OnClickListener positiveListener,
                                           final CharSequence negativeText, final DialogInterface.OnClickListener negativeListener)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(owner);
        builder.setTitle(title);
        builder.setMessage(text);
        builder.setCancelable(cancelable);
        if (positiveText != null && positiveListener != null)
        {
            builder.setPositiveButton(positiveText, positiveListener);
        }
        if (negativeText != null && negativeListener != null)
        {
            builder.setNegativeButton(negativeText, negativeListener);
        }
        return builder.create();
    }

    public static String convertToNiceTime(final int mPlayingTime)
    {
        int seconds = mPlayingTime / 1000;
        int minutes = seconds / 60;
        seconds = (seconds - (minutes * 60));
        return zeroPad(minutes) + ":" + zeroPad(seconds);
    }

    public static String zeroPad(final int number)
    {
        return (number <= 9 ? "0" + number : "" + number);
    }

    public static String toHex(String txt)
    {
        return toHex(txt.getBytes());
    }

    public static String fromHex(String hex)
    {
        return new String(toByte(hex));
    }

    public static byte[] toByte(String hexString)
    {
        int len = hexString.length() / 2;
        byte[] result = new byte[len];
        for (int i = 0; i < len; i++)
        {
            result[i] = Integer.valueOf(hexString.substring(2 * i, 2 * i + 2), 16).byteValue();
        }
        return result;
    }

    public static String toHex(byte[] buf)
    {
        if (buf == null)
        {
            return "";
        }
        StringBuffer result = new StringBuffer(2 * buf.length);
        for (int i = 0; i < buf.length; i++)
        {
            appendHex(result, buf[i]);
        }
        return result.toString();
    }

    private final static String HEX = "0123456789ABCDEF";

    private static void appendHex(StringBuffer sb, byte b)
    {
        sb.append(HEX.charAt((b >> 4) & 0x0f)).append(HEX.charAt(b & 0x0f));
    }

    public static int extractStartFrom(final Context context, final Intent lastIntent, final URI uri)
    {
        // Figure out if we should start from the beginning or seeking into the content
        // The intent usually contains the startFrom time if the license expired and the play
        // was restarted
        Log.d(TAG, "Extracting timestamp for: " + uri.toString());
        int startFrom = 0;
        if (lastIntent != null)
        {
            startFrom = lastIntent.getIntExtra(Constants.START_FROM, 0);
            Log.d(TAG, "Intent Timestamp: " + startFrom);
        }

        if (startFrom == 0)
        {
            // Try our settings to see if we can find a last place to play from
            final SharedPreferences insideSecureSharedPref = context.getSharedPreferences(Constants.PREFERENCES_FILENAME, Context.MODE_PRIVATE);
            if (insideSecureSharedPref.getBoolean(Constants.PREFERENCES_RESUME_PLAY_FROM_LAST_KNOWN_TIME,false))
            {
                final int aLong = (int) insideSecureSharedPref.getLong(uri.toString() + ".time", -1);
                if (aLong != -1)
                {
                    Log.d(TAG, "Saved Timestamp: " + aLong);
                    startFrom = aLong;
                }
            }
        }
        Log.d(TAG, "Extracted timestamp: " + startFrom);
        return startFrom;
    }


    public static void saveStartFrom(final Context context, final long timestamp, final URI uri)
    {
        if (timestamp != 0)
        {
            final SharedPreferences insideSecureSharedPref = context.getSharedPreferences(Constants.PREFERENCES_FILENAME, Context.MODE_PRIVATE);
            if (insideSecureSharedPref.getBoolean(Constants.PREFERENCES_RESUME_PLAY_FROM_LAST_KNOWN_TIME,false))
            {
                Log.d(TAG, "Saving current timestamp for: " + uri.toString());
                Log.d(TAG, "Timestamp: " + timestamp);

                final SharedPreferences.Editor edit = insideSecureSharedPref.edit();
                edit.putString(uri.toString() + ".uri", uri.toString());
                edit.putLong(uri.toString() + ".time", timestamp);
                edit.commit();
            }
        }
    }


    public static String toGUID(final byte[] raw)
    {
        byte[] first = new byte[4];
        byte[] second = new byte[2];
        byte[] third = new byte[2];
        byte[] fourth = new byte[2];
        byte[] fifth = new byte[6];

        System.arraycopy(raw, 0, first, 0, 4);
        System.arraycopy(raw, 4, second, 0, 2);
        System.arraycopy(raw, 6, third, 0, 2);
        System.arraycopy(raw, 8, fourth, 0, 2);
        System.arraycopy(raw, 10, fifth, 0, 6);

        final StringBuffer sb = new StringBuffer();

        sb.append(Tools.toHex(reverse(first)));
        sb.append('-');
        sb.append(Tools.toHex(reverse(second)));
        sb.append('-');
        sb.append(Tools.toHex(reverse(third)));
        sb.append('-');
        sb.append(Tools.toHex(fourth));
        sb.append('-');
        sb.append(Tools.toHex(fifth));

        return sb.toString().toUpperCase();

    }

    private static byte[] reverse(byte[] input)
    {
        byte[] output = new byte[input.length];

        int out = 0;
        for (int i = input.length-1; i >=0; i--)
        {
            output[out++] = input[i];
        }
        return output;
    }
    
    public static String exportHDS(String source)
    {
        Log.d(TAG, "Source: " + source);
        
        File sourceFile = new File(source);
        if(!sourceFile.exists())
        {
            Log.d(TAG, "Source file DOES not exit (" + source + ")");
            return "";
        }

        String destination = "sdcard/insidesecure/playready.hds";
        File destFile = new File(destination);

        if(destFile.exists())
        {
            Log.d(TAG, "Destination file ("+ destFile.getName() + ") exist; delete it before proceeding.");
            destFile.delete();
        }
        try
        {
            Log.d(TAG, "Creating destination file: " + destFile.getAbsolutePath() + ".");

            destFile.createNewFile();
        }
        catch (IOException iOException)
        {
            Log.d(TAG,"Exception: " + iOException.getMessage());
        }

        try
        {
            boolean copyRes = copyFile(sourceFile,destFile);
            Log.d(TAG,"Copy completed " + (copyRes ? "successfully" : "NOT successfully"));
        }
        catch(Exception ex)
        {
            Log.d(TAG, "Copy failed with msg=" + ex.getMessage());
        }
        return destination;
    }

    private static boolean copyFile(File source, File destination)
    {
        boolean retVal = false;

        FileInputStream inputStream = null;
        FileOutputStream outputStream = null;
        try
        {
            inputStream = new FileInputStream(source);
            outputStream = new FileOutputStream(destination);
            Tools.copyStream(inputStream, outputStream);
            retVal = true;

        }
        catch(FileNotFoundException ex)
        {
            Log.d(TAG,"FileNotFoundException=" + ex.getMessage());
        }
        catch(IOException ex2)
        {
            Log.d(TAG,"IOException=" + ex2.getMessage());
        }
        finally
        {
            try
            {
                if (inputStream != null)
                    inputStream.close();
                if (outputStream != null)
                {
                    outputStream.flush();
                    outputStream.close();
                }
            }
            catch (IOException e)
            {
                Log.e(TAG, e.getMessage());
            }
        }

        return retVal;
    }

    // Returns the extension of the found file or null
    // path is the full filename _without_ the extension, e.g. /sdcard/insidesecure/subtitle
    private static String subFileExists(String path, String[] supportedExtensions)
    {
        for (String extension: supportedExtensions)
            if (new File(path + dot + extension).exists())
                return extension;
        return null;
    }

    // Returns the full path of the found subtitle file or null if none was found
    public static String getSubtitlePath(String title, String[] supportedExtensions)
    {
        if  (title == null
                || supportedExtensions == null || supportedExtensions.length == 0) return null;

        String extension = subFileExists(Constants.CDESC_PATH + title, supportedExtensions);
        String result = null;
        if (extension != null)
        {
            result = Constants.CDESC_PATH + title + dot + extension;
        }
        else
        {
            extension = subFileExists(defaultSubfileName, supportedExtensions);
            if (extension != null)
                result = defaultSubfileName + dot + extension;
        }
        Log.i(TAG, "Returning subtitle filename: " + result);
        return result;
    }


    public static String readableFileSize(long size)
    {
        if (size <= 0) return "0";
        final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    public static boolean isHTTPScheme(final URI uri)
    {
        return "http".equals(uri.getScheme()) || "https".equals(uri.getScheme());
    }

}
