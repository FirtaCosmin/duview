package com.crmsoftware.duview.player.drm;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Calendar;

import org.json.JSONArray;

import com.crmsoftware.duview.utils.Log;

import android.content.Context;
import android.content.SharedPreferences;


public class MvpLicenseAcquisitionHelper extends LicenseAcquisitionHelper 
{
	public static boolean DEBUG = Log.DEBUG; 
	
    private static final String TAG = MvpLicenseAcquisitionHelper.class.getSimpleName();
    
    protected static boolean isTTS(final String laURL)
    {
        final boolean b = laURL.startsWith("http://essdrmproducts-eu.insidesecure.com/test-portal/") ||
                laURL.startsWith("http://essdrmproducts-eu.insidesecure.com:80/test-portal/") ||
                laURL.startsWith("http://essdrmproducts.insidesecure.com/safenet-test-portal/") ||
                laURL.startsWith("http://essdrmproducts.insidesecure.com:80/safenet-test-portal/");
        if (DEBUG) Log.d(TAG, "Web Initiator: " + (b ? "supported" : "not supported"));
        return b;
    }
    
    protected static String createRights(final Context context)
    {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.PREFERENCES_FILENAME, Context.MODE_PRIVATE);
        final boolean forceTimeBasedLicense = sharedPreferences.getBoolean(Constants.PREFERENCES_FORCE_TIMEBASED_LICENSE_FOR_CONTENT, false);
        final boolean forceOPLBasedLicense = sharedPreferences.getBoolean(Constants.PREFERENCES_FORCE_OPL_LICENSE_FOR_CONTENT, false);
        final boolean forceIntervalLicense = sharedPreferences.getBoolean(Constants.PREFERENCES_FORCE_INTERVAL_LICENSE_FOR_CONTENT, false);
        int intervalSeconds = sharedPreferences.getInt(Constants.PREFERENCES_INTERVAL_LICENSE_NUM_SECONDS, 30);

        int numMinutes = sharedPreferences.getInt(Constants.PREFERENCES_TIMEBASED_LICENSE_NUM_MINUTES, 5);
        int oplLevel = sharedPreferences.getInt(Constants.PREFERENCES_OPL_LICENSE_LEVEL, 100);

        String playEnablers = "";

        Calendar timeBasedDate;

        try
        {
            JSONArray storedData = new JSONArray(sharedPreferences.getString(Constants.PREFERENCES_PLAY_ENABLERS, "[]"));

            for (int i = 0; i < storedData.length(); i ++)
            {
                playEnablers = storedData.getString(i) + (playEnablers.length() == 0 ? "" : ",") + playEnablers;
            }
        }

        catch (Exception exception)
        {
            exception.printStackTrace();
            Log.e(TAG, "Unexpected error " + exception.toString());
        }

        if (DEBUG) Log.d(TAG, "Force time-based license: " + (forceTimeBasedLicense ? "Yes" : "No"));
        if (DEBUG) Log.d(TAG, "Number of Minutes: " + numMinutes);
        if (DEBUG) Log.d(TAG, "Force OPL license: " + (forceOPLBasedLicense ? "Yes" : "No"));
        if (DEBUG) Log.d(TAG, "OPL Level: " + oplLevel);
        if (DEBUG) Log.d(TAG, "Force Interval license: " + (forceIntervalLicense ? "Yes" : "No"));
        if (DEBUG) Log.d(TAG, "Number of Second: " + intervalSeconds);
        if (DEBUG) Log.d(TAG, "Play enablers: " + playEnablers);

        String rawRights = "PlayRight=1";

        if (forceTimeBasedLicense)
        {
            try
            {
                timeBasedDate = Calendar.getInstance();

                timeBasedDate.add(Calendar.MINUTE, -numMinutes);
                rawRights += "&BeginDate=" + String.format(Constants.MMDDYYHHMMSS_DATE_FORMAT, timeBasedDate).replaceAll(" ", URLEncoder.encode(" ", "UTF-8"));

                // Multiply by two to make sure we are really adding to the date
                timeBasedDate.add(Calendar.MINUTE, numMinutes * 2);
                rawRights += "&Expiration=" + String.format(Constants.MMDDYYHHMMSS_DATE_FORMAT, timeBasedDate).replaceAll(" ", URLEncoder.encode(" ", "UTF-8"));
            }

            catch (Exception exception)
            {
                exception.printStackTrace();
                Log.e(TAG, "Unexpected error " + exception.toString());
            }

        }

        if (forceIntervalLicense)
        {
            rawRights += "&FirstPlayExpiration=" + intervalSeconds;
        }

        if (forceOPLBasedLicense)
        {
            rawRights += "&UncompressedDigitalVideoOPL=" + oplLevel;
        }

        if (playEnablers.length() != 0)
        {
            rawRights += "&PlayEnablers=" + playEnablers;
        }

        return rawRights;
    }

    
	 public static URL performURLRewrite(final Context context, final URL la1URL, final DRMContentInfo drmContentInfo, String token, String accoutId, String pid)
	   {

	       final SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.PREFERENCES_FILENAME, Context.MODE_PRIVATE);
	       final boolean forceLicenseAcknowledgement = sharedPreferences.getBoolean(Constants.PREFERENCES_FORCE_ACKNOWLEDGEMENT_LICENSE_FOR_CONTENT, false);

	       final boolean useUrlFromHeader = sharedPreferences.getBoolean(Constants.PREFERENCES_USE_URL_FROM_HEADER, false);

	       if (DEBUG) Log.d(TAG, "License Acquisition URL from header: " + la1URL);
	       if (DEBUG) Log.d(TAG, "Force Acknowledgement: " + (forceLicenseAcknowledgement ? "Yes" : "No"));

	       URL actualLicenseAcquisitionURL = la1URL;

	       if (drmContentInfo.mLaURLOverride != null)
	       {
	           if (DEBUG) Log.d(TAG, "Using mLaURLOverride: " + la1URL);
				actualLicenseAcquisitionURL = drmContentInfo.mLaURLOverride;
	       }

	       // Extract the CID from the URL
	       // NOTE: This is present in the code to handle license acquisition based on the INSIDE Secure's TTS license server.
	       // NOTE: The logic here would not apply to other, custom, license servers
	       final String actualLicenseAcquisitionURLStr = actualLicenseAcquisitionURL.toString();
	       if (!useUrlFromHeader && isTTS(actualLicenseAcquisitionURLStr) && actualLicenseAcquisitionURLStr.contains("?b="))
	       {
	           if (DEBUG) Log.d(TAG, "Detected the TTS license server, will reconstruct the LA_URL accordingly");
	           try
	           {
	               String cid = null;

	               String encodedString = actualLicenseAcquisitionURLStr.split("=")[1];
	               String hexString = new String(Tools.toByte(encodedString));
	               final String[] segments = hexString.split("\\|\\|");
	               for (String segment : segments)
	               {
	                   if (DEBUG) Log.d(TAG, "Segment: " + segment);
	                   if (segment.startsWith("cid"))
	                   {
	                       cid = segment.split("=")[1];
	                       break;
	                   }
	               }
	               if (cid != null)
	               {

	                   String rights = createRights(context);

	                   String parameters = "m=PRDynamicSilentLicenseDelivery||";
	                   if (forceLicenseAcknowledgement)
	                   {
	                       parameters = parameters + "acknowledgeLicenseInstall=true||";
	                   }
	                   parameters = parameters + "acid=eval||rights=" + rights + "||cid=" + cid;

	                   hexString = Tools.toHex(parameters);
	                   final URL newLaURL = new URL("http://essdrmproducts-eu.insidesecure.com/test-portal/device/portal?b=" + hexString);
	                   if (DEBUG) Log.d(TAG, "New URL: " + newLaURL);
	                   return newLaURL;
	               }

	           }
	           catch (Exception e)
	           {
	               Log.e(TAG, "Error while constructing URL based on: " + actualLicenseAcquisitionURL);
	               throw new RuntimeException("Error while constructing URL from: " + actualLicenseAcquisitionURL);
	           }
	       }
	       String urlString = actualLicenseAcquisitionURL.toString() + 
	       		"?schema=1.0" +
	       		"&account=" +
	       		accoutId +
	       		"&auth=" + token + 
	       		"&releasePid=" + pid;
	       try{
	       	actualLicenseAcquisitionURL = new URL(urlString);
	       }
	       catch (MalformedURLException e)
	       {
	       	Log.e(TAG, "Error while parsing url: " + urlString);
	       	throw new RuntimeException("Error while attempting to use LA URL override: " + urlString);
	       }
	       if (DEBUG) Log.d(TAG,"Returning unmodified LA URL (from header or mLaURLOverride): " + actualLicenseAcquisitionURL);
	       return actualLicenseAcquisitionURL;
	   }
}
