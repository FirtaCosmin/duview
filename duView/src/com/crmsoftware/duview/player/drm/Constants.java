/**
 * $URL$
 * DAAND
 * Copyright (C) 2004 - 2013, INSIDE Secure BV.  All rights reserved.
 */
package com.crmsoftware.duview.player.drm;

public class Constants
{
    public static final String PREFERENCES_NEXTREAMING_LOG_LEVEL = "NEXTREAMING_LOG_LEVEL";
    public static final String PREFERENCES_USE_URL_FROM_HEADER = "PREFERENCES_USE_URL_FROM_HEADER";
    public static final String PREFERENCES_EMBED_LICENSE = "PREFERENCES_EMBED_LICENSE";
    public static final String PREFERENCES_FORCE_TIMEBASED_LICENSE_FOR_CONTENT = "PREFERENCES_FORCE_TIMEBASED_LICENSE_FOR_CONTENT";
    public static final String PREFERENCES_TIMEBASED_LICENSE_NUM_MINUTES = "PREFERENCES_TIMEBASED_LICENSE_NUM_MINUTES";
    public static final String PREFERENCES_INTERVAL_LICENSE_NUM_SECONDS = "PREFERENCES_INTERVAL_LICENSE_NUM_SECONDS";
    public static final String PREFERENCES_FORCE_OPL_LICENSE_FOR_CONTENT = "PREFERENCES_FORCE_OPL_LICENSE_FOR_CONTENT";
    public static final String PREFERENCES_FORCE_INTERVAL_LICENSE_FOR_CONTENT = "PREFERENCES_FORCE_INTERVAL_LICENSE_FOR_CONTENT";
    public static final String PREFERENCES_OPL_LICENSE_LEVEL = "PREFERENCES_OPL_LICENSE_LEVEL";

    public static final String PREFERENCES_RESUME_PLAY_FROM_LAST_KNOWN_TIME = "PREFERENCES_RESUME_PLAY_FROM_LAST_KNOWN_TIME";

    public static final String PREFERENCES_FILENAME= "insideSecureSharedPref";

    public static final String DRM_AGENT_USER_STRING = "InsideSecure/X (DRM Fusion Agent Reference Player)";

    public static final String PREFERENCES_SHOW_ADVANCED_OPTIONS = "PREFERENCES_SHOW_ADVANCED_OPTIONS";
    public static final String PREFERENCES_OFFLINE_MODE = "PREFERENCES_OFFLINE_MODE";

    public static final String ADVANCED_OPTIONS_TAG = "advanced";
    public static final String PREFERENCES_AGENT_LOG_LEVEL = "AGENT_LOG_LEVEL";
    public static final String PREFERENCES_NEXTREAMING_RENDERER = "NEXTREAMING_RENDERER";
    public static final String PREFERENCES_USE_REMOTE_URL_FOR_NATIVE_PLAYER = "PREFERENCES_USE_REMOTE_URL_FOR_NATIVE_PLAYER";
    public static final String PREFERENCES_NATIVE_PLAYER_BUFFER_SIZE = "PREFERENCES_NATIVE_PLAYER_BUFFER_SIZE";
    public static final String PREFERENCES_IGNORE_MIMETYPE_VALIDATION_ERRORS = "PREFERENCES_IGNORE_MIMETYPE_VALIDATION_ERRORS";
    public static final String PREFERENCES_SHOW_SUBTITLES = "PREFERENCES_SHOW_SUBTITLES";
    public static final String PREFERENCES_FORCE_ACKNOWLEDGEMENT_LICENSE_FOR_CONTENT = "PREFERENCES_FORCE_ACKNOWLEDGEMENT_LICENSE_FOR_CONTENT";
    public static final String PREFERENCES_LOCK_SCREEN_TO_LANDSCAPE_MODE_FOR_NEXPLAYER = "PREFERENCES_LOCK_SCREEN_TO_LANDSCAPE_MODE_FOR_NEXPLAYER";

    public static final String PREFERENCES_ACKNOWLEDGE_LICENSES_ASYNCHRONOUSLY = "PREFERENCES_ACKNOWLEDGE_LICENSES_ASYNCHRONOUSLY";
    public static final String PREFERENCES_USE_HEADLESS_HLS = "PREFERENCES_USE_HEADLESS_HLS";

    public static final String START_FROM = "startFrom";
    public static final String DOWNLOAD_AND_PLAY = "DOWNLOAD_AND_PLAY";
    public static final String DOWNLOAD_AND_PLAY_MODE = "DOWNLOAD_AND_PLAY_MODE";
    public static final String ENABLED_VIDEO_TRACKS = "ENABLED_VIDEO_TRACKS";
    public static final String ENABLED_AUDIO_TRACK = "ENABLED_AUDIO_TRACK";
    public static final String ENABLED_AUDIO_TRACKS = "ENABLED_AUDIO_TRACKS";
    public static final String ENABLED_AUDIO_TRACK_QUALITY_LEVEL = "ENABLED_AUDIO_TRACK_QUALITY_LEVEL";
    public static final String ENABLED_HDMI_CONTROL = "ENABLED_HDMI_CONTROL";
    public static final String ENABLED_SUBTITLE_TRACK = "ENABLED_SUBTITLE_TRACK";
    public static final String ENABLED_SUBTITLE_TRACKS = "ENABLED_SUBTITLE_TRACKS";
    public static final String ENABLED_SIDELOADEAD_SUBTITLES = "ENABLED_SIDELOADEAD_SUBTITLES";

    public static final String LAST_ACTIVATION_METHOD = "LAST_ACTIVATION_METHOD";
    public static final String LAST_ACTIVATION_METHOD_OFFLINE = "OFFLINE";
    public static final String LAST_ACTIVATION_METHOD_ONLINE = "ONLINE";

    public static final String DOMAIN_JOIN_WEB_INITIATOR_URL = "http://essdrmproducts-eu.insidesecure.com/test-portal/device/triggerGenerator.form?deviceAction=join&domainID=0136A6A335C4E490D&contentDistributorID=eval&rightsIssuerID=PR1&dotwopass=true&technologyType=MS-DRM-PLAYREADY&device=anonymous";
    public static final String DOMAIN_LEAVE_WEB_INITIATOR_URL = "http://essdrmproducts-eu.insidesecure.com/test-portal/device/triggerGenerator.form?deviceAction=leave&domainID=0136A6A335C4E490D&contentDistributorID=eval&rightsIssuerID=PR1&dotwopass=true&technologyType=MS-DRM-PLAYREADY&device=anonymous";

    public static final String HDMI_SETTING = "HDMI_SETTING";

    public static final String PREFERENCES_USE_CONTENT_FROM_US = "PREFERENCES_USE_CONTENT_FROM_US";

    public static final String EU_HOST_NAME = "http://essdrmproducts-eu.insidesecure.com";
    public static final String AMS_HOST_NAME = "http://essdrmproducts-ams.insidesecure.com";
    public static final String US_HOST_NAME = "http://essdrmproducts-us.insidesecure.com";

    public static final String PREFERENCES_HTTPS_CLIENT_TRUST_ALL_CERTS = "PREFERENCES_HTTPS_CLIENT_TRUST_ALL_CERTS";
    public static final String PREFERENCES_SHOW_TIMESTAMP_IN_LIVE_STREAMS = "PREFERENCES_SHOW_TIMESTAMP_IN_LIVE_STREAMS";
    public static final String CDESC_PATH = "/sdcard/insidesecure/";
    public static final String CUSTOM_DATA_FILE_NAME = "custom-data.txt";
    public static final String PREFERENCES_EULA_ACCEPTED = "PREFERENCES_EULA_ACCEPTED";

    public static final String PREFERENCES_PLAY_ENABLERS = "PREFERENCES_PLAY ENABLERS";

    public static final String MMDDYYHHMMSS_DATE_FORMAT = "%1$tm/%1$td/%1$tY %1$tH:%1$tM:%1$tS";
    public static final String PREFERENCES_ENABLE_QOS = "PREFERENCES_ENABLE_QOS";

    public static final String PREFERENCES_WEB_INITIATOR_URL = "PREFERENCES_WEB_INITIATOR_URL";

    public static String CONTENT_DRM_CONTENT_FORMAT = "CONTENT_DRM_CONTENT_FORMAT";
    public static String CONTENT_DRM_SCHEME = "CONTENT_DRM_SCHEME";
    public static String CONTENT_URI = "CONTENT_URI";
    public static String CONTENT_SUBTITLE_FILE_URI = "CONTENT_SUBTITLE_FILE_URI";
    public static String CONTENT_CUSTOM_DATA = "CONTENT_CUSTOM_DATA";
    public static String CONTENT_LICENSE_ACQUISITION_URL_OVERRIDE = "CONTENT_LICENSE_ACQUISITION_URL_OVERRIDE";

    public static String CONTENT_TITLE = "CONTENT_TITLE";

    public static final String CONFIGURATION_PROPERTIES_FILE = "drmagent-configuration.properties";
    public static final String CONFIGURATION_RUNTIME_APPLICATION_DATA_FILE = "drmagent.rad";
}