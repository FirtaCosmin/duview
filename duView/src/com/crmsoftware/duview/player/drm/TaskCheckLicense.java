package com.crmsoftware.duview.player.drm;

import java.net.URI;
import java.net.URL;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpRequestBase;

import android.content.Context;
import android.content.SharedPreferences;

import com.crmsoftware.duview.configuration.PlatformSettings;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.data.MvpContent;
import com.crmsoftware.duview.data.MvpError;
import com.crmsoftware.duview.data.MvpError.ErrorType;
import com.crmsoftware.duview.ui.watchnow.LiveTvFragment;
import com.crmsoftware.duview.utils.Log;
import com.insidesecure.drmagent.v2.DRMContent;
import com.insidesecure.drmagent.v2.DRMContentFormat;
import com.insidesecure.drmagent.v2.DRMError;
import com.insidesecure.drmagent.v2.DRMLicenseAcquisitionHandler;
import com.insidesecure.drmagent.v2.DRMRights;
import com.insidesecure.drmagent.v2.DRMScheme;
import com.insidesecure.drmagent.v2.PlayReadySoapError;
import com.insidesecure.drmagent.v2.utils.PlayReadyDRMJoinDomainHandler;
import com.insidesecure.drmagent.v2.utils.PlayReadyDRMLicenseAcquisitionHandler;
import com.insidesecure.drmagent.v2.utils.WMDRMLicenseAcquisitionHandler;

public class TaskCheckLicense extends Task{
	public static boolean DEBUG_DRM = Log.DEBUG; 
	public final static String TAG = TaskCheckLicense.class.getSimpleName();
	
    protected static enum LicenseAcquisitionResult
    {
        NONE, SUCCESS, TIMEOUT, CANCELLED, ERROR, JOIN_DOMAIN_REQUIRED, JOIN_DOMAIN_SUCCESS, LICENSE_ACKNOWLEDGEMENT_REQUESTED
    }
    
    public static final int LICENSE_ACQUISITION_MAX_WAIT = 30;
    
	String mCustomData;
	
    private DRMContent mDRMContent;
    private DRMContentInfo mDRMContentInfo;
    Context context;
    private String mediaUrl;
    private String mediaPid;
    private String accountId;
    private String token;
    private DRMContentInfo returnData;
    private boolean isProtected = false;

    
    private Lock mLicenseAcquisitionResultLock = new ReentrantLock();
    private LicenseAcquisitionResult mLicenseAcquisitionResult = LicenseAcquisitionResult.NONE;
    private DRMLicenseAcquisitionHandler mDRMLicenseAcquisitionHandler;
    private byte[] mAcquireLicenseResponse = null;
    private PlayReadyDRMJoinDomainHandler mPlayReadyDRMJoinDomainHandler;
    
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public TaskCheckLicense(ITaskReturn callback, Context context, String mediaUrl, MvpContent content)
	{
		super(callback);
		
		this.context = context;

        this.mediaUrl = mediaUrl;
        this.mediaPid = content.getReleasesList().getItem(0).getPid();
        this.isProtected = content.isProtected();

        this.mCustomData = null; //extras.getString(Constants.CONTENT_CUSTOM_DATA);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public Object getReturnData() {

		return returnData;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	protected void onExecute() {

        MvpUserData data = MvpUserData.getFromMvpObject();
        if (data != null)
        {
        	token = data.getToken();
        }
        
        accountId = PlatformSettings.getInstance().getTpMpxAccoutUrl();
		
		mDRMContentInfo = null;

        final URI uri = URI.create(mediaUrl);
        try
        {
        	mDRMContentInfo = ContentHandler.createDRMContentInfoForUnknownURI(context, uri,isProtected);
        }
        catch (Exception e)
        {
        	taskCompleted(new MvpError(ErrorType.Processing, e));
        	return;
        }
              
        mDRMContent = mDRMContentInfo.getDRMContent(context);

        if (mDRMContent == null)
        {
            Log.e(TAG, "Error while opening content");
            taskCompleted(new MvpError(ErrorType.Processing, DRMError.GENERAL_DRM_ERROR));
            return;
        }
        
        
        if (mDRMContent.isProtected())
        {
            // This checks whether or not the Agent has rights for the content in question.  This check will
            // verify both if the content is clear-text or protected & rights exists
            final DRMRights.DRMRightsType drmRightsType = mDRMContent.getDRMRights().getDRMRightsType();

            switch (drmRightsType)
            {
            	case VALID:
            		 if (DEBUG_DRM) Log.d(TAG, "Rights available ... starting player");
            		 startPlayer();
                     break;
            	case RIGHTS_ON_DEMAND:
                    if (DEBUG_DRM) Log.d(TAG, "Rights availability is actually not known (for this live stream), the rights should have being delivered... starting player");
                    startPlayer();
                    break;
                    
            	case UNTRUSTED_TIME:
            		
            		// some task to do
                case NO_RIGHTS:
                	if (DEBUG_DRM) Log.d(TAG, "No rights available");
                	initiateRightsIssuerLicenseAcquisition();
                	break;
                	
                case RIGHTS_IN_FUTURE:
                	if (DEBUG_DRM) Log.d(TAG, "Rights in the future");
                	initiateRightsIssuerLicenseAcquisition();
                	break;
                	
                case EXPIRED:
                	if (DEBUG_DRM) Log.d(TAG, "Rights expired");
                	initiateRightsIssuerLicenseAcquisition();
                    break;
                    
                default:
                	if (DEBUG_DRM) Log.d(TAG, "No rights available");
                	taskCompleted(new MvpError(ErrorType.Processing, drmRightsType));
                    throw new RuntimeException("Unhandled DRM rights type: " + drmRightsType);
            }
            
        }
        else
        {
            if (DEBUG_DRM) Log.d(TAG, "Content is clear-text... starting player");
            startPlayer();
        }
        
	}
	
	
   public static DRMContent removeLevels(DRMContent drmContent)
   {      
	   if (null == drmContent)
	   {
		   return null;
	   }
	   
       List<DRMContent.VideoQualityLevel> levels = drmContent.getVideoQualityLevels();
       if (DEBUG_DRM) Log.d(TAG, "DRMContent before: ");
       printDRMContent(drmContent);

       levels.remove(DRMContent.AUDIO_ONLY_VIDEO_QUALITY_LEVEL);
       
//      for (int i=levels.size()-1; i>=0 ;i--)
//       {
//    	   DRMContent.VideoQualityLevel videoQualityLevel = levels.get(i);
//	   		if (videoQualityLevel.mBitRate > 200000 && videoQualityLevel.mBitRate < 800000)
//	   		{
//	   		
//		   
//	   		levels.remove(i);
//	       	 if (DEBUG_DRM) Log.d(TAG, "removed " + " Profile=" + videoQualityLevel.mVideoProfile.name() + " bitrate=" + videoQualityLevel.mBitRate + " bps (W=" + videoQualityLevel.mWidth
//	       		        + "H=" + videoQualityLevel.mHeight + ")");
//	   		}
//
//       } 
       
//       drmContent.setMaxBandwidth(1000000);
       
       drmContent.setVideoQualityLevels(levels);
       
       if (DEBUG_DRM) Log.d(TAG, "DRMContent after: ");
       printDRMContent(drmContent);
            
       return drmContent;
   }
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
    private void startPlayer()
    {
    	returnData = mDRMContentInfo;
        if (returnData != null)
        {
        	DRMContent content  = returnData.getDRMContent(context);
        	removeLevels(content);

        	taskCompleted();
        }
        else
        {
        	taskCompleted(new MvpError(ErrorType.Processing));
        }
    }

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
   private void showError(DRMError drmError)
   {
       switch (drmError)
       {
           case NO_RIGHTS:
               
               break;
           case UNTRUSTED_TIME:
    
               break;
           case POLICY_CHECK_FAILURE:
   
               break;
//           case CONTENT_RETRIEVAL_ERROR:
//
//               break;
           case STREAM_BLACKOUT:
       
               break;
           case IO_HTTP_ERROR_NOT_FOUND:
 
               break;
           case IO_HTTP_ERROR:
         
               break;
           case PLAYER_VERIFICATION_FAILED:
     
               break;
           case NOT_SUPPORTED:
   
               break;
           case CONTENT_DOWNLOAD_PLAY_AUDIO_TRACK_NAME_MISMATCH:
           case CONTENT_DOWNLOAD_PLAY_BITRATE_MISMATCH:
            
               break;
           case GENERAL_DRM_ERROR:
           default:
               if (DEBUG_DRM) Log.d(TAG, "DRM error: " + drmError);
             
               break;
       }	   
   }
   
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
   public static void printDRMContent(DRMContent drmContent)
   {
       if (DEBUG_DRM) Log.d(TAG, "DRMContent: " + drmContent + " \n" + drmContent.toString());
       List<DRMContent.VideoQualityLevel> levels = drmContent.getVideoQualityLevels();
	   int i=0;
       for (DRMContent.VideoQualityLevel videoQualityLevel : levels)
       {
       	 if (DEBUG_DRM) Log.d(TAG, (i++) + ". " + " Profile=" + videoQualityLevel.mVideoProfile.name() + " bitrate=" + videoQualityLevel.mBitRate + " bps (W=" + videoQualityLevel.mWidth
       		        + "H=" + videoQualityLevel.mHeight + ")");
       } 
//     	 if (DEBUG_DRM) Log.d(TAG, "MAX bandwitdh: " + drmContent.getMaxBandwidth());
       
   }
	
   /**
    * Method responsible for initiating delegated license acquisition within the sample application.  The actual
    * acquisition is spun out to a separate thread doing the HTTP Post as well as into one waiting & controlling
    * the UI
    */
   protected void initiateRightsIssuerLicenseAcquisition()
   {
       // Reset the flag whether or not the acquisition has been cancelled
       // This is set to true if the user cancels
       mLicenseAcquisitionResult = LicenseAcquisitionResult.NONE;

       // Use the latch to synchronize between the two threads
       final CountDownLatch countDownLatch = new CountDownLatch(1);

       //setupDialogsRetrievingLicense();

       // This thread is responsible for doing the SOAP call out to the PlayReady License Server.  It will post the
       // SOAP challenge together with the correct headers etc to the URL provided
       final Thread licenseAcquisitionThread = new Thread()
       {
           @Override
           public void run()
           {
               boolean usePlayReadyDRMLicenseAcquisitionHandler = true;

               final DRMScheme drmScheme = mDRMContent.getDRMScheme();

               if (mDRMContent.getDRMContentFormat() == DRMContentFormat.WINDOWS_MEDIA)
               {
                   switch (drmScheme)
                   {
                       case PLAYREADY:
                           if (DEBUG_DRM) Log.d(TAG, "Windows Media Content format detected with DRM scheme of PlayReady, will use cocktail license");
                           usePlayReadyDRMLicenseAcquisitionHandler = true;
                           break;
                       case WMDRM:
                       default:
                           usePlayReadyDRMLicenseAcquisitionHandler = false;
                   }
               }

               if (usePlayReadyDRMLicenseAcquisitionHandler)
               {
                   mDRMLicenseAcquisitionHandler = new PlayReadyDRMLicenseAcquisitionHandler()
                   {
                       private String _profile = null;

                       @Override
                       protected void processResponse(final URL url, final HttpResponse response)
                       {
                    	   Log.v(TAG, "Installing license � " + url);
                           super.processResponse(url, response);
                       }

                       @Override
                       protected void processBodyResponse(final String bodyResponse)
                       {
                           Log.v(TAG, "processBodyResponse called with bodyResponse=" + bodyResponse);
                       }

                       //override to handle join domain in this activity
                       @Override
                       protected boolean joinDomainRequired(final byte[] acquireLicenseResponse, final String customData)
                       {
                           if (DEBUG_DRM) Log.d(TAG, "Hit joinDomainRequired; joinDomainSilently=" + getJoinDomainSilently());

                           if (getJoinDomainSilently())
                           {
                               return super.joinDomainRequired(acquireLicenseResponse, customData);
                           }
                           else
                           {
                               setLicenseAcquisitionResponse(acquireLicenseResponse);

                               setLicenseAcquisitionResult(LicenseAcquisitionResult.JOIN_DOMAIN_REQUIRED);

                               countDownLatch.countDown();

                               //return false to handle license acquisition in this activity
                               return false;
                           }
                       }

                       @Override
                       protected void processError(final PlayReadySoapError error)
                       {
                           if (error != null)
                           {
                               String faultCode = error.getFaultCode();
                               String faultString = error.getFaultString();
                               String faultActor = error.getFaultActor();
                               String statusCode = error.getStatusCode();
                               String serviceId = error.getServiceId();
                               String customData = error.getCustomData();
                               String redirectUrl = error.getRedirectUrl();

                               if (DEBUG_DRM) Log.d(TAG, "Received PlayReady Soap Error: ");
                               if (DEBUG_DRM) Log.d(TAG, "    FaultCode: " + faultCode);
                               if (DEBUG_DRM) Log.d(TAG, "    FaultString: " + faultString);
                               if (DEBUG_DRM) Log.d(TAG, "    FaultActor: " + faultActor);
                               if (DEBUG_DRM) Log.d(TAG, "    StatusCode: " + statusCode);
                               if (DEBUG_DRM) Log.d(TAG, "    ServiceId: " + serviceId);
                               if (DEBUG_DRM) Log.d(TAG, "    CustomData: " + customData);
                               if (DEBUG_DRM) Log.d(TAG, "    RedirectUrl: " + redirectUrl);
                           }
                           return;
                       }

                       @Override
                       protected boolean isCancelled()
                       {
                           return mLicenseAcquisitionResult == LicenseAcquisitionResult.CANCELLED;
                       }

                       @Override
                       protected void cancelled()
                       {
                           countDownLatch.countDown();
                       }

                       @Override
                       protected void licenseAcknowledged()
                       {
                    	   if (DEBUG_DRM) Log.d(TAG, "License acknowledged !");
                    	   setLicenseAcquisitionResult(LicenseAcquisitionResult.SUCCESS);
                    	   countDownLatch.countDown();
                       }

                       @Override
                       public void licenseInstalled()
                       {
                    	   if (DEBUG_DRM) Log.d(TAG, "License installed !");
                    	   
                           if (mLicenseAcquisitionResult != LicenseAcquisitionResult.LICENSE_ACKNOWLEDGEMENT_REQUESTED)
                           {
                               setLicenseAcquisitionResult(LicenseAcquisitionResult.SUCCESS);
                               countDownLatch.countDown();
                           }
                       }

                       @Override
                       protected void licenseAcknowledgementRequested(final URL laURL, final String ackMessage)
                       {
                    	   if (DEBUG_DRM) Log.d(TAG, "License acknowledgement requested !");
                    	   
                    	   setLicenseAcquisitionResult(LicenseAcquisitionResult.LICENSE_ACKNOWLEDGEMENT_REQUESTED);
                    	   
                           super.licenseAcknowledgementRequested(laURL, ackMessage);
                       }


                       @Override
                       public void error(final String errorMessage, final Exception exception)
                       {
                    	   if (DEBUG_DRM) Log.d(TAG, "Error:  " + errorMessage + "\n" + exception);
                    	   
                           setLicenseAcquisitionResult(LicenseAcquisitionResult.ERROR);
                           countDownLatch.countDown();
                       }

                       @Override
                       protected void setupRequest(final URL url, final HttpRequestBase httpRequestBase)
                       {
                           if (_profile != null)
                           {
                               if (DEBUG_DRM) Log.d(TAG, "Profile setup, adding to request: " + _profile);

                               httpRequestBase.addHeader("xmlProfile", _profile);
                           }

                           super.setupRequest(url, httpRequestBase);
                       }

                       @Override
                       public void acquireLicense(final DRMContent drmContent, final URL laURL, final String challenge)
                       {
                           if (DEBUG_DRM) Log.d(TAG, "Acquiring license from: " + laURL);

                           if (laURL.toString().contains("Guido"))
                           {
                               if (DEBUG_DRM) Log.d(TAG, "Detected the GUIDO license server issuer - will add required headers");

                               _profile += "</play></profile>";

                               super.acquireLicense(drmContent, laURL, challenge);
                           }
                           else
                           {
                               // If not a special 'Guido' URL, simply use the acquisition helper to (possibly)
                               // rewrite the LA url (based on configuration settings and such) to the URL to
                               // use
                               final URL newLicenseAcquisitionURL = MvpLicenseAcquisitionHelper.performURLRewrite(context, laURL, mDRMContentInfo, token, accountId, mediaPid);
                               super.acquireLicense(drmContent, newLicenseAcquisitionURL, challenge);
                           }
                       }
                   };
                   ((PlayReadyDRMLicenseAcquisitionHandler) mDRMLicenseAcquisitionHandler).setAcknowledgeLicenseAsynchronously(useAsynchLicenseAcknowledgement());
               }
               else
               {
                   mDRMLicenseAcquisitionHandler = new WMDRMLicenseAcquisitionHandler()
                   {

                       @Override
                       protected boolean isCancelled()
                       {
                           return mLicenseAcquisitionResult != LicenseAcquisitionResult.NONE;
                       }

                       @Override
                       protected void cancelled()
                       {
                           countDownLatch.countDown();
                       }

                       @Override
                       public void licenseInstalled()
                       {
                           setLicenseAcquisitionResult(LicenseAcquisitionResult.SUCCESS);
                           countDownLatch.countDown();
                       }

                       @Override
                       public void error(final String errorMessage, final Exception exception)
                       {
                           if (DEBUG_DRM) Log.d(TAG, "Error received: " + errorMessage);
                           setLicenseAcquisitionResult(LicenseAcquisitionResult.ERROR);
                           countDownLatch.countDown();
                       }
                   };
               }

               DRMAgentDelegate.activateDRMContent(context, mDRMContent, mCustomData, mDRMLicenseAcquisitionHandler);
           }
       };

       // start aqusition
       licenseAcquisitionThread.start();
       
       // This is the waiting thread which is responsible for displaying the correct dialog for the progress of
       // the license acquisition.  It will, after successful installation or a time-out, display the result.
       boolean activated = false;
		try 
		{
			activated = countDownLatch.await(LICENSE_ACQUISITION_MAX_WAIT, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

       if (DEBUG_DRM) Log.d(TAG, "Wait completed: " + mLicenseAcquisitionResult);
       if (!activated)
       {
           if (DEBUG_DRM) Log.d(TAG, "Not activated, setting timeout as result");
           setLicenseAcquisitionResult(LicenseAcquisitionResult.TIMEOUT);
       }

       handleAcquisitionWaitCompleted(mLicenseAcquisitionResult);
   }
   
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
   protected boolean getJoinDomainSilently()
   {
       return false;
   }
   
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
   private void setLicenseAcquisitionResponse(final byte[] acquireLicenseResponse)
   {
       mAcquireLicenseResponse = acquireLicenseResponse; // usefull for joinDomains
   }
   
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
   private void setLicenseAcquisitionResult(final LicenseAcquisitionResult licenseAcquisitionResult)
   {
       mLicenseAcquisitionResultLock.lock();
       try
       {
           if (mLicenseAcquisitionResult == null || mLicenseAcquisitionResult == LicenseAcquisitionResult.NONE || mLicenseAcquisitionResult == LicenseAcquisitionResult.LICENSE_ACKNOWLEDGEMENT_REQUESTED)
           {
               mLicenseAcquisitionResult = licenseAcquisitionResult;
           }
       }
       finally
       {
           mLicenseAcquisitionResultLock.unlock();
       }
   }
   
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
   protected boolean useAsynchLicenseAcknowledgement()
   {
       final SharedPreferences insideSecureSharedPref = context.getSharedPreferences(Constants.PREFERENCES_FILENAME, Context.MODE_PRIVATE);
       return insideSecureSharedPref.getBoolean(Constants.PREFERENCES_ACKNOWLEDGE_LICENSES_ASYNCHRONOUSLY, false);
   }

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
   protected void handleAcquisitionWaitCompleted(final LicenseAcquisitionResult licenseAcquisitionResult)
   {
   	if (licenseAcquisitionResult == LicenseAcquisitionResult.SUCCESS)
   	{
   		boolean embedLicense = false;
           if(mDRMContentInfo.getDRMContent(context).getDRMRights().getDRMRightsType()==DRMRights.DRMRightsType.VALID
                   && embedLicense)
           {
               if (DEBUG_DRM) Log.d(TAG, "We have a license and valid rights: try to embed license");
               mDRMContentInfo.getDRMContent(context).embedLicenses();
           }
           startPlayer();
   	} 
   	else 
   	{
   		Log.e(TAG, "Error while retrieving license, result: " + licenseAcquisitionResult);
    }
   }
   
   /**
    * Method responsible for initiating delegated join domain within the sample application.  The actual
    * join is spun out to a separate thread doing the HTTP Post as well as into one waiting & controlling
    * the UI
    */
   private void initiateJoinDomain()
   {
       // Reset the flag whether or not the acquisition has been cancelled
       // This is set to true if the user cancels
       mLicenseAcquisitionResult = LicenseAcquisitionResult.NONE;

       // Use the latch to synchronize between the two threads
       final CountDownLatch countDownLatch = new CountDownLatch(1);

       //removeDialog(LicenseAcquisitionActivity.JOIN_DOMAIN_REQUIRED);
      //showDialog(LicenseAcquisitionActivity.RETRIEVING_JOIN_DOMAIN);

       // This thread is responsible for doing the SOAP call out to the PlayReady Domain Controller Server.  It will post the
       // SOAP challenge together with the correct headers etc to the URL provided
       final Thread joinDomainThread = new Thread()
       {
           @Override
           public void run()
           {
               mPlayReadyDRMJoinDomainHandler = new PlayReadyDRMJoinDomainHandler()
               {

                   @Override
                   public void joinDomainInstalled()
                   {
                       setLicenseAcquisitionResult(LicenseAcquisitionResult.JOIN_DOMAIN_SUCCESS);
                       countDownLatch.countDown();
                   }

                   @Override
                   public void error(final String errorMessage, final Exception exception)
                   {
                       super.error(errorMessage, exception);
                       setLicenseAcquisitionResult(LicenseAcquisitionResult.ERROR);
                       countDownLatch.countDown();
                   }

                   @Override
                   public void joinDomain(URL domainControllerURL, String joinDomainChallenge)
                   {
                       super.joinDomain(domainControllerURL, joinDomainChallenge);
                   }

               };
               DRMAgentDelegate.joinDomain(context, mAcquireLicenseResponse, mCustomData, mPlayReadyDRMJoinDomainHandler);
           }
       };

       // Finally, we start the threads
       joinDomainThread.start();

       
       // This waits for the activation callback to occur - for up to 20 seconds
       boolean activated = false;
		try 
		{
			activated = countDownLatch.await(LICENSE_ACQUISITION_MAX_WAIT, TimeUnit.SECONDS);
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}

       if (!activated)
       {
           setLicenseAcquisitionResult(LicenseAcquisitionResult.TIMEOUT);
       }

       if (DEBUG_DRM) Log.d(TAG, "Wait completed: " + mLicenseAcquisitionResult);

       handleAcquisitionWaitCompleted(mLicenseAcquisitionResult);


   }
   
   
  
   
}
