/**
 * $URL$
 * DAAND
 * Copyright (C) 2004 - 2013, INSIDE Secure BV.  All rights reserved.
 */
package com.crmsoftware.duview.player.drm;

import android.content.Context;
import android.content.SharedPreferences;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.utils.Log;
import com.insidesecure.drmagent.v2.*;
import com.insidesecure.drmagent.v2.utils.HTTPConnectionHelperImpl;
import com.insidesecure.drmagent.v2.utils.PlayReadyDRMJoinDomainHandler;
import com.insidesecure.drmagent.v2.utils.PlayReadyDRMLicenseAcquisitionHandler;

import org.apache.http.client.methods.HttpRequestBase;

import java.io.*;
import java.net.URI;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DRMAgentDelegate
{
    private static final String TAG = "DRMAgentDelegate";

    public static DRMAgent DRMAGENT;
    public static CertificateProvider sCertificateProvider;
    private static Lock sAgentLock = new ReentrantLock();

    private static int DRMAGENT_REFERENCE_COUNTER = 0;
    private static String sSelectedMediaPlayer;

    public static boolean performRevocationListUpdate(final Context context)
    {
        Tools.DEFENSE("DRMAGENT", DRMAGENT);
        try
        {
            DRMAGENT.updateRevocationData(DRMScheme.PLAYREADY);
            return true;
        }
        catch (Exception e)
        {
            Log.e(TAG,"Error while retrieving revocation data update: " + e.getMessage(), e);
            return false;
        }
    }

    public static void deleteDRMDatabases(final Context context)
    {
        sAgentLock.lock();
        try
        {
            int previousReferenceCount = DRMAGENT_REFERENCE_COUNTER;

            // Force a release
            DRMAGENT_REFERENCE_COUNTER = 1;

            uninitialize();

            Log.d(TAG, "Deleting DRM databases");


            File f = new File(context.getFilesDir().getParent() + File.separator + "playready.hds");
            if (!f.delete())
            {
                Log.w(TAG, "Could not delete playready.hds");
            }

            f = new File(context.getFilesDir().getParent() + File.separator + "keyfile.dat");
            if (!f.delete())
            {
                Log.w(TAG, "Could not delete keyfile.dat");

            }

            if (previousReferenceCount > 0)
            {
                initialize(context);
            }

            DRMAGENT_REFERENCE_COUNTER = previousReferenceCount;

        }
        finally
        {
            sAgentLock.unlock();
        }
    }

    public static void reconfigure(Context context)
    {
        try
        {
            if (!sAgentLock.tryLock(5000, TimeUnit.MILLISECONDS))
            {
                Log.e(TAG,"Timeout received while waiting for lock");
                throw new RuntimeException("Timeout received while waiting for lock");
            }
            try
            {
                if (DRMAGENT != null)
                {
                    Log.d(TAG, "Agent initialized, will reconfigure");
                    setupDRMAgentConfiguration(DRMAGENT, context);
                }
            }
            finally
            {
                sAgentLock.unlock();
            }
        }
        catch (Exception e)
        {
            Log.e(TAG,"Error while reconfiguring: " + e.getMessage(), e);
        }
    }

    public static void setSelectedMediaPlayer(final String selectedMediaPlayer)
    {
        sSelectedMediaPlayer = selectedMediaPlayer;
    }

    public interface CertificateProvider
    {
        public int getWMPrivateKey();

        public int getWMCertificate();

        public int getPRPrivateKey();

        public int getPRCertificate();
    }

    public static DRMAgent initialize(final Context context)
    {
        Tools.DEFENSE("context", context);
        try
        {
            if (!sAgentLock.tryLock(5000, TimeUnit.MILLISECONDS))
            {
                Log.e(TAG,"Timeout received while waiting for lock");
                throw new RuntimeException("Timeout received while waiting for lock");
            }

            try
            {
                Log.d(TAG, "Initializing with " + DRMAGENT_REFERENCE_COUNTER + " DRM agent instances available");

                if (DRMAGENT != null)
                {
                    Log.d(TAG, "Already initialized, will not do that again");

                    setupDRMAgentConfiguration(DRMAGENT, context);

                    DRMAGENT_REFERENCE_COUNTER++;

                    return DRMAGENT;
                }

                if (DRMAgent.DRMAgentFactory.isInitialized())
                {
                    Log.w(TAG, "Agent instance already initialized, but we have no reference to it");
                }

                // Load the certificates now
                try
                {
                    Log.i(TAG, "Importing certificates and keys: " + sCertificateProvider);

                    // These are always imported since they are still required
                    DRMAgent.DRMAgentFactory.installPKI(context, PKIType.WMDRMPD_PRIVATE_KEY, resolveResource(context, PKIType.WMDRMPD_PRIVATE_KEY));
                    DRMAgent.DRMAgentFactory.installPKI(context, PKIType.WMDRMPD_TEMPLATE_CERTIFICATE, resolveResource(context, PKIType.WMDRMPD_TEMPLATE_CERTIFICATE));

                    Log.i(TAG, "Importing offline credentials");
                    DRMAgent.DRMAgentFactory.installPKI(context, PKIType.PLAYREADY_MODEL_PRIVATE_KEY, resolveResource(context, PKIType.PLAYREADY_MODEL_PRIVATE_KEY));
                    DRMAgent.DRMAgentFactory.installPKI(context, PKIType.PLAYREADY_MODEL_CERTIFICATE, resolveResource(context, PKIType.PLAYREADY_MODEL_CERTIFICATE));

                    Log.i(TAG, "Certificates and keys imported");
                }
                catch (Exception e)
                {
                    Log.w(TAG, "Could not import certificates and keys: " + e.getMessage(), e);
                }


                Log.d(TAG, "Initializing Agent instance");

                // Setup and instantiate the DRM agent using the factory provided

                final SharedPreferences insideSecureSharedPref = context.getSharedPreferences(Constants.PREFERENCES_FILENAME, Context.MODE_PRIVATE);
                int logLevel = insideSecureSharedPref.getInt(Constants.PREFERENCES_AGENT_LOG_LEVEL, 3);

                // Ensure the value is within bounds
                if (logLevel < 0)
                {
                    logLevel = 0;
                }
                else if (logLevel >= DRMLogLevel.values().length)
                {
                    logLevel = DRMLogLevel.values().length-1;
                }

                Log.d(TAG, "Initializing DRM Agent with log level: " + logLevel + " : (" + DRMLogLevel.values()[logLevel] + ")");

                // Initialize using the Inside Secure Runtime Activation Data
                byte[] runtimeActivationData = resolveRuntimeApplicationData();

                Log.d(TAG,"Using " + runtimeActivationData.length + " byte(s) of runtime activation data");

                DRMAGENT = DRMAgent.DRMAgentFactory.getInstance(context, DRMLogLevel.values()[logLevel], runtimeActivationData );

                try
                {
                    Log.d(TAG, "Device State: " + (DRMAgent.DRMAgentFactory.isSecureDevice(context) ? "Secure" : "Insecure"));
                }

                catch (Exception exception)
                {
                    Log.e(TAG,"Error while getting device state: " + exception.getMessage(), exception);
                }

                setupDRMAgentConfiguration(DRMAGENT, context);

                // Add the DRM callback listener
                DRMAGENT.addDRMCallbackListener(new AbstractDRMCallbackListener()
                {
                    public void contentActivated(final URI uri)
                    {
                        Log.d(TAG, "Content Activated: " + uri);
                    }

                    public void errorReceived(final DRMError drmError, final URI uri)
                    {
                        String errorMessage = "Error received: " + drmError;

                        if(uri!=null)
                        {
                            errorMessage+= "; uri: " + uri.toString();
                        }
                        Log.d(TAG, errorMessage);
                    }

                    public String mediaDescriptorReceived(String descriptor, final URI uri)
                    {
                        Log.d(TAG, "Media descriptor received: " + uri);
                        return descriptor;
                    }

                    public void silentEntitlementAcquisitionInitiated(final AcquireLicenseRequest acquireLicenseRequest)
                    {
                        Log.d(TAG, "Silent acquisition initiated");
                    }
                });

                DRMAGENT_REFERENCE_COUNTER++;

                return DRMAGENT;
            }
            finally
            {
                sAgentLock.unlock();
            }

        }
        catch (Exception e)
        {
            Log.e(TAG,"Error while initializing: " + e.getMessage(), e);
            throw new RuntimeException("Error while initializing: " + e.getMessage(), e);
        }
    }

    private static InputStream resolveResource(Context context, final PKIType pkiType) throws FileNotFoundException
    {
        final String path;
        final InputStream rawResourceInputStream;
        switch (pkiType)
        {
            case WMDRMPD_PRIVATE_KEY:
                path = "/sdcard/insidesecure/wm_privatekey";
                rawResourceInputStream = context.getResources().openRawResource(sCertificateProvider.getWMPrivateKey());
                break;
            case WMDRMPD_TEMPLATE_CERTIFICATE:
                path = "/sdcard/insidesecure/wm_certificate";
                rawResourceInputStream = context.getResources().openRawResource(sCertificateProvider.getWMCertificate());
                break;
            case PLAYREADY_MODEL_PRIVATE_KEY:
                path = "/sdcard/insidesecure/pr_privatekey";
                rawResourceInputStream = context.getResources().openRawResource(sCertificateProvider.getPRPrivateKey());
                break;
            case PLAYREADY_MODEL_CERTIFICATE:
                path = "/sdcard/insidesecure/pr_certificate";
                rawResourceInputStream = context.getResources().openRawResource(sCertificateProvider.getPRCertificate());
                break;
            default:
                throw new IllegalArgumentException("Unhandled PKI type: " + pkiType);
        }

        InputStream inputStream = openFileInputStream(path);
        if (inputStream != null)
        {
            Log.d(TAG,"Loaded resource of type " + pkiType + " from filesystem path " + path);
            return inputStream;
        }
        Log.d(TAG,"Resource of type " + pkiType + " loaded from raw resources");
        return rawResourceInputStream;
    }

    private static InputStream openFileInputStream(final String path) throws FileNotFoundException
    {
        File file = new File(path);
        if (file.exists() && file.canRead())
        {
            return new FileInputStream(file);
        }
        return null;
    }

    private static void setupDRMAgentConfiguration(final DRMAgent drmAgentInstance, final Context context)
    {
        // Always update any of the preferences that are used dynamically
        final DRMAgentConfiguration drmAgentConfiguration = drmAgentInstance.getDRMAgentConfiguration();

        if (sSelectedMediaPlayer != null)
        {
            // Setup the selected media player
            drmAgentConfiguration.getDeviceProperties().put("general.selected-media-player", sSelectedMediaPlayer);
        }

        // Setup the agent to use a cache outside of the application folder
        drmAgentConfiguration.setContentCachePath("/sdcard/insidesecure/content-cache");

        final SharedPreferences insideSecureSharedPref = context.getSharedPreferences(Constants.PREFERENCES_FILENAME, Context.MODE_PRIVATE);


        drmAgentConfiguration.setOfflineMode(insideSecureSharedPref.getBoolean(Constants.PREFERENCES_OFFLINE_MODE, false));
        drmAgentConfiguration.setNativePlayerBufferSize(insideSecureSharedPref.getInt(Constants.PREFERENCES_NATIVE_PLAYER_BUFFER_SIZE, 10000));
        drmAgentConfiguration.setUseHeadlessHLS(insideSecureSharedPref.getBoolean(Constants.PREFERENCES_USE_HEADLESS_HLS, false));
        drmAgentConfiguration.setHttpsTrustAllSSLCertificates(insideSecureSharedPref.getBoolean(Constants.PREFERENCES_HTTPS_CLIENT_TRUST_ALL_CERTS, false));

        try
        {
            drmAgentConfiguration.setQosEnabled(insideSecureSharedPref.getBoolean(Constants.PREFERENCES_ENABLE_QOS,false));
        }

        catch (Exception exception)
        {
            Log.e(TAG,"Error while enabling Quality of service: " + exception.getMessage(), exception);
        }

        // This adds a global license acquisition handler - in the case where no acquisition handler is added to
        // the acquisition request, this instance will be used
        final PlayReadyDRMLicenseAcquisitionHandler drmLicenseAcquisitionHandler = new PlayReadyDRMLicenseAcquisitionHandler()
        {
            @Override
            public void licenseInstalled()
            {
                Log.d(TAG, "License installed using the global DRM license acquisition handler");
                super.licenseInstalled();
            }
        };
        drmAgentConfiguration.setDRMLicenseAcquisitionHandler(drmLicenseAcquisitionHandler);

        drmAgentConfiguration.setHttpConnectionHelper(new HTTPConnectionHelperImpl()
        {
            @Override
            public void setupRequest(final HttpRequestBase httpRequestBase, final URL url)
            {
                // Allow the super class to do what it needs to do to the request (if anything)
                super.setupRequest(httpRequestBase, url);
                httpRequestBase.addHeader("INSIDESecure", "1.0");
            }
        });

        // Update the configuration to be used
        drmAgentConfiguration.setUserAgent(Constants.DRM_AGENT_USER_STRING);

        //uncomment the following and set the friendlyName to be used during joinDomain
        //drmAgentConfiguration.setJoinDomainFriendlyName("set_here_the_join_domain_friendly_name");

        // Load any additional properties from file (if available)
        loadDRMAgentConfigurationProperties(drmAgentConfiguration);

        drmAgentInstance.setDRMAgentConfiguration(drmAgentConfiguration);
    }

    private static byte[] resolveRuntimeApplicationData()
    {
        File f = new File(Constants.CDESC_PATH + Constants.CONFIGURATION_RUNTIME_APPLICATION_DATA_FILE);

        if (f.exists())
        {
            Log.d(TAG, "Found available runtime activation data in " + Constants.CDESC_PATH + ", will use that");

            try
            {
                FileInputStream fis = new FileInputStream(f);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                Tools.copyStream(fis, outputStream);

                fis.close();
                outputStream.flush();
                outputStream.close();

                return outputStream.toByteArray();
            }
            catch (Exception e)
            {
                Log.e(TAG, "Error while loading rad: " + e.getMessage(), e);
                throw new RuntimeException("Error while loading rad: " + e.getMessage(), e);
            }
        }
        else
        {
            return Tools.toByte("494e534901000000dbbaf08fe146b059255d4f678b59abaa010000000a000000d0010000c0a6f1c9a0130eb1fd1c57e73366fd6bb947991faff733cfee3a2f3c2b0197a2000bcbfd4e37ec05acf6296ba1d3e7247e025239d98ca05de76e336997f4d91fceeb3148211672e42bbefd96cb8ea6fcb19d54e13df433716f309f090eecbd28e92fa1d321581a16c335dbfc63318a9ee79e5735be63086c21d774471561e9aef58dadc99becbdbbdd0b6aa5fb71f21f47b3bb18cb30f81a3a3961c250717dfd08af6f42925ee520e2db8042be7162ed5499738425137dd3198b33e37e5e5425b0472d3815a738bf42bd8a4792b7cc1f42a406fd0da2f0bffbee5c641f663ff63093d2ec3585075c6612038c67254f802f2b04363048a0e96428d57daa05c399fb32c1ddf7516df165d54f50e873e1e1ece32eccaf31bf59b95be2ae27a5c5d1a20b7c7a3850cf7c0ee6c2622ac315a9c30d798540ad10dc30feca9d86b3a77bf1d1eecf7e75073e9ab8c632e84730649b8e57077f6eb6a1685c54af096056f8e048d07320d95f0e8efde2616b2d67ec157b2d110f137897275e64975f96b34a9282675de9154f561fb52704fdf846c4c0205d906acb3d91e7d82a7951bab71fb584648a9dabc5118d4d3942e828d83dafaa40af7ec8c16d848aae600ff268830249b78fc56d5a943d7865e352479f4e6ebd4da538a4314d28705f68b9d6b125251a2b5b");
        }
    }


    private static void loadDRMAgentConfigurationProperties(final DRMAgentConfiguration drmAgentConfiguration)
    {
        File f = new File(Constants.CDESC_PATH + Constants.CONFIGURATION_PROPERTIES_FILE);

        if (!f.exists())
        {
            Log.d(TAG,"No drmagent-configuration.properties found, will bail");
            return;
        }

        try
        {
            Properties properties = new Properties();
            properties.load(new BufferedInputStream(new FileInputStream(f)));

            Set<Object> keys = properties.keySet();

            Log.v(TAG,"Loaded configuration properties: " + keys.size() + " properties");

            for (Object key : keys)
            {
                String strKey = (String) key;
                final int firstDotIndex = strKey.indexOf(".");
                String type = strKey.substring(0, firstDotIndex);
                String configKey = strKey.substring(firstDotIndex+1);
                String value = properties.getProperty(strKey);

                Log.v(TAG,"   " + configKey + "/" + value + "/" + type);

                if ("float".equals(type))
                {
                    Float fValue = Float.parseFloat(value);
                    drmAgentConfiguration.getDeviceProperties().put(configKey,fValue);

                }
                else if ("long".equals(type))
                {
                    Long lValue = Long.parseLong(value);
                    drmAgentConfiguration.getDeviceProperties().put(configKey,lValue);
                }
                else if ("int".equals(type))
                {
                    Integer iValue = Integer.parseInt(value);
                    drmAgentConfiguration.getDeviceProperties().put(configKey,iValue);
                }
                else if ("boolean".equals(type))
                {
                    Boolean bValue = Boolean.parseBoolean(value);
                    drmAgentConfiguration.getDeviceProperties().put(configKey,bValue);
                }
                else if ("string".equals(type))
                {
                    drmAgentConfiguration.getDeviceProperties().put(configKey,value);
                }
                else
                {
                    Log.e(TAG,"Unhandled type: " + type);
                }

            }


        }
        catch (Exception e)
        {
            Log.e(TAG,"Error while loading properties: " + e.getMessage(), e);
            throw new RuntimeException("Error while loading properties: " + e.getMessage(),e);
        }

    }

    public static String getDRMInformation(Context context)
    {
        Tools.DEFENSE("DRMAGENT", DRMAGENT);

        final DRMAgentVersionInfo drmVersion = getDRMAgentVersionInfo();

        if (drmVersion != null)
        {
            String isSecureDevice = context.getString(R.string.drm_security_state_unknown);
            try
            {
                isSecureDevice = DRMAgent.DRMAgentFactory.isSecureDevice(context) ? context.getString(R.string.drm_security_state_secure) : context.getString(R.string.drm_security_state_compromised);
            }

            catch (Exception exception)
            {
                Log.e(TAG,"Error while getting device state: " + exception.getMessage(), exception);
            }

            return String.format("%s\t%s\n%s\t%s\n%s\t%s\n%s\t%s",
                    context.getString(R.string.drm_api_version_text), drmVersion.getAPIVersion(),
                    context.getString(R.string.drm_api_build_timestamp_text), drmVersion.getAPIBuildDate(),
                    context.getString(R.string.drm_playready_device_id), getDRMAgentInstance(context).getPlayReadyDeviceID().toString(),
                    context.getString(R.string.drm_security_state), isSecureDevice
            ) + DRMFusionAgentReferenceApplication.sMiscVersionInfo;
        }
        return null;
    }

    public static DRMAgentVersionInfo getDRMAgentVersionInfo()
    {
        return DRMAgent.DRMAgentFactory.getDRMVersion();
    }

    public static void purgeDatabase(final Context context)
    {
        DRMAgent.DRMAgentFactory.purgeDatabase(context);
    }

    public static void purgeCachedFiles(final Context context)
    {
        Tools.DEFENSE("DRMAGENT", DRMAGENT);
        DRMAGENT.purgeDatabase(DRMPurgeOption.CACHED_MEDIA);
        ContentHandler.clearCache();
    }

    public static void purgePooledObjects(final Context context)
    {
        Tools.DEFENSE("DRMAGENT", DRMAGENT);
        DRMAGENT.purgeDatabase(DRMPurgeOption.POOLED_OBJECT_INSTANCES);
    }


    public static void purgeLicenses(final Context context)
    {
        Tools.DEFENSE("DRMAGENT", DRMAGENT);
        // Time to purge the database
        DRMAGENT.purgeDatabase(DRMPurgeOption.LICENSES);
    }

    public static boolean joinDomain(final Context context, final byte[] acquireLicenseResponse, final String customData, final PlayReadyDRMJoinDomainHandler playReadyDRMJoinDomainHandler)
    {
        Tools.DEFENSE("DRMAGENT", DRMAGENT);
        try
        {
            final JoinDomainRequest joinDomainRequest;
            if (playReadyDRMJoinDomainHandler == null)
            {
                joinDomainRequest = new JoinDomainRequest(acquireLicenseResponse);
            }
            else
            {
                joinDomainRequest = new JoinDomainRequest(acquireLicenseResponse, playReadyDRMJoinDomainHandler);
            }

            if (customData != null)
            {
                Log.d(TAG, "Setting Join Domain Request custom data to: " + customData);
                joinDomainRequest.setCustomData(customData);
            }
            else
            {
                Log.d(TAG, "Custom data not found in file, Join Domain Request custom data will be: " + joinDomainRequest.getCustomData());
            }

            return DRMAGENT.joinDomain(joinDomainRequest);
        }
        catch (DRMAgentException e)
        {
            Log.e(TAG, e.getMessage(), e);
            return false;
        }
    }

    public static boolean activateDRMContent(final Context context, final DRMContent drmContent, String customData, final DRMLicenseAcquisitionHandler drmLicenseAcquisitionHandler)
    {
        Tools.DEFENSE("DRMAGENT", DRMAGENT);
        try
        {
            Tools.DEFENSE("drmContent", drmContent);

            final AcquireLicenseRequest acquireLicenseRequest;
            if (drmLicenseAcquisitionHandler == null)
            {
                acquireLicenseRequest = new AcquireLicenseRequest(drmContent);
                acquireLicenseRequest.setDRMScheme(drmContent.getDRMScheme());
            }
            else
            {
                acquireLicenseRequest = new AcquireLicenseRequest(drmContent, drmLicenseAcquisitionHandler);
                acquireLicenseRequest.setDRMScheme(drmLicenseAcquisitionHandler.getRequiredDRMScheme());
            }

            if (customData != null)
            {
                Log.d(TAG, "Setting License acquisition request custom data to: " + customData);
                acquireLicenseRequest.setCustomData(customData);
            }
            else
            {
                Log.d(TAG, "Custom data not provided, License acquisition request custom data will be: " + acquireLicenseRequest.getCustomData());
            }

            return DRMAGENT.acquireLicense(acquireLicenseRequest);
        }
        catch (DRMAgentException e)
        {
            Log.e(TAG, e.getMessage(), e);
            return false;
        }
    }


    public static DRMContent getDRMContent(final Context context1, final URI uri, DRMContentFormat drmContentFormat, DRMScheme drmScheme)
    {
        Tools.DEFENSE("DRMAGENT", DRMAGENT);
        return DRMAGENT.getDRMContent(uri, drmContentFormat, drmScheme);
    }

    public static DRMAgent getDRMAgentInstance(final Context context)
    {
        Tools.DEFENSE("DRMAGENT", DRMAGENT);
        return DRMAGENT;
    }

    public static void addDRMCallbackListener(final DRMCallbackListener drmCallbackListener)
    {
        Tools.DEFENSE("DRMAGENT", DRMAGENT);
        DRMAGENT.addDRMCallbackListener(drmCallbackListener);
    }

    public static void removeDRMCallbackListener(final DRMCallbackListener drmCallbackListener)
    {
        Tools.DEFENSE("DRMAGENT", DRMAGENT);
        DRMAGENT.removeDRMCallbackListener(drmCallbackListener);
    }

    public static void addHDMIBroadcastReceiver(final Context context)
    {
        Tools.DEFENSE("DRMAGENT", DRMAGENT);
        DRMAGENT.addHDMIBroadcastReceiver(context);
    }

    public static void removeHDMIBroadcastReceiver(final Context context)
    {
        Tools.DEFENSE("DRMAGENT", DRMAGENT);
        DRMAGENT.removeHDMIBroadcastReceiver(context);
    }


    public static void uninitialize()
    {
        try
        {
            if (!sAgentLock.tryLock(5000, TimeUnit.MILLISECONDS))
            {
                Log.e(TAG,"Timeout received while waiting for lock");
                throw new RuntimeException("Timeout received while waiting for lock");
            }
        }
        catch (InterruptedException e)
        {
            Log.e(TAG,"Interrupted while waiting for lock");
            throw new RuntimeException("Interrupted while waiting for lock");
        }

        try
        {
            if (DRMAGENT != null)
            {
                Log.d(TAG,"DRM agent instance references: " + DRMAGENT_REFERENCE_COUNTER);
                if (DRMAGENT_REFERENCE_COUNTER == 1)
                {
                    Log.d(TAG,"Last referenced DRM agent instance reached, will release it");
                    DRMAgent.DRMAgentFactory.releaseInstance();
                    DRMAGENT = null;
                }
                else
                {
                    Log.d(TAG,"Still " + (DRMAGENT_REFERENCE_COUNTER-1) + " references to DRM agent instance out there, will not release");
                }
                DRMAGENT_REFERENCE_COUNTER--;
            }
        }
        finally
        {
            sAgentLock.unlock();
        }
    }

}
