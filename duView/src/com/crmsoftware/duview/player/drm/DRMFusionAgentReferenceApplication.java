/**
 * $URL$
 * DAAND
 * Copyright (C) 2004 - 2013, INSIDE Secure BV.  All rights reserved.
 */
package com.crmsoftware.duview.player.drm;

import android.app.Application;

public class DRMFusionAgentReferenceApplication extends Application
{
    private String TAG = "DRMFusionAgentReferenceApplication";

    public static Class sSettingsClass;
    public static Class sPlayMediaActivityClass;
    public static String sMiscVersionInfo = "";

    public static boolean sUsesNativePlayer = false;
    public static boolean sSupportOldAndroidVersions = false;
    public static boolean sSupportBackgroundDownloadAndPlay = false;
    public DRMAgentDelegate.CertificateProvider sCertificateProvider;

    @Override
    public void onCreate()
    {
        Tools.DEFENSE("sCertificateProvider", sCertificateProvider);

        // Setup the DRM agent delegate with the provider of our certificates
        DRMAgentDelegate.sCertificateProvider = sCertificateProvider;


/*
         StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                 .detectNetwork()
                 .penaltyLog()
                 .penaltyDeath()
                 .build());
         StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                 .detectLeakedClosableObjects().detectLeakedRegistrationObjects()
                 .penaltyDropBox()
                 .penaltyLog()
                 .penaltyDeath()
                 .build());
*/

        super.onCreate();
    }

}
