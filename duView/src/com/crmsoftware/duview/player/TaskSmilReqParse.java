package com.crmsoftware.duview.player;

//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************


import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.core.common.DataType;
import com.crmsoftware.duview.core.common.HttpReqParams;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.core.common.TaskHttpRequest;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.data.MvpError;
import com.crmsoftware.duview.data.MvpError.ErrorType;
import com.crmsoftware.duview.player.smil.model.MpxSmil;
import com.crmsoftware.duview.utils.IHttpConst;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;

public class TaskSmilReqParse extends Task{

	public static boolean DEBUG = Log.DEBUG; 
	public final static String TAG = TaskSmilReqParse.class.getSimpleName();
	
	private String mediaUrl;
	
	private TaskHttpRequest taskServerDataReq = null;
	private MpxSmil smilHandler;
	
	public TaskSmilReqParse(ITaskReturn callback, String mediaUrl) 
	{
		super(callback);
		
		if (DEBUG) Log.m(TAG,this,"ctor()");
		

		this.mediaUrl = mediaUrl;
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onExecute() 
	{
		if (DEBUG) Log.m(TAG,this,"onExecute");

        MvpUserData data = MvpUserData.getFromMvpObject();
        
		HttpReqParams httpParams = new HttpReqParams();
		
		httpParams.setParam(IHttpConst.PARAM_SMIL_AUTH, data.getToken());
		httpParams.setParam(IHttpConst.PARAM_USERNAME_MPX, data.getUserName());
		httpParams.setParam(IHttpConst.PARAM_SMIL_CLIENT_ID, MainApp.getInstance().getDeviceId().toString());
		
		String sURL=" ";
		try 
		{
			sURL = httpParams.asString();
		} 
		catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sURL = mediaUrl + IHttpConst.CHAR_AND + sURL.substring(1);
		
		
		taskServerDataReq = new TaskHttpRequest(
				TaskHttpRequest.HTTP_GET,
				sURL, 
				null,
				DataType.String);
		executeSubTask(taskServerDataReq);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onResetData()
	{
		super.onResetData();
		
		taskServerDataReq = null;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	
	public Object getReturnData()
	{
		return  smilHandler;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void cancel(boolean noParentNotif) 
	{
		// this will disabled the cancel - this task is important to be 100% executed in order to unlock if there is a cancel request
		setState(State.CanceledWithNotif);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onReturnFromTask(Task subTask, boolean canceled) 
	{
		if (DEBUG) Log.m(TAG,this,"onReturnFromTask");
		
		super.onReturnFromTask(subTask, canceled);


		//----------------------------------------		
		if (subTask ==  taskServerDataReq) // return from getting data from server
		{	
		
			if (taskServerDataReq.isError())
			{
				taskCompleted(taskServerDataReq.getError());
				return;
			}
			smilHandler = new MpxSmil();
			try 
			{
				//onXMLResponse(httpRequest);
			XMLReader XMLReader = null;
			SAXParserFactory spf = SAXParserFactory.newInstance();
			spf.setNamespaceAware(true);	// for CDATA
			SAXParser SAXParser = spf.newSAXParser();
			
			XMLReader = SAXParser.getXMLReader();
			XMLReader.setFeature("http://xml.org/sax/features/namespaces", true);

			XMLReader.setContentHandler(smilHandler);
			XMLReader.setErrorHandler(smilHandler);	
			
			String data = taskServerDataReq.getStringData();
			
			if (DEBUG) Log.d(TAG,"data: " + data);

			XMLReader.parse(new InputSource(new StringReader(data)));

			taskCompleted();
			return;
	
		} 
		catch (IOException e) {
			smilHandler = null;
			taskCompleted(new MvpError(ErrorType.Processing));		
			e.printStackTrace();
		} catch (SAXException e) {
			smilHandler = null;
			taskCompleted(new MvpError(ErrorType.Processing));
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			smilHandler = null;
			taskCompleted(new MvpError(ErrorType.Processing));
			e.printStackTrace();
		}
			
		smilHandler = null;

		return;
		}
	}
}