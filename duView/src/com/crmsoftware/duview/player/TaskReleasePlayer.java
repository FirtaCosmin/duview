package com.crmsoftware.duview.player;





import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.player.drm.DRMContentInfo;
import com.crmsoftware.duview.utils.Log;
import com.insidesecure.drmagent.v2.mediaplayer.MediaPlayer;

public class TaskReleasePlayer extends Task {

	public static boolean DEBUG = Log.DEBUG; 
	public static boolean IS_RELEASE = Log.IS_RELEASE; 
	public final static String TAG = TaskRegisterDevice.class.getSimpleName();
	
	private MediaPlayer mediaPlayer;
	private DRMContentInfo mDRMContentInfo;


	public TaskReleasePlayer(com.insidesecure.drmagent.v2.mediaplayer.MediaPlayer mMediaPlayer, DRMContentInfo dRMContentInfo) 
	{
		if (DEBUG) Log.m(TAG,this,"ctor()");
		
		this.mediaPlayer = mMediaPlayer;
		this.mDRMContentInfo = dRMContentInfo;

	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onExecute() 
	{
		if (DEBUG) Log.m(TAG,this,"onExecute");

		if (null != mediaPlayer)
		{	
			try 
			{
				mediaPlayer.stop();
	
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			try 
			{
				mediaPlayer.reset();
	
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			try 
			{
				mediaPlayer.release();
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			mediaPlayer = null;
		}
		
		if (null != mDRMContentInfo)
		{
			try 
			{
				mDRMContentInfo.releaseDRMContent();	
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
			mDRMContentInfo = null;
		}
		
    	super.onExecute();
	}

}
