package com.crmsoftware.duview.player;


import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.configuration.PlatformSettings;
import com.crmsoftware.duview.core.common.HttpReqParams;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.core.common.TaskSingleReqParse;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.data.MvpServerError;
import com.crmsoftware.duview.data.MvpString;
import com.crmsoftware.duview.player.drm.DRMAgentDelegate;
import com.crmsoftware.duview.utils.IHttpConst;
import com.crmsoftware.duview.utils.Log;

public class TaskRegisterDevice extends Task {

	public static boolean DEBUG = Log.DEBUG; 
	public static boolean IS_RELEASE = Log.IS_RELEASE; 
	public final static String TAG = TaskRegisterDevice.class.getSimpleName();


	private TaskSingleReqParse taskMakeRegistration = null;
	public TaskRegisterDevice(ITaskReturn callback) 
	{
		super(callback);
		
		if (DEBUG) Log.m(TAG,this,"ctor()");
		
		
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onExecute() 
	{
		if (DEBUG) Log.m(TAG,this,"onExecute");

		// Application validity check
/*		int id1 = (int)MainApp.getRes().getDimension(R.dimen.app_device_id1);
		int id2 = (int)MainApp.getRes().getDimension(R.dimen.app_device_id2);
		
		if (DRMAgentDelegate.id2 >= id2)
		{
			if (DRMAgentDelegate.id1 >= id1)
			{
				taskCompleted(new MvpServerError(500,MainApp.getRes().getString(R.string.device_registr_error)));
				return;
			}
		}*/
		
		// DRM Play ready check
		DRMAgentDelegate.initialize(MainApp.getContext());

        MvpUserData data = MvpUserData.getFromMvpObject();
		
        String token = "";
        if (null != data)
        {
        	token = data.getToken();
        }
        
		PlatformSettings platfSett = PlatformSettings.getInstance();
		HttpReqParams httpParams = new HttpReqParams(IHttpConst.VALUE_SCHEMA_1_0);
		
		httpParams.setParam(IHttpConst.PARAM_REG_SERIAL_ID, MainApp.getInstance().getDeviceId().toString());
		httpParams.setParam(IHttpConst.PARAM_REG_ACCOUNT_ID,platfSett.getTpMpxAccoutUrl());
		httpParams.setParam(IHttpConst.PARAM_TOKEN, token);
		
		if (IS_RELEASE == false) Log.d(TAG, "\ntoken=" + token);
		
		taskMakeRegistration = new TaskSingleReqParse (
				this,
				MvpString.class,
				PlatformSettings.getInstance().getRegisterDevUrl(),
				httpParams
				);
		
		executeSubTask(taskMakeRegistration);
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onResetData()
	{
		super.onResetData();
		
		taskMakeRegistration = null;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onReturnFromTask(Task subTask, boolean canceled) 
	{
		super.onReturnFromTask(subTask, canceled);
		
		if (subTask == taskMakeRegistration)
		{
			if (taskMakeRegistration.isError())
			{
				taskCompleted(taskMakeRegistration.getError());
			}
			
			taskCompleted();
			
			return;
		}
		
		
	}
}
