package com.crmsoftware.duview.player;


public interface IPlayerView {
	
	public void onVideoSizeChanged(int width, int height);
	public void onVideoCompletion();
	
}
