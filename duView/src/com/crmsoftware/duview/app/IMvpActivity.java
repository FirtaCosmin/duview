package com.crmsoftware.duview.app;

import android.os.Bundle;

import com.crmsoftware.duview.slidemenu.MvpMenu;
import com.crmsoftware.duview.ui.MvpFragment;

public interface  IMvpActivity 
{

	public void setActionBarVisibility(int visibility);
	public void setMenuEnabled(boolean bState);
	public void setTitle(String title);
	public void goTo(final Class fragmentType, final String fragmentTag, final Bundle fragmentData, final boolean addToBackStack);
	public MvpMenu getMenu();
	public MvpFragment getCurrentScreen();
	public void clearStack(); 
	public void showLoadingDialog();
	public void hideLoadingDialog();
	public void setHomeButtonActionBar(boolean isVisible);
	
	public void show(Class classType, String fragmentTag, Bundle fragmentData);
	public void hide(String fragmentTag);
	
	
}
