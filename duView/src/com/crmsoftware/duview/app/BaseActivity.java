package com.crmsoftware.duview.app;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.crmsoftware.duview.R;
import com.crmsoftware.duview.R.dimen;
import com.crmsoftware.duview.R.drawable;
import com.crmsoftware.duview.R.id;
import com.crmsoftware.duview.R.layout;
import com.crmsoftware.duview.R.menu;
import com.crmsoftware.duview.R.string;
import com.crmsoftware.duview.slidemenu.SlideListFragment;
import com.crmsoftware.duview.slidemenu.library.SlidingMenu;
import com.crmsoftware.duview.slidemenu.library.app.SlidingFragmentActivity;
import com.crmsoftware.duview.utils.IConst;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.LogOffDialogView;

public class BaseActivity extends SlidingFragmentActivity {

	public static boolean DEBUG = Log.DEBUG; 
	public static boolean VERBOUSE = Log.VERBOUSE; 
    private static final String TAG = BaseActivity.class.getSimpleName();
    
	public static List<BaseActivity> activities 		= new ArrayList<BaseActivity>();
	protected ListFragment mFrag;
	private boolean activityIsFinishing = true;
	private Bundle activityData = new Bundle();
	private boolean hasDoneSavedInstace = false;
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/*
		 * register the uncaught exception handler to CRMExceptionHandler
		 * This way when an exception is thrown and nothing is registered to catch it then CRMExceptionHandler will catch it and will restart the app
		 * */
		int numberOfCrashes = 0;
		if ( getIntent() != null &&
			 getIntent().getExtras() != null &&
			 getIntent().getExtras().containsKey(CRMExceptionHandler.CRASH_ERROR_NO_INTENT_TITLE) ){
			numberOfCrashes = getIntent().getExtras().getInt(CRMExceptionHandler.CRASH_ERROR_NO_INTENT_TITLE);
			Log.d(TAG,"number of crashes: "+numberOfCrashes);
		}
		Thread.setDefaultUncaughtExceptionHandler(new CRMExceptionHandler(this, numberOfCrashes));
		if (DEBUG) Log.m(TAG,this,"onCreate");
		
		activities.add(this);
		
		if (savedInstanceState != null)
		{
			activityData = new Bundle();
			activityData.putAll(savedInstanceState);
		}

		// set the Behind View
		setBehindContentView(R.layout.menu_frame);
		if (savedInstanceState == null) 
		{
			FragmentTransaction t = this.getSupportFragmentManager().beginTransaction();
			mFrag = new SlideListFragment();
			t.replace(R.id.menu_frame, mFrag);
			t.commit();
		} 

		// customize the SlidingMenu
		SlidingMenu sm = getSlidingMenu();
		sm.setShadowWidthRes(R.dimen.shadow_width);
		sm.setShadowDrawable(R.drawable.shadow);

		//sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		sm.setBehindWidthRes(R.dimen.slidingmenu_width);
		sm.setFadeDegree(0.35f);
		sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
		

	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */
	public Bundle getData() 
	{
		return activityData;
	}
	
	@Override
	protected void onRestart() {
		if (DEBUG) Log.m(TAG,this,"onRestart");
		super.onRestart();
	}
	
	@Override
	protected void onStart() {
		if (DEBUG) Log.m(TAG,this,"onStart");		
		super.onRestart();
	}
	
	@Override
	protected void onResume() {
		if (DEBUG) Log.m(TAG,this,"onResume");
		
		activityIsFinishing = false;
		
		super.onResume();
	}
	
	@Override
	protected void onPause() {
		if (DEBUG) Log.m(TAG,this,"onPause");
		
		activityIsFinishing = true;
		
		super.onPause();
	}
	
	@Override
	public void onPostCreate(Bundle savedInstanceState) {
		if (DEBUG) Log.m(TAG,this,"onPostCreate");
		super.onPostCreate(savedInstanceState);
	}
	
	@Override
	protected void onPostResume() {
		if (DEBUG) Log.m(TAG,this,"onPostResume");
		super.onPostResume();
	}
	
	@Override
	protected void onResumeFragments() {
		if (DEBUG) Log.m(TAG,this,"onResumeFragments");
		super.onResumeFragments();
	}
	
	@Override
	protected void onDestroy() {
		if (DEBUG) Log.m(TAG,this,"onDestroy");
		super.onDestroy();
	}
	
	@Override
	public void onAttachedToWindow() {
		if (DEBUG) Log.m(TAG,this,"onAttachedToWindow");
		super.onAttachedToWindow();
	}
	
	@Override
	public void onDetachedFromWindow() {
		if (DEBUG) Log.m(TAG,this,"onDetachedFromWindow");
		super.onDetachedFromWindow();
	}
	
	@Override
	public void onAttachFragment(Fragment fragment) {
		if (DEBUG) Log.m(TAG,this,"onAttachFragment");
		super.onAttachFragment(fragment);
	}
	
	@Override
	public View onCreateView(String name, Context context, AttributeSet attrs) {
		//if (VERBOUSE) Log.m(TAG,this,"onCreateView");
		return super.onCreateView(name, context, attrs);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if (DEBUG) Log.m(TAG,this,"onSaveInstanceState");
		
		hasDoneSavedInstace = true;
		
		if (activityData != null)
		{
			outState.putAll(activityData);
		}
		
		super.onSaveInstanceState(outState);
	}
	
	protected boolean isActivityOn()
	{
		if (DEBUG) Log.m(TAG,this,"isActivityOn");
		
		return activityIsFinishing == false;
	}
	

	 
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			toggle();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}
	
	
	@Override
	public Dialog onCreateDialog(int id) {
		
		switch(id) {
		case IConst.QUIT_DIALOG:
			LogOffDialogView logOffDialog = new LogOffDialogView(true, MainApp.getRes().getString(R.string.quit_message), true);
			logOffDialog.show(getSupportFragmentManager(), "dialog");
//			AlertDialog.Builder quitDialogBuilder = new AlertDialog.Builder(
//					getDialogContext());
//			quitDialogBuilder
//					.setMessage(R.string.quit_message)
//					.setCancelable(false)
//					// on Yes exit the application
//					.setPositiveButton(R.string.pozitive_button_text,
//							new DialogInterface.OnClickListener() {
//								@Override
//								public void onClick(DialogInterface dialog,
//										int viewId) {
//									makeQuitAction();
//								}
//							})
//					// on no just dismiss the dialog
//					.setNegativeButton(R.string.negative_button_textn,
//							new DialogInterface.OnClickListener() {
//								@Override
//								public void onClick(DialogInterface dialog,
//										int viewId) {
//									dialog.dismiss();
//								}
//							});
//			return quitDialogBuilder.create();
		}
		return super.onCreateDialog(id);
	}

	protected void makeQuitAction() {
		MainApp.exitApplication();
		
	}
	
	protected Context getDialogContext() {
		Context context = null;
		Activity activityParent = getParent();
		if (activityParent != null) {
			context = activityParent;
		} else {
			context = this;
		}
		return context;
	}
}
