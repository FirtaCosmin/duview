package com.crmsoftware.duview.app;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread.UncaughtExceptionHandler;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.WindowManager;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.core.common.MvpImageFactory;
import com.crmsoftware.duview.data.MvpMedia;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.data.MvpSession;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.player.drm.Constants;
import com.crmsoftware.duview.player.drm.DRMAgentDelegate;
import com.crmsoftware.duview.ui.AuthFragment;
import com.crmsoftware.duview.ui.common.MvpHorizontalListView;
import com.crmsoftware.duview.ui.common.MvpPagContext;
import com.crmsoftware.duview.ui.common.MvpVerticalListView;
import com.crmsoftware.duview.ui.common.MvpViewPager;
import com.crmsoftware.duview.utils.IConst;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;

public class MainApp extends Application{
	public static boolean DEBUG = Log.DEBUG; 
	public static final String TAG = MainApp.class.getSimpleName();
	private static boolean isTablet = false;
    private boolean blockProcessRotation = false;
    
	public static final long WAIT_TIMER_DURATION_BEFORE_EXIT = 5 * 1000L;
	public static final boolean IS_MANAGING_CONFIGURATION = false;;
	
	private static MainApp instance;	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'-'HH:mm:ss");
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public static MainApp getInstance()
	{
		if (null == instance)
		{
			instance  = new MainApp();
		}

		return instance;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public static Context getContext()
	{
		return getInstance().getApplicationContext();
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public static Resources getRes()
	{
		return getContext().getResources();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
    public static boolean hasHoneycomb() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
    }
    
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
    public static boolean isTablet() {
        return isTablet;
    }
    
    
    
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
    public static LayoutInflater getInflaterService()
    {
    	return (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    
    
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	

	public void writetoLogFile(String sOut) {
		
		try {
			
			String currentDateandTime = sdf.format(new Date());
			
			String dir = Environment.getExternalStorageDirectory() + "/duView";
			Utils.createDirIfNotExists(dir);
			
			String fileName = dir + "/log.txt";
			
			File file = new File(fileName);
			
			FileOutputStream fOut = new FileOutputStream(file,true);
			OutputStreamWriter osw = new OutputStreamWriter(fOut);
			osw.write("\n" + currentDateandTime + " ");
			osw.write(sOut);
			osw.flush();
			osw.close();fOut.close();
		}
		 catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
			 if (DEBUG) e.printStackTrace();
			}
		catch (IOException e) {
			// TODO Auto-generated catch block
			if (DEBUG) e.printStackTrace();
		}			
	}
	
	public MainApp() 
	{
		Log.setLogLevels();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onCreate() {
		if (DEBUG) Log.m(TAG,this, "onCreate");
		
		instance = this;
		
		selectImagesProfiles();
		
		try
		{
			initDRM();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		isTablet = getRes().getBoolean(R.bool.isTablet);
		blockProcessRotation = false;
	
		MvpObject mvpObjAuthData = MvpObject.newReference(AuthFragment.MVP_AUTH_DATA);
		MvpUserData authData = new MvpUserData();
		mvpObjAuthData.setData(authData);
		authData.initFromPref();
		
		Calendar cal = Calendar.getInstance();	// application device expiration
//		DRMAgentDelegate.id1 = cal.get(Calendar.DAY_OF_MONTH);
//		DRMAgentDelegate.id2 = cal.get(Calendar.MONTH) + 1;
		
		MvpViewPager.PAGESIZE = MvpPagContext.PAGESIZE;
		MvpHorizontalListView.PAGESIZE = MvpPagContext.PAGESIZE;
		MvpVerticalListView.PAGESIZE = MvpPagContext.PAGESIZE;
		

		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
		String currentDateandTime = sdf.format(new Date());
		
		writetoLogFile("\nDate: " + currentDateandTime);
		
		Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() {
			
			@Override
			public void uncaughtException(Thread thread, Throwable ex) {
				ex.printStackTrace();
				
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				ex.printStackTrace(pw);
				
				String sOut = "";
				
		    	String versionName = IConst.EMPTY_STRING;
		 		try 
		 		{
		 			versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
		 		} 
		 		catch (NameNotFoundException e) 
		 		{
		 			
		 		}
				sOut += "\n" + versionName;				
				sOut += "\n" + Build.MANUFACTURER;
				sOut += "\n" + Build.MODEL;
				sOut += "\n" + Build.VERSION.RELEASE;
				sOut += "\n\nThread " + thread.getId();
				sOut += "\n" + sw.toString();
				writetoLogFile(sOut);
				
				android.os.Process.killProcess(android.os.Process.myPid());
			}
		});
		
		MvpSession.INSTANCE.getNoUserInitialInfo();
		
		super.onCreate();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onTerminate() {
		if (DEBUG) Log.m(TAG,this, "onTerminate");
		
		super.onTerminate();
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@SuppressLint("NewApi")
	@Override
	public void onTrimMemory(int level) {
		if (DEBUG) Log.m(TAG,this, "onTrimMemory");
		
		super.onTrimMemory(level);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onLowMemory() {
		if (DEBUG) Log.m(TAG,this, "onLowMemory");
		
		MvpObject.cleanBitmaps();
		
		super.onLowMemory();
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public static final void exitApplication() 
	{
		if (DEBUG) Log.m(TAG,instance, "exitApplication");
		
		closeActivities();
		
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private static void closeActivities() 
	{
		if (DEBUG) Log.m(TAG,instance, "closeActivities");
		
		List<BaseActivity> activities = BaseActivity.activities;
		// finish each activity
		try {
			for (BaseActivity activity : activities) {
				activity.finish();
			}
		} catch (IllegalArgumentException e) {
		}

		// clean the activities list
		activities.clear();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private static boolean isNetworkAvailable() 
	{
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public UUID getDeviceId()
	{
		if (DRMAgentDelegate.DRMAGENT == null)
		{
			return new UUID(4, 4);
		}
		
		return DRMAgentDelegate.DRMAGENT.getPlayReadyDeviceID();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void initDRM()
	{
		if (DEBUG) Log.m(TAG,instance, "iniDRM");
		
        DRMAgentDelegate.setSelectedMediaPlayer("ANDROID_NATIVE");
		
        DRMAgentDelegate.sCertificateProvider = new DRMAgentDelegate.CertificateProvider()
        {
            public int getWMPrivateKey()
            {
                return R.raw.wm_privatekey;
            }

            public int getWMCertificate()
            {
                return R.raw.wm_certificate;
            }

            public int getPRPrivateKey()
            {
                return R.raw.pr_privatekey;
            }

            public int getPRCertificate()
            {
                return R.raw.pr_certificate;
            }

            public int getSSLCertificate()
            {
                return R.raw.ssl_certificate;
            }

            public int getSSLServerCertificate()
            {
                return R.raw.ssl_cacertificate;
            }

            public int getSSLPrivateKey()
            {
                return R.raw.ssl_privatekey;
            }

            @Override
            public String toString()
            {
                return TAG;
            }
        };
		
        final SharedPreferences insideSecureSharedPref = getSharedPreferences(Constants.PREFERENCES_FILENAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor pref = insideSecureSharedPref.edit();  
        pref.putInt(Constants.PREFERENCES_AGENT_LOG_LEVEL, 3);
        pref.apply();
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	
	public void blockRotationProcess(boolean state)
	{
		blockProcessRotation = state;
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public boolean isBlockedRotationProcess()
	{
		return blockProcessRotation;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void selectImagesProfiles()
	{
		// default for screens Width <=600
		MvpMedia.selectedThumbPortrait = 0;
		MvpMedia.selectedThumbLand = 1;
		MvpMedia.selectedDetails = 1;
		
		DisplayMetrics metrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) MainApp.getContext()
                .getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(metrics);
        
        int posterWidth = getResources().getDimensionPixelSize(R.dimen.horizontal_poster_item_width);
        int pageWidth = getResources().getDimensionPixelSize(R.dimen.horizontal_pager_item_width);

        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        int screenHeight = getResources().getDisplayMetrics().heightPixels;
        
        MvpImageFactory.TotalSpace = 4 * screenWidth * screenHeight;	// memory equiv. to 4 screens full of images
        
        if (screenWidth > 600*1/2) 	// 900 pixels Width screen
        {
    		MvpMedia.selectedThumbPortrait++;
    		MvpMedia.selectedThumbLand++;
    		MvpMedia.selectedDetails++;       	
        }
        
        if (screenWidth > 600*2) 	// 1200 pixels Width screen
        {
    		MvpMedia.selectedDetails++;       	
        }      
	}
	
	
	
}
