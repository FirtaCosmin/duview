package com.crmsoftware.duview.app;

import java.io.PrintWriter;
import java.io.StringWriter;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

public class CRMExceptionHandler implements
	java.lang.Thread.UncaughtExceptionHandler {
	private final Activity myContext;
	private final String LINE_SEPARATOR = "\n";
	private static final String TAG = CRMExceptionHandler.class.getSimpleName();
	protected static String CRASH_ERROR_NO_INTENT_TITLE = "CRASH_ERROR_NO_INTENT_TITLE";
	
	
	private static final int MAX_NO_OF_CRACHES = 5;
	/*
	 * a counter for the number of crashes. 
	 * When this counter reaches MAX_NO_OF_CRACHES the application will 
	 * not restart but a toast will be displayed.
	 * */
	private static int numerOfCrashes = 0 ;
	
	public CRMExceptionHandler(Activity context, int noOfCrashes) {
		myContext = context;
		numerOfCrashes = noOfCrashes;
		Log.d(TAG, "number of crashes: "+numerOfCrashes);
	}
	
	public void uncaughtException(Thread thread, Throwable exception) {
		StringWriter stackTrace = new StringWriter();
		exception.printStackTrace(new PrintWriter(stackTrace));
		StringBuilder errorReport = new StringBuilder();
		errorReport.append("************ CAUSE OF ERROR ************\n\n");
		errorReport.append(stackTrace.toString());
		
		errorReport.append("\n************ DEVICE INFORMATION ***********\n");
		errorReport.append("Brand: ");
		errorReport.append(Build.BRAND);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Device: ");
		errorReport.append(Build.DEVICE);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Model: ");
		errorReport.append(Build.MODEL);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Id: ");
		errorReport.append(Build.ID);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Product: ");
		errorReport.append(Build.PRODUCT);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("\n************ FIRMWARE ************\n");
		errorReport.append("SDK: ");
		errorReport.append(Build.VERSION.SDK);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Release: ");
		errorReport.append(Build.VERSION.RELEASE);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Incremental: ");
		errorReport.append(Build.VERSION.INCREMENTAL);
		errorReport.append(LINE_SEPARATOR);
		
		MainApp.getInstance().writetoLogFile(errorReport.toString());
		
		
		for (Activity act :FragmentChangeActivity.activities){
			act.finish();
		}
		Log.d(TAG,"CRASH intercepted");
		Log.e(TAG, "CRASH data: "+errorReport);
		
		numerOfCrashes++;
		if ( numerOfCrashes <= MAX_NO_OF_CRACHES ){
			Log.d(TAG, "crash number :"+numerOfCrashes);
			Intent intent = new Intent(myContext, FragmentChangeActivity.class);
			intent.putExtra("error", errorReport.toString());
			intent.putExtra(CRASH_ERROR_NO_INTENT_TITLE,numerOfCrashes);
			myContext.startActivity(intent);
		}else{
			/*
			 * If the app has crushed more then the maximum number of crashes allowed then 
			 * display a toast to inform the user
			 * */
			Toast toast = Toast.makeText(MainApp.getContext(), 
					                     "Unfortunately du View has encountered an error and could not recover from it. If the problem persists please contact du View suport. We are sory for the inconvenience.", 
					                     Toast.LENGTH_LONG);
			toast.show();
		}
		android.os.Process.killProcess(android.os.Process.myPid());
		System.exit(10);
	}

}