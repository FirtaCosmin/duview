package com.crmsoftware.duview.app;

import java.util.List;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.MenuItem;
import com.crmsoftware.duview.R;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.data.MvpServerError;
import com.crmsoftware.duview.data.MvpSession;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.player.TaskRegisterDevice;
import com.crmsoftware.duview.slidemenu.MvpMenu;
import com.crmsoftware.duview.slidemenu.library.SlidingMenu;
import com.crmsoftware.duview.ui.AuthFragment;
import com.crmsoftware.duview.ui.MvpFragment;
import com.crmsoftware.duview.ui.apphelp.AppHelpFragment;
import com.crmsoftware.duview.ui.common.FragmentStack;
import com.crmsoftware.duview.ui.common.MvpPagContext;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.utils.IConst;
import com.crmsoftware.duview.utils.LoadingView;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.LogOffDialogView;

public class FragmentChangeActivity extends BaseActivity implements IMvpActivity, ITaskReturn {
	public static boolean DEBUG = Log.DEBUG; 
    private static final String TAG = FragmentChangeActivity.class.getSimpleName();
    
	private static final String FORCEDORIENTATION = "FORCEDORIENTATION";
	private static final String STACK = "stack";

	private FragmentStack fragmentsStack = new FragmentStack();
	private LoadingView loadingView;
	private String title = IConst.EMPTY_STRING;
	private ActionBar actionbar;
	private ImageView actionLogo;
	private TextView actionTitle;
	
	private TaskRegisterDevice taskRegDevice;
	private boolean isEnable;
	private boolean doubleBackToExitPressedOnce;
	
	
	/**
	 * @desc the class of the fragment to which to fallback when the back button is 
	 * pressed and the user is on a non content page
	 */
	private Class fallbackClass;
	private static final String FALLBACK_CLASS_BUNDLE_NAME = "FALLBACK_CLASS";

	/**
	 * @desc the tag of the fragment to which to fallback when the back button is 
	 * pressed and the user is on a non content page
	 */
	private String fallbackTag;
	private static final String FALLBACK_TAG_BUNDLE_NAME = "FALLBACK_TAG";
	/**
	 * @desc flag set when on back pressed the app must fallback to the last fragment 
	 * containing content data 
	 * */
	private boolean fallback = false;
	private static final String FALLBACK_BUNDLE_NAME = "FALLBACK";

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onCreate(Bundle savedInstanceData) 
	{
		if (DEBUG) Log.m(TAG,this,"onCreate");
		
		super.onCreate(savedInstanceData);
		
		MvpPagContext.handler  = new Handler();
		
		boolean isNew = savedInstanceData == null;
		
		//if (isNew)	
		{
			// set the Behind View
			setBehindContentView(R.layout.menu_frame);
			
			// set the Above View
			setContentView(R.layout.content_frame);
		}
		
		MainApp mainApp = MainApp.getInstance();
		
		if (false == mainApp.isBlockedRotationProcess())	// player in  fullscreen is blocking and forcing rotation to landscape
		{		
			if (mainApp.isTablet())
			{
				if (DEBUG) Log.d(TAG," TABLET device ");
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
			}
			else
			{
				if (DEBUG) Log.d(TAG," SMARTPHONE device ");
				int currentOrient = MainApp.getRes().getConfiguration().orientation;
				
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
				if (currentOrient != Configuration.ORIENTATION_PORTRAIT)
				{
					getData().putInt(FORCEDORIENTATION, currentOrient);
					return;
				}
			}
		}	
		

		if (false == isNew)
		{
			// loading on?
			Fragment fragment = getSupportFragmentManager().findFragmentByTag(IConst.LOADING_VIEW);
			if (fragment instanceof LoadingView)
			{
				loadingView = (LoadingView) fragment;
			}
			
			
			Bundle data = getData(); // retrieve the current fragment stack
			fragmentsStack = (FragmentStack) data.getParcelable(STACK);
			retriveFallbackInfo();
			
			if (data.containsKey(FORCEDORIENTATION)) // is coming from a forcing state
			{
				data.remove(FORCEDORIENTATION);
				isNew = true;
			}
		}
		
		
		if (isNew)
		{
			MvpMenu menu = new MvpMenu();
			
			getSupportFragmentManager()
			.beginTransaction()
			.replace(R.id.menu_frame, menu)
			.commit();
			
			fragmentsStack = new FragmentStack();
			getData().putParcelable(STACK, fragmentsStack);
			
			MvpUserData authData = MvpUserData.getFromMvpObject();
			authData.initFromPref();
			
			if (false == authData.isRemember())
			{
				new Handler().post(new Runnable() {
					
					@Override
					public void run() {
						goTo(AuthFragment.class, AuthFragment.TAG, new Bundle(), true);
					}
				});

			}
			else
			{
				doPostLogin();
			}
		}
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	protected void onDestroy() 
	{	
		if (DEBUG) Log.m(TAG,this,"onDestroy");
		
		super.onDestroy();
		
		if (taskRegDevice != null)
		{
			taskRegDevice.cancel(true);
			taskRegDevice = null;
			return;
		}

	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void registerDevice()
	{
		if (DEBUG) Log.m(TAG,this,"registerDevice");
		
		showLoadingDialog();
		
		taskRegDevice = new TaskRegisterDevice(this);
		taskRegDevice.execute();
	}
	
	/**
	 * @desc the method will do the actions that need to be done after the login is successful.
	 * Actions: Register Device
	 * 			get initial info for the session
	 */
	public void doPostLogin(){
		registerDevice();
		MvpSession.INSTANCE.getUserInitialInfo();
	}
	
	/**
	 * App is going to background
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	protected void onUserLeaveHint() 
	{
		if (DEBUG) Log.m(TAG,this,"onUserLeaveHint");
		
		AppHelpFragment.tempDisabled = false;
		super.onUserLeaveHint();
	}

	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onConfigurationChanged(Configuration newConfig) 
	{
		if (DEBUG) Log.m(TAG,this,"onConfigurationChanged");

		if (MainApp.IS_MANAGING_CONFIGURATION)
		{
			// set the Above View
			setContentView(R.layout.content_frame);
		
	
			MvpMenu menu = new MvpMenu();
			
			// set the Behind View
			setBehindContentView(R.layout.menu_frame);
			getSupportFragmentManager()
			.beginTransaction()
			.replace(R.id.menu_frame, menu)
			.commit();
		
	
			
			// disable menu
			setMenuEnabled(false);
		}
		
		super.onConfigurationChanged(newConfig);
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void goTo(final Class fragmentType, final String fragmentTag, final Bundle fragmentData, final boolean addToBackStack) 
	{
		if (DEBUG) Log.m(TAG,this,"goTo");
		
		ConnectivityManager concectionM = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo networkIn= concectionM.getActiveNetworkInfo();
	    if(networkIn == null)
	    {
	        Toast.makeText(this, MainApp.getRes().getString(R.string.internet_no_connection), Toast.LENGTH_LONG).show();
	    }
	    
		if (!isActivityOn())
		{
			if (DEBUG) Log.d(TAG,"can not perform in finishing state");
			return;
		}
		
		MvpFragment fragment = null;
		try {
			fragment = (MvpFragment) fragmentType.newInstance();
		} catch (InstantiationException e) 
		{
			if (DEBUG) Log.d(TAG,"error: " + e);
			e.printStackTrace();
		} 
		catch (IllegalAccessException e) 
		{
			if (DEBUG) Log.d(TAG,"error: " + e);
			e.printStackTrace();
		}
		
		if (null == fragment)
		{
			if (DEBUG) Log.d(TAG,"null fragment");
			return;
		}
		
		fragment.setData(fragmentData);
		
		
		final Fragment content = fragment;
		
		Handler handler = new Handler();
		handler.post(new Runnable() {
			
			@Override
			public void run() {
				if (DEBUG) Log.d(TAG,"transition");
				
				if (!isActivityOn())
				{
					if (DEBUG) Log.d(TAG,"runable: can not perform in finishing state");
					return;
				}
				
				FragmentTransaction ft =  getSupportFragmentManager().beginTransaction();
				if (!addToBackStack)
				{
					ft.setCustomAnimations(R.animator.slide_in_left, R.animator.slide_out_right);
				}
				else
				{
					 					
				}
				
				ft.replace(R.id.content_frame, content,fragmentTag);
				if (addToBackStack)
				{
					fragmentsStack.push(fragmentType, fragmentTag, fragmentData);
				}
				ft.commit();
				
				/*if the fragment that will be displayed is one with content*/
				if ( getMenu().hasContent(fragmentType) ){
					/*if the fragment that will be displayed is one with content*/
					/*
					 * there will be no need to fall back to it 
					 * because it is already a fragment with content
					 * */
					fallback = false;
					/*
					 * save the identifications to the fragment in case a fragment with no content 
					 * will be displayed and the need to return to this one will appear
					 * */
					fallbackClass = fragmentType;
					fallbackTag = fragmentTag;
					Bundle activityData = getData();
					/*save the fallback information to bundle to be restored on create view*/
					activityData.putBoolean(FALLBACK_BUNDLE_NAME, fallback);
					activityData.putString(FALLBACK_TAG_BUNDLE_NAME, fallbackTag);
					activityData.putSerializable(FALLBACK_CLASS_BUNDLE_NAME, fallbackClass);
				}else{
					/*if the fragment is not one with content*/
					/*
					 * check the fallback flag 
					 * this way when back is pressed the app will not stop but fallback to the 
					 * last fragment with content
					 * */
					fallback = true;
					/*save the fallback information to bundle to be restored on create view*/
					getData().putBoolean(FALLBACK_BUNDLE_NAME, fallback);
				}
				
				getSlidingMenu().showContent();

			}
		});

	}
	
	  
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public MvpFragment getFragment(int layoutId)
	{
		if (DEBUG) Log.m(TAG,this, "getFragment");
		 
		FragmentManager fm = getSupportFragmentManager();
				
		if (null == fm)
		{
			return null;
		}

		return (MvpFragment) fm.findFragmentById(layoutId);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void setMenuEnabled(boolean bState)
	{
		if (DEBUG) Log.m(TAG,this, "setMenuEnabled");
		this.isEnable = bState;
		
		if (!bState)
		{
			getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
			return;
		}
		

		getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		getSlidingMenu().setShadowDrawable(R.drawable.menu_shadow);
		getSlidingMenu().setShadowWidthRes(R.dimen.shadow_width);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void setActionBarVisibility(int visibility)
	{
		if (DEBUG) Log.m(TAG,this, "setActionBarVisibility");		
		
		if (visibility == View.VISIBLE)
		{
			getSupportActionBar().show();
			return;
		}
		

		getSupportActionBar().hide();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onBackPressed() {
		
		if (DEBUG) Log.m(TAG,this, "onBackPressed");	
		
		MvpFragment currentFragment = (MvpFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
		if (null != currentFragment)
		{
			if (false == currentFragment.onBackPressed())
			{
				return;		
			}
			
		
		}
		
		
		int count = fragmentsStack.getBackStackCount();
		if (count > 1)
		{
			/*the fallback flag is reset */
			fallback = false;
			if (null != currentFragment)
			{
				currentFragment.onResetData();
			}
			
			fragmentsStack.pop();	
			FragmentStack.FragmentData fd = fragmentsStack.getLastEntry();
			goTo(fd.fragmentType, fd.fragmentTag, fd.fragmentData, false);
		}
		else if ( fallback ){
			fallback = false;
			this.getMenu().selectMenuItem(fallbackClass, fallbackTag, true);
			return;
		}else
		{	
//			showDialog(IConst.QUIT_DIALOG);
			 if (doubleBackToExitPressedOnce) {
				 MainApp.exitApplication();
			        super.onBackPressed();
			        return;
			    }

			    this.doubleBackToExitPressedOnce = true;
			    Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

			    new Handler().postDelayed(new Runnable() {

			        @Override
			        public void run() {
			            doubleBackToExitPressedOnce=false;                       
			        }
			    }, 2000);
		}
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public MvpMenu getMenu() {
		return (MvpMenu) getSupportFragmentManager().findFragmentById(R.id.menu_frame);
	}

	@Override
	public void clearStack() {
		fragmentsStack.clear();
		
		List<Fragment> list = (List<Fragment>) getSupportFragmentManager().getFragments();
		if (list == null)
		{
			return;
		}
		
		MvpFragment mvpFragment = getCurrentScreen();
		if (mvpFragment instanceof MvpFragment)
		{
			mvpFragment.onResetData();
		}
		
		for (int i=0; i < list.size(); i++)
		{
			Fragment fragment =  list.get(i);
			
			if (null == fragment || mvpFragment == fragment)
			{
				break;
			}
			
			if (fragment instanceof MvpFragment)
			{
				mvpFragment = (MvpFragment)fragment;
				if (getMenu() == mvpFragment)
				{
					continue;
				}

				mvpFragment.onResetData();
			}
		}
		
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void showLoadingDialog() {
		if (DEBUG) Log.m(TAG,this,"showLoadingDialog");
		
		new Handler().post(new Runnable() {
			
			@Override
			public void run() {
				if (!isActivityOn()) 
				{
					return;
				}
				
				if (loadingView == null) 
				{
					loadingView = LoadingView.newInstance();
				}
				else
				{
					return;
				}
				
				
				try 
				{
					if (false == loadingView.isAdded())
					{
						loadingView.show(getSupportFragmentManager(), IConst.LOADING_VIEW);
					}
					
				} 
				catch (Exception e) 
				{
					if (DEBUG) Log.m(TAG,this,"showLoadingDialog"+ e.getMessage());
				}
				
			}
		});
	
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void hideLoadingDialog() {
		if (DEBUG) Log.m(TAG,this,"showLoadingDialog"+ this);
		
		
		new Handler().post(new Runnable() {
			
			@Override
			public void run() {
				
				if (!isActivityOn()) 
				{
					return;
				}
				
				if (loadingView == null) 
				{
					return;
				}
				
				try 
				{
					if (loadingView.isAdded())
					{
						loadingView.dismiss();
						loadingView = null;
					}

				} catch(Exception e) 
				{
					if (DEBUG) Log.m(TAG,this,"showLoadingDialog"+ e.getMessage());
				}
			}
		});

	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			if(isEnable)
			{
				toggle();
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void gestionateActionBar()
	{
		
		LayoutInflater inflater = (LayoutInflater) getSupportActionBar().getThemedContext()
	            .getSystemService(LAYOUT_INFLATER_SERVICE);
	    final View customActionBarView = inflater.inflate(
	            R.layout.actionbar_custom_view, null);
	    
	    MvpViewHolder holder = new MvpViewHolder(customActionBarView);
	    
	    actionLogo =  (ImageView) holder.getView(R.id.app_logo);
	    actionTitle = (TextView) holder.getView(R.id.action_title);
	    
	    ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
	            ActionBar.LayoutParams.WRAP_CONTENT, 
	            ActionBar.LayoutParams.MATCH_PARENT, 
	            Gravity.CENTER);
	    
	    actionbar = getSupportActionBar();

	    actionbar.setDisplayHomeAsUpEnabled(false);
	    
	    actionbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.action_bar_gradient));
	    actionbar.setDisplayShowTitleEnabled(false);
	    actionbar.setDisplayShowCustomEnabled(true);
	    actionbar.setLogo(getResources().getDrawable(R.drawable.menu));
	    actionbar.setCustomView(customActionBarView, params);
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void setTitle(String title) {
//		if(actionTitle == null)
//		{
			gestionateActionBar();
//		}
		
		actionTitle.setText(title);
		if(title.equals(IConst.EMPTY_STRING))
		{
			actionLogo.setVisibility(View.VISIBLE);
		}
		else
		{
			actionLogo.setVisibility(View.GONE);
		}
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */
	@Override
	public MvpFragment getCurrentScreen() 
	{
		return (MvpFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
	}


	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void setHomeButtonActionBar(boolean isVisible) {
		getSupportActionBar().setHomeButtonEnabled(isVisible);
		getSupportActionBar().setDisplayShowHomeEnabled(isVisible);
	}


	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void show(Class classType, String fragmentTag, Bundle fragmentData) 
	{
		try 
		{
			MvpFragment fragment = (MvpFragment)classType.newInstance();
			if (false == fragment.isAdded())
			{
				if (null == fragmentData)
				{
					fragmentData = new Bundle();
				}
				
				fragment.setData(fragmentData);
				
				FragmentTransaction ft =  getSupportFragmentManager().beginTransaction();
				
				ft.setCustomAnimations(R.animator.dlg_zoom_in, R.animator.dlg_zoom_out);
				
				ft.add(R.id.dialog_frame, fragment,"dlg_"+fragmentTag);
				ft.commit();
			}
		} 
		catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void hide(String fragmentTag) 
	{
		Fragment fragment = getSupportFragmentManager().findFragmentByTag("dlg_" + fragmentTag);
		if (null != fragment)
		{
			if (fragment.isAdded())
			{
				FragmentTransaction ft =  getSupportFragmentManager().beginTransaction();
				ft.setCustomAnimations(R.animator.dlg_zoom_in, R.animator.dlg_zoom_out);
				ft.remove(fragment);
				ft.commit();
			}
		}
		
	}


	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onReturnFromTask(Task task, boolean canceled) 
	{
		if (task == taskRegDevice)
		{
			hideLoadingDialog();
			
			
			if (task.isError())
			{
				if (task.getError() instanceof MvpServerError)
				{
					MvpServerError taskError = (MvpServerError) task.getError();
					
//					if (taskError.getResponseCode() != 401)	// no authentication header = no token
//					{
//						Toast toast = Toast.makeText(this, taskError.displayError(), Toast.LENGTH_LONG);
//						toast.show();
//					}
					
					
					
					goTo(AuthFragment.class, AuthFragment.TAG, new Bundle(), false);
					
					if (taskError.getResponseCode() != 401)	// no authentication header = no token
						{
							Toast toast = Toast.makeText(this, taskError.displayError(), Toast.LENGTH_LONG);
							toast.show();
						}
					return;
				}
				
				LogOffDialogView logOff = new LogOffDialogView(false,task.getError().toString(), false);
				logOff.show(getSupportFragmentManager(), "");
				return;
			}
			
			MvpUserData authData = new MvpUserData();
			authData.initFromPref();
			authData.saveToMvpObject();
			
			MvpMenu mvpMenu = getMenu();
			
			if (null != mvpMenu)
			{
				mvpMenu.update();
				mvpMenu.selectDefaultItem();
			}
			else
			{
				Log.e(TAG,"Menu is not ready");
			}
		}
		
	}
	
	/**
	 * @desc this method will retrive the fallback information from the BaseActivitys data
	 */
	private void retriveFallbackInfo(){
		fallback = getData().getBoolean(FALLBACK_BUNDLE_NAME);
		fallbackClass = (Class) getData().getSerializable(FALLBACK_CLASS_BUNDLE_NAME);
		fallbackTag = getData().getString(FALLBACK_TAG_BUNDLE_NAME);
	}

}
