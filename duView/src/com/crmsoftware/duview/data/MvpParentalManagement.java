package com.crmsoftware.duview.data;

import java.util.Calendar;
import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

import com.crmsoftware.duview.configuration.PlatformSettings;
import com.crmsoftware.duview.core.common.HttpReqParams;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.core.common.TaskSingleReqParse;
import com.crmsoftware.duview.utils.IHttpConst;
import com.crmsoftware.duview.utils.Log;

public class MvpParentalManagement  extends Observable implements ITaskReturn {

	public static boolean DEBUG = Log.DEBUG; 
	public static boolean VERBOUSE = Log.VERBOUSE ; 
	public final static String TAG = MvpParentalManagement.class.getSimpleName();
	
	public final static int REQUIRED_PIN = -10;
	public final static int LEVEL_NOK = 0;
	public final static int LEVEL_OK = 1;
	
	public final static int MAX_LEVEL = 10000;
	
	public final static int TEMP_VALIDITY = 20*60*1000; //10 * (60 * 1000);
	public final static int SERVER_VALIDITY = 15*60*1000; //10 * (60 * 1000);
	
	private int serverLevel = MAX_LEVEL;
	private int configuredLevel = MAX_LEVEL;
	
	private long serverExpirationTime;
	private long levelExpirationTime;
	
	private TaskSingleReqParse taskGetParentalLevel;
	private TaskSingleReqParse taskSetParentalLevel;
	private TaskSingleReqParse taskChangePin;
	private TaskSingleReqParse taskCheckParentalPin;
	private int tempServerLevel;
	
	private MvpError error;
	
	private Timer timer;
	
	private ITaskReturn callbackCheckPin;
	private ITaskReturn callbackChangePin;
	private ITaskReturn callbackSetParentalLevel;
	
	class CheckTask extends TimerTask 
	{
        @Override
        public void run() 
        {
    		if (DEBUG) Log.m(TAG,MvpParentalManagement.this,"ctor()");
    		
        	long currentTime = Calendar.getInstance().getTimeInMillis();
        	
        	if (serverExpirationTime <= currentTime)
        	{
        		retrieveServerParentalLevel();
        		serverExpirationTime = currentTime + SERVER_VALIDITY;
        	}
        	
        	if (levelExpirationTime <= currentTime)
        	{
        		setLevel(serverLevel);
        		levelExpirationTime = currentTime + TEMP_VALIDITY;
        	}
		}
    };
    
    
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private static MvpParentalManagement instance;
	public static MvpParentalManagement getInstance()
	{
		if (null == instance)
		{	  		
			instance = new MvpParentalManagement();
		}
		return instance;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public static void  releaseInstance()
	{
		if (null != instance)
		{	  		
			instance.release();
		}
		instance = null;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void release()
	{
		if (DEBUG) Log.m(TAG,this,"ctor()");
		
		if (null != timer)
		{
			timer.cancel();
			timer.purge();
			timer = null;
		}
		
		if (null != taskChangePin)
		{
			taskChangePin.cancel(true);
			taskChangePin = null;
		}
		
		if (null != taskSetParentalLevel)
		{
			taskSetParentalLevel.cancel(true);
			taskSetParentalLevel = null;
		}
		
		if (null != taskGetParentalLevel)
		{
			taskGetParentalLevel.cancel(true);
			taskGetParentalLevel = null;
		}
		
		if (null != taskCheckParentalPin)
		{
			taskCheckParentalPin.cancel(true);
			taskCheckParentalPin = null;
		}
		
		instance = null;
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	private MvpParentalManagement()
	{
		if (DEBUG) Log.m(TAG,this,"ctor()");
		
  		timer = new Timer();
  		timer.schedule(new CheckTask(), 0, 5*60*1000);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void setLevel(int newLevel)
	{	
		if (DEBUG) Log.m(TAG,this,"setLevel");
		
		if (newLevel != configuredLevel)
		{
			configuredLevel = newLevel;
			
			setChanged();
			notifyObservers();
		}
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public Task checkPin(String pin, ITaskReturn callback)
	{
		if (DEBUG) Log.m(TAG,this,"checkPin");
		
		this.callbackCheckPin = callback;
		
		HttpReqParams httpReqParams = new HttpReqParams(
				IHttpConst.VALUE_SCHEMA_1_1);
		
		httpReqParams.setParam(IHttpConst.ACCOUNT_NAME, PlatformSettings.getInstance().getAccountNameValue());
		httpReqParams.setParam(IHttpConst.PARENTAL_PIN_CODE, pin);
		httpReqParams.setParam(IHttpConst.PARAM_TOKEN, MvpUserData.getFromMvpObject().getToken());
		
		taskCheckParentalPin = new TaskSingleReqParse<MvpString>(this, MvpString.class, PlatformSettings.getInstance().getCheckParentalPinUrl(), httpReqParams);
		taskCheckParentalPin.execute();
		
		return taskCheckParentalPin;
	}
	
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void changePin(String newPin, String currentPin, ITaskReturn callback)
	{
		if (DEBUG) Log.m(TAG,this,"changePin");
		
		this.callbackChangePin = callback;
		
		HttpReqParams httpReqParams = new HttpReqParams(
				IHttpConst.VALUE_SCHEMA_1_1);
		
		httpReqParams.setParam(IHttpConst.OLD_PARENTAL_PIN, currentPin);
		httpReqParams.setParam(IHttpConst.NEW_PARENTAL_PIN, newPin);
		httpReqParams.setParam(IHttpConst.ACCOUNT_NAME, PlatformSettings.getInstance().getAccountNameValue());
		httpReqParams.setParam(IHttpConst.DIRECTORY_PID, PlatformSettings.getInstance().getTpMpxDirPid());
		httpReqParams.setParam(IHttpConst.PARAM_TOKEN, MvpUserData.getFromMvpObject().getToken());
		
		taskChangePin = new TaskSingleReqParse<MvpString>(
				this,
				MvpString.class,
				PlatformSettings.getInstance().getChangePinUrl(), 
				httpReqParams);	
		
		taskChangePin.execute();
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	private void retrieveServerParentalLevel()
	{
		if (DEBUG) Log.m(TAG,this,"retrieveServerParentalLevel");
		
		HttpReqParams httpReqParams = new HttpReqParams(
				IHttpConst.VALUE_SCHEMA_1_1);
		
		httpReqParams.setParam(IHttpConst.ACCOUNT_NAME, PlatformSettings.getInstance().getAccountNameValue());
		httpReqParams.setParam(IHttpConst.PARAM_TOKEN, MvpUserData.getFromMvpObject().getToken());
		
		taskGetParentalLevel = new TaskSingleReqParse(this, MvpParentalLevel.class,
				PlatformSettings.getInstance().getParentalUserLevelUrl(), 
				httpReqParams);	
		
		taskGetParentalLevel.execute();
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void setServerParentalLevel(String pin, int newLevel, ITaskReturn callback)
	{
		if (DEBUG) Log.m(TAG,this,"setServerParentalLevel");
		
		tempServerLevel = newLevel;
		this.callbackSetParentalLevel = callback;
		
		HttpReqParams httpReqParams = new HttpReqParams(
				IHttpConst.VALUE_SCHEMA_1_1);
		
		httpReqParams.setParam(IHttpConst.PARENTAL_PIN_CODE, pin);
		httpReqParams.setParam(IHttpConst.PARENTAL_LEVEL, ""+newLevel);
		httpReqParams.setParam(IHttpConst.ACCOUNT_NAME, PlatformSettings.getInstance().getAccountNameValue());
		httpReqParams.setParam(IHttpConst.PARAM_TOKEN, MvpUserData.getFromMvpObject().getToken());
		
		
		taskSetParentalLevel = new TaskSingleReqParse(
				this,
				null,
				PlatformSettings.getInstance().getSetParentalUserLevelUrl(), 
				httpReqParams);	

		taskSetParentalLevel.execute();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onReturnFromTask(Task task, boolean canceled) 
	{
		if (DEBUG) Log.m(TAG,this,"onReturnFromTask");
		
		if (task == taskGetParentalLevel)
		{
			int currentLevel = serverLevel;
			if (task.isError())
			{
				error = task.getError();
			}
			else
			{
				MvpParentalLevel level = (MvpParentalLevel)task.getReturnData();
				if (null != level)
				{
					currentLevel = level.getLevel();
				}
			}
			
			if (currentLevel != serverLevel)
			{
				serverLevel = currentLevel;
				setLevel(serverLevel);
			}
			taskGetParentalLevel = null;
			return;
		}
		
		
		if (task == taskSetParentalLevel)
		{
			if (task.isError())
			{
				error  = task.getError();
				MvpServerError mvpError = (MvpServerError)error;
				int erroCode = mvpError.getResponseCode();
				if(erroCode == 200)
				{
					serverLevel = tempServerLevel;
				}
			}
			else
			{
				serverLevel = tempServerLevel;
			}
			
			setLevel(serverLevel);
			
			if (null != callbackSetParentalLevel)
			{
				callbackSetParentalLevel.onReturnFromTask(task, canceled);	// this has to be enhanced when fragment is recreated
				callbackSetParentalLevel = null;
			}
			
			taskSetParentalLevel = null;
			
			return;
		}
		
		
		if (task == taskCheckParentalPin)
		{
			if (task.isError())
			{
				MvpServerError mvpError = (MvpServerError) task.getError();
				int erroCode = mvpError.getResponseCode();
				if(erroCode == 200)
				{
					setLevel(MAX_LEVEL);
				}
			}

			
			if (null != callbackCheckPin)
			{
				callbackCheckPin.onReturnFromTask(task, canceled);	// this has to be enhanced when fragment is recreated
				callbackCheckPin = null;
			}
			
			taskCheckParentalPin = null;
			
			return;
		}
		
		if(task == taskChangePin)
		{

			if (null != callbackChangePin)
			{
				callbackChangePin.onReturnFromTask(task, canceled);	// this has to be enhanced when fragment is recreated
				callbackChangePin = null;
			}
			taskChangePin = null;
			
			return;
		}
	}
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public int getServerLevel()
	{	
		if (DEBUG) Log.m(TAG,this,"getServerLevel");
		
		return serverLevel;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public int checkLevel(int itemLevel)
	{
		if (DEBUG) Log.m(TAG,this,"checkLevel");
		
		if (itemLevel >= configuredLevel)
		{
			return LEVEL_NOK;
		}
		
		return LEVEL_OK;
	}

	
}
