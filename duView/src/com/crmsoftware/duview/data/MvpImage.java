package com.crmsoftware.duview.data;

import java.util.Comparator;

import android.graphics.Bitmap;
import android.widget.ImageView;

public class MvpImage implements Comparable<MvpImage> {

	private String sURL="";
	private Bitmap bitmap;
	private int refCount = 0;
	
	public MvpImage() 
	{
	}
	
	public void update(String sURL, Bitmap bitmap) 
	{
		synchronized (this) 
		{
			this.bitmap = bitmap;
			this.sURL = sURL;			
		}
	}
	
	public String getUrl()
	{
		return sURL;
	}
	
	public void attachView(ImageView view)
	{
		synchronized (this) 
		{
			view.setImageBitmap(bitmap);	
			refCount++;
		}
	}
	
	
	public void dettachView(ImageView view)
	{
		synchronized (this) 
		{
			view.setImageBitmap(null);		
			refCount--;
		}
	}
	
	public long getSize()
	{
		if (null != bitmap)
		{
			return bitmap.getHeight() * bitmap.getWidth();
		}
		return 0;
	}
	
	
	public boolean isReferenced()
	{
		return refCount > 0;
	}


	@Override
	public int compareTo(MvpImage another) {

		long currentSize = getSize();
		if (currentSize == another.getSize())
		{
			return 0;
		}
		
		if (currentSize < another.getSize())
		{
			return -1;
		}
		
		if (currentSize > another.getSize())
		{
			return 1;
		}		
		return 0;
	}
}
