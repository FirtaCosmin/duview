package com.crmsoftware.duview.data;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************

import org.json.JSONException;
import org.json.JSONObject;

public class DuAppsUpdateResp extends Object implements IJsonParser {
	
	public static final String TAG_RESPONSE = "response";
	public static final String TAG_PAYLOAD = "payLoad";

	protected DuAppsResponse response = new DuAppsResponse();
	protected MvpList<DuAppsItem> listApps = new MvpList<DuAppsItem>(DuAppsItem.class);

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	@Override
	public void fromJson(JSONObject jsonObj,String tag) throws JSONException, InstantiationException, IllegalAccessException {
		
		response.fromJson(jsonObj, TAG_RESPONSE);
		listApps.fromJson(jsonObj,TAG_PAYLOAD);
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {
		

		response.toJson(jsonObj, TAG_RESPONSE);
		listApps.toJson(jsonObj,TAG_PAYLOAD);
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public String toJsonString() {
		
		JSONObject jsonObj = new JSONObject();
		
		try 
		{
			toJson(jsonObj, null);
			
		} catch (Exception e) {
			
			return "{error}";
		}
		
		return jsonObj.toString();
	}
	
	
	public DuAppsResponse getResponse() {
		return response;
	}

	public MvpList<DuAppsItem> getListApps() {
		return listApps;
	}


	
}
