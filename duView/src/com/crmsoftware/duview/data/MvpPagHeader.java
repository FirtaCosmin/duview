package com.crmsoftware.duview.data;

import org.json.JSONException;
import org.json.JSONObject;

import com.crmsoftware.duview.utils.Utils;

public class MvpPagHeader extends PagHeader
{
	public static final String TAG_DOCS = "docs";
	public static final String TAG_NUM_FOUND = "numFound";
	public static final String TAG_START = "start";
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */  
	@Override
	public void fromJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {

		totalResults = Utils.getJSONAsInt(jsonObj,TAG_NUM_FOUND);
		startIndex = Utils.getJSONAsInt(jsonObj,TAG_START);
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */  
	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {

		Utils.setJSONAsInt(jsonObj, TAG_NUM_FOUND, totalResults);
		Utils.setJSONAsInt(jsonObj, TAG_START, startIndex);
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */  
	@Override
	public String getListTag() 
	{
		return TAG_DOCS;
	}
	
}