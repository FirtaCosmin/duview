package com.crmsoftware.duview.data;


import java.util.HashSet;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import com.crmsoftware.duview.utils.Utils;

public class MvpContent implements IJsonParser{
	
	public static final String TAG_WIDTH = "width";
	public static final String TAG_HEIGHT = "height";
	public static final String TAG_URL = "url";
	public static final String TAG_DURATION = "duration";
	public static final String TAG_ISPROTECTED = "isProtected";
	public static final String TAG_FORMAT = "format";
	public static final String TAG_RELEASES = "releases";	

	public int height = 0;
	public String url = "";
	public double duration = 0;
	public boolean isProtected = false;
	public String format = "";
	public int width = 0;
	public MvpList<MvpRelease> releasesList = new MvpList<MvpRelease>(MvpRelease.class);
	

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public Set<String>	listAssets = new HashSet<String>();
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void fromJson(JSONObject jsonObj, String tag) throws JSONException, InstantiationException, IllegalAccessException {
		
		width = Utils.getJSONAsInt(jsonObj, TAG_WIDTH);
		height = Utils.getJSONAsInt(jsonObj, TAG_HEIGHT);
		url = Utils.getJSONAsString(jsonObj, TAG_URL);
		
		duration = Utils.getJSONAsDouble(jsonObj, TAG_DURATION);
		isProtected = Utils.getJSONAsBool(jsonObj, TAG_ISPROTECTED);
		format = Utils.getJSONAsString(jsonObj, TAG_FORMAT);
		
		releasesList.fromJson(jsonObj, TAG_RELEASES);		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */
	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {
		
		Utils.setJSONAsInt(jsonObj, TAG_WIDTH, width);
		Utils.setJSONAsInt(jsonObj, TAG_HEIGHT, height);
		Utils.setJSONAsString(jsonObj, TAG_URL, url);
		
		Utils.setJSONAsDouble(jsonObj, TAG_DURATION, duration);
		Utils.setJSONAsBool(jsonObj, TAG_ISPROTECTED, isProtected);
		Utils.setJSONAsString(jsonObj, TAG_FORMAT, format);
		
		releasesList.toJson(jsonObj, TAG_RELEASES);
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public String toJsonString() {
		
		JSONObject jsonObj = new JSONObject();
		
		try 
		{
			toJson(jsonObj, null);
			
		} catch (Exception e) {
			
			return "{error}";
		}
		
		return jsonObj.toString();
	}
	
	
	public MvpList<MvpRelease> getReleasesList() {
		return releasesList;
	}

	public void setReleasesList(MvpList<MvpRelease> releasesList) {
		this.releasesList = releasesList;
	}
	

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDuration(String minutes ) {
		return Utils.convertSecToMinutes((int) duration, minutes);
	}

	public void setDuration(double duration) {
		this.duration = duration;
	}

	public boolean isProtected() {
		return isProtected;
	}

	public void setProtected(boolean isProtected) {
		this.isProtected = isProtected;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public Set<String> getListAssets() {
		return listAssets;
	}

	public void setListAssets(Set<String> listAssets) {
		this.listAssets = listAssets;
	}



	
}