package com.crmsoftware.duview.data;
//******************************************************************************************
//Description:  file created for duView - CRM project
// Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import com.crmsoftware.duview.utils.Utils;

public class MvpProgram implements IJsonParser {
	
	public static final String TAG_ID = "@programId";
	public static final String TAG_DETAILS_URL = "ProgramDetailsURL";
	public static final String TAG_TITLE = "Title";
	public static final String TAG_CHANNELID = "ChannelId";
	public static final String TAG_SHORT_DESCRIPTION = "ShortDescription";
	public static final String TAG_DURATION = "Duration";
	public static final String TAG_START_TIME = "PublishedStartTime";
	public static final String TAG_END_TIME = "PublishedEndTime";	
	
	private String id;
	private String detailsUrl;


	private String title;
	
	private String channelId;

	private String shortDescription;
	private long startTime;
	private long endTime;
	
	private long timelineWidth = 0;
	
	public SimpleDateFormat programDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	@Override
	public void fromJson(JSONObject jsonObj,String tag) throws JSONException, InstantiationException, IllegalAccessException {
		
		id = Utils.getJSONAsString(jsonObj, TAG_ID);
		
		detailsUrl = Utils.decodeUrl(Utils.getJSONAsString(jsonObj,TAG_DETAILS_URL));

		title = Utils.getJSONAsString(jsonObj,TAG_TITLE);
		
		channelId = Utils.getJSONAsString(jsonObj,TAG_CHANNELID);
		
		shortDescription = Utils.getJSONAsString(jsonObj,TAG_SHORT_DESCRIPTION);	
	
		String sstartTime =Utils.getJSONAsString(jsonObj,TAG_START_TIME);
		startTime =Utils.stringToDate(sstartTime, programDateFormat);	
		
		String sendTime =Utils.getJSONAsString(jsonObj,TAG_END_TIME);
		endTime = Utils.stringToDate(sendTime, programDateFormat);	
	
//		Log.d("", "title=" + title + " sT " + sstartTime + " eT=" + sendTime + "  sc = " + printSchedule() );
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {
		
		Utils.setJSONAsString(jsonObj, TAG_ID, id);
		Utils.setJSONAsString(jsonObj, TAG_DETAILS_URL, detailsUrl);
		Utils.setJSONAsString(jsonObj, TAG_TITLE, title);
		Utils.setJSONAsString(jsonObj, TAG_CHANNELID, channelId);
		Utils.setJSONAsString(jsonObj, TAG_SHORT_DESCRIPTION, shortDescription);

		Utils.setJSONAsString(jsonObj, TAG_START_TIME, Utils.dateToString(startTime, programDateFormat));
		Utils.setJSONAsString(jsonObj, TAG_END_TIME, Utils.dateToString(endTime, programDateFormat));
	}
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public String toJsonString() 
	{
		JSONObject jsonObj = new JSONObject();
		
		try 
		{
			toJson(jsonObj, null);
			
		} catch (Exception e) {
			
			return "{error}";
		}
		
		return jsonObj.toString();
	}
	
	public String getId() {
		if (null == id)
		{
			return "";
		}
		return id;
	}
	
	public void setId(String id) 
	{
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public long getStartTime() {
		return startTime;
	}

	public long getEndTime() {
		return endTime;
	}
	
	public long getTimelineWidth() {
		return timelineWidth;
	}

	public void setTimelineWidth(long timelineWidth) {
		this.timelineWidth = timelineWidth;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}
	
	public String printSchedule()
	{
		return Utils.dateToString(startTime,programDateFormat) + " - " + Utils.dateToString(endTime,programDateFormat);
	}
	
	public String printSchedule(SimpleDateFormat dateFormat )
	{
		return Utils.dateToString(startTime,dateFormat) + "-" + Utils.dateToString(endTime,dateFormat);
	}

	public String getDetailsUrl() {
		return detailsUrl;
	}

	public void setDetailsUrl(String detailsUrl) {
		this.detailsUrl = detailsUrl;
	}
	
	public boolean isCurrent() 
	{
		long time = Calendar.getInstance().getTimeInMillis();
		
		return startTime <= time && time <= endTime;
	}
	
	public long getDurationInMin() 
	{	
		return (endTime - startTime)/(1000*60);
	}
	
	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
}
