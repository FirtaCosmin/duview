package com.crmsoftware.duview.data;

//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************

import org.json.JSONException;
import org.json.JSONObject;

import com.crmsoftware.duview.utils.Utils;




public class DuAppsResponse extends Object implements IJsonParser {
	
	public static final String TAG_CODE = "code";
	public static final String TAG_MESSAGE = "message";


	
	protected int code;

	protected String message;

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	@Override
	public void fromJson(JSONObject jsonObj,String tag) throws JSONException, InstantiationException, IllegalAccessException {
		
		JSONObject jsonResp = jsonObj;
		if (false == Utils.isEmpty(tag))
		{
			jsonResp = jsonObj.getJSONObject(tag);
		}
		
		code = Utils.getJSONAsInt(jsonResp, TAG_CODE);
		message = Utils.getJSONAsString(jsonResp,TAG_MESSAGE);
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException 
	{
		JSONObject jsonResp = jsonObj;
		if (false == Utils.isEmpty(tag))
		{
			jsonResp = new JSONObject();
			jsonObj.put(tag, jsonResp);
		}
		
		Utils.setJSONAsInt(jsonResp, TAG_CODE, code);
		Utils.setJSONAsString(jsonResp, TAG_MESSAGE, message);
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public String toJsonString() {
		
		JSONObject jsonObj = new JSONObject();
		
		try 
		{
			toJson(jsonObj, null);
			
		} catch (Exception e) {
			
			return "{error}";
		}
		
		return jsonObj.toString();
	}


	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
