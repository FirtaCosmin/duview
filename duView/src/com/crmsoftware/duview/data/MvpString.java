package com.crmsoftware.duview.data;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************

import org.json.JSONException;
import org.json.JSONObject;

import com.crmsoftware.duview.utils.Utils;


public class MvpString extends Object implements IJsonParser {
	
	protected String data;

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	@Override
	public void fromJson(JSONObject jsonObj,String tag) throws JSONException, InstantiationException, IllegalAccessException {
		
		data = Utils.getJSONAsString(jsonObj, tag);
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {
		
		Utils.setJSONAsString(jsonObj, tag, data);
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public String toJsonString() {
		
		JSONObject jsonObj = new JSONObject();
		
		try 
		{
			toJson(jsonObj, null);
			
		} catch (Exception e) {
			
			return "{error}";
		}
		
		return jsonObj.toString();
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}


	
}
