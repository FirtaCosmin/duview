package com.crmsoftware.duview.data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.crmsoftware.duview.utils.Utils;

public class MvpFavoritesList extends MvpList 
{
	
	public static final String TAG_CHANNEL_LIST= "channelList";
	public static final String TAG_CHANNEL_ID = "channelId";

	public MvpFavoritesList() 
	{
		super(String.class);
		
		
	}
	
	
	@Override
	public void fromJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException 
	{
		
		if (false == jsonObj.has(TAG_CHANNEL_LIST))
		{
			return;
		}

		JSONArray jsonArray  = null;
		try 
		{
			jsonArray = jsonObj.getJSONArray(TAG_CHANNEL_LIST);
		} 
		catch (Exception e) 
		{

		}
		       
        if (null == jsonArray)
        {
        	return;
        }
        
        for (int i=0; i<jsonArray.length(); i++)
        {
        	JSONObject item = jsonArray.getJSONObject(i);
        	String id = Utils.getJSONAsString(item, TAG_CHANNEL_ID);
        	
        	listItems.add(id);
        }
	}
	
	
	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {
		
		JSONArray jsonArray  = new JSONArray();
		
        for (int i=0; i<listItems.size(); i++)
        {
        	JSONObject item = new JSONObject();
        	Utils.setJSONAsString(item, TAG_CHANNEL_ID, (String)listItems.get(i));
        	
        	jsonArray.put(item);
        }
        
        jsonObj.put(TAG_CHANNEL_LIST, jsonArray);
	}

}
