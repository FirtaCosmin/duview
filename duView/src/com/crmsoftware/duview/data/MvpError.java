package com.crmsoftware.duview.data;
//******************************************************************************************
//Description:  file created for duView - CRM project
// Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************

public class MvpError {
	
	public enum ErrorType {Http, Parser, System, Processing}; 
	
	private Object data;

	private ErrorType type;
	
	public MvpError(ErrorType type) 
	{
		this.type = type;
	}
	
	public MvpError(ErrorType type, Object data) 
	{
		this.type = type;
		this.data = data;
	}
	
	@Override
	public String toString() {
		
		return "Type: " + type + " data: " + data;
	}
	
	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public ErrorType getType() {
		return type;
	}

	public void setType(ErrorType type) {
		this.type = type;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public String displayError() 
	{
		return data.toString();
	}
	
}
