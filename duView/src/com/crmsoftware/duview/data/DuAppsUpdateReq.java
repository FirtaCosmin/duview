package com.crmsoftware.duview.data;

//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************

import org.json.JSONException;
import org.json.JSONObject;

import com.crmsoftware.duview.utils.Utils;




public class DuAppsUpdateReq extends Object implements IJsonParser {
	
	public static final String TAG_PLATFORM = "platform";
	public static final String TAG_SECTION = "section";
	public static final String TAG_UPDATEDATE = "updateDate";


	protected String platform;

	

	protected String section;
	
	protected String updatedate;

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	@Override
	public void fromJson(JSONObject jsonObj,String tag) throws JSONException, InstantiationException, IllegalAccessException {
		
		JSONObject jsonResp = jsonObj;
		if (false == Utils.isEmpty(tag))
		{
			jsonResp = jsonObj.getJSONObject(tag);
		}
		
		platform = Utils.getJSONAsString(jsonResp, TAG_PLATFORM);
		section = Utils.getJSONAsString(jsonResp,TAG_SECTION);
		updatedate = Utils.getJSONAsString(jsonResp,TAG_UPDATEDATE);		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException 
	{
		JSONObject jsonResp = jsonObj;
		if (false == Utils.isEmpty(tag))
		{
			jsonResp = jsonObj.getJSONObject(tag);
			jsonObj.put(tag, jsonResp);
		}
		
		Utils.setJSONAsString(jsonResp, TAG_PLATFORM, platform);
		Utils.setJSONAsString(jsonResp,TAG_SECTION, section);
		Utils.setJSONAsString(jsonResp,TAG_UPDATEDATE, updatedate);	
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public String toJsonString() {
		
		JSONObject jsonObj = new JSONObject();
		
		try 
		{
			toJson(jsonObj, null);
			
		} catch (Exception e) {
			
			return "{error}";
		}
		
		return jsonObj.toString();
	}
	
	
	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platfrom) {
		this.platform = platfrom;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getUpdatedate() {
		return updatedate;
	}

	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
	
}
