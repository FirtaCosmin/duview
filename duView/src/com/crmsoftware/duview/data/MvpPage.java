package com.crmsoftware.duview.data;

import org.json.JSONException;
import org.json.JSONObject;

public class MvpPage<TemplateType>  extends Object implements IJsonParser
{
	
	private PagHeader pagHeader;
	private Class classType;

	private MvpList<TemplateType> listItems = new MvpList<TemplateType>(classType);
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */    
    public MvpPage(Class classType, Class classPagHeader)
    {
    	this.classType = classType;
    	try 
    	{
			this.pagHeader = (PagHeader) classPagHeader.newInstance();
		} 
    	catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	listItems = new MvpList<TemplateType>(classType);
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */   
    public TemplateType getItem(int index)
    {
    	if (index>=0 && index <listItems.size())
    	{
    		return listItems.getItem(index);
    	}
    	return null;
	}
    
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */   
    public void addItem(TemplateType item)
    {
    	listItems.add(item);
	}
    
    
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */   
    public void removeItem(int index)
    {
    	if (index>=0 && index <listItems.size())
    	{
    		listItems.remove(index);
    	}
	}
    
    
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */   
    public int size()
    {
    	return listItems.size();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */   
    public int getTotalResults()
    {
    	return pagHeader.getTotalResults();
	}
    
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */   
    public MvpList<TemplateType> getList()
    {
    	return listItems;
	}
    
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */   
    public void setTotalResults(int value)
    {
    	pagHeader.setTotalResults(value);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */    
    public void fromJson(JSONObject jsonObj, String tag) throws JSONException, InstantiationException, IllegalAccessException
    {
    	String itemListTag = pagHeader.getListTag();
    	if (jsonObj.has(itemListTag))
    	{
    		pagHeader.fromJson(jsonObj,tag);
	        listItems.fromJson(jsonObj,itemListTag);
    	}
    	else // in case of server 1 element stupidity 
    	{
    		pagHeader.setTotalResults(1);
    		pagHeader.setStartIndex(1);
    		
    		TemplateType instance = (TemplateType) classType.newInstance();
            listItems.add(instance);
    	}
    }
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */
	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {
		
		pagHeader.toJson(jsonObj, tag);		
		listItems.toJson(jsonObj, pagHeader.getListTag());
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public String toJsonString() {
		
		JSONObject jsonObj = new JSONObject();
		
		try 
		{
			toJson(jsonObj, null);
			
		} catch (Exception e) {
			
			return "{error}";
		}
		
		return jsonObj.toString();
	}
}
