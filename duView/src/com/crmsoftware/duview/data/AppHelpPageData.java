package com.crmsoftware.duview.data;

public class AppHelpPageData {

	private String imageFile;

	private int descriptionId;

	public AppHelpPageData(String imageFile, int descriptionid)
	{
		this.imageFile = imageFile;
		this.descriptionId = descriptionid;
	}
	
	
	public String getImageFile() {
		return imageFile;
	}

	public void setImageFile(String imageFile) {
		this.imageFile = imageFile;
	}
	
	
	public int getDescriptionId() {
		return descriptionId;
	}
	
	
	public void setDescriptionId(int descriptionId) {
		this.descriptionId = descriptionId;
	}


}
