package com.crmsoftware.duview.data;

//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************

import org.json.JSONException;
import org.json.JSONObject;

import com.crmsoftware.duview.configuration.AppSettings;
import com.crmsoftware.duview.utils.Utils;

public class DuAppsItem extends Object implements IJsonParser {
	
	public static final String TAG_APPID = "appId";
	public static final String TAG_TITLE = "title";
	public static final String TAG_TITLEARABIC = "titleArabic";
	public static final String TAG_SHORTDESC = "shortDesc";
	public static final String TAG_SHORTDESCARABIC = "shortDescArabic";
	public static final String TAG_DESCRIPTION = "description";
	public static final String TAG_DESCRIPTIONARABIC = "descriptionArabic";
	public static final String TAG_IMAGEPATH = "imagePath";
	public static final String TAG_EXTERNALLINK = "externalLink";
	public static final String TAG_ISACTIVE = "isActive";
	public static final String TAG_FBSHARE = "fbShare";
	public static final String TAG_CREATEDATE = "createdDate";
	public static final String TAG_UPDATEDDATE = "updatedDate";


	
	protected int appId;
	protected String title[] = new String[AppSettings.LANG_COUNT];
	protected String shortDesc[] = new String[AppSettings.LANG_COUNT];;
	protected String description[] = new String[AppSettings.LANG_COUNT];
	protected String imagePath;
	protected String externalLink;
	protected int isActive;
	protected String fbShare;
	protected String createDate;
	protected String updateDate;

	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	@Override
	public void fromJson(JSONObject jsonObj,String tag) throws JSONException, InstantiationException, IllegalAccessException {
		
		appId = Utils.getJSONAsInt(jsonObj,TAG_APPID);
		title[AppSettings.LANG_ENGLISH] = Utils.getJSONAsString(jsonObj,TAG_TITLE);
		title[AppSettings.LANG_ARABIC] = Utils.getJSONAsString(jsonObj,TAG_TITLEARABIC);
		description[AppSettings.LANG_ENGLISH] = Utils.getJSONAsString(jsonObj,TAG_DESCRIPTION);
		description[AppSettings.LANG_ARABIC] = Utils.getJSONAsString(jsonObj,TAG_DESCRIPTIONARABIC);
		shortDesc[AppSettings.LANG_ENGLISH] = Utils.getJSONAsString(jsonObj,TAG_SHORTDESC);
		shortDesc[AppSettings.LANG_ARABIC] = Utils.getJSONAsString(jsonObj,TAG_SHORTDESCARABIC);
		imagePath = Utils.getJSONAsString(jsonObj,TAG_IMAGEPATH);
		fbShare = Utils.getJSONAsString(jsonObj,TAG_FBSHARE);
		isActive = Utils.getJSONAsInt(jsonObj,TAG_ISACTIVE);
		externalLink = Utils.getJSONAsString(jsonObj,TAG_EXTERNALLINK);
		createDate = Utils.getJSONAsString(jsonObj,TAG_CREATEDATE);
		updateDate = Utils.getJSONAsString(jsonObj,TAG_UPDATEDDATE);
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {

		Utils.setJSONAsInt(jsonObj,TAG_APPID,appId);
		Utils.setJSONAsString(jsonObj,TAG_TITLE, title[AppSettings.LANG_ENGLISH]);
		Utils.setJSONAsString(jsonObj,TAG_TITLEARABIC,title[AppSettings.LANG_ARABIC]);
		Utils.setJSONAsString(jsonObj,TAG_DESCRIPTION,description[AppSettings.LANG_ENGLISH]);
		Utils.setJSONAsString(jsonObj,TAG_DESCRIPTIONARABIC,description[AppSettings.LANG_ARABIC]);
		Utils.setJSONAsString(jsonObj,TAG_SHORTDESC, shortDesc[AppSettings.LANG_ENGLISH]);
		Utils.setJSONAsString(jsonObj,TAG_SHORTDESCARABIC, shortDesc[AppSettings.LANG_ARABIC]);
		Utils.setJSONAsString(jsonObj,TAG_IMAGEPATH, imagePath);
		Utils.setJSONAsString(jsonObj,TAG_FBSHARE, fbShare);
		Utils.setJSONAsInt(jsonObj,TAG_ISACTIVE, isActive);
		Utils.setJSONAsString(jsonObj,TAG_EXTERNALLINK, externalLink);
		Utils.setJSONAsString(jsonObj,TAG_CREATEDATE, createDate);
		Utils.setJSONAsString(jsonObj,TAG_UPDATEDDATE, updateDate);
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public String toJsonString() {
		
		JSONObject jsonObj = new JSONObject();
		
		try 
		{
			toJson(jsonObj, null);
			
		} catch (Exception e) {
			
			return "{error}";
		}
		
		return jsonObj.toString();
	}


	public int getAppId() {
		return appId;
	}

	public String[] getTitle() {
		return title;
	}

	public String[] getShortDesc() {
		return shortDesc;
	}

	public String[] getDescription() {
		return description;
	}

	public String getImagePath() {
		return imagePath;
	}

	public String getExternalLink() {
		return externalLink;
	}

	public int getIsActive() {
		return isActive;
	}

	public String getFbShare() {
		return fbShare;
	}

	public String getCreateDate() {
		return createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}
	
}
