package com.crmsoftware.duview.data;

import org.json.JSONException;
import org.json.JSONObject;

import com.crmsoftware.duview.utils.Utils;

public class MpxPagHeader extends PagHeader
{
	public static final String TAG_ENTRIES = "entries";
	public static final String TAG_TOTALRESULTS = "totalResults";
	public static final String TAG_STARTINDEX = "startIndex";
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */  
	@Override
	public void fromJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {

		totalResults = Utils.getJSONAsInt(jsonObj,TAG_TOTALRESULTS);
		startIndex = Utils.getJSONAsInt(jsonObj,TAG_STARTINDEX);
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */  
	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {

		Utils.setJSONAsInt(jsonObj, TAG_TOTALRESULTS, totalResults);
		Utils.setJSONAsInt(jsonObj, TAG_STARTINDEX, startIndex);
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */  
	@Override
	public String getListTag() 
	{

		return TAG_ENTRIES;
	}

}
