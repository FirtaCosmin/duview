package com.crmsoftware.duview.data;

import org.json.JSONException;
import org.json.JSONObject;

import com.crmsoftware.duview.utils.IConst;
import com.crmsoftware.duview.utils.Utils;

public class MvpCategories implements IJsonParser {
	public static final String TAG_NAME		 = "name";
	public static final String TAG_SCHEME 	 = "scheme";
	
	private String name;
	private String scheme;
	
	public String getName() {
		return name;
	}
	
	public String getScheme() {
		return scheme;
	}
	
	@Override
	public void fromJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {
		name = Utils.getJSONAsString(jsonObj, TAG_NAME);
		scheme = Utils.getJSONAsString(jsonObj, TAG_SCHEME);
		
	}

	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {
	
		Utils.setJSONAsString(jsonObj, TAG_NAME, name);
		Utils.setJSONAsString(jsonObj, TAG_SCHEME, scheme);
	}

	@Override
	public String toJsonString() {
		JSONObject jsonObj = new JSONObject();
		
		try 
		{
			toJson(jsonObj, null);
			
		} catch (Exception e) {
			
			return "{error}";
		}
		
		return jsonObj.toString();
	}



}
