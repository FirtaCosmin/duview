package com.crmsoftware.duview.data;
//******************************************************************************************
//Description:  file created for duView - CRM project
// Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.json.JSONException;
import org.json.JSONObject;

import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;

public class MvpChannelPrograms implements IJsonParser {
	public static boolean VERBOUSE = Log.VERBOUSE ; 
	public final static String TAG = MvpChannelPrograms.class.getSimpleName();
	
	public static String NO_PROG_INFO="";
	
	public static final String TAG_PROGRAM = "Program";
	public static final String TAG_CHANNEL = "Channel";
	private MvpList<MvpProgram> listItems = new MvpList<MvpProgram>(MvpProgram.class);
	private int[] cellWidths; 
	
	long endOfDayTime = 0;
	long startOfDayTime = 0;
	
	private TimeZone timeZone = Calendar.getInstance().getTimeZone();
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	@Override
	public void fromJson(JSONObject jsonObj, String tag) throws JSONException, InstantiationException, IllegalAccessException 
	{
		JSONObject channel = jsonObj.getJSONObject(TAG_CHANNEL);
		if (null != channel)
		{
			listItems.fromJson(channel, TAG_PROGRAM);			
		}
	}

	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {
		JSONObject channel = new JSONObject();
		
		listItems.toJson(channel, TAG_PROGRAM);
		
		jsonObj.put(TAG_CHANNEL, channel);
	}
	
	public int getCount()
	{
		return listItems.size();
	}
	
	public MvpProgram getProgram(int position)
	{
		if (position <0 || position >= listItems.size())
		{
			return null;
		}
		return listItems.getItem(position);
	}
	
	
	public void addProgram(MvpProgram program)
	{
		listItems.add(program);
	}

	@Override
	public String toJsonString() 
	{
		JSONObject jsonObj = new JSONObject();
		
		try 
		{
			toJson(jsonObj, null);
			
		} catch (Exception e) {
			
			return "{error}";
		}
		
		return jsonObj.toString();
	}
	
	public int getCellSize(int pos)
	{
		if (null == cellWidths)
		{
			return 1;
		}
		
		return cellWidths[pos];
	}
	
	public void generateTimeline(Date date, long timeLineWidth)
	{
		int count = getCount();
		cellWidths = new int[count];

		if (VERBOUSE) Log.d(TAG, " timeline=" + timeLineWidth  );

		Double width = new Double(0);
		for (int i=0; i<count; i++)
		{

			MvpProgram program = getProgram(i);
			
			String title = program.getTitle();
			if (null == title)
			{
				program.setId(null);
			}
			else
			{
				if (NO_PROG_INFO.toUpperCase().equals(title.toUpperCase()))
				{
					program.setId(null);
					program.setTitle(null);
				}
			}
			
			int offset = timeZone.getOffset(program.getStartTime());
			program.setStartTime(program.getStartTime() + offset);
			program.setEndTime(program.getEndTime() + offset);
			
			long duration = (program.getEndTime() - program.getStartTime())/1000;
			if (duration > 0)
			{
				width =  (double)(duration * timeLineWidth)/(double)(24*60*60);
			}
			
			cellWidths[i] = Utils.roundToInt(width);

			
			if (VERBOUSE) Log.d(TAG,"i= " + i + " w=" + width + "("+cellWidths[i]+") sec=" +duration  +"  title = " + program.getTitle() + " " + program.printSchedule() );
		}
	}
	
	
	public void processPrograms(Date date)
	{
		
		Calendar startTime = Calendar.getInstance();
		startTime.setTime(date); 
		startTime.set(Calendar.HOUR_OF_DAY, 0);
		startTime.set(Calendar.MINUTE, 0);
		startTime.set(Calendar.SECOND, 0);
		startTime.set(Calendar.MILLISECOND, 0);	

		
		Calendar endTime = Calendar.getInstance();
		endTime.setTime(date); 
		endTime.set(Calendar.HOUR_OF_DAY, 23);
		endTime.set(Calendar.MINUTE, 59);
		endTime.set(Calendar.SECOND, 59);
		endTime.set(Calendar.MILLISECOND, 0);	

		int offset = Calendar.getInstance().get(Calendar.ZONE_OFFSET) + Calendar.getInstance().get(Calendar.DST_OFFSET);
		endOfDayTime = endTime.getTimeInMillis() - offset;
		startOfDayTime = startTime.getTimeInMillis() - offset;
		
		
		if (VERBOUSE) Log.d(TAG,"day startTime: " + Utils.dateToString(startOfDayTime, Utils.FORMAT_DT));
		
		if (VERBOUSE) Log.d(TAG,"day endTime: " + Utils.dateToString(endOfDayTime, Utils.FORMAT_DT));
		
		processList();
		checkFirstProgram();
		checkLastProgram();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	private void checkFirstProgram()
	{

		if (getCount() == 0)
		{
			MvpProgram firstProgram = new MvpProgram();
			addProgram(firstProgram);
			firstProgram.setStartTime(startOfDayTime);
			firstProgram.setEndTime(endOfDayTime);
		}	
		
		MvpProgram firstProgram = getProgram(0);
		
		long diffSec = (startOfDayTime - firstProgram.getEndTime())/1000;
		if (diffSec == 0)
		{
			return;
		}
		
		if (diffSec > 0 && firstProgram.getId() != null)
		{
			long progEndTime = firstProgram.getStartTime(); 

			firstProgram = new MvpProgram();
			
			firstProgram.setEndTime(progEndTime);
			listItems.add(0, firstProgram);	

		}
		
		firstProgram.setStartTime(startOfDayTime);
	}
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	private void checkLastProgram()
	{
		int count = getCount();
		MvpProgram lastProgram = getProgram(count - 1);
		
		long endProgramTime = lastProgram.getEndTime();
		
		long diffSec = (endProgramTime - endOfDayTime)/1000;
		if (diffSec == 0)
		{
			return;
		}
		
		
		if (diffSec < 0 && lastProgram.getId() != null)
		{

			long firstProgramTime = lastProgram.getEndTime(); 

			lastProgram = new MvpProgram();
			
			lastProgram.setStartTime(firstProgramTime);
			
			addProgram(lastProgram);

		}
		
		lastProgram.setEndTime(endOfDayTime);

	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	private boolean isValidProgram(MvpProgram program)
	{
		long progDuration = program.getEndTime() - program.getStartTime();
		if (progDuration == 0) // remove is duration 0
		{
			if (VERBOUSE) Log.d(TAG,"removed (duration 0): " + program.getTitle() + "   " + program.getId());
			return false;
		}
		
		if ( program.getStartTime() >= endOfDayTime) // program is in next day
		{
			if (VERBOUSE) Log.d(TAG,"removed (next day): " + program.getTitle() + "   " + program.getId());
			return false;
		}
		
		if ( program.getEndTime() <= startOfDayTime) // program is in prev day
		{
			if (VERBOUSE) Log.d(TAG,"removed (prev day): " + program.getTitle() + "   " + program.getId());
			return false;
		}	
				
		return true;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	private void processList()
	{

		int count = getCount();
		for(int i=count-1;i>=0;i--)
		{
			MvpProgram program = getProgram(i);
			
			if (null == program)
			{
				return;
			}
			
			if (VERBOUSE) Log.d(TAG,"current program:  " + program.getTitle() + "   " + program.printSchedule());
			
			if (false == isValidProgram(program))
			{
				listItems.remove(i);
				i++; // perform re-iteration
				continue;			
			}
			
			MvpProgram prevProgram = getProgram(i-1);
			while (prevProgram != null)
			{				
				if (VERBOUSE) Log.d(TAG,"previous program:  " + prevProgram.getTitle() + "   " + prevProgram.getId());
				
				if (isValidProgram(prevProgram))
				{
					break;
				}
				
				listItems.remove(i-1);
				prevProgram = getProgram(i-1);
			}
			
			if (prevProgram == null)
			{
				return;
			}
			
			long diff = program.getStartTime() - prevProgram.getEndTime();
			if (diff ==0)
			{
				continue;
			}
			
			if (diff < 0) // overlap LAST OVER PREV
			{
				long diffPrev = program.getStartTime() - prevProgram.getStartTime();
				
				if (diffPrev <= 0) // complete overlap (eclipse)
				{
					if (VERBOUSE) Log.d(TAG,"removed prev(eclipsed): " + prevProgram.getTitle() + "   " + prevProgram.getId());
					listItems.remove(i-1);
					i++; // perform re-iteration
				}
				else // truncate previous
				{
					if (VERBOUSE) Log.d(TAG,"truncted prev: " + prevProgram.getTitle() + "   " + prevProgram.getId());
					prevProgram.setEndTime(program.getStartTime());
				}
			}
			else	// gap between them
			{
				if (VERBOUSE) Log.d(TAG,"added gap before " + program.getTitle() + "   " + program.getId());
				MvpProgram gapProgram = new MvpProgram();
				gapProgram.setStartTime(prevProgram.getEndTime());
				gapProgram.setEndTime(program.getStartTime());
				listItems.add(i, gapProgram);
			}
		}
	}
	
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public MvpProgram getCurrentProgram() 
	{
		long currentTime = Calendar.getInstance().getTimeInMillis();
		for (int i = 0 ; i< listItems.size(); i++)
		{
			MvpProgram program = listItems.getItem(i);
			if (program.getStartTime()<= currentTime && currentTime <= program.getEndTime())
			{
				return program;
			}
		}
		
		return null;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public MvpProgram getProgram(String programId) 
	{
		for (int i = 0 ; i< listItems.size(); i++)
		{
			MvpProgram program = listItems.getItem(i);
			if (program.getId().equals(programId))
			{
				return program;
			}
		}
		
		return null;
	}


}
