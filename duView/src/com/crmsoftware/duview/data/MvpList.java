package com.crmsoftware.duview.data;
//******************************************************************************************
//Description:  file created for duView - CRM project
// Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MvpList<TemplateType> extends Object implements IJsonParser{
	
	protected Class<TemplateType> classType;
	
	protected List<TemplateType> listItems = new ArrayList<TemplateType>();
	
	public MvpList(Class<TemplateType> classType) 
	{
		this.classType = classType;
	}
	
	@Override
	public void fromJson(JSONObject jsonObj,String tag) throws JSONException, InstantiationException, IllegalAccessException {
		
		if (false == jsonObj.has(tag))
		{
			return;
		}

		JSONArray jsonArray  = null;
		try 
		{
			jsonArray = jsonObj.getJSONArray(tag);
		} 
		catch (Exception e) 
		{

		}
		
        
        if (null != jsonArray)
        {
	         for(int i = 0; i < jsonArray.length(); i++)
	         {
	               JSONObject jsonItem = jsonArray.getJSONObject(i);
	               IJsonParser instance = (IJsonParser) classType.newInstance();
	               instance.fromJson(jsonItem,null);
	               
	               listItems.add((TemplateType)instance);
	         }
        }
        else
        {
        	JSONObject jsonItem = jsonObj.getJSONObject(tag);
            IJsonParser instance = (IJsonParser) classType.newInstance();
            instance.fromJson(jsonItem,null);
            
            listItems.add((TemplateType)instance);
        }
		
	}
	
	public TemplateType getItem(int index)
	{
		return listItems.get(index);
	}
	
	public void add(TemplateType object)
	{
		listItems.add(object);
	}
	
	public void add(int pos, TemplateType object)
	{
		listItems.add(pos, object);
	}
	
	public void remove(int pos)
	{
		listItems.remove(pos);
	}
	
	public void remove(Object item)
	{
		listItems.remove(item);
	}
	
	
	public int size()
	{
		return listItems.size();
	}

	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {

        JSONArray jsonArray  = new JSONArray ();
        
        if (null != listItems)
        {
	         for(int i = 0; i < listItems.size(); i++)
	         {
	               JSONObject jsonItem = new JSONObject();
	               
	               IJsonParser instance = (IJsonParser) listItems.get(i);
	               
	               instance.toJson(jsonItem, null);
	               
	               jsonArray.put(jsonItem);
	         }
        }
        
        jsonObj.put(tag, jsonArray);
		
	}
	
	@Override
	public String toJsonString() {
		
		JSONObject jsonObj = new JSONObject();
		
		try 
		{
			toJson(jsonObj, null);
			return jsonObj.toString(1);
			
		} catch (Exception e) {
			
			
		}
		
		return "{error}";
	}
}
