package com.crmsoftware.duview.data;

import java.util.Observable;
import java.util.Observer;

import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.ui.vodcategories.TaskGetVodCategories;
import com.crmsoftware.duview.ui.watchnow.TaskGetTvChannels;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;

/**
 * @author firtacosmin
 * @desc this class is created to load and save initial information and data from the server 
 * at app start
 */
public enum MvpSession implements 
Observer,
ITaskReturn{
	/*
	 * this is the singleton instance of the session
	 * */
	INSTANCE;
	
	
	private static String TAG = "MvpSession";
	/*
	 * @desc the life time of a request
	 * 60min life (the data is taken from cache and not updated from server)
	 * */
	public static long DATA_LIFE_TIME = 60*60*1000; // 
	

	/**
	 * @desc this is the MvpObject with the Program channels
	 * It will be initialized with the favorite channels
	 */
	private MvpObject mvpChannelsObj = null;
	/**
	 * @desc this is the task that will send the request for the channels 
	 */
	private TaskGetTvChannels taskGetTvChannels = null;

	
	/**
	 * @desc the constructor of the session
	 */
	private MvpSession(){
		Log.d("MvpSession", " Constructor");
	}
	
	/**
	 * @desc this method will send all the requests that require user authentication 
	 * to buffer initial information.
	 * - favorite channels
	 */
	public void getUserInitialInfo(){
		initFavoriteChannels();
		
	}
	
	/**
	 * @desc this method will gather the information from that server that does not require 
	 * any user authentication.
	 * - what's new  categories
	 */
	public void getNoUserInitialInfo(){
//		initCategoryList();
	}
	
	/**********  Overrides   ***********/
	
	/*
	 * Override from Observer
	 * 
	 * */
	
	@Override
	public void update(Observable observable, Object data) {
	}
	

	/*
	 * Override from ITaskReturn
	 * */
	
	@Override
	public void onReturnFromTask(Task task, boolean canceled) {
		
	}
	
	/*
	 * Private part
	 * */
	
	
	/**
	 * @desc method that sends the request for the categories.
	 */
	private void initCategoryList(){

        TaskGetVodCategories whatsNew = new TaskGetVodCategories(this, MvpObject.newReference(TaskGetVodCategories.OBJPREFIX_CATEGORIES));
        whatsNew.execute();
	}
	
	/**
	 * @desc will initialize the mvpChannelsObj and will send the request to 
	 * populate it with the favorite channels
	 */
	private void initFavoriteChannels(){
		

		Log.d(TAG, TAG+" initFavoriteChannels");
		
		mvpChannelsObj = MvpObject.newReference(Utils.formatLink(TaskGetTvChannels.OBJPREFIX_FILTEREDCHANNELS));
		mvpChannelsObj.addObserver(this);
		getFavoriteChannels();
		
	}
	
	/**
	 * @desc will send the request to get the favorite channels
	 */
	private void getFavoriteChannels(){
		
		Boolean withFav = false;
		
		if ( MvpUserData.getFromMvpObject().isOpenTvWithFavorites() )
		{
			withFav = true;
		}
		
		taskGetTvChannels = new TaskGetTvChannels(this, 
											      mvpChannelsObj, 
											      withFav,
											      DATA_LIFE_TIME);
		taskGetTvChannels.execute();
	}


}
