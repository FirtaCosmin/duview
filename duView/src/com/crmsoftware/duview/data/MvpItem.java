package com.crmsoftware.duview.data;
//******************************************************************************************
//Description:  file created for duView - CRM project
// Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************

import org.json.JSONException;
import org.json.JSONObject;

import com.crmsoftware.duview.utils.Utils;


public class MvpItem extends Object implements IJsonParser {
	
	public static final String TAG_ID = "id";
	public static final String TAG_TITLE = "title";


	
	protected String id;
	protected String title;

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	@Override
	public void fromJson(JSONObject jsonObj,String tag) throws JSONException, InstantiationException, IllegalAccessException {
		
		id = Utils.getJSONAsString(jsonObj, TAG_ID);
		title = Utils.getJSONAsString(jsonObj,TAG_TITLE);
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {
		
		Utils.setJSONAsString(jsonObj, TAG_ID, id);
		Utils.setJSONAsString(jsonObj, TAG_TITLE, title);
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public String toJsonString() {
		
		JSONObject jsonObj = new JSONObject();
		
		try 
		{
			toJson(jsonObj, null);
			
		} catch (Exception e) {
			
			return "{error}";
		}
		
		return jsonObj.toString();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	
}
