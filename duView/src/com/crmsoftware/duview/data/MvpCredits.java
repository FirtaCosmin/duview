package com.crmsoftware.duview.data;

import org.json.JSONException;
import org.json.JSONObject;

import com.crmsoftware.duview.utils.IConst;
import com.crmsoftware.duview.utils.Utils;

public class MvpCredits implements IJsonParser {
	public static final String TAG_ROLE	 = "role";
	public static final String TAG_VALUE 	 = "value";
	
	private String role;
	private String value;
	
	public String getRole() {
		return role;
	}

	public String getValue() {
		return value;
	}
	

	@Override
	public void fromJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {
		
		
		role = Utils.getJSONAsString(jsonObj, TAG_ROLE);
		value = Utils.getJSONAsString(jsonObj, TAG_VALUE);
		
	}

	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {
		
		Utils.setJSONAsString(jsonObj, TAG_ROLE, role);
		Utils.setJSONAsString(jsonObj, TAG_VALUE, value);
	}

	@Override
	public String toJsonString() {
		JSONObject jsonObj = new JSONObject();
		
		try 
		{
			toJson(jsonObj, null);
			
		} catch (Exception e) {
			
			return "{error}";
		}
		
		return jsonObj.toString();
	}
}