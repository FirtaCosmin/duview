package com.crmsoftware.duview.data;

import org.json.JSONException;
import org.json.JSONObject;

import com.crmsoftware.duview.utils.Utils;

public class MvpRatings implements IJsonParser  {
	
	public static final String TAG_RATING = "rating";
	
	private String rating;
	
	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {
		Utils.setJSONAsString(jsonObj, TAG_RATING, rating);
	}
	
	
	@Override
	public void fromJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {
		rating = Utils.getJSONAsString(jsonObj, TAG_RATING);
	}
	
	public String getRating() {
		return rating;
	}


	@Override
	public String toJsonString() {
		JSONObject jsonObj = new JSONObject();
		
		try 
		{
			toJson(jsonObj, null);
			
		} catch (Exception e) {
			
			return "{error}";
		}
		
		return jsonObj.toString();
	}
}
