package com.crmsoftware.duview.data;

import org.json.JSONException;
import org.json.JSONObject;

import com.crmsoftware.duview.utils.Utils;
//******************************************************************************************
//Description:  file created for duView - CRM project
//   Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************
public class MvpCategory extends MvpItem{

	public static final String FULL_TITLE 					 = "plcategory$fullTitle";
	public static final String PL_CATEGORY_SCHEME 	 		 = "plcategory$scheme";
	public static final String PL_CATEGORY_PARENT_ID 		 = "plcategory$parentId";	
	public static final String ALU_DU_UI_MODE 	 			 = "alu-du$uimode";
	public static final String ALU_DU_LOGO 	 			  	 = "alu-du$logourl_280_120";
	
	private String fullTitle;
	private String categoryScheme;
	private String categoryParentId;
	private String uiMode = "";
	private String premiumLogo = "";
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void fromJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {

		super.fromJson(jsonObj, tag);
		
		fullTitle = Utils.getJSONAsString(jsonObj, FULL_TITLE);
		categoryScheme = Utils.getJSONAsString(jsonObj, PL_CATEGORY_SCHEME);
		categoryParentId = Utils.getJSONAsString(jsonObj, PL_CATEGORY_PARENT_ID);
		uiMode = Utils.getJSONAsString(jsonObj, ALU_DU_UI_MODE);
		premiumLogo = Utils.getJSONAsString(jsonObj, ALU_DU_LOGO);
		
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {

		super.toJson(jsonObj, tag);
		
		Utils.setJSONAsString(jsonObj, FULL_TITLE, fullTitle);
		Utils.setJSONAsString(jsonObj, PL_CATEGORY_SCHEME, categoryScheme);
		Utils.setJSONAsString(jsonObj, PL_CATEGORY_PARENT_ID, categoryParentId);
		Utils.setJSONAsString(jsonObj, ALU_DU_UI_MODE, uiMode);		
		Utils.setJSONAsString(jsonObj, ALU_DU_LOGO, premiumLogo);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public String getFullTitle() {
		return fullTitle;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void setScheme(String plCategoryScheme) {
		this.categoryScheme = plCategoryScheme;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public String getScheme() {
		return categoryScheme;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void setFullTitle(String fullTitle) {
		this.fullTitle = fullTitle;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public String getParentId() {
		return categoryParentId;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void setPrentId(String plCategoryParentId) {
		this.categoryParentId = plCategoryParentId;
	}
		
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public String getUiMode() {
		return uiMode;
	}


	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public String getPremiumLogo() {
		return premiumLogo;
	}

}
