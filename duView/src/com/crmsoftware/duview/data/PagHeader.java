package com.crmsoftware.duview.data;

import org.json.JSONObject;

public abstract class PagHeader implements IJsonParser // this class is base class for each pagiantion header Mpx or Mvp
{
	public int totalResults = -1;
	public int startIndex = -1;
	
	abstract public String getListTag();
	
	public int getTotalResults() {
		return totalResults;
	}

	public void setTotalResults(int totalResults) {
		this.totalResults = totalResults;
	}

	public int getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}
	
	@Override
	public String toJsonString() 
	{
		JSONObject jsonObj = new JSONObject();
		
		try 
		{
			toJson(jsonObj, null);
			
		} catch (Exception e) {
			
			return "{error}";
		}
		
		return jsonObj.toString();
	}
}
