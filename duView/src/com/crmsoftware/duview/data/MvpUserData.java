package com.crmsoftware.duview.data;
//******************************************************************************************
//Description:  file created for duView - CRM project
// Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************

import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.ui.AuthFragment;
import com.crmsoftware.duview.ui.apphelp.AppHelpFragment;
import com.crmsoftware.duview.utils.Utils;


public class MvpUserData extends Object implements IJsonParser {
	
	public static final String TAG_SIGN_IN_RESPONSE = "signInResponse";
	public static final String TAG_USER_NAME = "userName";
	public static final String TAG_USER_ID = "userId";
	public static final String TAG_DURATION = "duration";
	public static final String TAG_IDLE_TIMEOUT = "idleTimeout";
	public static final String TAG_TOKEN = "token";
	public static final String TAG_LOGO = "logo";
	public static final String TAG_HELP = "help";
	public static final String TAG_REMEMBER = "remember";
	public static final String TAG_ANONYMOUS = "anonymous";
	public static final String FAVORITES_ENABLED = "FAVORITES_ENABLED";
	
	public static final String IS_FILTER_ACTIVE = "is_filter_active";
	
	
	public final static String MVP_AUTH_USER = "MvpAuthUser";
	public final static String MVP_AUTH_RETRIEVAL_TIME = "MvpAuthRetrievalTime";
	
	/*the tags for the configuration data*/
	private final static String TAG_CONFIG_FASTPLAYER = "FastPlayer";


	




	private String userName = "";
	private String authUserName = "";
	private String userId = "";
	private String token = "";
	private String logo = "";
	private boolean help = true;
	private boolean remember = false;
	private boolean anonymous = true;
	/*if this is the first time the user opens TV tab then it 
	 * will open with favorites channels*/
	private Boolean openTvWithFavorites = true;
	
	private boolean isFilter = false;
	
	

	/*values for the configuration fields*/
	/**
	 * @desc This variable tells the application if it will use the 
	 *       Fast Loading in the player -> if the player will load when gone 
	 *       into full screen or not.
	 *       
	 */
	private boolean fastPlayer = true;




	private long tokenLifeTime = 0;
	private long tokenRetrivalTime = 0;
	private long duration = 0;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}


	public long getTokenRetrivalTime() {
		return tokenRetrivalTime;
	}

	public void setTokenRetrivalTime(long tokenRetrivalTime) {
		this.tokenRetrivalTime = tokenRetrivalTime;
	}

	public void setTokenRetrivalTime() {
		Date data = new Date();
		this.tokenRetrivalTime = data.getTime();
	}
	
	public String getAuthUserName() {
		return authUserName;
	}

	public void setAuthUserName(String username) {
		this.authUserName = username;
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(MainApp.getContext());
		logo = settings.getString(authUserName.trim().toUpperCase() + TAG_LOGO, "");
	}
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public long getTokenLifeTime() {
		return tokenLifeTime;
	}

	public void setLifeTime(long tokenExpTime) {
		this.tokenLifeTime = tokenExpTime;
	}
	
	public boolean isFastPlayer() {
		return fastPlayer;
	}

	public void setFastPlayer(boolean fastPlayer) {
		this.fastPlayer = fastPlayer;
		updateBooleanPreference(TAG_CONFIG_FASTPLAYER, fastPlayer);
	}


	public boolean isOpenTvWithFavorites() {
		return openTvWithFavorites;
	}

	public void setOpenTvWithFavorites(boolean openTvWithFavorites) {
		this.openTvWithFavorites = openTvWithFavorites;

        updateBooleanPreference(FAVORITES_ENABLED, openTvWithFavorites);
	}
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	public static MvpUserData getFromMvpObject()
	{
		MvpObject mvpObjAuthData = MvpObject.newReference(AuthFragment.MVP_AUTH_DATA);
		return (MvpUserData) mvpObjAuthData.getData();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	public void saveToMvpObject()
	{
		MvpObject mvpObjAuthData = MvpObject.newReference(AuthFragment.MVP_AUTH_DATA);
		mvpObjAuthData.setData(this);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	@Override
	public void fromJson(JSONObject jsonObj,String tag) throws JSONException, InstantiationException, IllegalAccessException {
		
		JSONObject jsonSign = jsonObj.getJSONObject(TAG_SIGN_IN_RESPONSE);
		
		if (null != jsonSign)
		{
			userName = Utils.getJSONAsString(jsonSign, TAG_USER_NAME);
			userId = Utils.getJSONAsString(jsonSign, TAG_USER_ID);
			token = Utils.getJSONAsString(jsonSign, TAG_TOKEN);
			tokenLifeTime = Utils.getJSONAsLong(jsonSign, TAG_IDLE_TIMEOUT);	
			duration = Utils.getJSONAsLong(jsonSign, TAG_DURATION);	
			
		}
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {
		
		JSONObject jsonSign = new JSONObject();
		
		 Utils.setJSONAsString(jsonSign, TAG_USER_NAME,userName);
		 Utils.setJSONAsString(jsonSign, TAG_USER_ID,userId);
		 Utils.setJSONAsString(jsonSign, TAG_TOKEN,token);
		 Utils.setJSONAsLong(jsonSign, TAG_IDLE_TIMEOUT,tokenLifeTime);	
		 Utils.setJSONAsLong(jsonSign, TAG_DURATION,duration);	

		 jsonObj.put(TAG_SIGN_IN_RESPONSE, jsonSign);
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public String toJsonString() {
		
		JSONObject jsonObj = new JSONObject();
		
		try 
		{
			toJson(jsonObj, null);
			
		} catch (Exception e) {
			
			return "{error}";
		}
		
		return jsonObj.toString();
	}
	
	
	public Boolean getHelp() {
		return help;
	}

	public void setHelp(Boolean help) 
	{
		this.help = help;
		/*save the new value to preference*/
        updateBooleanPreference(authUserName.trim().toUpperCase() +TAG_HELP, help);
		
	}
	
	public Boolean isAnonymous() 
	{
		return anonymous;
	}

	public void setAnonymous(boolean anonymous)
	{
		this.anonymous = anonymous;
		/*save the new value to preference*/
		updateBooleanPreference(TAG_ANONYMOUS, anonymous);
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) 
	{
		this.logo = logo;

		/*save the new value to preference*/
        updateStringPreference(authUserName.trim().toUpperCase() +TAG_LOGO, logo);
	}
	
	

	public boolean isRemember() {
		return remember;
	}

	public void setRemember(boolean remember) {
		this.remember = remember;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void initFromPref()
	{	
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(MainApp.getContext());
	
		authUserName = settings.getString(MVP_AUTH_USER, "");
		userName = settings.getString(TAG_USER_NAME, "");
		userId = settings.getString(TAG_USER_ID, "");
		token = settings.getString(TAG_TOKEN, "");
		tokenLifeTime = settings.getLong(TAG_IDLE_TIMEOUT, 0);
		tokenRetrivalTime = settings.getLong(MVP_AUTH_RETRIEVAL_TIME, 0);
		duration = settings.getLong(TAG_DURATION, 0);		
		logo = settings.getString(authUserName.trim().toUpperCase() + TAG_LOGO, "");
		help = settings.getBoolean(authUserName.trim().toUpperCase() + TAG_HELP, true);
		remember = settings.getBoolean(TAG_REMEMBER, false);
		fastPlayer = settings.getBoolean(TAG_CONFIG_FASTPLAYER, true);
		anonymous = settings.getBoolean(TAG_ANONYMOUS, true);
		openTvWithFavorites = settings.getBoolean(FAVORITES_ENABLED, true);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void saveAuthDataToPref()
	{	
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(MainApp.getContext());
        SharedPreferences.Editor editor = settings.edit();
        
        editor.putString(MVP_AUTH_USER, authUserName);
        editor.putString(TAG_USER_NAME, userName);
        editor.putString(TAG_USER_ID, userId);
        editor.putString(TAG_TOKEN, token);
        
        editor.putLong(TAG_IDLE_TIMEOUT, tokenLifeTime);
        editor.putLong(MVP_AUTH_RETRIEVAL_TIME, tokenRetrivalTime);	
        editor.putLong(TAG_DURATION, duration);
        editor.putBoolean(TAG_REMEMBER, remember);
        
        editor.putBoolean(TAG_CONFIG_FASTPLAYER, fastPlayer);
        
        editor.putBoolean(TAG_ANONYMOUS, anonymous);
        
        editor.putBoolean(FAVORITES_ENABLED, openTvWithFavorites);
        
        editor.commit();
        
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void removeFromPrefToken()
	{	
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(MainApp.getContext());
        SharedPreferences.Editor editor = settings.edit();


        editor.remove(TAG_TOKEN);
        editor.remove(TAG_IDLE_TIMEOUT);
        editor.remove(MVP_AUTH_RETRIEVAL_TIME);
        editor.remove(TAG_DURATION);
       
        updateBooleanPreference(TAG_ANONYMOUS, anonymous);
        
        editor.commit();
        
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void removeFromPrefAll()
	{	
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(MainApp.getContext());
        SharedPreferences.Editor editor = settings.edit();

        editor.remove(MVP_AUTH_USER);
        editor.remove(TAG_USER_NAME);
        editor.remove(TAG_TOKEN);
        editor.remove(TAG_IDLE_TIMEOUT);
        editor.remove(MVP_AUTH_RETRIEVAL_TIME);
        editor.remove(TAG_DURATION);       
        editor.remove(TAG_REMEMBER); 
        editor.remove(TAG_CONFIG_FASTPLAYER);
        editor.remove(FAVORITES_ENABLED);
        
        updateBooleanPreference(TAG_ANONYMOUS, anonymous);
        
        
        editor.commit();
        
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public boolean isTokenExpired()
	{
		if (token.isEmpty())
		{
			return true;
		}
		
		if (tokenLifeTime == 0)
		{
			return true;
		}
		
		Date date = new Date();
		long tokenTime = tokenRetrivalTime + duration;
		long currentTime = date.getTime();
		if (tokenTime < currentTime)
		{
			return true;
		}
		
		return false;
	}	
	
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void signOut()
	{
		userId = "";
		userName = "";
		token = "";
		tokenLifeTime = 0;
		tokenRetrivalTime = 0;
		duration = 0;
		setAnonymous(false);
		/*
		 * set it to false so if a new login will be
		 *  done the help dialog will be displayed
		 */
		AppHelpFragment.tempDisabled = false;
		saveAuthDataToPref();
	}
	
	
	/**
	 * @param tag
	 * @param value
	 * @desc method will get the shared preferences with the apps context 
	 * and will put into it the passed tag<->value as string
	 */
	private void updateStringPreference(String tag, String value){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(MainApp.getContext());
        SharedPreferences.Editor editor = settings.edit();

        editor.putString(tag, value);     
        editor.commit();
	}
	/**
	 * @param tag
	 * @param value
	 * @desc method will get the shared preferences with the apps context 
	 * and will put into it the passed tag<->value as Boolean
	 */
	private void updateBooleanPreference(String tag, Boolean value){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(MainApp.getContext());
        SharedPreferences.Editor editor = settings.edit();

        editor.putBoolean(tag, value);     
        editor.commit();
	}

	public boolean isFilter() {
		return isFilter;
	}

	public void setFilter(boolean isFilter) {
		this.isFilter = isFilter;
	}
	
}
