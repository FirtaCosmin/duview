package com.crmsoftware.duview.data;
//******************************************************************************************
//Description:  file created for duView - CRM project
// Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.crmsoftware.duview.utils.IConst;
import com.crmsoftware.duview.utils.IHttpConst;
import com.crmsoftware.duview.utils.Utils;

public class MvpMedia extends MvpItem {

	
	public static final String TAG_CONTENT = "content";
	
	public static final String TAG_THUMBNAILS = "thumbnails";
	public static final String TAG_RATINGS = "ratings";
	public static final String TAG_CATEGORIES = "categories";
	public static final String TAG_CREDITS = "credits";
	public static final String TAG_YEAR = "alu-commerce$releaseYear";
	public static final String TAG_DESCRIPTION = "description";
	public static final String TAG_SERIESNAME = "alu-du$seriesName";
	public static final String TAG_EPISODESCOUNT = "alu-du$episodeNumber";
	public static final String TAG_PARENTALCONTROL = "alu-parentalcontrol$ageRating";
	
	private static String ACTOR = "Actor";
	private static String DIRECTOR = "Director";
	private static String SERIES = "series/";
	private static String CATEGORY_VOD_TAG = "category_VoD";
	
	
	private MvpContent thumbPoster = new MvpContent();;
	private MvpContent thumbLand = new MvpContent();
	private MvpContent detailsLand = new MvpContent();
	
	private MvpContent content = new MvpContent();
	private MvpRatings ratings = new MvpRatings();
	
	private MvpList<MvpCredits> listCredits =  new MvpList<MvpCredits>(MvpCredits.class);
    
	private MvpList<MvpCategories> listCategory =  new MvpList<MvpCategories>(MvpCategories.class);

	
	private String genres = "";
	private String actors = "";
	private String directors = "";
	private String releaseYear = "";
	private String description="";
	private String seriesName="";
	private int episodesCount=0;
	private int parentalLevel=0;


	private boolean isSelected = false;
	private boolean isEpisode = false;

	public static long sizeLogo = 300*300; 	// channel logo hack due to platform missalignment 
	
	public static long sizesPortrait[] = {150*215, 300*430, 450*645, 600*860}; 
	public static long sizesLand[] = {200*150, 320*240, 427*320, 640*480};
	public static long sizesLand16_9[] = {320*180, 480*270, 600*338, 960*540, 1280*720};	

	public static int selectedThumbPortrait = 0;
	public static int selectedThumbLand = 0;	
	public static int selectedDetails = 0;
	
	public MvpMedia()
	{

	}
	
	public MvpMedia(String title, String logoUrl, String id)
	{
		this.id = id;
		this.title = title;
		this.thumbPoster = new MvpContent();
		thumbPoster.setUrl(logoUrl);
	}


	private void processRoles(MvpList<MvpCredits> lsitCredits) 
	{
		actors = IConst.EMPTY_STRING;
		directors = IConst.EMPTY_STRING;
		
		int count =  lsitCredits.size();
		for (int i=0; i < count; i++)
		{
			MvpCredits item = lsitCredits.getItem(i);
			
			if (ACTOR.equals(item.getRole()))
			{
				actors += actors.length() > 0 ? ',' + IConst.SPACE_CHAR : IConst.EMPTY_STRING;
				actors += item.getValue() ;
				
			}
			
			if (DIRECTOR.equals(item.getRole()))
			{
				directors += directors.length() > 0 ? ',' + IConst.SPACE_CHAR : IConst.EMPTY_STRING;
				directors += item.getValue() ;
				
			}
		}
	}
	


	private void processSchemes(MvpList<MvpCategories> listCategory) 
	{
		genres = IConst.EMPTY_STRING;
		
		int count =  listCategory.size();
		for (int i=0; i < count; i++)
		{
			MvpCategories item = listCategory.getItem(i);
			
			if (IHttpConst.SCHEME_CATEGORY_FILTER.equals(item.getScheme()))
			{
				genres += genres.length() > 0 ? ',' : IConst.EMPTY_STRING;
				genres += item.getName() ;
				continue;
			}
			
			if (item.getName().startsWith(SERIES))
			{
				isEpisode = true;
				continue;
			}
		}
	}	
	

	

	public MvpRatings getRatings() {
		return ratings;
	}

	public MvpContent getThumbPoster() {
		return thumbPoster;
	}

	public void setThumbPoster(MvpContent thumbPoster) {
		this.thumbPoster = thumbPoster;
	}

	public MvpContent getThumbLandscape() {
		return thumbLand;
	}

	public void setThumbLandscape(MvpContent thumbLandscape) {
		this.thumbLand = thumbLandscape;
	}

	public MvpContent getContent() {
		return content;
	}

	public void setContent(MvpContent content) {
		this.content = content;
	}

	public MvpContent getDetailsLandscape() {
		return detailsLand;
	}

	public void setDetailsLandscape(MvpContent detailsLandscape) {
		this.detailsLand = detailsLandscape;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	@Override
	public void fromJson(JSONObject jsonObj, String tag) throws JSONException, InstantiationException, IllegalAccessException {
		
		super.fromJson(jsonObj, tag);

		releaseYear = Utils.getJSONAsString(jsonObj,TAG_YEAR);	
		description = Utils.getJSONAsString(jsonObj,TAG_DESCRIPTION);		
		seriesName = Utils.getJSONAsString(jsonObj,TAG_SERIESNAME);	
		episodesCount = Utils.getJSONAsInt(jsonObj,TAG_EPISODESCOUNT);	
		parentalLevel = Utils.getJSONAsInt(jsonObj,TAG_PARENTALCONTROL);	
		
        JSONArray jsonArrayContent  = jsonObj.getJSONArray(TAG_CONTENT);
        
        if (null != jsonArrayContent)
        {
	         for(int i = 0; i < jsonArrayContent.length(); i++)
	         {
				JSONObject jsonItem = jsonArrayContent.getJSONObject(i);
					 
				MvpContent item = new MvpContent();
				item.fromJson(jsonItem,null);
					 	  
				if (content.width <= item.width)	// keep the higher screen
				{
					content = item;
				}
	         }
        }
                	
        JSONArray jsonArrayThumb  = jsonObj.getJSONArray(TAG_THUMBNAILS);
        
        if (null != jsonArrayThumb)
        {
	         for(int i = 0; i < jsonArrayThumb.length(); i++)
	         {
				JSONObject jsonItem = jsonArrayThumb.getJSONObject(i);
					 
				MvpContent item = new MvpContent();
				item.fromJson(jsonItem, null);
					
				long size = item.width*item.height;
				
				if (size == sizeLogo)		// channel logo hack due to platform missalignment 
				{
					thumbPoster = item;					
				}
				
				if (size == sizesPortrait[selectedThumbPortrait])	
				{
					thumbPoster = item;
				}
				
				if (size == sizesLand16_9[selectedThumbLand])	
				{
					thumbLand = item;
				}
				
				if (size == sizesLand16_9[selectedDetails])	
				{
					detailsLand = item;
				}
	         }
        }
        
        JSONArray jsonArrayRatings  = jsonObj.getJSONArray(TAG_RATINGS);
        
        if (null != jsonArrayRatings)
        {
	         for(int i = 0; i < jsonArrayRatings.length(); i++)
	         {
				JSONObject jsonItem = jsonArrayRatings.getJSONObject(i);
					 
				ratings.fromJson(jsonItem,null);
					 	  
	         }
        }
    	
        
        listCredits.fromJson(jsonObj, TAG_CREDITS);		  
        processRoles(listCredits);
        
    	listCategory.fromJson(jsonObj, TAG_CATEGORIES); 	
    	processSchemes(listCategory);
    
	}

	@Override
	public void toJson(JSONObject jsonObj, String tag) throws JSONException,
			InstantiationException, IllegalAccessException {

		super.toJson(jsonObj, tag);
		
		Utils.setJSONAsString(jsonObj, TAG_YEAR, releaseYear);
		Utils.setJSONAsString(jsonObj, TAG_DESCRIPTION, description);
		Utils.setJSONAsString(jsonObj,TAG_SERIESNAME, seriesName);	
		Utils.setJSONAsInt(jsonObj,TAG_EPISODESCOUNT, episodesCount);	
		Utils.setJSONAsInt(jsonObj,TAG_PARENTALCONTROL, parentalLevel);	
		
		
		JSONArray arrayContent = new JSONArray();

		Utils.putJSONArrayItem(arrayContent, content, null);
		jsonObj.put(TAG_CONTENT, arrayContent);
		
		
		JSONArray arrayThumb = new JSONArray();
		Utils.putJSONArrayItem(arrayThumb, thumbLand, null);
		Utils.putJSONArrayItem(arrayThumb, thumbPoster, null);
		Utils.putJSONArrayItem(arrayThumb, detailsLand, null);
		
		jsonObj.put(TAG_THUMBNAILS, arrayThumb);
		
		JSONArray arrayRating = new JSONArray();
		Utils.putJSONArrayItem(arrayRating, ratings, null);
		jsonObj.put(TAG_RATINGS, arrayRating);
		
		listCategory.toJson(jsonObj, TAG_CATEGORIES);
		
		listCredits.toJson(jsonObj, TAG_CREDITS);
	}
	
	public String getReleaseYear() {
		return releaseYear;
	}

	public void setReleaseYear(String year) {
		this.releaseYear = year;
	}
	
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	
	public boolean getSelected() {
		return isSelected;
	}	
	
	public String getSeriesName() {
		return seriesName;
	}

	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}
	
	public int getEpisodesCount() {
		return episodesCount;
	}

	public void setEpisodesCount(int episodesCount) {
		this.episodesCount = episodesCount;
	}
	
	public int getParentalLevel() {
		return parentalLevel;
	}

	public void setParentalLevel(int parentalLevel) {
		this.parentalLevel = parentalLevel;
	}
	
	public boolean isEpisode() {
		return isEpisode;
	}
	
	public String getGenres() {
		return genres;
	}

	public String getActors() {
		return actors;
	}

	public String getDirectors() {
		return directors;
	}

}
