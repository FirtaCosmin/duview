package com.crmsoftware.duview.data;
//******************************************************************************************
//Description:  file created for duView - CRM project
// Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************

import org.json.JSONException;
import org.json.JSONObject;

public interface IJsonParser 
{
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */
	public void fromJson(JSONObject jsonObj, String tag) throws JSONException, InstantiationException, IllegalAccessException;
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */
	public void toJson(JSONObject jsonObj, String tag) throws JSONException, InstantiationException, IllegalAccessException;
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */
	public  String  toJsonString();
}
