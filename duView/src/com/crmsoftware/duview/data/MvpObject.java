package com.crmsoftware.duview.data;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;

import android.graphics.Bitmap;


//******************************************************************************************
//Description:  file created for duView - CRM project
// Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************


import com.crmsoftware.duview.ui.AuthFragment;
import com.crmsoftware.duview.utils.Log;


public class MvpObject  extends Observable {
	public static boolean DEBUG = Log.DEBUG; 
	public final static String TAG = MvpObject.class.getSimpleName();	
	
	private String id;
	private Object data = null;
	private long lastUpdateTime = 0;
	private long lastAccessTime = 0;
	
	public static Map<String, MvpObject> mapObjects =  Collections.synchronizedMap(new HashMap<String, MvpObject>());
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public static MvpObject getReference(String id)
	{
		synchronized (mapObjects) 
		{
			return mapObjects.get(id);
		}
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public static MvpObject newReference(String id)
	{
		MvpObject obj = null;
		synchronized (mapObjects) 
		{
			obj = mapObjects.get(id);
			
			if (null == obj)
			{
				if (DEBUG) Log.d(TAG, " *** new MVPOBJ: " + id);
				obj = new MvpObject();
				obj.id = id;
				mapObjects.put(id,obj);
			}
		}
		
		return obj;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public static void addReference(String id, MvpObject obj)
	{

		synchronized (mapObjects) 
		{
			if (null == obj)
			{
				return;
			}
				if (DEBUG) Log.d(TAG, " *** add MVPOBJ: " + id);

				mapObjects.put(id,obj);
			
		}
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public static MvpObject removeReference(String id)
	{
		synchronized (mapObjects) 
		{
			return mapObjects.remove(id);
		}
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void remove()
	{
		synchronized (mapObjects) 
		{
			mapObjects.remove(getId());
		}
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public static void removeAllData()
	{
		synchronized (mapObjects) 
		{
			MvpObject mvpObjAuthData = MvpObject.newReference(AuthFragment.MVP_AUTH_DATA);
			mapObjects.clear();
			mapObjects.put(AuthFragment.MVP_AUTH_DATA, mvpObjAuthData);
		}
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public static void cleanBitmaps()
	{
		synchronized (mapObjects) 
		{
			int i=0;
		   for(Iterator<Entry<String, MvpObject>> it = mapObjects.entrySet().iterator(); it.hasNext(); i++ ) 
		   {
		      Map.Entry<String, MvpObject> entry = it.next();
		      
		      
				if (DEBUG) Log.d(TAG, i+ ". mvpObj = " + entry.getKey());
				MvpObject mvpObject = (MvpObject) entry.getValue();
			    Object objData = mvpObject.getData();

			    if (objData instanceof Bitmap)
			    {
				    mvpObject.setData(null);
				    it.remove();
			    	Bitmap bmp = (Bitmap) objData;
			    	
		    		synchronized (bmp) 
		    		{
		    			if (false == bmp.isRecycled())
		    			{
		    				//bmp.recycle();
		    			}
					}
			    }
			    
		    }

		}	
		System.gc();
	}
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public static MvpObject newReference(String id, Class classType)
	{
		MvpObject obj = newReference(id);
		
		if (null == obj.getData())
		{
			try 
			{
				obj.setData(classType.newInstance());
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return obj;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public static void deleteInstance(String id)
	{
		synchronized (mapObjects) 
		{
			mapObjects.remove(id);
		}
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */					
	private void MvpObject()
	{

	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */				
	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */
	public Object getData()
	{

		return data;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public String getId()
	{
		return id;
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void setData(Object data)
	{
		this.data = data;
		setChanged();
		notifyObservers();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void setUpdateTime(long time)
	{
		lastUpdateTime = time;
	}
}
