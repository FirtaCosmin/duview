package com.crmsoftware.duview.slidemenu;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.slidemenu.menumodel.MenuItem;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.utils.Log;

public class ExpandableAdapter extends BaseExpandableListAdapter {
	public static boolean DEBUG = Log.DEBUG; 
	public final static String TAG = ExpandableAdapter.class.getSimpleName();
	
	private MenuItem menuRoot;
	
	public ExpandableAdapter(MenuItem menuRoot) {
		
		this.menuRoot = menuRoot;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return menuRoot.getSubMenuItem(groupPosition).getSubMenuItem(childPosition);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return 0;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
			
		if (DEBUG) Log.d(TAG,"groupPosition " + groupPosition + " childPosition " + childPosition);
		MvpViewHolder holder = null;
		if(null != convertView)
		{
			holder = (MvpViewHolder)convertView.getTag();	
		}
		else
		{
			View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_item, parent, false);
			holder = new MvpViewHolder(view);
			view.setTag(holder);
		}

		MenuItem menuItem = (MenuItem) getChild(groupPosition, childPosition);
		
		if (menuItem.isSelected())
		{
			holder.getView().setBackgroundResource(R.color.slide_item_background_active);
		}
		else
		{
			holder.getView().setBackgroundResource(R.color.slide_item_background_normal);	
		}
		
		
		holder.setText(R.id.itemName,menuItem.getName());
		holder.setVisibility(R.id.item_contents_number, View.INVISIBLE);
		
		
		return holder.getView();
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public int getChildrenCount(int groupPosition) {

		return menuRoot.getSubMenuItem(groupPosition).getSubMenuItems().size();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public Object getGroup(int groupPosition) {
		return menuRoot.getSubMenuItem(groupPosition);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public int getGroupCount() {
	  return menuRoot.getSubMenuItems().size();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public long getGroupId(int groupPosition) {
		return 0;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {

		MvpViewHolder holder = null;
		if(null != convertView)
		{
			holder = (MvpViewHolder)convertView.getTag();	
		}
		else
		{
			View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_group_item, parent, false);
			holder = new MvpViewHolder(view);
			view.setTag(holder);
		}
		
		MenuItem menuItem = (MenuItem) getGroup(groupPosition);
		
		if (menuItem.isSelected())
		{
			holder.getView().setBackgroundResource(R.color.slide_item_background_active);
		}
		else
		{
			holder.getView().setBackgroundResource(isExpanded ? R.color.slide_sub_item_background_active : R.color.slide_background);	
		}
		holder.setImage(R.id.arrow, isExpanded ? R.drawable.menu_collapse_arrow : R.drawable.menu_expand_arrow);
		holder.setVisibility(R.id.arrow, menuItem.hasSubItems() ? View.VISIBLE : ViewGroup.INVISIBLE);
			
		holder.setText(R.id.groupName,menuItem.getName());
		holder.setImage(R.id.groupimage,(int) menuItem.getImageId());
		
		
		return holder.getView();

	}

	
	/**
	 * 
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public boolean hasStableIds() {
		return true;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void getSelection(Integer group, Integer item) 
	{
		
	}

}
