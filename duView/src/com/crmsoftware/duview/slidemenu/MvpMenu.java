package com.crmsoftware.duview.slidemenu;

import java.io.File;
import java.util.ArrayList;
import java.util.Vector;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.IMvpActivity;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.configuration.PlatformSettings;
import com.crmsoftware.duview.core.common.HttpReqParams;
import com.crmsoftware.duview.core.common.TaskJsonRequest;
import com.crmsoftware.duview.core.common.TaskSingleReqParse;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.data.MvpParentalManagement;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.slidemenu.imagechooser.ChooserListener;
import com.crmsoftware.duview.slidemenu.imagechooser.ChooserManager;
import com.crmsoftware.duview.slidemenu.imagechooser.ImageChoosen;
import com.crmsoftware.duview.slidemenu.menumodel.MenuItem;
import com.crmsoftware.duview.ui.AboutFragment;
import com.crmsoftware.duview.ui.AuthFragment;
import com.crmsoftware.duview.ui.BrowserFragment;
import com.crmsoftware.duview.ui.ConfigsFragment;
import com.crmsoftware.duview.ui.MvpFragment;
import com.crmsoftware.duview.ui.TermsAndConditionsFragment;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.ui.common.UnderConstrFragment;
import com.crmsoftware.duview.ui.duapps.DuAppsFragment;
import com.crmsoftware.duview.ui.settings.ParentalPinFragment;
import com.crmsoftware.duview.ui.settings.parentalcontrol.ParentalControlFragment;
import com.crmsoftware.duview.ui.vodcategories.TaskGetVodCategories;
import com.crmsoftware.duview.ui.vodcategories.VodCategoriesFragment;
import com.crmsoftware.duview.ui.watchnow.LiveTvFragment;
import com.crmsoftware.duview.utils.IConst;
import com.crmsoftware.duview.utils.IHttpConst;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.LogOffDialogView;

public class MvpMenu 
extends MvpFragment 
implements 
OnGroupClickListener, 
OnChildClickListener, 
OnClickListener, 
ChooserListener
{

	private static final String SELECTION = "selection";
	
	private MenuItem menuRoot;
	private MenuItem selectedMenuItem = null;
    ExpandableAdapter menuAdapter;
    ExpandableListView exList;
    
    private TaskSingleReqParse<MvpUserData> taskGetParentalLevel;
    
    private String filePath;
    private ChooserManager chooserManager;
    private ImageView userImage;
    
    /**
     * @desc vector of Class elements containing the classes of the menu fragments 
     * that contain media products
     */
    private Vector<Class> fragmentsWithContent = new Vector<Class>();
   

	private TextView userName;
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
    public MvpMenu() 
    {
		
	}
    
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = setContentView(R.layout.slidemenu_main, container, inflater) ;
		MvpViewHolder holder = getHolder();

		initData();
		
//		getServerParentalLevel();
		holder.setClickListener(this, R.id.log_out_in, R.id.user_image);

		userImage = (ImageView) holder.getView(R.id.user_image_value);
		userImage.setScaleType(ScaleType.CENTER_INSIDE);
		exList = (ExpandableListView) holder.getView(R.id.expandableListView1);
	        exList.setIndicatorBounds(5, 5);
	        menuAdapter = new ExpandableAdapter(menuRoot);
	        exList.setIndicatorBounds(0, 20);
	        exList.setChildIndicatorBounds(100, 20);
	        exList.setAdapter(menuAdapter);
	        exList.setOnChildClickListener(this);
	        exList.setOnGroupClickListener(this);
	    

	    update();
	    
	    userName = (TextView) holder.getView(R.id.user_name);
	    userName.setEllipsize(TextUtils.TruncateAt.MARQUEE);
	    userName.setSingleLine(true);
	    userName.setMarqueeRepeatLimit(5);
	    userName.setSelected(true);
	    
	    if (savedInstanceState != null)
	    {
	    	ArrayList<Integer> path = savedInstanceState.getIntegerArrayList(SELECTION);
	    	if (null != path)
	    	{
		    	selectedMenuItem = menuRoot.getMenuFromPath(path);
		    	selectMenuItem(selectedMenuItem, false);
	    	}
	    }
	    
		return view;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onResume() 
	{
		update();
		super.onResume();
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onResetData() {
		
		if (null == exList)
		{
			return;
		}
		
		int count =  menuAdapter.getGroupCount();
		for (int i = 0; i <count ; i++)
		{
			exList.collapseGroup(i);
		}
		
		super.onResetData();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onDestroyView() {
		
		exList = null;
		
		super.onDestroyView();
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void update()
	{
		MvpViewHolder holder = getHolder();
		
		MvpObject mvpAuthObj = MvpObject.newReference(AuthFragment.MVP_AUTH_DATA);
		MvpUserData data = (MvpUserData)mvpAuthObj.getData();
		
		if (null != data)
		{
			holder.setText(R.id.user_name, data.isAnonymous() ? MainApp.getRes().getString(R.string.anonymous_user_name) 
																								: data.getAuthUserName());
			holder.setText(R.id.log_out_in, data.isAnonymous() ? MainApp.getRes().getString(R.string.login)
																			: MainApp.getRes().getString(R.string.logout));
			
			try 
			{
				if (false == data.getLogo().isEmpty() && !data.isAnonymous())
				{
					userImage.setImageURI(Uri.parse(new File(data.getLogo()).toString()));
					userImage.setScaleType(ScaleType.FIT_XY);
				}
				else
				{
					userImage.setImageResource(R.drawable.du_logo_white);
					userImage.setScaleType(ScaleType.CENTER_INSIDE);
				}
				
			} 
			catch (Exception e) 
			{
				userImage.setImageResource(R.drawable.du_logo_white);
				userImage.setScaleType(ScaleType.CENTER_INSIDE);
			}
			
			
		}	
		else
		{
			holder.setText(R.id.user_name, MainApp.getRes().getString(R.string.anonymous_user_name));
		}
		

	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void selectMenuItem(final MenuItem item,  final boolean simulateClick)
	{
		
		if (item == null)
		{
			return;
		}
		
		Handler handler = new Handler();
		handler.post(new Runnable() {
			
			@Override
			public void run() {
				
				/*this part will collapse a submenu when an other menu is selected*/
				/*
				 * if the selected menu item is not null, is from an expanded list and the 
				 * parent is different from the new menu item 
				 * */
				if (selectedMenuItem != null && 
				    selectedMenuItem.getParent() != menuRoot &&
				    selectedMenuItem.getParent() != item.getParent()){
					exList.collapseGroup(selectedMenuItem.getParent().getPosition());
				}
				
				menuRoot.clearSelection();
				item.setSelected(true);
				menuAdapter.notifyDataSetChanged();
				
				if (item.getParent() != menuRoot)
				{
					int group = item.getParent().getPosition();
					int child = item.getPosition();
					
					exList.expandGroup(group);
					exList.setSelectedChild(group,child , true);
				}
				
				ArrayList<Integer> path = new ArrayList<Integer>();
				selectedMenuItem = item;
				selectedMenuItem.getSelectionPath(path);
	    		path.remove(0); // rootMenu
				getData().putIntegerArrayList(SELECTION, path); //save the data for fragment recovery state
				
				if (simulateClick)
				{
					IMvpActivity mvpActivity = getMvpActivity();
					
					mvpActivity.clearStack();
					
					mvpActivity.goTo(item.getClassType(),item.getTag(), item.getData(), true);
				}
			}
		});
					
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void selectMenuItem(Class classType, String tag, boolean simulateClick)
	{
		selectMenuItem(menuRoot.getItemByClassType(classType, tag), simulateClick);

	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void selectDefaultItem()
	{		
		if (Log.IS_TESTING)
		{
			
			selectMenuItem(menuRoot.getItemByClassType(VodCategoriesFragment.class, TaskGetVodCategories.ON_DEMAND_CLUB_CATEG + "/Highlights"), true);	
			
			//selectMenuItem(menuRoot.getItemByClassType(LiveTvFragment.class, LiveTvFragment.TAG), true);
			
			//selectMenuItem(menuRoot.getItemByClassType(DuAppsFragment.class, DuAppsFragment.TAG), true);
			
/*			Bundle data = new Bundle();
			
			data.putInt(ProgramsListViewFragment.DATA_POSITION, 0);
			data.putString(ProgramsListViewFragment.DATA_QUERY, "sa'ah");
			
			FragmentChangeActivity fca = (FragmentChangeActivity) getActivity();
			
			fca.goTo(ProgramsListViewFragment.class,ProgramsListViewFragment.TAG, data, true);*/
		}
		else
		{
			selectMenuItem(menuRoot.getItemByClassType(VodCategoriesFragment.class, TaskGetVodCategories.ON_DEMAND_CLUB_CATEG + "/Highlights"), true);	
			//selectMenuItem(menuRoot.getItemByClassType(LiveTvFragment.class, LiveTvFragment.TAG), true);			
		}
		
		MvpParentalManagement.releaseInstance();
		MvpParentalManagement.getInstance();
	}
	
	
	/**
	 * @desc method used to check if a class is contains content information
	 * @param c - the class that is tested if it is from a fragment with content
	 * @return
	 */
	public boolean hasContent(Class c){
		return fragmentsWithContent.contains(c);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void addDummySubMenuItems(MenuItem item, Class classType, String... names)
	{
		for (String name: names)
		{
			item.addSubMenuItem(classType,name,UnderConstrFragment.buildData(name + " view"),name,0);
		}
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void initData() {
		
	menuRoot = new MenuItem();
	MvpUserData authData = MvpUserData.getFromMvpObject();
Bundle bundleHighlights = new Bundle();
bundleHighlights.putString(VodCategoriesFragment.ROOT_CATEGORY_ID, TaskGetVodCategories.ON_DEMAND_CLUB_CATEG + "/Highlights");
bundleHighlights.putString(VodCategoriesFragment.ROOT_CATEGORY_TITLE, getResources().getString(R.string.whats_new));
MenuItem whatsNewCategory = new MenuItem(
		VodCategoriesFragment.class,
		TaskGetVodCategories.ON_DEMAND_CLUB_CATEG + "/Highlights",
		bundleHighlights,
		getResources().getString(R.string.whats_new), 
		R.drawable.menu_icon_whats_new );
menuRoot.addSubMenuItem(whatsNewCategory);

fragmentsWithContent.add(VodCategoriesFragment.class);
    	
    	MenuItem watchTvCategory = new MenuItem(
    			LiveTvFragment.class,
    			LiveTvFragment.TAG,
    			null,    			
    			getResources().getString(R.string.watch_now), 
    			R.drawable.menu_icon_watch_tv);  
    	menuRoot.addSubMenuItem(watchTvCategory);
    	fragmentsWithContent.add(LiveTvFragment.class);
    	
    
MenuItem onDemandClub = new MenuItem(
		VodCategoriesFragment.class,
		VodCategoriesFragment.TAG,
		null,
		"On demand club", 
		R.drawable.menu_icon_video_on_demand);

menuRoot.addSubMenuItem(onDemandClub);    	


Bundle bundleTV = new Bundle();
bundleTV.putString(VodCategoriesFragment.ROOT_CATEGORY_ID,  TaskGetVodCategories.ON_DEMAND_CLUB_CATEG + "/TV");
bundleTV.putString(VodCategoriesFragment.ROOT_CATEGORY_TITLE, "TV");

MenuItem odcTV = new MenuItem(
		VodCategoriesFragment.class,
		TaskGetVodCategories.ON_DEMAND_CLUB_CATEG + "/TV",
		bundleTV,
		"TV", 
		R.drawable.menu_icon_video_on_demand);
onDemandClub.addSubMenuItem(odcTV);

Bundle bundleKids = new Bundle();
bundleKids.putString(VodCategoriesFragment.ROOT_CATEGORY_ID,  TaskGetVodCategories.ON_DEMAND_CLUB_CATEG + "/Kids");
bundleKids.putString(VodCategoriesFragment.ROOT_CATEGORY_TITLE, "Kids");

MenuItem odcKids = new MenuItem(
		VodCategoriesFragment.class,
		TaskGetVodCategories.ON_DEMAND_CLUB_CATEG + "/Kids",
		bundleKids,
		"Kids", 
		R.drawable.menu_icon_video_on_demand);
onDemandClub.addSubMenuItem(odcKids);
	

Bundle bundleMovies = new Bundle();
bundleMovies.putString(VodCategoriesFragment.ROOT_CATEGORY_ID,  TaskGetVodCategories.ON_DEMAND_CLUB_CATEG + "/Movies");
bundleMovies.putString(VodCategoriesFragment.ROOT_CATEGORY_TITLE, "Movies");
MenuItem odcMovies = new MenuItem(
		VodCategoriesFragment.class,
		TaskGetVodCategories.ON_DEMAND_CLUB_CATEG + "/Movies",
		bundleMovies,
		"Movies", 
		R.drawable.menu_icon_video_on_demand);
onDemandClub.addSubMenuItem(odcMovies);


if (Log.IS_TESTING)
{
//	    	MenuItem xxxx = new MenuItem(
//	    			TestHListFragment.class,
//	    			TestHListFragment.TAG,
//	    			null,
//	    			"Test", 
//	    			0);
//	    	onDemandClub.addSubMenuItem(xxxx);
}
    	if(!authData.isAnonymous())
    	{
    		Bundle dataAccount = new Bundle();
    		dataAccount.putString(BrowserFragment.URL, PlatformSettings.getInstance().getDuSelfcareUrl());
    		MenuItem myAccount = new MenuItem(
    				BrowserFragment.class,
    				BrowserFragment.TAG,
    				dataAccount,
    				getResources().getString(R.string.my_account), 
    				R.drawable.du_logo_white);
    		menuRoot.addSubMenuItem(myAccount);
    	}
    	
    	MenuItem settingsCategory = new MenuItem(
    			UnderConstrFragment.class,
    			UnderConstrFragment.TAG,
    			null,
    			getResources().getString(R.string.settings), 
    			R.drawable.menu_icon_settings);  	
    	menuRoot.addSubMenuItem(settingsCategory);
    	
    	if(!authData.isAnonymous())
    	{
    		
    	MenuItem parentalPin = new MenuItem(
    			ParentalPinFragment.class,
    			ParentalPinFragment.TAG,
    			null,
    			getResources().getString(R.string.parental_pin), 
    			0);
    	settingsCategory.addSubMenuItem(parentalPin);

    	MenuItem parentalLevel = new MenuItem(
    			ParentalControlFragment.class,
    			ParentalControlFragment.TAG,
    			null,
    			getResources().getString(R.string.parental_level), 
    			0);
    	settingsCategory.addSubMenuItem(parentalLevel);

    	/*removed because it is contained into the options menu*/
//    	MenuItem social = new MenuItem(
//    			SocialFragment.class,
//    			SocialFragment.TAG,
//    			null,
//    			getResources().getString(R.string.social), 
//    			0);
//    	settingsCategory.addSubMenuItem(social);
    	}


    	MenuItem configMenu = new MenuItem(
    			ConfigsFragment.class,
    			ConfigsFragment.TAG,
    			null,
    			getResources().getString(R.string.config_menuBtn),
    			0);
    	settingsCategory.addSubMenuItem(configMenu);
    	
    	
    	
    	MenuItem duApps = new MenuItem(
    			DuAppsFragment.class,
    			DuAppsFragment.TAG,
    			null,
    			getResources().getString(R.string.apps) , 
    			0);  	
    	settingsCategory.addSubMenuItem(duApps);
    	
    	
    	MenuItem termsAndCond = new MenuItem(
    			TermsAndConditionsFragment.class,
    			TermsAndConditionsFragment.TAG,
    			null,
    			getResources().getString(R.string.terms_and_cond), 
    			0);
    	settingsCategory.addSubMenuItem(termsAndCond);
    	
    	MenuItem about = new MenuItem(
    			AboutFragment.class,
    			AboutFragment.TAG,
    			null,
    			getResources().getString(R.string.about), 
    			0);
    	settingsCategory.addSubMenuItem(about);


    	
 	

    }

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) 
	{
		MenuItem menuItem = menuRoot.getSubMenuItem(groupPosition).getSubMenuItem(childPosition);		
		
		selectMenuItem(menuItem,true);
		
		
		return true;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public boolean onGroupClick(ExpandableListView parent, View v,
			int groupPosition, long id) 
	{

		MenuItem menuItem = menuRoot.getSubMenuItem(groupPosition);
		
		if (menuItem.hasSubItems())
		{
			return false;
		}
		
		selectMenuItem(menuItem, true);
		
		return true;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onClick(View v) {
		MvpUserData authData = MvpUserData.getFromMvpObject();
		
		switch (v.getId()) {
		case R.id.user_image:
			{	
				if(!authData.isAnonymous())
				{
					selectMenuItem(ConfigsFragment.class, ConfigsFragment.TAG, true);
				}
			}
			break;
			
		case R.id.log_out_in:
			{
				if(authData.isAnonymous())
				{
					getMvpActivity().goTo(AuthFragment.class, AuthFragment.TAG, fragmentData, true);
				}
				else
				{
					LogOffDialogView logOff = LogOffDialogView.getInstance();
					logOff.show(getFragmentManager(), "dialog");
//					performLogOut();
				}
			}
			break;

		default:
			break;
		}
		
		
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void performLogOut() 
	{
		MvpUserData authData = MvpUserData.getFromMvpObject();
		serverLogOut();
		authData.signOut();
		onResetData();
		
		IMvpActivity activity = getMvpActivity();
	
		activity.clearStack();
			
		activity.goTo(AuthFragment.class,AuthFragment.TAG, new Bundle(), true);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void chooseImage() {
		chooserManager = new ChooserManager(this,
				IConst.REQUEST_PICK_PICTURE, "duView", true);
		chooserManager.setImageChooserListener(this);
		try {
			filePath = chooserManager.choose();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onImageChosen(final ImageChoosen image) {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (image != null)
				{
					try 
					{
						String filePath = image.getFileThumbnail();
						userImage.setImageURI(Uri.parse(new File(filePath).toString()));	
						userImage.setScaleType(ScaleType.FIT_XY);
						
						userImage.setImageURI(Uri.parse(new File(filePath).toString()));	
						
						MvpObject mvpAuthObj = MvpObject.newReference(AuthFragment.MVP_AUTH_DATA);
						MvpUserData data = (MvpUserData)mvpAuthObj.getData();
						data.setLogo(filePath);
					} 
					catch (Exception e) 
					{
						// TODO: handle exception
					}
					
				}
			}
		});
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onError(final String reason) {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				showToast(reason);
			}
		});
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == -1
				&& (requestCode == IConst.REQUEST_PICK_PICTURE)) {
			if (chooserManager == null) {
				reinitializeImageChooser();
			}
			chooserManager.submit(requestCode, data);
		}
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void reinitializeImageChooser() {
		chooserManager = new ChooserManager(this, IConst.REQUEST_PICK_PICTURE,
				"myfolder", false);
		chooserManager.setImageChooserListener(this);
		chooserManager.reinitialize(filePath);
	}
	
	public ImageView getImage()
	{
		return userImage;
	}
	
	
	protected void serverLogOut()
	{
		if (DEBUG) Log.m(TAG,this,"serverLogOut");
		
		HttpReqParams httpReqParams = new HttpReqParams(
				IHttpConst.VALUE_SCHEMA_1_0, 
				IHttpConst.VALUE_JSON);
		httpReqParams.setParam(IHttpConst.PARAM_TOKEN_SECOND, MvpUserData.getFromMvpObject().getToken());
		
		TaskJsonRequest task = new TaskJsonRequest(
				PlatformSettings.getInstance().getMpxLogOff(), 
				httpReqParams);	
		
		task.execute();
		
	}
	
	public void refreshMenu()
	{
		menuRoot = null;
		
		initData();
		
		menuAdapter = new ExpandableAdapter(menuRoot);
		menuAdapter.notifyDataSetChanged();		
		
		exList.setAdapter(menuAdapter);
		
	}
//	
//	protected void getServerParentalLevel()
//	{
//		if (DEBUG) Log.m(TAG,this,"getServerParentalLevel");
//		
//		HttpReqParams httpReqParams = new HttpReqParams(
//				IHttpConst.VALUE_SCHEMA_1_1);
//		
//		httpReqParams.setParam(IHttpConst.ACCOUNT_NAME, PlatformSettings.getInstance().getAccountNameValue());
//		httpReqParams.setParam(IHttpConst.PARAM_TOKEN, MvpAuthData.getFromMvpObject().getToken());
//		
//		 taskGetParentalLevel = new TaskSingleReqParse<MvpAuthData>(
//				 this,
//				 null,
//				 PlatformSettings.getInstance().getParentalUserLevelUrl(), 
//				 httpReqParams);	
//		 
//		 taskGetParentalLevel.execute();
//	}
//
//	@Override
//	public void onReturnFromTask(Task task, boolean canceled) {
//		
//		if (DEBUG) Log.m(TAG,this,"onReturnFromTask");
//		
//		if (task == taskGetParentalLevel)
//		{
//			if (task.isError())
//			{
//			}
//			else
//			{
//			}
//		}		
//	}

	
}
