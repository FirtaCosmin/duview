package com.crmsoftware.duview.slidemenu.imagechooser.thread;

import com.crmsoftware.duview.slidemenu.imagechooser.ImageChoosen;

public interface ProcessorListener {
    public void onProcessedImage(ImageChoosen image);

    public void onError(String reason);
}
