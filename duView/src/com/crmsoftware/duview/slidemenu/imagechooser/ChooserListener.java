
package com.crmsoftware.duview.slidemenu.imagechooser;

public interface ChooserListener {
    /**
     * When the processing is complete, you will receive this callback with
     * {@link ImageChoosen}
     * 
     * @param image
     */
    public void onImageChosen(ImageChoosen image);

    /**
     * Handle any error conditions if at all, when you receieve this callback
     * 
     * @param reason
     */
    public void onError(String reason);
}
