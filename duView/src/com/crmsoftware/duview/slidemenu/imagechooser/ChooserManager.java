package com.crmsoftware.duview.slidemenu.imagechooser;

import java.io.File;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.crmsoftware.duview.slidemenu.imagechooser.thread.ProcessorListener;
import com.crmsoftware.duview.slidemenu.imagechooser.thread.ProcessorThread;
import com.crmsoftware.duview.utils.IConst;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;

public class ChooserManager extends Chooser implements
		ProcessorListener {
	
	public static boolean DEBUG = Log.DEBUG; 
	public final static String TAG = ChooserManager.class.getSimpleName();

	private final static String DIRECTORY = "bimagechooser";

	private ChooserListener listener;

	/**
	 * 
	 * 
	 * @param activity
	 * @param type
	 */
	public ChooserManager(Activity activity, int type) {
		super(activity, type, DIRECTORY, true);
	}

	public ChooserManager(Fragment fragment, int type) {
		super(fragment, type, DIRECTORY, true);
	}

	public ChooserManager(android.app.Fragment fragment, int type) {
		super(fragment, type, DIRECTORY, true);
	}

	/**
	 * Specify the type {@link ChooserType.REQUEST_PICK_PICTURE} or
	 * {@link ChooserType.REQUEST_CAPTURE_PICTURE}
	 * <p>
	 * Optionally, you can control where the exported images with their
	 * thumbnails would be stored.
	 * </p>
	 * 
	 * @param activity
	 * @param type
	 * @param foldername
	 */
	public ChooserManager(Activity activity, int type, String foldername) {
		super(activity, type, foldername, true);
	}

	public ChooserManager(Fragment fragment, int type, String foldername) {
		super(fragment, type, foldername, true);
	}

	public ChooserManager(android.app.Fragment fragment, int type,
			String foldername) {
		super(fragment, type, foldername, true);
	}

	/**
	 * Specify the type {@link ICons.REQUEST_PICK_PICTURE} or
	 * {@link ICons.REQUEST_CAPTURE_PICTURE}
	 * <p>
	 * Optionally, you can set whether you need thumbnail generation or not. If
	 * not, you would get the original image for the thumbnails as well
	 * </p>
	 * 
	 * @param activity
	 * @param type
	 * @param shouldCreateThumbnails
	 */
	public ChooserManager(Activity activity, int type,
			boolean shouldCreateThumbnails) {
		super(activity, type, DIRECTORY, shouldCreateThumbnails);
	}

	public ChooserManager(Fragment fragment, int type,
			boolean shouldCreateThumbnails) {
		super(fragment, type, DIRECTORY, shouldCreateThumbnails);
	}

	public ChooserManager(android.app.Fragment fragment, int type,
			boolean shouldCreateThumbnails) {
		super(fragment, type, DIRECTORY, shouldCreateThumbnails);
	}

	/**
	 * Specify the type {@link ICons.REQUEST_PICK_PICTURE} or
	 * {@link ICons.REQUEST_CAPTURE_PICTURE}
	 * <p>
	 * Specify your own foldername and whether you want the generated thumbnails
	 * or not
	 * </p>
	 * 
	 * @param activity
	 * @param type
	 * @param foldername
	 * @param shouldCreateThumbnails
	 */
	public ChooserManager(Activity activity, int type, String foldername,
			boolean shouldCreateThumbnails) {
		super(activity, type, foldername, shouldCreateThumbnails);
	}

	public ChooserManager(Fragment fragment, int type, String foldername,
			boolean shouldCreateThumbnails) {
		super(fragment, type, foldername, shouldCreateThumbnails);
	}

	public ChooserManager(android.app.Fragment fragment, int type,
			String foldername, boolean shouldCreateThumbnails) {
		super(fragment, type, foldername, shouldCreateThumbnails);
	}

	/**
	 * Set a listener, to get callbacks when the images and the thumbnails are
	 * processed
	 * 
	 * @param listener
	 */
	public void setImageChooserListener(ChooserListener listener) {
		this.listener = listener;
	}

	@Override
	public String choose() throws Exception {
		String path = null;
		if (listener == null) {
			throw new IllegalArgumentException(
					"ImageChooserListener cannot be null. Forgot to set ImageChooserListener???");
		}
		switch (type) {
		case IConst.REQUEST_PICK_PICTURE:
			choosePicture();
			break;
			
		case IConst.REQUEST_CAPTURE_PICTURE:
			path = takePicture();
			break;
		default:
			throw new IllegalArgumentException(
					"Cannot choose a video in ImageChooserManager");
		}
		return path;
	}

	private void choosePicture() throws Exception {
		checkDirectory();
		try {
			Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
			intent.setType("image/*");
			if (extras != null) {
				intent.putExtras(extras);
			}
			startActivity(intent);
		} catch (ActivityNotFoundException e) {
			throw new Exception("Activity not found");
		}
	}
	
	private String takePicture() throws Exception {
		checkDirectory();
		try {
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			filePathOriginal = Utils.getDirectory(foldername)
					+ File.separator + Calendar.getInstance().getTimeInMillis()
					+ ".jpg";
			intent.putExtra(MediaStore.EXTRA_OUTPUT,
					Uri.fromFile(new File(filePathOriginal)));
			if (extras != null) {
				intent.putExtras(extras);
			}
			startActivity(intent);
		} catch (ActivityNotFoundException e) {
			throw new Exception("Activity not found");
		}
		return filePathOriginal;
	}


	@Override
	public void submit(int requestCode, Intent data) {
		if (requestCode != type) {
			onError("onActivityResult requestCode is different from the type the chooser was initialized with.");
		} else {
			switch (requestCode) {
			case IConst.REQUEST_PICK_PICTURE:
				processImageFromGallery(data);
				break;
			case IConst.REQUEST_CAPTURE_PICTURE:
				processCameraImage();
				break;
					
			}
		}
	}

	@SuppressLint("NewApi")
	private void processImageFromGallery(Intent data) {
		if (data != null && data.getDataString() != null) {
			String uri = data.getData().toString();
			sanitizeURI(uri);
			if (filePathOriginal == null || TextUtils.isEmpty(filePathOriginal)) {
				onError("File path was null");
			} else {
				if (DEBUG) Log.m(TAG,this,"File: " + filePathOriginal);
				String path = filePathOriginal;
				ProcessorThread thread = new ProcessorThread(path,
						foldername, shouldCreateThumbnails);
				thread.setListener(this);
				if (activity != null) {
					thread.setContext(activity.getApplicationContext());
				} else if (fragment != null) {
					thread.setContext(fragment.getActivity()
							.getApplicationContext());
				} else if (appFragment != null) {
					thread.setContext(appFragment.getActivity()
							.getApplicationContext());
				}
				thread.start();
			}
		} else {
			onError("Image Uri was null!");
		}
	}
	
	private void processCameraImage() {
		String path = filePathOriginal;
		ProcessorThread thread = new ProcessorThread(path,
				foldername, shouldCreateThumbnails);
		thread.setListener(this);
		thread.start();
	}
	@Override
	public void onProcessedImage(ImageChoosen image) {
		if (listener != null) {
			listener.onImageChosen(image);
		}
	}

	@Override
	public void onError(String reason) {
		if (listener != null) {
			listener.onError(reason);
		}
	}
}
