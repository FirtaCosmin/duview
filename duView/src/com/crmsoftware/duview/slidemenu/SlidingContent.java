package com.crmsoftware.duview.slidemenu;

import android.os.Bundle;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.BaseActivity;


public class SlidingContent extends BaseActivity {
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// set the Above View
		setContentView(R.layout.content_frame);
		getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.content_frame, new SlideListFragment())
		.commit();
		
		setSlidingActionBarEnabled(false);
	}

}
