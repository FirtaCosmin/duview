package com.crmsoftware.duview.slidemenu.animation;

import android.graphics.Canvas;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.slidemenu.library.SlidingMenu.CanvasTransformer;

public class CustomScaleAnimation extends CustomAnimation {

	public CustomScaleAnimation() {
		super(R.string.anim_scale, new CanvasTransformer() {
			@Override
			public void transformCanvas(Canvas canvas, float percentOpen) {
				canvas.scale(percentOpen, 1, 0, 0);
			}			
		});
	}

}
