package com.crmsoftware.duview.slidemenu.animation;

import android.os.Bundle;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.BaseActivity;
import com.crmsoftware.duview.slidemenu.SlideListFragment;
import com.crmsoftware.duview.slidemenu.library.SlidingMenu;
import com.crmsoftware.duview.slidemenu.library.SlidingMenu.CanvasTransformer;

public abstract class CustomAnimation extends BaseActivity {
	
	private CanvasTransformer mTransformer;
	
	public CustomAnimation(int titleRes, CanvasTransformer transformer) {
		mTransformer = transformer;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// set the Above View
		setContentView(R.layout.content_frame);
		getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.content_frame, new SlideListFragment())
		.commit();
		
		SlidingMenu sm = getSlidingMenu();
		setSlidingActionBarEnabled(true);
		sm.setBehindScrollScale(0.0f);
		sm.setBehindCanvasTransformer(mTransformer);
	}

}
