
package com.crmsoftware.duview.slidemenu.menumodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.crmsoftware.duview.ui.common.UnderConstrFragment;

import android.os.Bundle;


public class MenuItem implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3339956027749963475L;
	
	public static final String TAG_DATA= "data" ; 
	
	private String tag;
	private long imageId;
	private String name;
	private Class classType;
	private Bundle data;
	
	boolean selected = false;
	int position = 0;
	MenuItem parent = null;

	public MenuItem getParent() {
		return parent;
	}


	public void setParent(MenuItem parent) {
		this.parent = parent;
	}

	
	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}


	public boolean isSelected() {
		return selected;
	}


	public void setSelected(boolean selected) {
		this.selected = selected;
	}


	public void setData(Bundle data) {
		this.data = data;
	}

	
	private List<MenuItem> itemList = new ArrayList<MenuItem>();

	public MenuItem() 
	{
		
	}

		
	public MenuItem(Class classType, String tag, Bundle data, String name, long imageId) {
		this.tag = tag;
		this.name = name;
		this.imageId = imageId;
		this.classType = classType;
		this.data = data;
		
		if (null == data)
		{
			this.data = new Bundle();
			this.data.putString(TAG_DATA, name + " view");
		}
		
	}

	public String getTag() {
		return tag;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<MenuItem> getSubMenuItems() {
		return itemList;
	}

	public void addSubMenuItem(MenuItem menuItem) {
		itemList.add(menuItem);
		menuItem.setPosition(itemList.size()-1);
		menuItem.setParent(this);
	}
	
	public void addSubMenuItem(Class classType, String tag, Bundle data, String name, long imageId) {
		
		addSubMenuItem(new MenuItem(classType, tag, data, name, imageId));
	}
	
	public MenuItem getSubMenuItem(int pos) {
		
		if (pos >=0 && pos < itemList.size())
		{
			return itemList.get(pos);
		}
		
		return null;
	}
	
	public boolean hasSubItems() {
		return itemList.size()>0;
	}


	public long getImageId() {
		return imageId;
	}

	public void setImageId(long imageId) {
		this.imageId = imageId;
	}
	
	public Class getClassType() {
		return classType;
	}


	public void setClassType(Class classType) {
		this.classType = classType;
	}


	public Bundle getData() {
		return data;
	}
	
	public void clearSelection() 
	{
		for (MenuItem item : itemList)
		{
			item.setSelected(false);
			item.clearSelection();
		}
	}
	
	
	public MenuItem getItemByClassType(Class classType, String tag) 
	{
		for (MenuItem item : itemList)
		{
			if (item.classType.equals(classType))
			{
				if (tag != null)
				{
					if (tag.equals(item.tag))
					{
						return item;
					}
				}
			}
			
			MenuItem ret = item.getItemByClassType(classType, tag);
			if (ret != null)
			{
				return ret;
			}
		}
		
		return null;
	}
	
	
	public void getSelectionPath(List<Integer> list) 
	{
		list.add(0, position);
		
		if (parent != null)
		{
			parent.getSelectionPath(list);
		}
	}
	
	
	public MenuItem getMenuFromPath(List<Integer> path) 
	{
		if (path.size() == 0)
		{
			return null;
		}
		
		int pos = path.get(0);
		path.remove(0);
		if (pos >= itemList.size())
		{
			return null;
		}
		
		MenuItem selectedItem = itemList.get(pos);
		MenuItem nextSelected = selectedItem.getMenuFromPath(path);
		if (nextSelected != null)
		{
			return nextSelected;
		}
		
		return selectedItem;
	}	
}
