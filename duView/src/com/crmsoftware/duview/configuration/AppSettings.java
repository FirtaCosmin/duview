package com.crmsoftware.duview.configuration;

public class AppSettings {
	
	public static final int LANG_COUNT = 2;
	public static int LANG_SELECTED = 0;
	
	
	public static final int LANG_ENGLISH = 0;
	public static final int LANG_ARABIC = 1;	
}
