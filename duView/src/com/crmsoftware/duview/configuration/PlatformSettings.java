package com.crmsoftware.duview.configuration;

import com.crmsoftware.duview.utils.IHttpConst;

public class PlatformSettings {
	
	public static final String TAG = PlatformSettings.class.getSimpleName();	
	public static final boolean IS_PRODUCTION = true;	
	
	private static PlatformSettings instance = null;
	public static PlatformSettings getInstance()
	{
		if (null == instance)
		{
			instance = new PlatformSettings();
		}
		
		return instance;
	}
	
//	url's
	private final String tpMpxAccoutUrl 			= IS_PRODUCTION ?"http://access.auth.theplatform.com/data/Account/2337367106" : "http://access.auth.theplatform.com/data/Account/2400537963";
	private final String tpMpxFeedUrl 				= IS_PRODUCTION ? "http://feed.theplatform.com/f/d-YTLC/xCZsFmbqvzUI" : "http://feed.theplatform.com/f/d-YTLC/rzfd9XqsIxzV";
	private final String userAuthUrl 				= "https://view.du.ae:720/du-authentication/client/signIn/username";
	private final String registerWsUrl 				= "http://register-mvp.alu.theplatform.com/alu-register";
	private final String tpMpxDirPid				= "_QlJ_2E9VzwBks_T"; //IS_PRODUCTION ? "_QlJ_2E9VzwBks_T" : "yAbcCKd74Z1xwRnq";
	private final String accountNameValue 			= "FTDUMVP"; //IS_PRODUCTION ? "FTDUMVP" : "TBDUMVP";
	private final String registerDevUrl 			= "http://devicemngt-mvp.alu.theplatform.com/device-mgm/subscriberDevice/registerDevice";
	private final String mpxIdentityBsUrl		    = "https://euid.theplatform.com";
	private final String epgUrl						= "http://epg-mvp.alu.theplatform.com/alu-epg";
	private final String filterScheme				= IHttpConst.SCHEME_CATEGORY_FILTER;	//IS_PRODUCTION ? IHttpConst.SCHEME_CATEGORY_VOD : 

	private final String logOffSufix 				= "/idm/web/Authentication/signOut";
	private final String mpxCategoryUrlSufix		= "/categories";
	private final String channelFavoritesSuffix	    = "/ChannelFavourites";
	private final String changeParentalPinUrl 		= "/User/parentalPinCode/change/byToken";
	private final String programPath				= "/Program";
	
	private final String userGetParentalLevelSufix			= "/User/parentalLevel/get/byToken";
	private final String changeParentalUserLevelSufix		= "/User/parentalLevel/change/byToken";
	
	private final String duAppsUpdateUrl			= "http://duvmstd.cloudapp.net:8080/DuWebservice/update/section";
	private final String duSelfcareUrl				= "https://selfcare.du.ae/selfcare-portal-web/nonLoggedInSelfcare.portal";
	private final String crmSoftwareUrl				= "http://www.crmsoftware.ro";
	private final String checkParentalPinSufix 		= "/User/parentalPinCode/check/byToken";
	

	private final String getParentalUserLevelUrl 	= registerWsUrl + userGetParentalLevelSufix;
	private final String setParentalUserLevelUrl	= registerWsUrl + changeParentalUserLevelSufix;
	private final String checkParentalPinUrl		= registerWsUrl + checkParentalPinSufix;
	


	private final String mvpEpgOverview = "http://epg-server-mvp.alu.theplatform.com/MiTV_FTDUMVP/overview.do";

	private  String tpToken = "";

	
/*	USER_REGISTER_URL = "";
	
	MPX_ACCOUNT_ID_URL = "http://access.auth.theplatform.com/data/Account/2337367106";
	MPX_ACCOUNT_ID_URL_LEGACY = "http://mps.theplatform.com/data/Account/2337367106";
	ACCOUNT_NAME_VALUE = "FTDUMVP";
	MPX_DIRECTORY_PID_URL = "_QlJ_2E9VzwBks_T";
	
	EPG_BASE_URL = "http://epg-server-mvp.alu.theplatform.com/MiTV_FTDUMVP";
	EPG_WS_URL = "http://epg-mvp.alu.theplatform.com/alu-epg";
	REGISTER_WS_URL = "http://register-mvp.alu.theplatform.com/alu-register";
	MPX_IDENTITY_BS_URL = "https://euid.theplatform.com";
	AUTHENTICATION_WRAPPER_URL = "https://view.du.ae:720/du-authentication/client";
	MPX_MEDIAFEED_URL = "http://feed.theplatform.com/f/d-YTLC/xCZsFmbqvzUI";
	MPX_MEDIA_DS_URL = "http://productfeed-mvp.alcatel-lucent.com";
	MPX_PRODUCTFEED_URL = "http://productfeed-mvp.alcatel-lucent.com";
	MPX_STOREFRONT_BS_URL = "https://storefront.commerce.theplatform.com";
	
	MPX_PAYFLOWPRO_TYPE = "ALU.PayflowPro";
	DATA_SOCIAL_COMUNITY_URL = "http://data.social.community.theplatform.com"; 			//for rating and comments
	MPX_USER_PROFILE_DS_URL = "http://data.userprofile.community.theplatform.com";  	//for bookmark
	GET_DEVICE_REGISTER_URL = "http://devicemngt-mvp.alu.theplatform.com/device-mgm/subscriberDevice/registerDevice";
	TEST_USER = "mkanlica";// "ali.dernaika";//"aperatif"; "mkanlica"
	TEST_PASS = "dumvp2013"; //"LetMeIn!";//"srs5h6y3";//"dumvp2013" 
*/	
	
/*	
	USER_REGISTER_URL = "";

	MPX_ACCOUNT_ID_URL = "http://access.auth.theplatform.com/data/Account/2400537963";
	MPX_ACCOUNT_ID_URL_LEGACY = "http://mps.theplatform.com/data/Account/2400537963";
	ACCOUNT_NAME_VALUE = "TBDUMVP";
	MPX_DIRECTORY_PID_URL = "yAbcCKd74Z1xwRnq";

	EPG_BASE_URL = "http://epg-server-mvp.alu.theplatform.com/MiTV_TBDUMVP";
	EPG_WS_URL = "http://epg-mvp.alu.theplatform.com/alu-epg";
	REGISTER_WS_URL = "http://register-mvp.alu.theplatform.com/alu-register";
	MPX_IDENTITY_BS_URL = "https://euid.theplatform.com";
	AUTHENTICATION_WRAPPER_URL = "https://pppfconnect.duiptv.ae:720/du-authentication/client";
	MPX_MEDIAFEED_URL = "http://feed.theplatform.com/f/d-YTLC/rzfd9XqsIxzV";
	MPX_MEDIA_DS_URL = "http://productfeed-mvp.alcatel-lucent.com";
	MPX_PRODUCTFEED_URL = "http://productfeed-mvp.alcatel-lucent.com";
	MPX_STOREFRONT_BS_URL = "https://storefront.commerce.theplatform.com";

	MPX_PAYFLOWPRO_TYPE = "ALU.PayflowPro";
	DATA_SOCIAL_COMUNITY_URL = "http://data.social.community.theplatform.com"; 			//for rating and comments
	MPX_USER_PROFILE_DS_URL = "http://data.userprofile.community.theplatform.com";  	//for bookmark
	GET_DEVICE_REGISTER_URL = "http://devicemngt-mvp.alu.theplatform.com/device-mgm/subscriberDevice/registerDevice";
	TEST_USER = "1.17828519";
	TEST_PASS = "Alu12345";
*/	
	
	
	public void PlatformSettings()
	{

	}
	
	public String getTpMpxFeedUrl()
	{
		return tpMpxFeedUrl;
	}
	
	public String getTpMpxDirPid()
	{
		return tpMpxDirPid;
	}
	
	public String getTpToken()
	{
		return tpToken;
	}
	
	public void setTpToken(String tpToken)
	{
		 this.tpToken=tpToken;
	}
	
	public String getUserAuthUrl()
	{
		return userAuthUrl;
	}
	
	public String getEpgOverviewUrl()
	{
		return mvpEpgOverview;
	}

	public String getRegisterWsUrl() {
		return registerWsUrl;
	}
	
	public String getChangePinUrl() {
		return registerWsUrl + changeParentalPinUrl;
	}
	
	public String getFilterVoDList(){
		return tpMpxFeedUrl + mpxCategoryUrlSufix;
	}
	
	public String getAccountNameValue() {
		return accountNameValue;
	}
	
	public String getTpMpxAccoutUrl() {
		return tpMpxAccoutUrl;
	}

	public String getRegisterDevUrl() {
		return registerDevUrl;
	}
	
	public String getMpxLogOff(){
		return mpxIdentityBsUrl + logOffSufix;
	}

	public String getFavoriteEpgUrl() {
		return epgUrl + channelFavoritesSuffix ;
	}
	
	public String getSearchUrl() {
		return epgUrl + programPath ;
	}
	
	public String getParentalUserLevelUrl() {
		return getParentalUserLevelUrl;
	}
	
	public String getSetParentalUserLevelUrl() {
		return setParentalUserLevelUrl;
	}
	
	public String getDuAppsUpdateUrl() {
		return duAppsUpdateUrl;
	}
	

	public String getDuSelfcareUrl() {
		return duSelfcareUrl;
	}
	
	public String getCRMSoftwareUrl() {
		return crmSoftwareUrl;
	}
	public String getFilterScheme() {
		return filterScheme;
	}
	
	public String getCheckParentalPinUrl() {
		return checkParentalPinUrl;
	}
	
}
