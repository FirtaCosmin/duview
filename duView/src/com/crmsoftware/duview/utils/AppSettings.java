package com.crmsoftware.duview.utils;

import com.crmsoftware.duview.app.MainApp;


public class AppSettings {
	
	public final String ASSET_PHONE="SmartPhone";
	public final String ASSET_TABLET="Tablet";	
	private String deviceType = ASSET_PHONE;
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	   private static AppSettings instance = null;
	   
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	   public static AppSettings getInstance()
	   {
			if (null == instance)
			{
				instance = new AppSettings();
			}
			return instance;
	   }
	   
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public boolean isPhone()
	{
		return deviceType == ASSET_PHONE;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public boolean isTablet()
	{
		return deviceType == ASSET_TABLET;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public String getCacheDirectory()
	{
		return Utils.createDirIfNotExists(MainApp.getInstance().getFilesDir() + "/cachedFiles");
	}
}
