package com.crmsoftware.duview.utils;

public interface IHttpConst {

	public static final char	CHAR_OPEN_BRACKET			= '(';
	public static final char	CHAR_CLOSE_BRACKET			= ')';
	public static final char	CHAR_QUOTES					= '"';
	public static final char 	CHAR_DASH					= '-';
	public static final char 	CHAR_QUESTION				= '?';
	public static final char 	CHAR_AND					= '&';
	public static final char 	CHAR_COMMA					= ',';
	public static final char 	CHAR_DOTS					= ':';
	public static final char 	CHAR_SLASH					= '/';
	public static final char 	CHAR_EQ						= '=';
	
	public static final String PARAM_SCHEMA       		= "schema";
	public static final String VALUE_SCHEMA_1_2         = "1.2";
	public static final String VALUE_SCHEMA_1_0         = "1.0";
	public static final String VALUE_SCHEMA_1_1         = "1.1";

	public static final String PARAM_FORM       		= "form";
	public static final String VALUE_CJSON			    = "cjson";
	public static final String VALUE_JSON			    = "json";
	
	public static final String PROP_CONTENT_TYPE		= "Content-Type";
	public static final String PROP_CONTENT_LENGTH		= "Content-Length";
	public static final String VALUE_APPJSON			= "application/json";

	public static final String PARAM_RANGE 				= "range";
	
	public static final String PARAM_COUNT 				= "count";
	public static final String VALUE_TRUE				= "true";
	public static final String VALUE_FALSE				= "false";
	
	public static final String PARAM_BY_CATEGORIES 		= "byCategories";
	public static final String PARAM_BY_ASSETTYPES		= "byAssetTypes";
	public static final String PARAM_BY_CATEGORY_VOD	= "category_VoD";
	
	public static final String PARAM_SORT				= "sort";	
	public static final String PARAM_QUERY				= "q";
	
	public static final String PARAM_THUMBNAILFILTER	= "thumbnailFilter";
	public static final String PARAM_FILEFIELDS			= "fileFields";
	
	public static final String PARAM_USERNAME				= "userName";
	public static final String PARAM_PASSWORD				= "password";
	public static final String PARAM_IDLE_TIMEOUT        	= "_idleTimeout";
	
	public static final String VALUE_IDLE_TIMEOUT		= "14400000";
	
	public static final String ACCOUNT_NAME 				= "account";
	public static final String DIRECTORY_PID				= "directory";
	
	//parameter for change of pin
	public static final String OLD_PARENTAL_PIN				= "currentParentalPinCode";
	public static final String NEW_PARENTAL_PIN				= "newParentalPinCode";
	
	//parameter for change of parental level
	public static final String PARENTAL_PIN_CODE			= "parentalPinCode";
	public static final String PARENTAL_LEVEL				= "parentalLevel";
	
	// parameters for VoD category
	public static final String BY_SCHEME       				= "byScheme";
	public static final String BY_FULL_TITLE_PREFIX       	= "byfullTitlePrefix";
	
	// common parameters values for VoDs 
	public static final String SCHEME_CATEGORY_VOD			= "category_VOD";
	public static final String SCHEME_CATEGORY_GENRE		= "genre";	
	public static final String SCHEME_CATEGORY_FILTER		= "filter";	

	public static final String VALUE_CATEGORY_ROOT_ONDEMANDCLUB		= "on demand club";
	public static final String VALUE_CATEGORY_ROOT_SERIES		= "Series";
	
	public static final String VALUE_FIELD_ID				= "id";
	public static final String VALUE_FIELD_TITLE			= "title";
	public static final String VALUE_FIELD_CATEGORIES		= "categories";	
	public static final String VALUE_FIELD_DESCRIPTION		= "description";
	public static final String VALUE_FIELD_THUMBNAILS		= "thumbnails";
	public static final String VALUE_FIELD_RELEASE_YEAR_D	= ":releaseYear";
	public static final String VALUE_FIELD_CONTENT			= "content";
	public static final String VALUE_FIELD_CREDITS			= "credits";
	public static final String VALUE_FIELD_RATINGS			= "ratings";
	public static final String VALUE_FIELD_AGE_RATING_D		= ":ageRating";	
	public static final String VALUE_FIELD_SORTTITLE		= "alu-du$sortTitle";
	public static final String VALUE_FIELD_ADDED			= "added|desc";
	public static final String VALUE_FIELD_ORDER			= "order";
	
	public static final String VALUE_FILEFIELDS				= "assetTypes";	
	
	public static final String PARAM_TOKEN        				= "token";
	public static final String PARAM_TOKEN_SECOND        		= "_token";
	
	public static final String VALUE_CHANNELS				= "content:LiveTV|content:FreeTV";
	public static final String VALUE_LINE_UP_ID 			= ":lineUpId";
	public static final String PARAM_TYPES					= "types";
	public static final String VALUE_NONE 					= "none";
	
	public static final String PARAM_CHANNEL_ID				= "channelId";
	public static final String PARAM_TYPE					= "type";
	public static final String PARAM_START_DATE_TIME		= "startDateTime";
	public static final String PARAM_END_DATE_TIME			= "endDateTime";
	public static final String VALUE_TYPE_TV				= "tv";
	
	
	public static final String PARAM_SMIL_AUTH				= "auth";
	public static final String PARAM_SMIL_CLIENT_ID			= "clientId";
	public static final String PARAM_USERNAME_MPX			= "u";
	
	public static final String PARAM_REG_SERIAL_ID			= "_serialId";
	public static final String PARAM_REG_ACCOUNT_ID			= "account";	
	
	//stream concurrency
	public static final String CLIENT_ID 					= "_clientId";
	public static final String LOCK_ID 						= "_id";
	public static final String LOCK_SEQUENCE_TOKEN 			= "_sequenceToken";
	public static final String LOCK 						= "_encryptedLock";
	
	// favorites
	public static final String METHOD_PARAM					 = "method";
	public static final String METHOD_PARAM_GET				 = "get";
	public static final String METHOD_PARAM_POST			 = "post";
	public static final String METHOD_PARAM_DELETE			 = "delete";
	public static final String METHOD_PARAM_PUT				 = "put";
	public static final String CHANNEL_LIST					 = "channelList";
	
	public static final String PARAM_KEY 							= "q";
	public static final String PARAM_START_DATE					= "startDate";
	public static final String PARAM_END_DATE						= "endDate";
	public static final String PARAM_FAVORITES_ON					= "favoritesOn";
	public static final String VALUE_FAVORITES_TRUE					= "True";	
}
