package com.crmsoftware.duview.utils;
//******************************************************************************************
//Description:  file created for duView - CRM project
//   Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import android.os.Environment;

import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.core.common.TaskGetDataFromStorage;
import com.crmsoftware.duview.core.common.TaskHttpRequest;
import com.crmsoftware.duview.core.common.TaskImageReqCache;
import com.crmsoftware.duview.core.common.TaskJsonRequest;
import com.crmsoftware.duview.data.MvpChannelPrograms;
import com.crmsoftware.duview.ui.common.HorizontalAdapterView;
import com.crmsoftware.duview.ui.common.MediaViewPagerFragment;
import com.crmsoftware.duview.ui.common.MvpHorizontalListView;
import com.crmsoftware.duview.ui.common.MvpPagContext;
import com.crmsoftware.duview.ui.common.MvpPagPagerAdapter;
import com.crmsoftware.duview.ui.common.MvpVerticalListView;
import com.crmsoftware.duview.ui.common.MvpViewPager;
import com.crmsoftware.duview.workers.WorkerTest;



public class Log {
	public static final int LOG_VERBOSE = 0;
	public static final int LOG_DEBUG = 1;
	public static final int LOG_INFO = 2;
	public static final int LOG_WARN = 3;
	public static final int LOG_ERROR = 4;
	public static final int LOG_ASSERT = 5;
	
	public static final int LOG_LEVEL = LOG_DEBUG;
	
	// flag to deactivate all logs
	public static boolean DEBUG = true;
	public static boolean VERBOUSE = true;
	public static boolean INFO = false;
	public static boolean IS_TESTING = false;
	public static boolean IS_RELEASE = true;
 
	public static void setLogLevels()
	{
		if (DEBUG)
		{
			WorkerTest.DEBUG = false;
			//Task.DEBUG = false;
			TaskJsonRequest.DEBUG = false;
			TaskHttpRequest.DEBUG = true;
			TaskImageReqCache.DEBUG = false;
			//TaskGetDataFromStorage.DEBUG = false;			
			//MvpPagContext.DEBUG = true;
			MvpPagPagerAdapter.DEBUG = false;
			MediaViewPagerFragment.DEBUG = false;
			//HorizontalAdapterView.DEBUG = true;
		}
		
		if (VERBOUSE)
		{
			MvpChannelPrograms.VERBOUSE = false;
			MvpViewPager.VERBOUSE = false;
			MvpVerticalListView.VERBOUSE = false;
			MvpHorizontalListView.VERBOUSE = false;
			HorizontalAdapterView.VERBOUSE = true;
		}
		
	}
	
	public static void d(String tag, String message){
		if (LOG_LEVEL <= LOG_DEBUG){

			android.util.Log.d(tag, "[" + Thread.currentThread().getId()+"] " + message);

		}
	}

	
	public static void m(String tag, Object obj, String function){
		if (LOG_LEVEL <= LOG_DEBUG)
		{
			android.util.Log.d(tag, "[" + Thread.currentThread().getId()+"] ::" + function + "\t " + obj);
		}
	}
	
	public static void e(String tag, String message){
		if (LOG_LEVEL <= LOG_ERROR){

			android.util.Log.e(tag, "[" + Thread.currentThread().getId()+"] " + message);

		}
	}
	
	public static void e(String tag, String message, Exception e){
		if (LOG_LEVEL <= LOG_ERROR){
			android.util.Log.e(tag, "[" + Thread.currentThread().getId()+"] " + message, e);
		}
	}
	
	public static void w(String tag, String message){
		if (LOG_LEVEL <= LOG_WARN){

			android.util.Log.w(tag, "[" + Thread.currentThread().getId()+"] " + message);
		}
	}
	
	public static void w(String tag, String message, Exception e){
		if (LOG_LEVEL <= LOG_WARN)
		{
			android.util.Log.w(tag, "[" + Thread.currentThread().getId()+"] " + message, e);

		}
	}
	
	public static void i(String tag, String message){
		if (LOG_LEVEL <= LOG_INFO){

			android.util.Log.i(tag, "[" + Thread.currentThread().getId()+"] " + message);

		}
	}
	
	public static void v(String tag, String message){
		if (LOG_LEVEL <= LOG_VERBOSE){

			android.util.Log.v(tag, "[" + Thread.currentThread().getId()+"] " + message);

		}
	}
	
	private static void logLongString(String tag, String longMessage){
		int maxLogSize = 1000;
	    for(int i = 0; i <= longMessage.length() / maxLogSize; i++) {
	        int start = i * maxLogSize;
	        int end = (i+1) * maxLogSize;
	        end = end > longMessage.length() ? longMessage.length() : end;
	        android.util.Log.d(tag, longMessage.substring(start, end));
	    }
	}
	
	private static void appendLog(String text)
	{ 
	   String filePath = Environment.getExternalStorageDirectory() + "/duView" /*+SettingsConstants.defaultSettings.getClass().getSimpleName()*/ + "_log.file";
	   File logFile = new File(filePath);
	   if (!logFile.exists())
	   {
	      try
	      {
	         logFile.createNewFile();
	      } 
	      catch (IOException e)
	      {
	         e.printStackTrace();
	      }
	   }
	   try
	   {
	      //BufferedWriter for performance, true to set append to file flag
	      BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true)); 
	      buf.append(text);
	      buf.newLine();
	      buf.close();
	   }
	   catch (IOException e)
	   {
	      e.printStackTrace();
	   }
	}
}
