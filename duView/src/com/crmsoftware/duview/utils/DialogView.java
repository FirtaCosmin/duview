package com.crmsoftware.duview.utils;

import android.R.color;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.ui.vodcategories.FilterView;

public class DialogView extends DialogFragment implements  OnClickListener{
	
	public static boolean DEBUG = Log.DEBUG;
	public final static String TAG = DialogView.class.getSimpleName();
	private Dialog dialog;
	protected boolean isVisible;
	private MvpViewHolder viewHolder;
	private String message;
	
	
	 public static final String 			DIALOG_VISIBILITY 	= "dialogVisibility";
	 public static final String 			DIALOG_MSG 			= "dialogMsg";
	 public static final String 			DIALOG_CANCEL 			= "dialogCancel";
	
	private static DialogView f = null;
	public static DialogView getInstance() {
		if(f==null)
		{
			return new DialogView();
		}
		return f;
	}
	
	public DialogView()
	{
		
	}
	
	public DialogView(String message)
	{
		this.message = message;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		f= this;
		if (DEBUG) Log.m(TAG, this, "onCreateView");
		View containerView = inflater.inflate(R.layout.error_dialog_message_view,
				container, false);
		viewHolder = new MvpViewHolder(containerView);
		viewHolder.setClickListener(this, R.id.button_pozitive 
									,R.id.button_negative);
		
		
		if (savedInstanceState != null)
		{
			message = savedInstanceState.getString(DIALOG_MSG,null);
		}
		
		if (null != message)
		{
			viewHolder.setText(R.id.dlg_text, message);
		}
		
		
		
		initDialog();

		return containerView;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void initDialog() {
		if (DEBUG)
			Log.m(TAG, this, "initLoadingFragment");
		dialog = getDialog();
		if (dialog != null) {
			dialog.setCanceledOnTouchOutside(false);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300, getActivity().getResources().getDisplayMetrics());
			WindowManager.LayoutParams params = new WindowManager.LayoutParams();
			params.gravity = Gravity.CENTER;
			dialog.getWindow().setAttributes(params); 
			dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
			dialog.getWindow().setBackgroundDrawableResource(color.transparent);
			dialog.show();
			Window window = dialog.getWindow();
			window.setLayout(size, size / 2);
		}
	}	
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
	    outState.putBoolean(DIALOG_VISIBILITY, isVisible);
	    outState.putString(DIALOG_MSG, message);
	    super.onSaveInstanceState(outState);
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button_pozitive:
				cancelAction();
			break;
			
		
		default:
			break;
		}
	}
	
	protected void makeQuitAction() {
		MainApp.exitApplication();
	}
	
	protected void cancelAction() {
		dialog.cancel();
		dialog.dismiss();
	}
	
}
