package com.crmsoftware.duview.utils;

public interface IConst {
	
	public static final String 				TOKEN_DEFAULT_VALUE 							= "1";
	//public static final String 				INVALID_TOKEN_ERROR_MESSAGE 					= "Invalid token."; 
	//public static final String 				INVALID_TOKEN_ERROR_MESSAGE_MPX					= "Invalid security token.";
	public static final int 				AUTHENTICATION_EXCEPTION_VALUE 					= 401;
	public static final int 				AUTHENTICATION_EXCEPTION_SECOND_VALUE		    = 403;
	public static final int 				AUTHENTICATION_EXCEPTION_500					= 500;
	public static final String 				SMIL_INVALID_AUTHENT_TOKEN_EXCEPTION 			= "InvalidAuthToken";
	public static final String 				SMIL_MISSING_AUTHENT_EXCEPTION 					= "MissingAuth";
	public static final String 				SMIL_LICENSE_NOT_GRANTED_EXCEPTION 				= "LicenseNotGranted";
	public static final String 				AUTHENTICATION_EXCEPTION_TITLE				 	= "AuthenticationException";	
	public static final String 				CONCURRENCY_LIMIT_VIOLATION_AUTH 				= "ConcurrencyLimitViolationAuth";
	
	
	public static final String GUIDE_DATE_FORMAT			= "yyyy-MM-dd";
	
	public static final int				DEFAULT_NB_OF_PREVIOUS_DAYS								= 0;
	public static final int				DEFAULT_NB_OF_NEXT_DAYS									= 7;
	
	public static final String				EXCEPTION										= "exception";

	public static final String			LOADING_VIEW								= "loading_dialog";
	public static final String 			DIALOG_CATEGORY_TAG									= "tv_or_vod_caterory_dialog";
	public static final String 			EMPTY_STRING 								= "";
	public static final String 			SPACE_CHAR 									= " ";
	
	public static final int				QUIT_DIALOG											= 0;
	
//   parenta rating values
	public static final int 			LEVEL_G   											= 0;
	public static final int 			LEVEL_PG  											= 10;
	public static final int 			LEVEL_PG_13  										= 13;
	public static final int 			LEVEL_R  											= 16;
	public static final int 			LEVEL_NC_17 										= 17;
	public static final int 			NO_LEVEL  											= 21; 
	
	//Image chose constnts
	public final static int 			REQUEST_PICK_PICTURE 								= 291;
	public final static int 			REQUEST_CAPTURE_PICTURE 							= 294;
	
	
}
