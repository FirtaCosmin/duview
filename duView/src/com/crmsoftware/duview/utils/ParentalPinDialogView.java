package com.crmsoftware.duview.utils;

import android.R.color;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.ui.common.OnMvpDlgClose;

public class ParentalPinDialogView 
extends DialogFragment 
implements 
OnEditorActionListener ,
OnClickListener{
	public static boolean DEBUG = Log.DEBUG;
	
	public static String RETURN_STATE = "response";
	public static String PIN_VALUE = "pin";
	public final static String TAG = ParentalPinDialogView.class.getSimpleName();
	
	
	protected boolean isVisible;
	private EditText parentalPin;
	protected boolean addToBackStack;
	protected Bundle fragmentD;
	private Dialog dialog;
	private MvpViewHolder viewHolder;
	
	DialogInterface.OnDismissListener mOnDismissListener;
	
	 public static final String 			FRAGMENT_DATA 									= "fragmentData";
	 public static final String 			FRAGMENT_TAG 									= "fragmentTag";
	 
	private static ParentalPinDialogView f = null;
	public static ParentalPinDialogView getInstance() {
		if(f==null)
		{
			return new ParentalPinDialogView();
		}
		return f;
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		f= this;
		if (DEBUG) Log.m(TAG, this, "onCreateView");
		View containerView = inflater.inflate(R.layout.parental_pin_dialog,
				container, false);
		viewHolder = new MvpViewHolder(containerView);
		
		parentalPin = (EditText) viewHolder.getView(R.id.parental_pin);
		parentalPin.setOnEditorActionListener(this);
		parentalPin.setRawInputType(Configuration.KEYBOARD_12KEY);
		viewHolder.setClickListener(this, R.id.button_pozitive 
									,R.id.button_negative);
		
		initDialog();
		if (null != savedInstanceState)
		{
			fragmentD = savedInstanceState.getBundle(FRAGMENT_DATA);
		}
		
		return containerView;
	}
	
	
	private void initDialog() {
		if (DEBUG)
			Log.m(TAG, this, "initLoadingFragment");
		dialog = getDialog();
		if (dialog != null) {
			dialog.setCanceledOnTouchOutside(false);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300, getActivity().getResources().getDisplayMetrics());
			WindowManager.LayoutParams params = new WindowManager.LayoutParams();
			params.gravity = Gravity.CENTER;
			dialog.getWindow().setAttributes(params); 
			dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
			dialog.getWindow().setBackgroundDrawableResource(color.transparent);
			dialog.show();
			dialog.getWindow().setLayout(size, size);
		}
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
	    outState.putBundle(FRAGMENT_DATA, fragmentD);
	    super.onSaveInstanceState(outState);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		parentalPin.setOnFocusChangeListener(new View.OnFocusChangeListener() {
		    @Override
		    public void onFocusChange(View v, boolean hasFocus) {
		        if (hasFocus) {
		        	dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
		        }
		    }
		});
		
		parentalPin.setText(IConst.EMPTY_STRING);
	}
	
	public void cancelAction() {
		dialog.cancel();
		dialog.dismiss();
	}
	
	public void setEmptyField()
	{
		parentalPin.setText(IConst.EMPTY_STRING);
	}
	
	
	public boolean isValidPin(String pin) 
	{
		if(!Utils.isEmpty(pin))
		{
			return true;
		}
		return false;
		
	}

	
	public void setData(String fragmentTag, Bundle fragmentData)
	{
		fragmentD = fragmentData;
		fragmentD.putString(FRAGMENT_TAG, fragmentTag);
	}

	
	
	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (actionId == EditorInfo.IME_ACTION_GO) {
		}
		return false;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button_pozitive:
				//MvpParentalManagement.getInstance().checkPin(parentalPin.getText().toString(), this);
				//activity.showLoadingDialog();
				fragmentD.putInt(RETURN_STATE, v.getId());
				if(null != parentalPin)
				{
					fragmentD.putString(PIN_VALUE, parentalPin.getText().toString());
				}
				Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag(fragmentD.getString(FRAGMENT_TAG));
				if (fragment instanceof OnMvpDlgClose)
				{
					((OnMvpDlgClose)fragment).onMvpDlgClose(getTag(), fragmentD);
				}
				
			break;
			
		case R.id.button_negative:
			fragmentD.putInt(RETURN_STATE, v.getId());
			Fragment fragmentCancel = getActivity().getSupportFragmentManager().findFragmentByTag(fragmentD.getString(FRAGMENT_TAG));
			if (fragmentCancel instanceof OnMvpDlgClose)
			{
				((OnMvpDlgClose)fragmentCancel).onMvpDlgClose(getTag(), fragmentD);
			}
			cancelAction();
			
			
			break;
		
		default:
			break;
		}
	}
	
	@Override
	public void onDestroyView() {
		if (getDialog() != null && getRetainInstance())
		{
			getDialog().setOnDismissListener(null);
		}

		super.onDestroyView();
	}
	public void setOnDismissListener(DialogInterface.OnDismissListener listener){
		mOnDismissListener = listener;
	}
	
	@Override
	public void onDismiss(DialogInterface dialog){
		super.onDismiss(dialog);
		if ( mOnDismissListener != null ){
			mOnDismissListener.onDismiss(dialog);
		}
	}
	

	@Override
	public void onCancel(DialogInterface dialog){
		super.onCancel(dialog);
		if ( mOnDismissListener != null ){
			mOnDismissListener.onDismiss(dialog);
		}
	}



}
