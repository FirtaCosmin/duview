package com.crmsoftware.duview.utils;

import android.R.color;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.FragmentChangeActivity;


public class LoadingView extends DialogFragment {

	public static boolean DEBUG = Log.DEBUG; 
	public final static String TAG = LoadingView.class.getSimpleName();
	
	private Dialog dialog;
	public static LoadingView newInstance() {
        return new LoadingView();
    }
	@Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		if (DEBUG) Log.m(TAG,this,"onCreateView");
		View containerView = inflater.inflate(R.layout.loading_view, container, false);
		
		
		initLoading();
		return containerView;
    }
	
	private void initLoading() {
		if (DEBUG) Log.m(TAG,this,"initLoadingFragment");
		 dialog = getDialog();
		if(dialog!=null) 
		{
			dialog.setCanceledOnTouchOutside(false);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawableResource(color.transparent);
			dialog.show();
		}
	}
	
	@Override
	public void onDismiss(DialogInterface dialog) {

		Activity activity = getActivity();
		
		if (null == activity)
		{
			return;
		}
		
		if (activity instanceof FragmentChangeActivity)
		{
			((FragmentChangeActivity)activity).hideLoadingDialog();
		}
		

		super.onDismiss(dialog);
	}
	
}
