package com.crmsoftware.duview.utils;

import android.R.color;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.IMvpActivity;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.configuration.PlatformSettings;
import com.crmsoftware.duview.core.common.HttpReqParams;
import com.crmsoftware.duview.core.common.TaskJsonRequest;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.data.MvpParentalManagement;
import com.crmsoftware.duview.ui.AuthFragment;
import com.crmsoftware.duview.ui.MvpFragment;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.ui.vodcategories.FilterView;

public class LogOffDialogView extends DialogFragment implements  OnClickListener{
	
	public static boolean DEBUG = Log.DEBUG;
	public final static String TAG = LogOffDialogView.class.getSimpleName();
	private Dialog dialog;
	protected boolean isVisible;
	private MvpViewHolder viewHolder;
	private boolean cancelBtn = true;
	private String message;
	MvpFragment mvpFragment;
	
	private boolean noCancel;
	private boolean isForExit = false;
	
	 public static final String 			DIALOG_VISIBILITY 	= "dialogVisibility";
	 public static final String 			DIALOG_MSG 			= "dialogMsg";
	 public static final String 			DIALOG_CANCEL 			= "dialogCancel";
	 public static final String 			IS_FOR_EXIT 			= "is_for_exit";
	
	private static LogOffDialogView f = null;
	public static LogOffDialogView getInstance() {
		if(f==null)
		{
			return new LogOffDialogView();
		}
		return f;
	}
	
	public LogOffDialogView()
	{
		
	}
	
	public LogOffDialogView(boolean cancelBtn, String message, boolean isForExit)
	{
		this.cancelBtn = cancelBtn;
		this.message = message;
		this.isForExit = isForExit;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		f= this;
		if (DEBUG) Log.m(TAG, this, "onCreateView");
		View containerView = inflater.inflate(R.layout.dialog_message_view,
				container, false);
		viewHolder = new MvpViewHolder(containerView);
		viewHolder.setClickListener(this, R.id.button_pozitive 
									,R.id.button_negative);
		mvpFragment = (MvpFragment) getFragmentManager().findFragmentById(R.id.content_frame);
		
		if (savedInstanceState != null)
		{
			message = savedInstanceState.getString(DIALOG_MSG,null);
			cancelBtn = savedInstanceState.getBoolean(DIALOG_CANCEL, true);
			isForExit = savedInstanceState.getBoolean(IS_FOR_EXIT);
		}
		
		if (null != message)
		{
			viewHolder.setText(R.id.dlg_text, message);
		}
		
		if (false == cancelBtn)
		{
			viewHolder.setVisibility(R.id.button_negative, View.GONE);
		}
		
		
		initDialog();

		return containerView;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void initDialog() {
		if (DEBUG)
			Log.m(TAG, this, "initLoadingFragment");
		dialog = getDialog();
		if (dialog != null) {
			dialog.setCanceledOnTouchOutside(false);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300, getActivity().getResources().getDisplayMetrics());
			WindowManager.LayoutParams params = new WindowManager.LayoutParams();
			params.gravity = Gravity.CENTER;
			dialog.getWindow().setAttributes(params); 
			dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
			dialog.getWindow().setBackgroundDrawableResource(color.transparent);
			dialog.show();
			Window window = dialog.getWindow();
			window.setLayout(size, size / 2);
		}
	}	
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
	    outState.putBoolean(DIALOG_VISIBILITY, isVisible);
	    outState.putString(DIALOG_MSG, message);
	    outState.putBoolean(DIALOG_CANCEL, cancelBtn);
	    outState.putBoolean(IS_FOR_EXIT, isForExit);
	    super.onSaveInstanceState(outState);
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button_pozitive:
				if(isForExit)
				{
					MainApp.exitApplication();
				}
				else
				{
					performLogOut();
					MvpParentalManagement.releaseInstance();
				}
			break;
			
		case R.id.button_negative:
			cancelAction();
			break;
		
		default:
			break;
		}
	}
	
	
	protected void cancelAction() {
		dialog.cancel();
		dialog.dismiss();
	}
	
	public void performLogOut() 
	{
		MvpUserData authData = MvpUserData.getFromMvpObject();
		serverLogOut();
		authData.signOut();
		
		IMvpActivity activity = mvpFragment.getMvpActivity();
		activity.getMenu().onResetData();
		activity.clearStack();
			
		activity.goTo(AuthFragment.class,AuthFragment.TAG, new Bundle(), false);
		cancelAction();
	}
	
	protected void serverLogOut()
	{
		if (DEBUG) Log.m(TAG,this,"serverLogOut");
		
		HttpReqParams httpReqParams = new HttpReqParams(
				IHttpConst.VALUE_SCHEMA_1_0, 
				IHttpConst.VALUE_JSON);
		httpReqParams.setParam(IHttpConst.PARAM_TOKEN_SECOND, MvpUserData.getFromMvpObject().getToken());
		
		TaskJsonRequest task = new TaskJsonRequest(
				PlatformSettings.getInstance().getMpxLogOff(), 
				httpReqParams);	
		
		task.execute();
		
	}
	
}
