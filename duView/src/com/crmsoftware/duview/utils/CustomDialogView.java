package com.crmsoftware.duview.utils;

import android.R.color;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.IMvpActivity;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.configuration.PlatformSettings;
import com.crmsoftware.duview.core.common.HttpReqParams;
import com.crmsoftware.duview.core.common.TaskJsonRequest;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.data.MvpParentalManagement;
import com.crmsoftware.duview.ui.AuthFragment;
import com.crmsoftware.duview.ui.MvpFragment;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.ui.vodcategories.FilterView;

public class CustomDialogView extends DialogFragment implements  OnClickListener{
	
	public static boolean DEBUG = Log.DEBUG;
	public final static String TAG = CustomDialogView.class.getSimpleName();
	private Dialog dialog;
	protected boolean isVisible;
	private MvpViewHolder viewHolder;
	private boolean cancelBtn = true;
	private String message;
	MvpFragment mvpFragment;
	
	private boolean noCancel;
	
	 public static final String 			DIALOG_VISIBILITY 	= "dialogVisibility";
	 public static final String 			DIALOG_MSG 			= "dialogMsg";
	 public static final String 			DIALOG_CANCEL 			= "dialogCancel";
	
	private static CustomDialogView f = null;
	public static CustomDialogView getInstance() {
		if(f==null)
		{
			return new CustomDialogView();
		}
		return f;
	}
	
	public CustomDialogView()
	{
		
	}
	
	public CustomDialogView(boolean cancelBtn, String message)
	{
		this.cancelBtn = cancelBtn;
		this.message = message;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		f= this;
		if (DEBUG) Log.m(TAG, this, "onCreateView");
		View containerView = inflater.inflate(R.layout.genereal_dialog_message_view,
				container, false);
		viewHolder = new MvpViewHolder(containerView);
		viewHolder.setClickListener(this, R.id.button_pozitive 
									,R.id.button_negative);
		mvpFragment = (MvpFragment) getFragmentManager().findFragmentById(R.id.content_frame);
		
		initDialog();

		return containerView;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void initDialog() {
		if (DEBUG)
			Log.m(TAG, this, "initLoadingFragment");
		dialog = getDialog();
		if (dialog != null) {
			dialog.setCanceledOnTouchOutside(false);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300, getActivity().getResources().getDisplayMetrics());
			WindowManager.LayoutParams params = new WindowManager.LayoutParams();
			params.gravity = Gravity.CENTER;
			dialog.getWindow().setAttributes(params); 
			dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
			dialog.getWindow().setBackgroundDrawableResource(color.transparent);
			dialog.show();
			Window window = dialog.getWindow();
			window.setLayout(size, size );
		}
	}	
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
	    outState.putBoolean(DIALOG_VISIBILITY, isVisible);
	    outState.putString(DIALOG_MSG, message);
	    outState.putBoolean(DIALOG_CANCEL, cancelBtn);
	    super.onSaveInstanceState(outState);
	}


	@Override
	public void onClick(View v) 
	{
		switch (v.getId()) 
		{
		case R.id.button_pozitive:
			mvpFragment.getMvpActivity().goTo(AuthFragment.class,
					AuthFragment.TAG, mvpFragment.getData(), true);
			cancelAction();
			break;

		case R.id.button_negative:
			cancelAction();
			break;

		default:
			break;
		}
	}
	
	
	protected void cancelAction() {
		dialog.cancel();
		dialog.dismiss();
	}
	 
}
