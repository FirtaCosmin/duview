package com.crmsoftware.duview.workers;
//******************************************************************************************
//Description:  file created for duView - CRM project
//   Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************


import com.crmsoftware.duview.configuration.PlatformSettings;
import com.crmsoftware.duview.core.common.Chrono;
import com.crmsoftware.duview.core.common.HttpReqParams;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.core.common.TaskDataDownload;
import com.crmsoftware.duview.core.common.TaskHttpRequest;
import com.crmsoftware.duview.core.common.TaskJsonParser;
import com.crmsoftware.duview.core.common.TaskJsonRequest;
import com.crmsoftware.duview.core.common.TaskPageReqCache;
import com.crmsoftware.duview.data.MpxPagHeader;
import com.crmsoftware.duview.data.MvpContent;
import com.crmsoftware.duview.data.MvpList;
import com.crmsoftware.duview.data.MvpMedia;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.data.MvpPage;
import com.crmsoftware.duview.utils.IHttpConst;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;

public class WorkerTest extends Task{

	public static boolean DEBUG = Log.DEBUG; 
	public final static String TAG = "WorkerGetCategoryList"; 
	
	private int startRange;
	private int endRange;
	private String category;
	private String filter;	

	private Chrono chrono;
	private int counter = 0;
	
	MvpObject mvpObject;
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public WorkerTest(String category,int startRange, int endRange, String filter) 
	{
		if (DEBUG) Log.m(TAG,this,"WorkerGetCategoryList");
		
		this.category = category;
		this.filter = filter;
		this.startRange = startRange;
		this.endRange = endRange;
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onExecute() 
	{
		if (DEBUG) Log.m(TAG,this,"onExecute");
		
		chrono = new Chrono();
		
		
		for (int i=0; i<1 && false == isCanceled(); i+= 10)
		{
			
			HttpReqParams httpReqParams = new HttpReqParams(IHttpConst.VALUE_SCHEMA_1_2, IHttpConst.VALUE_CJSON);
			
			httpReqParams.setParam(IHttpConst.PARAM_BY_CATEGORIES, category);	
			httpReqParams.setRange(i, i+10);
			httpReqParams.setSort(IHttpConst.VALUE_FIELD_ADDED);
			//httpReqParams.setFileFields();	// all filefields 
			httpReqParams.setThubmnailFilter("Thumbnail");		
	
			httpReqParams.setFilter(filter);
			
			String sBaseUrl = PlatformSettings.getInstance().getTpMpxFeedUrl();;
			mvpObject = MvpObject.newReference(Utils.formatLink("TpMpxFeedUrl."+category+"."+i+"_"+(i+10)));
			
			TaskPageReqCache<MvpMedia> taskGetMpxFeed = new TaskPageReqCache(
					this, 
					MvpMedia.class,
					MpxPagHeader.class,
					sBaseUrl, 
					httpReqParams,
					mvpObject,0);
			
			executeSubTask(taskGetMpxFeed);

		}
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onReturnFromTask(Task subTask, boolean canceled) 
	{
		if (DEBUG) Log.m(TAG,this,"onReturnFromTask");
		
		super.onReturnFromTask(subTask, canceled);
		
		if (DEBUG)  Log.d(TAG,"elapsed time: " + chrono.print());
		
		if (canceled)
		{
			return;
		}
		

		
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	protected void getImage(MvpContent content) 
	{		
		TaskDataDownload taskGetImage = new TaskDataDownload(content.url);		
		executeSubTask(taskGetImage);			
	}
}
