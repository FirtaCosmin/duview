package com.crmsoftware.duview.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.utils.IConst;
import com.crmsoftware.duview.utils.Log;


public class BrowserFragment extends MvpFragment  {
	public static boolean DEBUG = Log.DEBUG; 
	
	public final static String TAG = BrowserFragment.class.getSimpleName();
	
	public final static  String DISABLE_MENU = "hideMenu";
	public final static  String TITLE = "title";
	public final static  String URL = "url";
	
	private WebView webView;
	String htmlLink;

	
	@Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		if (DEBUG) Log.m(TAG,this,"::onCreateView "  + this);
		
		View view = setContentView(R.layout.fragment_account, container, inflater);

		Boolean hideMenu = getData().getBoolean(DISABLE_MENU, false);
		htmlLink = getData().getString(URL, "");
    	
		getMvpActivity().setMenuEnabled(!hideMenu);
		//getMvpActivity().setActionBarVisibility(!hideMenu ? View.VISIBLE : View.GONE);
		getMvpActivity().setHomeButtonActionBar(!hideMenu);
		getMvpActivity().setTitle(getData().getString(TITLE, ""));
		
    	MvpViewHolder holder = getHolder();
		
		webView = (WebView) holder.getView(R.id.webview);
		webView.getSettings().setJavaScriptEnabled(true);
    	webView.setBackgroundColor(0x00000000);
    	
    	webView.setWebViewClient(
    	new WebViewClient()
    	{
    		public static final String	DBG_TAG					= "WebViewClient";
    		
    		boolean loadingFinished = true;
    		boolean redirect = false;
    		
    		public boolean shouldOverrideUrlLoading(WebView view, String url) 
    		{
    			if (DEBUG) Log.m(TAG,this,"::shouldOverrideUrlLoading "  + this);
    			
    		     if (!loadingFinished) {
    		          redirect = true;
    		       }

    		   loadingFinished = false;
    		   
		          view.loadUrl(url);
		          return true;
    		}
    		
    		public void onPageStarted(WebView view, String url, android.graphics.Bitmap favicon) 
    		{
   				super.onPageStarted(view, url,favicon);
   				
   				if (DEBUG) Log.m(TAG,this,"::onPageStarted "  + this);
   				if (DEBUG) Log.m(TAG,this,"\t\t url:       ->" + url + "<-");
   				if (DEBUG) Log.m(TAG,this,"\t\t htmlLink:  ->" + htmlLink + "<-");
   				
   				if (htmlLink.compareToIgnoreCase(url) == 0)
   				{
   					getMvpActivity().showLoadingDialog(); 
   					loadingFinished = false;
   				}
    		};
    		
		   public void onPageFinished(WebView view, String url) 
		   {
   				if (DEBUG) Log.m(TAG,this,"::onPageFinished" + this);
   				
   				super.onPageFinished(view, url);
   				
   				if (DEBUG) Log.m(TAG,this,"\t\t url:       ->" + url + "<-");
   				if (DEBUG) Log.m(TAG,this,"\t\t htmlLink:  ->" + htmlLink + "<-");
   				
   			   if(!redirect)
   			   {
   		          loadingFinished = true;
   		       }

   		       if(loadingFinished && !redirect)
   		       {
   		    	getMvpActivity().hideLoadingDialog();   
   		       } 
   		       else
   		       {
   		          redirect = false; 
   		       }
   		       
		        
				if (htmlLink.compareToIgnoreCase(url) == 0)
				{
					getMvpActivity().hideLoadingDialog();    	
				}
		    }
		   
		   
		   public void onReceivedError(WebView view, int errorCode, String description, String url) 
		   {
   				if (DEBUG) Log.m(TAG,this,"::onReceivedError" + this);
   				
   				super.onReceivedError(view,errorCode, description, url);
   				
   				if (DEBUG) Log.m(TAG,this,"\t\t url:       ->" + url + "<-");
   				if (DEBUG) Log.m(TAG,this,"\t\t htmlLink:  ->" + htmlLink + "<-");
		        
   				if (htmlLink.compareToIgnoreCase(url) == 0)
   				{
   					getMvpActivity().hideLoadingDialog();    	
   				}
		    }

    	}
    	); 
    	
    	restoreWebView();
 	 	
   	 	return view;
    }
	
	@Override
	public void onDestroyView() 
	{
		getMvpActivity().hideLoadingDialog(); 
		super.onDestroyView();
	}

    
    public void restoreWebView() 
    {
    	webView.loadUrl(htmlLink);    	
	}

}