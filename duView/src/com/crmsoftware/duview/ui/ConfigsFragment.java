package com.crmsoftware.duview.ui;


import java.util.Vector;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.ui.common.ConfigsAdapter;
import com.crmsoftware.duview.ui.common.IMvpGenericAdapter;
import com.crmsoftware.duview.ui.common.IMvpGenericListViewListener;
import com.crmsoftware.duview.ui.common.MvpItemHolder;
import com.crmsoftware.duview.ui.common.MvpSimpleListAdapter;
import com.crmsoftware.duview.ui.configurations.GoToConfigItem;
import com.crmsoftware.duview.ui.configurations.IConfigItem;
import com.crmsoftware.duview.ui.configurations.OnOffConfigItem;
import com.crmsoftware.duview.ui.configurations.OnOffConfigItem_FastPlayer;
import com.crmsoftware.duview.ui.configurations.OnOffConfigItem_Help;
import com.crmsoftware.duview.utils.Log;

public class ConfigsFragment extends MvpFragment implements IMvpGenericListViewListener{

	
	public static String TAG = ConfigsFragment.class.getSimpleName();
	
	private MvpSimpleListAdapter mConfigsListViewAdapter;
	
	/**
	 * @desc this list will contain the configuration items
	 */
	private Vector<IConfigItem> mconfigItemsForList;

	
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		super.onCreateView(inflater, container, savedInstanceState);
		View view = setContentView(R.layout.config_fragment, container, inflater);
		
		getMvpActivity().setMenuEnabled(true);
		getMvpActivity().setActionBarVisibility(View.VISIBLE);
		getMvpActivity().setHomeButtonActionBar(true);
		getMvpActivity().setTitle(MainApp.getRes().getString(R.string.config_fragmentTitle));

		
		/*create the adapter for the listView*/
		mConfigsListViewAdapter = new ConfigsAdapter(null, this);
		/*add the configuration items to the list*/
		initConfigs();
		mConfigsListViewAdapter.setCount(mconfigItemsForList.size());
		
		ListView theList = (ListView) getHolder().getView(android.R.id.list);
		theList.setAdapter(mConfigsListViewAdapter);
		
		
		
		return view;
	}
	
	
	@Override
	public void onStop (){
		super.onStop();
		/*will save the settings*/
		for( IConfigItem item : mconfigItemsForList ){
			item.saveTheData();
		}
	}
	

	
	
	
	
	/**
	 * ****************************************
	 * 					PRIVATE
	 * ****************************************
	 * */
	
	/**
	 * @desc method that will create the list of config items 
	 * 		 that will be printed
	 */
	private void initConfigs(){
		
		mconfigItemsForList = new Vector<IConfigItem>();
		MvpUserData data = MvpUserData.getFromMvpObject();
		Resources res = getResources();
		if(!data.isAnonymous())
		{
			/*
			 * The avatar and fastplayer options are available only if not in anonymous mode 
			 * */
			
			/*the Avatar item*/
			GoToConfigItem avatar = new GoToConfigItem(res.getString(R.string.config_avatar_title), 
					res.getString(R.string.config_avatar_desc),
					getMvpActivity(), 
					SocialFragment.class,
					SocialFragment.TAG);
			mconfigItemsForList.add(avatar);
			mConfigsListViewAdapter.receiveClickFrom(R.id.config_goto_item_layout);
			
			/*the FastPlayer Switch*/
			OnOffConfigItem fastPlayerSwitch = new OnOffConfigItem_FastPlayer(res.getString(R.string.config_fastPlay_title),
					res.getString(R.string.config_fastPlay_desc),
					this);
			fastPlayerSwitch.setValue(data.isFastPlayer());
			mconfigItemsForList.add(fastPlayerSwitch);
			
		}
		
		/*the help dialog*/
		OnOffConfigItem helpDiagSwitch = new OnOffConfigItem_Help(res.getString(R.string.config_help_title),
				res.getString(R.string.config_help_desc),
				this);
		helpDiagSwitch.setValue(data.getHelp());
		mconfigItemsForList.add(helpDiagSwitch);

		/*will receive click from pressing the toggle buttons on the OnOffConfigItems*/
		mConfigsListViewAdapter.receiveClickFrom(R.id.config_onoff_item_toggleButton);
		
	}
	

	
	
	/*******************************************
	 * 
	 * IMvpGenericListViewListener 
	 * 
	 * ========================================
	 * Implementation START
	 * 
	 * *****************************************/
	
	@Override
	public void onListItemViewClick(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder) {
		Log.d(TAG, "onListItemViewClick");
		/*forward the click event to the config item*/
		mconfigItemsForList.get(holder.getPosition()).doClickFor(holder, view.getId());
		
	}

	@Override
	public boolean onListItemViewTouch(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder, MotionEvent event) {
		return false;
	}

	@Override
	public void onListItemUpdateView(IMvpGenericAdapter adapter,
			MvpItemHolder holder, int position) {

		mconfigItemsForList.get(position).updateHolder(holder);
		
	}

	@Override
	public int onListItemGetLayoutId(IMvpGenericAdapter adapter, int position) {
		return mconfigItemsForList.get(position).getViewId();
	}

	@Override
	public void onListItemsCountChanged(IMvpGenericAdapter adapter, int newCount) {
		
	}
	
	/*******************************************
	 * 
	 * IMvpGenericListViewListener 
	 * 
	 * ========================================
	 * Implementation END
	 * 
	 * *****************************************/
}
