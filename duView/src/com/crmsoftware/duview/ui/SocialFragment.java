package com.crmsoftware.duview.ui;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Matrix.ScaleToFit;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.slidemenu.imagechooser.ChooserListener;
import com.crmsoftware.duview.slidemenu.imagechooser.ChooserManager;
import com.crmsoftware.duview.slidemenu.imagechooser.ImageChoosen;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.utils.IConst;
import com.crmsoftware.duview.utils.Log;

	public class SocialFragment extends MvpFragment implements OnClickListener, ChooserListener  {
		public static boolean DEBUG = Log.DEBUG; 
		
		public final static String TAG = AboutFragment.class.getSimpleName();

		private ImageView userImage;
		private ChooserManager chooserManager;
		private String filePath;


		/**
		 * 
		 *
		 * @param  
		 * @return      
		 * @see         
		 */	
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,

				Bundle savedInstanceState) 
		{		
			if (DEBUG) Log.m(TAG,this,"onCreateView");
			
			View view = setContentView(R.layout.social_view,container,inflater);
			
			MvpViewHolder holder = getHolder();
			
			holder.setClickListener(this, R.id.delete_button,
										  R.id.choose_picture_button,
										  R.id.take_picture_button);
			
			userImage = (ImageView) holder.getView(R.id.user_image_value);
			
			getMvpActivity().setMenuEnabled(true);
			getMvpActivity().setActionBarVisibility(View.VISIBLE);
			getMvpActivity().setHomeButtonActionBar(true);
			getMvpActivity().setTitle(MainApp.getRes().getString(R.string.social));

			update();
			return view;
		}
		
		public void update()
		{
			MvpObject mvpAuthObj = MvpObject.newReference(AuthFragment.MVP_AUTH_DATA);
			MvpUserData data = (MvpUserData)mvpAuthObj.getData();
			
			if (null != data)
			{
				try 
				{
					if (false == data.getLogo().isEmpty())
					{
						userImage.setImageURI(Uri.parse(new File(data.getLogo()).toString()));
						userImage.setScaleType(ScaleType.FIT_XY);
					}
					else
					{
						userImage.setImageResource(R.drawable.du_logo_white);
						userImage.setScaleType(ScaleType.CENTER_INSIDE);
					}
					
				} 
				catch (Exception e) 
				{
					userImage.setImageResource(R.drawable.du_logo_white);
					userImage.setScaleType(ScaleType.CENTER_INSIDE);
				}
				
				
			}	
			

		}

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.choose_picture_button:
				{	
					chooseImage();
				}
				break;
			case R.id.take_picture_button:
				{
					takePicture();
					break;
				}
			case R.id.delete_button:
				{
					MvpObject mvpAuthObj = MvpObject.newReference(AuthFragment.MVP_AUTH_DATA);
					MvpUserData data = (MvpUserData)mvpAuthObj.getData();
					/*delete the file from storage*/
					(new File(data.getLogo())).delete();
					/*delete the file from preferences*/
					data.setLogo("");
					
					userImage.setImageResource(R.drawable.du_logo_white);
					userImage.setScaleType(ScaleType.CENTER_INSIDE);
					
					
					ImageView view = getMvpActivity().getMenu().getImage();
					
					view.setImageResource(R.drawable.du_logo_white);
					view.setScaleType(ScaleType.CENTER_INSIDE);
					
				}
				break;

			default:
				break;
			}
		}
		
		private void chooseImage() {
			chooserManager = new ChooserManager(this,
					IConst.REQUEST_PICK_PICTURE, "duView", true);
			chooserManager.setImageChooserListener(this);
			try 
			{
				filePath = chooserManager.choose();
			} catch (IllegalArgumentException e) 
			{
				e.printStackTrace();
			} catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		
		private void takePicture() {
			chooserManager = new ChooserManager(this,
					IConst.REQUEST_CAPTURE_PICTURE,"duView", true);
			chooserManager.setImageChooserListener(this);
			try 
			{
				filePath = chooserManager.choose();
			} catch (IllegalArgumentException e) 
			{
				e.printStackTrace();
			} catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		
		

		@Override
		public void onImageChosen(final ImageChoosen image) {
			((Activity) getMvpActivity()).runOnUiThread(new Runnable() {

				@Override
				public void run() {
					if (image != null)
					{
						try 
						{
							String filePath = image.getFileThumbnail();
							userImage.setImageURI(Uri.parse(new File(filePath).toString()));	
							userImage.setScaleType(ScaleType.FIT_XY);
							
							userImage.setImageURI(Uri.parse(new File(filePath).toString()));
							getMvpActivity().getMenu().getImage().setImageURI(Uri.parse(new File(filePath).toString()));
							
							MvpObject mvpAuthObj = MvpObject.newReference(AuthFragment.MVP_AUTH_DATA);
							MvpUserData data = (MvpUserData)mvpAuthObj.getData();
							/*delete the old logo*/
							(new File(data.getLogo())).delete();
							/*save the new logo*/
							data.setLogo(filePath);
							cleanImages(image);
						} 
						catch (Exception e) 
						{
							// TODO: handle exception
						}
						
					}
				}
			});
			
		}

		@Override
		public void onError(final String reason) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					showToast(reason);
				}
			});
			
		}
		@Override
		public void onActivityResult(int requestCode, int resultCode,
				Intent data) {
			super.onActivityResult(requestCode, resultCode, data);
			if (resultCode == -1
					&& (requestCode == IConst.REQUEST_PICK_PICTURE || requestCode == IConst.REQUEST_CAPTURE_PICTURE)) {
				if (chooserManager == null) {
					int typeState = requestCode;
					reinitializeImageChooser(typeState);
				}
				chooserManager.submit(requestCode, data);
			}
		}
		
		private void reinitializeImageChooser(int typeState ) {
			chooserManager = new ChooserManager(this, typeState,
					"duView", true);
			chooserManager.setImageChooserListener(this);
			chooserManager.reinitialize(filePath);
		}
		
		/**
		 * @param image
		 * @desc deletes the small thumbnail and the big image
		 */
		private void cleanImages(final ImageChoosen image){
			(new File(image.getFilePathOriginal  ())).delete();
			(new File(image.getFileThumbnailSmall())).delete();
			
		}

	}
