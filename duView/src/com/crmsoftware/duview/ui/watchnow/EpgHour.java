package com.crmsoftware.duview.ui.watchnow;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************



import java.util.Calendar;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.utils.Log;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.LinearLayout;


public class EpgHour extends LinearLayout{
	public static boolean VERBOUSE = Log.VERBOUSE ; 
	
	public static boolean markCurrentHour = true;
	public static Paint paintBackground = null;

	public static Paint paintTextHour;
	
	public static int textHeight = 0;
	public static int hourY = 0;
	public static int hourX = 0;
	public static int textColorSelected;
	public static int textColorUnSelected;
	
	private boolean drawSelected  = false;
	private String text = "";
	private int hour = 0;
	private int half = 0;
	
	public EpgHour(Context context, AttributeSet attrs) {
		super(context, attrs);
		initAttr(attrs);
		setWillNotDraw(false) ;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	private void initAttr(AttributeSet attrs)
	{	
		if (paintBackground == null)
		{
			Resources res = getResources();
		
			
			textHeight = (int) res.getDimension(R.dimen.epg_hour_textHeight);
			hourX = (int)res.getDimension(R.dimen.epg_hour_X);
			hourY = (int)res.getDimension(R.dimen.epg_hour_Y);
			
			textColorSelected = res.getColor(R.color.epg_cell_selected_light);
			textColorUnSelected = res.getColor(R.color.epg_hour_textcolor);			

			
			paintBackground = new Paint();
			paintBackground.setStyle(Paint.Style.FILL);
			paintBackground.setColor(res.getColor(R.color.epg_backgroud));			
			
			paintTextHour = new Paint();
			paintTextHour.setAntiAlias(true);
			paintTextHour.setTextSize(textHeight);
			paintTextHour.setTypeface(MvpViewHolder.typefaceLight);		
		}
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);

	    this.setMeasuredDimension(12, MeasureSpec.getSize(heightMeasureSpec));
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onDraw(Canvas canvas) {
		
		RectF rect = new RectF(getPaddingLeft(), getPaddingTop(), getWidth() - getPaddingRight(), getHeight() - getPaddingBottom());
		
		drawHour(canvas, rect);
		
		super.onDraw(canvas);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void setData(int position) 
	{		    
		this.hour = position / 2;
		this.half = position % 2;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	protected void drawHour(Canvas canvas, RectF rect) 
	{	
	    Calendar calendar = Calendar.getInstance();
	    int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
	    int currentMin = calendar.get(Calendar.MINUTE);
	    
	    int hourProc = hour;
	    drawSelected = markCurrentHour && (currentHour == hour && (currentMin/30)==half);
	    
	    String postHour = " am";
	    
	    if (hourProc > 12)
	    {
	    	hourProc -= 12;
	    	postHour = " pm";
	    }
	    
	    text = ""+hourProc + ":";
	    
	    if (half == 0)
	    {
	    	text += "00";
	    }
	    else
	    {
	    	text += "30"; 
	    }
	       
	    text += postHour;
	    
		canvas.drawRect(rect, paintBackground);
		
		paintTextHour.setColor(drawSelected ? textColorSelected : textColorUnSelected);
		canvas.drawText(text, hourX, hourY, paintTextHour);
	}
	
}
