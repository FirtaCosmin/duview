package com.crmsoftware.duview.ui.watchnow;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.TextView;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.FragmentChangeActivity;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.configuration.PlatformSettings;
import com.crmsoftware.duview.core.common.HttpReqParams;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.data.MvpMedia;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.data.MvpPagHeader;
import com.crmsoftware.duview.data.MvpParentalManagement;
import com.crmsoftware.duview.data.MvpProgram;
import com.crmsoftware.duview.data.MvpServerError;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.ui.MvpFragment;
import com.crmsoftware.duview.ui.common.IMvpGenericAdapter;
import com.crmsoftware.duview.ui.common.IMvpGenericListViewListener;
import com.crmsoftware.duview.ui.common.MediaViewPagerFragment;
import com.crmsoftware.duview.ui.common.MvpImageView;
import com.crmsoftware.duview.ui.common.MvpItemHolder;
import com.crmsoftware.duview.ui.common.MvpPagContext;
import com.crmsoftware.duview.ui.common.MvpPagListAdapter;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.ui.common.UnderConstrFragment;
import com.crmsoftware.duview.utils.IConst;
import com.crmsoftware.duview.utils.IHttpConst;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.ParentalPinDialogView;
import com.crmsoftware.duview.utils.Utils;

public class ProgramsListViewFragment 
extends MvpFragment 
implements 
IMvpGenericListViewListener, 
OnClickListener, 
OnScrollListener,
ITaskReturn
{

	public static boolean DEBUG = Log.DEBUG; 
	public final static String TAG = ProgramsListViewFragment.class.getSimpleName();
	
	public final static String DATA_POSITION = "DATA_POSITION"; 
	public final static String DATA_QUERY = "DATA_QUERY"; 
	public final static String DATA_FAVORITES = "DATA_FAVORITES"; 
	
	private MvpObject mvpObjContext;
	private Map<String,MvpMedia> mapChannels;
	private String listDays[];
	private int offset;
	private MvpPagListAdapter adapter;
	private TextView title;
	private Bundle data;
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		if (DEBUG) Log.m(TAG,this,"onCreateView");

		View view = setContentView(R.layout.fragment_circular_parallax_vertical_list_view,container,inflater);
//		View view = setContentView(R.layout.fragment_vertical_list_view,container,inflater);
		MvpViewHolder holder = getHolder();
		
		getMvpActivity().setMenuEnabled(true);
		getMvpActivity().setActionBarVisibility(View.VISIBLE);
		getMvpActivity().setHomeButtonActionBar(true);

		
		MvpObject mvpChannelsMap = MvpObject.newReference(Utils.formatLink(TaskGetTvChannels.OBJPREFIX_TVCHANNELS_MAP));
		
		mapChannels = (Map<String,MvpMedia>) mvpChannelsMap.getData();
		
        listDays = getResources().getStringArray(R.array.epg_days);
		offset = Calendar.getInstance().get(Calendar.ZONE_OFFSET) + Calendar.getInstance().get(Calendar.DST_OFFSET);
		
		Bundle viewData = getData();
		
		final int position = viewData.getInt(DATA_POSITION,0);
		
		String queryText = viewData.getString(DATA_QUERY, "");
		Boolean favorites = viewData.getBoolean(DATA_FAVORITES, false);
		performQuery(queryText, favorites);

		MvpPagContext context = (MvpPagContext)mvpObjContext.getData();

		
		final ListView listView = (ListView) holder.getView(R.id.items_pagedlist);
		listView.setOnScrollListener(this);
		
		adapter = new MvpPagListAdapter<MvpProgram>(
	 			MvpProgram.class,
	 			1);
		
		adapter.setPaginationContext(mvpObjContext);
		adapter.setGenericListViewListener(this);
		adapter.receiveClickFrom(R.id.description_layout);

		
		listView.setAdapter(adapter);
		
		new Handler().post(new Runnable() {
			
			@Override
			public void run() {
				listView.setSelection(position);
			}
		});
		 
		return view;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onPause() {

		if (null != mvpObjContext)
		{
			MvpPagContext ctx = (MvpPagContext) mvpObjContext.getData();		
			ctx.cancelAllRequests();
		}
		
		super.onPause();
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onResetData() {
		
		if (null != mvpObjContext)	// release the context and free the memory
		{
			MvpObject.removeReference(mvpObjContext.getId());
		}
		
		if (null != adapter)
		{
			adapter.setGenericListViewListener(null);
			adapter = null;
		}
		
		super.onResetData();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onListItemViewClick(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder) {
		
		 data = getData();
		
		data.putInt(MediaViewPagerFragment.DATA_POSITION, holder.getPosition());
		data.putString(MediaViewPagerFragment.DATA_CONTEXT, mvpObjContext.getId());
		
		
		ProgramsViewPagerFragment fragment = new ProgramsViewPagerFragment();
		fragment.setData(data);
		MvpProgram  mvpProgram = (MvpProgram) adapter.getItem(holder.getPosition());
		if(null == mvpProgram)
		{
			return;
		}
		MvpMedia channel = mapChannels.get(mvpProgram.getChannelId());

			
		if (null != channel && MvpParentalManagement.getInstance().checkLevel(channel.getParentalLevel()) == MvpParentalManagement.LEVEL_NOK)
		{
			ParentalPinDialogView parentalDialogView =   ParentalPinDialogView.getInstance();
			parentalDialogView.setData(getTag(), data);
			parentalDialogView.show(getChildFragmentManager(), ParentalPinDialogView.TAG);
		}
		else
		{
			FragmentChangeActivity fca = (FragmentChangeActivity) getActivity();
			fca.goTo(ProgramsViewPagerFragment.class, ProgramsViewPagerFragment.TAG, data, true);
		}	
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public boolean onListItemViewTouch(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onListItemUpdateView(IMvpGenericAdapter adapter,
			MvpItemHolder holder, int position) {
		
		MvpProgram  mvpProgram = (MvpProgram) adapter.getItem(position);
		
		if (null == mvpProgram)
		{
			holder.setVisibility(R.id.item_loading, View.VISIBLE);
			holder.setVisibility(R.id.body_view, View.INVISIBLE);
			return;
		}
		
		holder.setClickListener(this, R.id.watch_later_button, R.id.watch_now_button);
		
		holder.setVisibility(R.id.item_loading, View.INVISIBLE);
		holder.setVisibility(R.id.body_view, View.VISIBLE);
		
		title = (TextView) holder.getView(R.id.item_title);
		holder.setText(R.id.item_title, /*"[" + position + "] " +*/ mvpProgram.getTitle());
		title.setEllipsize(TextUtils.TruncateAt.MARQUEE);
		title.setSingleLine(true);
		title.setMarqueeRepeatLimit(5);
		title.setSelected(true);
		
		holder.setText(R.id.item_description, mvpProgram.getShortDescription());
		

		holder.setText(R.id.item_duration,  mvpProgram.getDurationInMin() + " min");
		
		long startTime = mvpProgram.getStartTime() + offset;
		long endTime = mvpProgram.getEndTime() + offset;
		
		long currentTime = Calendar.getInstance().getTimeInMillis();
		if (startTime <= currentTime && currentTime <= endTime)
		{
			LiveProgressBarView progress = (LiveProgressBarView) holder.getView(R.id.media_seekbar);
			progress.setData(startTime, endTime);
			progress.setVisibility(View.VISIBLE);
			holder.setVisibility(R.id.item_schedule, View.GONE);
		}
		else
		{
			String sSchedule = Utils.dateToString(startTime,EpgProgramsList.dateFormat) + "-" + Utils.dateToString(endTime,EpgProgramsList.dateFormat);
			holder.setText(R.id.item_schedule, sSchedule);	
			holder.setVisibility(R.id.item_schedule, View.VISIBLE);
			holder.setVisibility(R.id.media_seekbar, View.GONE);
		}

		int day = ProgramDetailsView.getDay(startTime, endTime);
		
		 holder.setText(R.id.item_date_day, listDays[day]);
		 

		if (position % 2 == 0) 
		{
			holder.getView(R.id.body_view).setBackgroundResource(R.color.first_item);
		} 
		else
		{
			holder.getView(R.id.body_view).setBackgroundResource(R.color.second_item);
		}
		
		MvpImageView itemImage = (MvpImageView) holder.getView(R.id.item_image); 
		Utils.showChannelImage(mapChannels, mvpProgram, itemImage);
		 

	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.watch_later_button:
			getMvpActivity().goTo(UnderConstrFragment.class, UnderConstrFragment.TAG, UnderConstrFragment.buildData("Wish list feature"), true);
			break;
			
		case R.id.watch_now_button:
			getMvpActivity().goTo(UnderConstrFragment.class, UnderConstrFragment.TAG, UnderConstrFragment.buildData("Player feature"), true);			
			break;
		
		default:
			break;
		}
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onListItemsCountChanged(IMvpGenericAdapter fromAdapter, int newCount) 
	{

		getMvpActivity().hideLoadingDialog();
		
		if (null == adapter)
		{
			return;
		}
		
		MvpPagContext pagContext = (MvpPagContext)fromAdapter.getPaginationContext().getData();
		
		if (adapter.getCount() != newCount)
		{
			String queryText = getData().getString(DATA_QUERY, "");
			
			MvpPagContext context = (MvpPagContext)mvpObjContext.getData();
			getMvpActivity().setTitle("'"+queryText+"'" + " results: " + pagContext.getCount() +"");
		}
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void performQuery(String queryString, Boolean favorites)
	{	
		Bundle viewData = getData();
	
		String mvpObjContextId = "epg.search";
		
		mvpObjContext = MvpObject.getReference(mvpObjContextId);
		
		if (null != mvpObjContext)
		{
			MvpPagContext context = (MvpPagContext)mvpObjContext.getData();
			getMvpActivity().setTitle("'"+queryString+"'" + " results: " + context.getCount() +"");

			return;
		}
		
		getMvpActivity().showLoadingDialog();
		getMvpActivity().setTitle("Searching on '"+queryString+"'");
		
		MvpViewHolder holder = getHolder();
		
/*		holder.setVisibility(R.id.item_loading, View.VISIBLE);
		holder.setVisibility(R.id.body_view, View.INVISIBLE);*/
		

		int offset = Calendar.getInstance().get(Calendar.ZONE_OFFSET) + Calendar.getInstance().get(Calendar.DST_OFFSET);

		
		Calendar startTime = Calendar.getInstance();
		startTime.set(Calendar.HOUR_OF_DAY, 0);
		startTime.set(Calendar.MINUTE, 0);
		startTime.set(Calendar.SECOND, 0);
		startTime.set(Calendar.MILLISECOND, 0);	

			
		Calendar endTime = Calendar.getInstance();
		endTime.set(Calendar.HOUR_OF_DAY, 23);
		endTime.set(Calendar.MINUTE, 59);
		endTime.set(Calendar.SECOND, 59);
		endTime.set(Calendar.MILLISECOND, 0);	
		endTime.add(Calendar.DAY_OF_MONTH, IConst.DEFAULT_NB_OF_NEXT_DAYS);

		SimpleDateFormat dateFormater = new SimpleDateFormat(IConst.GUIDE_DATE_FORMAT+"'T'HH:mm:SS'Z'");
		String start = dateFormater.format(new Date(startTime.getTimeInMillis() - offset ));
		String end = dateFormater.format(new Date(endTime.getTimeInMillis() - offset));
		
		
		HttpReqParams httpReqParams = new HttpReqParams(
				IHttpConst.VALUE_SCHEMA_1_1
				);

		httpReqParams.setParam(IHttpConst.ACCOUNT_NAME, PlatformSettings.getInstance().getAccountNameValue());
		httpReqParams.setParam(IHttpConst.METHOD_PARAM, IHttpConst.METHOD_PARAM_GET);
		httpReqParams.setParam(IHttpConst.PARAM_TOKEN, MvpUserData.getFromMvpObject().getToken());
		httpReqParams.setParam(IHttpConst.PARAM_KEY, queryString.toLowerCase());
		httpReqParams.setParam(IHttpConst.PARAM_START_DATE, start);
		httpReqParams.setParam(IHttpConst.PARAM_END_DATE, end);
		
		if (favorites)
		{
			httpReqParams.setParam(IHttpConst.PARAM_FAVORITES_ON, IHttpConst.VALUE_FAVORITES_TRUE);			
		}
		
		mvpObjContext = MvpPagContext.getContext(
				MvpPagHeader.class,
				MvpPagContext.STARTWITH_ZERO,
				PlatformSettings.getInstance().getSearchUrl(),
	 			httpReqParams,
	 			mvpObjContextId,
	 			-1);
		
	}


	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		
		if (scrollState == SCROLL_STATE_IDLE)
		{
			int pos = 0;
			if (null != mvpObjContext)
			{
				MvpPagContext context = (MvpPagContext)mvpObjContext.getData();
			
				if (null != context)
				{
					pos = context.getFirstVisibleItem();
				}
			}
			getData().putInt(MediaViewPagerFragment.DATA_POSITION, pos);
		}
	}


	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int onListItemGetLayoutId(IMvpGenericAdapter adapter, int position) 
	{

		return R.layout.program_see_all_list_view_row;
	}
	
	@Override
	public void onMvpDlgClose(String dlgTag, Bundle data) {
		
     int viewId = data.getInt(ParentalPinDialogView.RETURN_STATE);
		
		switch (viewId) 
		{
		case R.id.button_pozitive:
			String pin = data.getString(ParentalPinDialogView.PIN_VALUE);
			if(Utils.isEmpty(pin))
			{
				showToast("Field is empty");
			}
			else
			{
				getMvpActivity().showLoadingDialog();
				MvpParentalManagement.getInstance().checkPin(pin, this);
			}
			break;

		default:
			break;
		}
		
		super.onMvpDlgClose(dlgTag, data);
	}

	@Override
	public void onReturnFromTask(Task task, boolean canceled) {
		if(task.isError())
		{
			Object error = task.getError();
			MvpServerError mvpError = (MvpServerError)error;
			int errorCode = mvpError.getResponseCode();
			getMvpActivity().hideLoadingDialog();
			if(errorCode == 200)
			{
				FragmentChangeActivity fca = (FragmentChangeActivity) getActivity();
				fca.goTo(ProgramsViewPagerFragment.class, MediaViewPagerFragment.TAG, data, true);
			}
			if (errorCode == 403)
			{
				showToast(mvpError.getTitle());
				return;
			}
		
			if (errorCode == 400)
			{
				showToast(MainApp.getRes().getString(R.string.change_pins_settings_wrong));
				return;
			}
		}
		
	}

}
