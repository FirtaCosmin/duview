package com.crmsoftware.duview.ui.watchnow;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************



import com.crmsoftware.duview.R;
import com.crmsoftware.duview.data.MvpChannelPrograms;
import com.crmsoftware.duview.data.MvpProgram;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.utils.Log;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.LinearLayout;


public class EpgProgram extends LinearLayout{
	public static boolean VERBOUSE = Log.VERBOUSE ; 
	
	public static Paint paintBackground = null;
	public static Paint paintBackgroundNoInfo;
	public static Paint paintCurrentDark;
	public static Paint paintCurrentLight;
	public static Paint paintProgramTitle;
	public static Paint paintProgramSchedule;
	
	public static int currentBottomBar = 0;
	public static int titleTextHeight = 0;
	public static int scheduleTextHeight = 0;
	public static int titleY = 0;
	public static int titleX = 0;
	public static int scheduleY = 0;
	public static int textColorWithInfo;
	public static int textColorWithoutInfo;
	public static int timelineBumpWidth;
	private RectF rect;
	
	public boolean selected  = false;
	private MvpProgram program = null;
	
	public EpgProgram(Context context, AttributeSet attrs) {
		super(context, attrs);
		initAttr(attrs);
		setWillNotDraw(false) ;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	private void initAttr(AttributeSet attrs)
	{	
		initGraphics(getResources());
	}
	
	
	public static void initGraphics(Resources res)
	{
		if (paintBackground == null)
		{
			
			currentBottomBar = (int)res.getDimension(R.dimen.epg_selected_barHeight);

			titleTextHeight = (int) res.getDimension(R.dimen.epg_program_titleHeight);
			scheduleTextHeight = (int)res.getDimension(R.dimen.epg_program_scheduleHeight);
			timelineBumpWidth = (int)res.getDimension(R.dimen.epg_timeline_bump_width);
			
			titleX = (int)res.getDimension(R.dimen.epg_program_title_X);
			titleY = (int)res.getDimension(R.dimen.epg_program_title_Y);
			scheduleY = (int)res.getDimension(R.dimen.epg_program_schedule_Y);
			
			textColorWithInfo = Color.WHITE;
			textColorWithoutInfo = res.getColor(R.color.light_gray);			
			
			paintBackground = new Paint();
			paintBackground.setStyle(Paint.Style.FILL);
			paintBackground.setColor(res.getColor(R.color.black));
			
			paintBackgroundNoInfo = new Paint();
			paintBackgroundNoInfo.setStyle(Paint.Style.FILL);
			paintBackgroundNoInfo.setColor(res.getColor(R.color.epg_cell_noinfo_background));
			
			
			paintCurrentDark = new Paint();
			paintCurrentDark.setStyle(Paint.Style.FILL);
			paintCurrentDark.setColor(res.getColor(R.color.epg_cell_selected_dark));		
			
			paintCurrentLight = new Paint();
			paintCurrentLight.setStyle(Paint.Style.FILL);
			paintCurrentLight.setColor(res.getColor(R.color.epg_cell_selected_light));	
			
			
			paintProgramTitle = new Paint();
			paintProgramTitle.setAntiAlias(true);
			paintProgramTitle.setTextSize(titleTextHeight);
			paintProgramTitle.setTypeface(MvpViewHolder.typefaceBold) ;
			
			paintProgramSchedule = new Paint();
			paintProgramSchedule.setAntiAlias(true);
			paintProgramSchedule.setTextSize(scheduleTextHeight);
			paintProgramSchedule.setTypeface(MvpViewHolder.typefaceLight) ;
		}
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);

	    this.setMeasuredDimension(12, MeasureSpec.getSize(heightMeasureSpec));
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) 
	{
		rect = new RectF(getPaddingLeft(), getPaddingTop(), getWidth() - getPaddingRight(), getHeight() - getPaddingBottom());
		
		super.onSizeChanged(w, h, oldw, oldh);
	}
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onDraw(Canvas canvas) {
		
		drawProgram(canvas, rect);
		
		super.onDraw(canvas);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void setSelected(boolean state) 
	{	
		selected = state;
		invalidate();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void setProgram(MvpProgram program) 
	{	
		this.program = program;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public MvpProgram getProgram()
	{	
		return this.program;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	protected void drawProgram(Canvas canvas, RectF rect) 
	{	

		int currentPos = EpgChannelsList.getTimeLinePosOnChild(this);
		boolean hasInfo = program != null && !program.getId().isEmpty();
		canvas.clipRect(rect);
		
		Paint cellBack = !hasInfo ? paintBackgroundNoInfo : paintBackground;
		canvas.drawRect(rect, selected ? paintCurrentLight : cellBack);
		
		if (currentPos != -1)
		{			
			drawProgressBar(canvas,rect,currentPos,!selected ? currentBottomBar : 0);
		}
		
		paintProgramTitle.setColor(hasInfo ? textColorWithInfo : textColorWithoutInfo);
		paintProgramSchedule.setColor(hasInfo ? textColorWithInfo : textColorWithoutInfo);
		
		String sTitle = MvpChannelPrograms.NO_PROG_INFO;
		String sSchedule = "";
		if (program != null)
		{
			if (program.getTitle() != null && program.getId() != null)
			{
				sTitle = program.getTitle();
			}
			sSchedule = program.printSchedule(EpgProgramsList.dateFormat);
		}
		
		canvas.drawText(sTitle, titleX, titleY , paintProgramTitle);
		canvas.drawText(sSchedule, titleX, scheduleY, paintProgramSchedule);	
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public static void drawProgressBar(Canvas canvas, RectF rect, int progress, int shrink)
	{
		if (progress == -1)
		{
			return;
		}
		
		RectF halfLeft = new RectF(rect);
		RectF halfRight = new RectF(rect);

		halfRight.left += progress;
		halfLeft.right = halfRight.left;
		
		if (0 != shrink)
		{
			halfLeft.top = halfLeft.bottom - currentBottomBar;
			halfRight.top = halfRight.bottom - currentBottomBar;
		}
		
		canvas.drawRect(halfLeft, paintCurrentDark);
		canvas.drawRect(halfRight, paintCurrentLight);		
	}
	
}
