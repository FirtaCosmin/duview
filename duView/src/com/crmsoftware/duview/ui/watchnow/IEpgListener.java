package com.crmsoftware.duview.ui.watchnow;

import com.crmsoftware.duview.data.MvpProgram;

import android.view.View;

public interface IEpgListener {
	public void OnProgramClick(View channelRow, MvpProgram program, boolean play);
	public void OnWatchChannelClick(String channelId, String programId);
}
