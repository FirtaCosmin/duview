package com.crmsoftware.duview.ui.watchnow;

import java.util.Calendar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class LiveProgressBarView extends View{

	private long dateStart = 0;
	private long dateEnd = 0;
	RectF rect;
	
	
	public LiveProgressBarView(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
		
		EpgProgram.initGraphics(getResources());
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
/*	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);

	    this.setMeasuredDimension(MeasureSpec.getSize(heightMeasureSpec), MeasureSpec.getSize(heightMeasureSpec));
	}*/
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		rect = new RectF(getPaddingLeft(), getPaddingTop(), getWidth() - getPaddingRight(), getHeight() - getPaddingBottom());
		super.onSizeChanged(w, h, oldw, oldh);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onDraw(Canvas canvas) {
		
		int progress = -1;

		long currentTime = Calendar.getInstance().getTimeInMillis();
		
		
		
		if (dateStart <= currentTime && currentTime <= dateEnd)
		{
			long totalTime = dateEnd - dateStart;
			long delta =  currentTime - dateStart;
			progress = (int) ( (double)(rect.width()) * (double)delta/ (double)totalTime);
		}
		else
		{
/*			if (currentTime < dateStart)
			{
				progress = (int) rect.width();
			}*/
		}
		
		EpgProgram.drawProgressBar(canvas,rect,progress,0);
		
		super.onDraw(canvas);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void setData(long start, long end) 
	{
		this.dateStart = start;
		this.dateEnd = end;
		
		invalidate();
	}
	

}
