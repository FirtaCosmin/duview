package com.crmsoftware.duview.ui.watchnow;

import java.util.Calendar;
import java.util.Observable;
import java.util.Observer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.FragmentChangeActivity;
import com.crmsoftware.duview.app.IMvpActivity;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.player.IPlayerView;
import com.crmsoftware.duview.player.MvpPlayer;
import com.crmsoftware.duview.player.MvpPlayerInfo;
import com.crmsoftware.duview.player.PlayerActivity;
import com.crmsoftware.duview.player.PlayerView;
import com.crmsoftware.duview.ui.common.MvpImageView;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.ui.common.UnderConstrFragment;
import com.crmsoftware.duview.utils.CustomDialogView;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;


public class ProgramDetailsView extends RelativeLayout implements View.OnClickListener, View.OnTouchListener, IPlayerView, Observer
{
	public static boolean DEBUG = Log.DEBUG ; 
	public static boolean VERBOUSE = Log.VERBOUSE ; 
	public static final String TAG = ProgramDetailsView.class.getSimpleName();
	public static final String MAXIMIZE_STATE = "MAXIMIZE_STATE";
	public static final String SHOWN = "RUNNING";
	public static final String PLAYER_ASPECTRATIO = "PLAYER_ASPECTRATIO";
	public static final String PLAYER_FULLSCREEN = "PLAYER_FULLSCREEN";
	public static final String ORIENTATION = "ORIENTATION";
	public static final String REQUESTED_ORIENTATION = "REQUESTED_ORIENTATION";
	

	/**
	 * @author Cosmin
	 * @desc interface for any class that wants to listen to minimization state changes
	 */
	public interface ProgramDetailsListener{
		/**
		 * @desc method called when the program details view will change it's minization state
		 * @param minimized - true if player is minimized
		 */
		public void minimized(boolean minimized);
		/**
		 * @desc will be called when the player has been closed from pressing the close player button
		 */
		public void closePlayer();
	}
	
	/**
	 * @desc This is the listener for the events occurring in the view 
	 */
	private ProgramDetailsListener mProgramDetailsListener;
	
	private int animationIncrement = 2;
	private int TouchAndDragDist = 70;
	
	private View viewDetails ;
	private ProgramDetailsView otherDetails;
	
	private PlayerView viewPlayer;
	private MvpViewHolder holderDetails;
	private MvpViewHolder holderPlayer;

	private Rect rectImage ;
	private Rect rectPlayerInDetails ;
	private Rect rectPlayerMinimized;
	private Rect rectPlayer;
	private double sinOfShiftAngle;
	private double aspectRatio =  0; //4.0F/3.0F; //16.0F/9.0F; 
	private boolean isFullscreen = false;
	private boolean isShown = false;
	private boolean isMaximized = true;
	private Rect rectDetails;
	
	private float playerHeight;
	private float playerMinimizedRightMargin;
	private float playerMinimizedBottomMargin;
	
	private boolean touchDownOnPlayer = false;
	private Bundle savedData;
	
	
	private float memDownX = Float.NaN;
	private float memDownY = Float.NaN;
	private float memMoveX = Float.NaN;
	private float memMoveY = Float.NaN;	
	private float memUpX = Float.NaN;
	private float memUpY = Float.NaN;	
	
	private MvpPlayerInfo playerInfo = new MvpPlayerInfo();
	
	private IEpgListener epgListener;

	private static String listDays[];
	 
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@SuppressLint("NewApi")
	private OnGlobalLayoutListener mGlobalLayoutListener = new OnGlobalLayoutListener() {
	    public void onGlobalLayout() {
	        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) 
	        {
	            ProgramDetailsView.this.getViewTreeObserver().removeOnGlobalLayoutListener(this);
	        } 
	        else 
	        {
	        	ProgramDetailsView.this.getViewTreeObserver().removeGlobalOnLayoutListener(this);
	        }

			if (rectDetails == null)
			{
				View viewImage = holderDetails.getView(R.id.item_image);
				rectImage = new Rect();
				Utils.getViewRectRelToOther(ProgramDetailsView.this, viewImage, rectImage);
				
				rectImage.top += viewImage.getPaddingTop();
				rectImage.bottom -= viewImage.getPaddingBottom();
				rectImage.left += viewImage.getPaddingLeft();
				rectImage.right -= viewImage.getPaddingRight();
				
				aspectRatio = (double)rectImage.width()/(double)rectImage.height();
				
				manageViews();
			}

	    }
	};
	private TextView title;
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public ProgramDetailsView(Context context, AttributeSet attrs) 
	{
		super(context,attrs);

		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		
		LayoutInflater inflater = LayoutInflater.from(getContext());
		
		viewDetails = inflater.inflate(R.layout.program_details, null);	
		addView(viewDetails, params);
		viewDetails.setVisibility(View.VISIBLE);
		
		MvpImageView itemImage = (MvpImageView) viewDetails.findViewById(R.id.item_image); 
		
		MvpUserData authData = MvpUserData.getFromMvpObject();
		
		itemImage.setData(authData.isAnonymous()?"anonAvatar" : " ");
		
		viewDetails.findViewById(R.id.player_view).setVisibility(View.INVISIBLE); 
		
        listDays = getResources().getStringArray(R.array.epg_days);
        
        playerInfo.addObserver(this);
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	private void calculatePlayerDetailsRect() 
	{
		rectPlayerInDetails  = new Rect(rectImage);
		rectPlayer = new Rect(rectPlayerInDetails);
		
		if (aspectRatio != 0)
		{
			rectPlayer.left = 0;
			rectPlayer.right = rectPlayer.left + Utils.roundToInt((double)(rectPlayer.height()) * aspectRatio); // aspectratio 
			rectPlayer.offset(rectPlayerInDetails.centerX() - rectPlayer.centerX(), 0);
			
			rectPlayerInDetails = new Rect(rectPlayer);
			if (DEBUG) Log.d(TAG, "INIT rectPlayerInitial after aspectratio: " + Utils.printRect(rectPlayerInDetails));
		}		
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onDetachedFromWindow() 
	{
		playerInfo.deleteObserver(this);
		
		super.onDetachedFromWindow();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	private void calculatePlayerMinimizedRect() 
	{
		Resources res = getResources();
		playerHeight = res.getDimension(R.dimen.player_minimized_height);
		
		if (playerHeight <= 0)	// this is proportional config
		{
			playerHeight = rectPlayerInDetails.height() * (-playerHeight);
		}
		
		playerMinimizedRightMargin =  res.getDimension(R.dimen.player_minimized_margin_right);
		if (playerMinimizedRightMargin < 0)
		{
			playerMinimizedRightMargin = rectPlayerInDetails.left;
		}
		
		playerMinimizedBottomMargin =  res.getDimension(R.dimen.player_minimized_margin_bottom);
		if (playerMinimizedBottomMargin < 0)
		{
			playerMinimizedBottomMargin = rectPlayerInDetails.top;
		}

		int collapsedHeight = (int) (playerHeight);
		int collapsedWidth = Utils.roundToInt((double)collapsedHeight*aspectRatio);
   	 	
   	 	rectPlayerMinimized = new Rect();
   	 	rectPlayerMinimized.left = rectDetails.width() - collapsedWidth - (int)playerMinimizedRightMargin;
   	 	rectPlayerMinimized.top = rectDetails.height() - collapsedHeight - (int)playerMinimizedBottomMargin;
   	 	rectPlayerMinimized.right = rectPlayerMinimized.left + collapsedWidth;
   	 	rectPlayerMinimized.bottom = rectPlayerMinimized.top + collapsedHeight;	   
   	 	
		int deltaY = rectPlayerMinimized.top - rectPlayerInDetails.top;
		int deltaX = rectPlayerMinimized.left - rectPlayerInDetails.left;
		
		double dist =  Math.sqrt(deltaY*deltaY + deltaX*deltaX);
		sinOfShiftAngle = (double)deltaY/(double)dist;	
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	private void manageViews() 
	{	
 		isShown = false;
 		isMaximized = true;
		if (savedData != null)
		{
			//aspectRatio = savedData.getDouble(PLAYER_ASPECTRATIO, aspectRatio);
			isFullscreen = savedData.getBoolean(PLAYER_FULLSCREEN);
			isShown = savedData.getBoolean(SHOWN, false);   	
			isMaximized = savedData.getBoolean(MAXIMIZE_STATE, true);
		}

   	 	setVisibility(isShown ? View.VISIBLE : View.INVISIBLE);
	 		

   	 	
   	 	if (null != viewPlayer)
   	 	{
   	 		viewPlayer.setVisibility(isShown ? View.VISIBLE : View.GONE);
   	 		viewPlayer.bringToFront();
   	 		viewPlayer.showBorder(!isMaximized);
   	 	}
   	 	
   	 	rectDetails = new Rect(0,0,getWidth(),getHeight());
   	 
		calculatePlayerDetailsRect();
		calculatePlayerMinimizedRect();
		
 		if (false == isMaximized)
 		{
 			int offset = rectPlayerMinimized.top - rectPlayerInDetails.top;
 			rectDetails.offset(0, offset);
 			rectPlayer = new Rect(rectPlayerMinimized);
 			viewDetails.setVisibility(View.GONE);
 		}

   	 	Utils.setViewBoundsInRelLayout(viewDetails, rectDetails);

 		
 		if (null != viewPlayer)
 		{
 			if (isFullscreen)
 			{
 	 			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);	
 	 			params.setMargins( 0,0,0,0);

 	 			viewPlayer.setLayoutParams(params);
 	 			viewPlayer.showBorder(false);
 	 			
 			}
 			else
 			{
 				Utils.setViewBoundsInRelLayout(viewPlayer, rectPlayer);   
 			}
 			
 		}
 
 		saveState();
 		
 		if (DEBUG) Log.d(TAG, "isShowing: " + isShown);
 		if (DEBUG) Log.d(TAG, "isMaximized: " + isMaximized);
 		if (DEBUG) Log.d(TAG, "isFullscreen: " + isFullscreen); 		
		if (DEBUG) Log.d(TAG, "INIT rectPlayerInitial: " + Utils.printRect(rectPlayerInDetails));
   	 	if (DEBUG) Log.d(TAG, "INIT rectPlayerMinimized: " + Utils.printRect(rectPlayerMinimized));
 		if (DEBUG) Log.d(TAG, "INIT rectPlayer: " + Utils.printRect(rectPlayer));
 		if (DEBUG) Log.d(TAG, "INIT rectDetails: " + Utils.printRect(rectDetails));
 		if (DEBUG) Log.d(TAG, "sinOfShiftAngle: " + sinOfShiftAngle);
 		if (DEBUG) Log.d(TAG, "aspectRatio: " + aspectRatio);
	}
	

	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void maximize(boolean state, boolean animated)
	{
 		if (DEBUG) Log.d(TAG, "maximize("+state+")");
 		
 		if (DEBUG) Log.d(TAG, "BEFORE \trectPlayer: " + Utils.printRect(rectPlayer));
 		if (DEBUG) Log.d(TAG, "BEFORE \trectView: " + Utils.printRect(rectDetails));
   	 	if (DEBUG) Log.d(TAG, "BEFORE \trectPlayerMinimized: " + Utils.printRect(rectPlayerMinimized));
 		
		if (state)
		{
			increaseView(rectPlayer.top - rectPlayerInDetails.top, animated);
		}
		else
		{
			decreaseView(rectPlayerMinimized.top - rectPlayer.top, animated);
		}
	}
	
	
	/**
	 * @desc will set the program details minimization listener. 
	 * 		 Only one listener can listen to this event. Calling this method will 
	 * 		 delete the last listener.
	 * @param listener
	 */
	public void setProgramDetailsListener(ProgramDetailsListener listener){
		mProgramDetailsListener = listener;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void show(boolean state)
	{
		this.isShown = state;
		int visibility = state ? View.VISIBLE : View.INVISIBLE;
		
		 MvpUserData authData = MvpUserData.getFromMvpObject();
		 
		 
		 
		if (state)
		{
			
			boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
			if (MvpPlayer.getInstance().getPlayer() != null && isPortrait)
			{
/*				if (MvpPlayer.getInstance().getPlayer() != null && isPortrait) 
				{
					rectDetails.bottom = rectPlayerMinimized.top;
				}	
				else
				{
					rectDetails.bottom = getHeight();
				}
				
				Utils.setViewBoundsInRelLayout(viewDetails, otherDetails.rectPlayerMinimized);	*/			
			}

			if (null == viewPlayer)
			{
				showDetails(true);	
			}
			else
			{
				if (viewPlayer.getVisibility() != View.VISIBLE)
				{
					maximize(true, false);
					showDetails(false);
				}
			}
			
		}
		else
		{
			if (viewPlayer != null)
			{
				viewPlayer.setVisibility(View.GONE);
			}	

			hideDetails();
		}
		
		if(authData.isAnonymous() && viewPlayer != null)
		 {
			viewPlayer.setVisibility(View.GONE);
		 }


		saveState();
	}
	
	

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public float calculateAlpha(int totalScrollDy, int direction)
	{		
		int totalSpace = rectPlayerMinimized.top - rectPlayerInDetails.top;
		
		float value = (float) 1 - ((float) totalScrollDy/(float)totalSpace);
		if (VERBOUSE) Log.d(TAG, "calculateAlpha = " + value);
		
		return  value;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public float calculateScale(float totalScroledDy)
	{		
		float maxScale =  1-(float)rectPlayerMinimized.height() / (float)rectPlayerInDetails.height();
		
		if (DEBUG) Log.d(TAG, "maxScale = " + maxScale);
		if (maxScale == 0)
		{
			return 1;
		}
		
		float totalDy = rectPlayerMinimized.top - rectPlayerInDetails.top;
		float scale = 1;
		if (maxScale < 1) // decrease mode
		{
			scale =  1 - totalScroledDy * maxScale / totalDy;
		}
		else // increase
		{
			scale =  totalScroledDy * (1 - maxScale) / totalDy;
		}
		
		if (DEBUG) Log.d(TAG, "calculateScale = " + scale);

		return scale;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private int calculateDx(int dy)
	{
		double distance = (double)dy / sinOfShiftAngle;
		double pitagora = (distance*distance) - (double)(dy*dy);
		pitagora = Math.sqrt(pitagora);
		
		return (int)Math.ceil(pitagora);	
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public boolean isMaximized()
	{
		return isMaximized;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public boolean isShown()
	{
		return isShown;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void saveState()
	{

		if (null!= savedData)
		{
			savedData.putBoolean(MAXIMIZE_STATE, isMaximized);					
			savedData.putBoolean(SHOWN, isShown);
			savedData.putDouble(PLAYER_ASPECTRATIO, aspectRatio);
			savedData.putBoolean(PLAYER_FULLSCREEN, isFullscreen);
		}		
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void increaseView(int dy, boolean animated)
	{
		if (DEBUG) Log.d(TAG, "increaseView: " +dy+ " animated = " + animated);

		if (!animated)
		{
			shiftUp(dy);
		}
		else
		{
			int offset =0;
			while (offset < dy )
			{
				shiftUp(animationIncrement);
				offset += animationIncrement;
			}
		}
		

		if (rectPlayer.top < rectPlayerMinimized.top )
		{
			viewDetails.setVisibility(View.VISIBLE);
		}
		
		if (rectPlayer.top > rectPlayerInDetails.top)
		{
			viewPlayer.showBorder(true);
			viewPlayer.fitTheVideo();
		}
		
		if (rectPlayer.top < rectPlayerInDetails.top + animationIncrement)
		{
			if (DEBUG) Log.d(TAG, "MAXIMIZED");
			viewDetails.setAlpha(1);
			isMaximized = true;
			anouncePlayerMinimized();
			
			if (null != viewPlayer)
			{
				viewPlayer.showBorder(false);
				
				holderPlayer.setVisibility(R.id.player_item_details_placeholder, View.INVISIBLE);
				holderPlayer.setVisibility(R.id.player_item_details, View.INVISIBLE);
				holderPlayer.setVisibility(R.id.player_item_image_layout, View.INVISIBLE);
				
				if (null != otherDetails && otherDetails.isShown())	// this is closing the other view
				{
					otherDetails.show(false);
				}
			}
		}
		
		saveState();
		
 		if (DEBUG) Log.d(TAG, "INCREASE AFTER \t rectPlayer: " + Utils.printRect(rectPlayer));
 		if (DEBUG) Log.d(TAG, "INCREASE AFTER \t rectPlayerInDetails: " + Utils.printRect(rectPlayerInDetails));
 		if (DEBUG) Log.d(TAG, "INCREASE AFTER \t rectDetails: " + Utils.printRect(rectDetails));
   	 	if (DEBUG) Log.d(TAG, "INCREASE AFTER \t rectPlayerMinimized: " + Utils.printRect(rectPlayerMinimized));
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void checkElasticMove(int dy)
	{
   	 
		int dTop = rectPlayer.top - rectPlayerInDetails.top;
		int dBottom = rectPlayerMinimized.top - rectPlayer.top;
		if (dy > 0)
		{
			if (dBottom < dTop)
			{
				decreaseView(dBottom, true);
		   	 }
			else
			{
				increaseView(dTop, true);
			}
		}
		else
		{
			if (dBottom < dTop)
			{
				decreaseView(dBottom, true);
		   	 }
			else
			{
				increaseView(dTop, true);
			}		
		}

	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void applyShift(int dx, int dy)
	{		
		rectDetails.offset(0, dy);
		Utils.setViewBoundsInRelLayout(viewDetails, rectDetails);	
		
		rectPlayer.offset(dx, dy);
			
		float scale = calculateScale(rectPlayer.top - rectPlayerInDetails.top);
		rectPlayer.right = rectPlayer.left + Utils.roundToInt((double)(rectPlayerInDetails.width()) * scale); 
		rectPlayer.bottom = rectPlayer.top + Utils.roundToInt((double)(rectPlayerInDetails.height()) * scale);
			
 		if (DEBUG) Log.d(TAG, "dx=" + dx + " dy=" + dy + "  \trectPlayer: " + Utils.printRect(rectPlayer));

		Utils.setViewBoundsInRelLayout(viewPlayer, rectPlayer);	
 		
		viewDetails.setAlpha(calculateAlpha(rectPlayer.top,dy));		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void shiftUp(int dy)
	{
		if (rectDetails.top - dy < 0)
		{
			dy = rectDetails.top;
		}
		
		if (rectPlayer.top - dy < rectPlayerInDetails.top)
		{
			dy = -rectPlayerInDetails.top + rectPlayer.top;
		}
		
		int dx = calculateDx(dy);
		
		if (rectPlayer.left - dx < rectPlayerInDetails.left)
		{
			dx = -rectPlayerInDetails.left + rectPlayer.left;
		}
		
		applyShift(-dx, -dy);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void shiftDown(int dy)
	{	
		if (rectPlayer.top + dy > rectPlayerMinimized.top)
		{
			dy = rectPlayerMinimized.top - rectPlayer.top;
		}
		
		int dx = calculateDx(dy);
		
		if (rectPlayer.left + dx > rectPlayerMinimized.left)
		{
			dx = rectPlayerMinimized.left - rectPlayer.left;
		}		
		
		applyShift(dx, dy);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void decreaseView(int dy, boolean animated)
	{
		if (DEBUG) Log.d(TAG, "decreaseView: " +dy+ " animated = " + animated);

		if (!animated)
		{
			shiftDown(dy);
		}
		else
		{
			int offset =0;
			while (offset < dy )
			{
				shiftDown(animationIncrement);
				offset += animationIncrement;
			}
		}

		if (rectPlayer.top > rectPlayerInDetails.top)
		{
			if (null != viewPlayer)
			{
				viewPlayer.showBorder(true);
			}
		}
		
		if (rectPlayer.top + animationIncrement >= rectPlayerMinimized.top)
		{
			if (DEBUG) Log.d(TAG, "MINIMIZED");
			
			viewDetails.setVisibility(View.GONE);
			if (null != viewPlayer)
			{
				holderPlayer.setVisibility(R.id.player_item_details_placeholder, View.VISIBLE);
				holderPlayer.setVisibility(R.id.player_item_details, View.VISIBLE);
				holderPlayer.setVisibility(R.id.player_item_image_layout, View.VISIBLE);	
				
				viewPlayer.fitTheVideo();
			}
			isMaximized = false;
			anouncePlayerMinimized();
		}

		saveState();
		
 		if (DEBUG) Log.d(TAG, "DECREASE AFTER \t rectPlayer: " + Utils.printRect(rectPlayer));
 		if (DEBUG) Log.d(TAG, "DECREASE AFTER \t rectPlayerInDetails: " + Utils.printRect(rectPlayerInDetails));
 		if (DEBUG) Log.d(TAG, "DECREASE AFTER \t rectDetails: " + Utils.printRect(rectDetails));
   	 	if (DEBUG) Log.d(TAG, "DECREASE AFTER \t rectPlayerMinimized: " + Utils.printRect(rectPlayerMinimized));
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		
	    this.setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.getSize(heightMeasureSpec));
	    
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void setData(Bundle instanceData)
	{
		this.savedData = instanceData;	
		
		playerInfo.loadFrom(instanceData);

	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void bindPlayer(PlayerView viewPlayer)
	{
		bringToFront();
		
		holderDetails = new MvpViewHolder(viewDetails);
		holderDetails.setClickListener(this, R.id.watch_now_button, R.id.body_view); 
		
		
		if (null != viewPlayer)
		{
			this.viewPlayer = viewPlayer;

			//viewPlayer.setData(savedData);
			
			viewPlayer.setPlayerListener(this);
			
			holderPlayer = new MvpViewHolder(viewPlayer);
			
			holderPlayer.setTouchListener(this, R.id.player_body_view);
			
			holderPlayer.setClickListener(this, 
							R.id.player_fullscreen,
							R.id.player_play,
							R.id.player_pause,
							R.id.player_close);
			holderPlayer.setVisibility(R.id.player_item_details_placeholder, View.INVISIBLE);
			
			setPlayerInfo(MvpPlayer.getPlayerInfo());
		}
		
		updateView();
		
		getViewTreeObserver().addOnGlobalLayoutListener(mGlobalLayoutListener);
		
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		viewDetails.setLayoutParams(params);
	}


	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void onClosePlayer()
	{
		if (DEBUG) Log.m(TAG,this,"onClosePlayer");	

		if (viewPlayer != null)
		{
			viewPlayer.destroyPlayer();
		}
		
		show(false);
		
		isShown = false;
		isMaximized = true;
		saveState();
		anouncePlayerClosed();
	}
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
/*	public void onPlayClick()
	{
		viewPlayer.play();
	}*/
	 
	/**	 
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
/*	public void onPauseClick()
	{
		viewPlayer.pause();
	}*/
	
	/**	 
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void onVideoCompletion()
	{
		viewPlayer.destroyPlayer();

		if (isFullscreen)
		{
			Utils.clickView(holderPlayer.getView(R.id.player_fullscreen),true);
		}
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public boolean onBackPressed()
	{
		
		if (isMaximized())
		{
			onClosePlayer();
			
			return false;
		}
		
		return true;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void onVideoSizeChanged(int width, int height) 
	{
		double newAspectRatio = (double)width/(double)height;
		
		if (Math.abs(newAspectRatio - aspectRatio) <= 0.1F)
		{
			return;
		}
		
		aspectRatio = (double)width/(double)height;
		saveState();
		manageViews();
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void showWatchButton(boolean state)
	{
		holderDetails.setVisibility(R.id.watch_now_button, state ? View.VISIBLE : View.INVISIBLE);
		holderDetails.setClickListener(this, R.id.watch_now_button);		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void updateView()
	{
		if (DEBUG) Log.m(TAG,this,"updateView");	
		
		if (null == holderDetails)
		{
			return;
		}
		
		//details
		MvpImageView channelLogo = (MvpImageView) holderDetails.getView(R.id.channel_logo); 
		channelLogo.setData(playerInfo.getChannelImage());
		
		MvpImageView itemImage = (MvpImageView) holderDetails.getView(R.id.item_image); 
		itemImage.setData(playerInfo.getProgramImage());
		
		title = (TextView) holderDetails.getView(R.id.item_title);
		holderDetails.setText(R.id.item_title, playerInfo.getTitle());
		title.setEllipsize(TextUtils.TruncateAt.MARQUEE);
		title.setSingleLine(true);
		title.setMarqueeRepeatLimit(5);
		title.setSelected(true);
		 
		holderDetails.setText(R.id.item_description,playerInfo.getLongDesc());
		 
		holderDetails.setText(R.id.item_parental_info, playerInfo.getParentalRatings());
		holderDetails.setText(R.id.item_duration_value, playerInfo.getDuration());
		
		String sSchedule = Utils.dateToString(playerInfo.getStart(),EpgProgramsList.dateFormat) + "-" + Utils.dateToString(playerInfo.getEnd(),EpgProgramsList.dateFormat);
		holderDetails.setText(R.id.item_schedule, sSchedule);
		holderDetails.setVisibility(R.id.item_loading, View.INVISIBLE);
		
		int day = ProgramDetailsView.getDay(playerInfo.getStart(), playerInfo.getEnd());
		
		holderDetails.setText(R.id.item_date_day, listDays[day]);
		
		MvpPlayerInfo livePlayerInfo = MvpPlayer.getPlayerInfo();
		boolean watchVisible = (false == this.playerInfo.getChannelId().equals(livePlayerInfo.getChannelId())) || MvpPlayer.getInstance().getState() == MvpPlayer.MvpPlayerState.DESTROYED;
		 MvpUserData authData = MvpUserData.getFromMvpObject();
		showWatchButton(authData.isAnonymous() ? true : watchVisible);

	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void setEpgListener(IEpgListener listener)
	{
		this.epgListener = listener;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onClick(View v) 
	{
		if (DEBUG) Log.m(TAG,this,"onClick");		
		
		switch (v.getId()) 
		{
		case R.id.watch_now_button:
			if (null == epgListener)
			{
				return;
			}
			MvpUserData authData = MvpUserData.getFromMvpObject();
			if(authData.isAnonymous())
			{
				FragmentChangeActivity activity = (FragmentChangeActivity)getContext();
				CustomDialogView customDialogView = CustomDialogView.getInstance();
				customDialogView.show(activity.getSupportFragmentManager(), CustomDialogView.TAG);

				return;
			}

			
			epgListener.OnWatchChannelClick(this.playerInfo.getChannelId(), this.playerInfo.getProgramId());

			break;
			
		case R.id.player_close:
		{
			onClosePlayer();
		}
		break;
		
		case R.id.player_play:
		{
			viewPlayer.play();
		}
		break;
		
		case R.id.player_pause: 
		{
			viewPlayer.pause();
		}
		break;
			
		case R.id.player_fullscreen:
		{
			Intent intent = new Intent(getContext(), PlayerActivity.class);
			intent.putExtra(PlayerActivity.LAYOUT_ID, R.layout.player_program);

			getContext().startActivity(intent);
		}
		break;
		
		default:
		break;
		}	
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */
	public boolean onTouchPlayerSurface(MotionEvent e) 
	{
		if (DEBUG) Log.m(TAG,this,"onTouchPlayerSurface");
		MvpUserData authData = MvpUserData.getFromMvpObject();
		if(authData.isAnonymous())
		{
			return true;
		}
		float dy = 0;
		switch(e.getAction())
		{
		
		case MotionEvent.ACTION_DOWN:
		{
	    	memMoveX = e.getRawX();
	    	memMoveY = e.getRawY();
	    	
	    	memDownX = e.getRawX();
	    	memDownY = e.getRawY();
	    	
	    	memUpX = 0;
	    	memUpY = 0;
		
		}
		break;
			
		case MotionEvent.ACTION_MOVE:
		{
		this.getParent().requestDisallowInterceptTouchEvent(true);
		
		if (VERBOUSE) Log.d(TAG, "ACTION_MOVE ");
		if (!isFullscreen)
		{
			 dy = e.getRawY() - memMoveY;
			 
			 if (dy > 0)
			 {
				 decreaseView((int)dy, false);
			 }
			 else
			 {
				 increaseView((int)-dy, false); 
			 }
		}
	   	 memMoveX = e.getRawX();
	   	 memMoveY = e.getRawY();

		}
		break;
			
		case MotionEvent.ACTION_UP:
		{
			this.getParent().requestDisallowInterceptTouchEvent(false);

	    	memUpX = e.getRawX();
	    	memUpY = e.getRawY();	
	    	
			dy = (int)(memUpY - memDownY);
			if (Math.abs(dy) < 5)
			{
				viewPlayer.showControlBar(!viewPlayer.isControlBarVisible());
				return true;				
			}
			
			if (!isFullscreen)
			{
				checkElasticMove((int)dy);
			}
		}
		break;
		
		}

		return true;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */
	@Override
	public boolean onTouch(View v, MotionEvent e) 
	{
		if (DEBUG) Log.m(TAG,this,"onTouch");
		
		switch (v.getId()) 
		{
		case R.id.player_body_view:
		{
			return onTouchPlayerSurface(e);
		}	


		default:
			break;
		}
		return false;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void showDetails(boolean animated)
	{
		TranslateAnimation anim = new TranslateAnimation(getWidth(), 0 , 0, 0);
		anim.setDuration(500);

		setVisibility(View.VISIBLE);
		viewDetails.setVisibility(View.VISIBLE);
		
		
		if (false == animated)
		{
			return;
		}
		
		anim.setAnimationListener(new TranslateAnimation.AnimationListener() {


		@Override
		public void onAnimationStart(Animation animation) {
			
		}

		@Override
		public void onAnimationEnd(Animation animation) {
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
		});

		viewDetails.startAnimation(anim);		
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void hideDetails()
	{
		TranslateAnimation anim = new TranslateAnimation(0 ,getWidth(),  0, 0);
		anim.setDuration(500);

		anim.setAnimationListener(new TranslateAnimation.AnimationListener() {


		@Override
		public void onAnimationStart(Animation animation) {
			// TODO Auto-generated method stub
			ProgramDetailsView.this.setPadding(10, 1, 1, 1);
			viewDetails.setPadding(10, 1, 1, 1);
		}

		@Override
		public void onAnimationEnd(Animation animation) {
			viewDetails.setPadding(0, 0, 0, 0);
			ProgramDetailsView.this.setPadding(0, 0, 0, 0);
			setVisibility(View.GONE);
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
		});

		viewDetails.startAnimation(anim);		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void expandWithPlayer()
	{
		TranslateAnimation anim = new TranslateAnimation(0, 0 , getHeight(), rectDetails.top);
		anim.setDuration(1100);

		anim.setAnimationListener(new TranslateAnimation.AnimationListener() {


		@Override
		public void onAnimationStart(Animation animation) 
		{	
			viewDetails.setVisibility(View.VISIBLE);
		}

		@Override
		public void onAnimationEnd(Animation animation) {

		    Utils.setViewBoundsInRelLayout(viewDetails, rectDetails);
		    
			if (viewPlayer != null)
			{
				viewPlayer.setVisibility(View.VISIBLE);
				viewPlayer.bringToFront();
			}
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
		});

		viewDetails.startAnimation(anim);		
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void bindOtherDetails(ProgramDetailsView otherDetails)
	{
		this.otherDetails = otherDetails;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public  static int getDay(long startTime, long endTime)
	{
		Calendar currentDate = Calendar.getInstance();
		int diffDays = (int)Utils.roundToInt((double)(startTime-currentDate.getTimeInMillis()) / (1000 * 60 * 60 * 24));
		if (diffDays < 0)
		{
			diffDays = 0;
		}
		
		currentDate.add(Calendar.DAY_OF_MONTH, diffDays);
		
		if (diffDays >= 2)
		{
			diffDays = currentDate.get(Calendar.DAY_OF_WEEK)+1;
		}	
		
		if (diffDays>= 0 && diffDays < 7)
		{
			return diffDays;
		}
		
		return 0;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void onParentPaused()
	{
		if (DEBUG) Log.m(TAG,this,"onParentPaused");
		
		if (null == viewPlayer)
		{
			return;
		}
		viewPlayer.dettachPlayer();
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void onParentResume()
	{
		if (DEBUG) Log.m(TAG,this,"onParentResume");
		
		if (null == viewPlayer)
		{
			return;
		}
		

		if (false == MvpPlayer.getInstance().isRunning())
		{
			if (viewPlayer.getVisibility() == View.VISIBLE)
			{
				onClosePlayer();
				return;
			}
		}
		
		viewPlayer.attachPlayer();

	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public MvpPlayerInfo getPlayerInfo() 
	{
		return playerInfo;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void setPlayerInfo(MvpPlayerInfo info) 
	{
		playerInfo.deleteObserver(this);
		playerInfo = info;
		playerInfo.addObserver(this);
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void update(Observable observable, Object data) {
		
		playerInfo.saveTo(savedData);
		updateView();	
	}

	
	
	
	/**
	 * @desc method that will call the minimized() method of the player details listener.
	 */
	private void anouncePlayerMinimized(){
		if ( mProgramDetailsListener != null ){
			mProgramDetailsListener.minimized(!isMaximized);
		}
	}
	/**
	 * @desc method that will call the closePlayer() method of the player details listener.
	 */
	private void anouncePlayerClosed(){
		if ( mProgramDetailsListener != null ){
			mProgramDetailsListener.closePlayer();
		}
	}
	
}
