package com.crmsoftware.duview.ui.watchnow;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************


import android.content.Context;
import android.util.AttributeSet;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.data.MvpMedia;
import com.crmsoftware.duview.ui.common.IMvpGenericAdapter;
import com.crmsoftware.duview.ui.common.MvpItemHolder;
import com.crmsoftware.duview.ui.common.MvpSimpleListAdapter;

public class EpgTimeHours extends EpgRow{

	
	public EpgTimeHours(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void createTimeLine()
	{
		
		MvpSimpleListAdapter adapter  = new MvpSimpleListAdapter(MvpMedia.class, this);
		setAdapter(adapter);
		
		adapter.setCount(24*2);

	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void markCurrentHour(boolean value)
	{
		EpgHour.markCurrentHour = value;
		for (int i=0; i< getChildCount(); i++)
		{
			EpgHour view = (EpgHour) getChildAt(i);
			view.invalidate();
		}
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onListItemUpdateView(IMvpGenericAdapter adapter,
			MvpItemHolder holder, int position) {
		
	    EpgHour epgHour= (EpgHour) holder.getView();
	    
	    epgHour.setData(position);
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public int onListItemGetLayoutId(IMvpGenericAdapter adapter, int position) 
	{

		return R.layout.epg_hour;
	}

}
