package com.crmsoftware.duview.ui.watchnow;

import java.util.Calendar;
import java.util.concurrent.locks.ReentrantLock;

import android.content.Context;
import android.os.Handler;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.Scroller;

import com.crmsoftware.duview.utils.Log;

/**
 * @author Cosmin
 * @desc This class will be used to manage the scroll, fling and touch events 
 * that occur on a specified context.
 *
 */
public class MvpEpgScrollHelper implements GestureDetector.OnGestureListener  {
	
	private static String TAG = MvpEpgScrollHelper.class.getSimpleName();
	/**
	 * @desc the total offset calculated by the scroller. it contains the 
	 * position from the left margin
	 */
	protected static int TOTAL_OFFSET = 0;
	/**
	 * @desc property used to scale the fling speed.
	 */
	private static final int FLING_SCROLL_SCALE = 2;

	/**
	 * @desc the scroller object that will calculate the fling effect
	 */
	private Scroller mScroller;
	/**
	 * @desc the object that will detect the gestures from the detected motion event
	 */
	private GestureDetector mGesture;
	/**
	 * @desc the maximum width of the scrolled View
	 */
	private int mMaxX = Integer.MAX_VALUE;
	/**
	 * @desc the UI element that is helped with the scroll events.
	 */
	private EpgChannelsList mTheEpgList;

	
	/**
	 * @desc counter for the number of reads on the fling value have been done.
	 * Variable used to see when all the view's children read the fling position to 
	 * recalculate the new fling position
	 */
	private int mFlingReads = 0;
	/**
	 * @desc property used to store the new fling position. It is used afterwards to get the 
	 * difference between a new fling position and the last fling position
	 */
	private int mNewFlingPos = 0;
	/**/
	private Handler mFLingHandler = new Handler();
	
	/**
	 * @desc Lock used to keep the new fling position calculation and reading safe from 
	 * concurrent reading and writing
	 */
	private final ReentrantLock lock = new ReentrantLock();
	
	public MvpEpgScrollHelper(Context context, EpgChannelsList theHelped){
	
		mTheEpgList = theHelped;
		mScroller = new Scroller(context);
		mGesture = new GestureDetector(context, this);
		mMaxX = mTheEpgList.mMaxX;
		
	}
	
	/**
	 * @desc this method is used to pass the touchEvent from the UI that uses this class to the 
	 * gesture detector used to detect the gestures
	 * This method must be called from the onTouchEvent(MotionEvent) method of the UI class that 
	 * wants to be helped with the scrolling. 
	 * If it is not called the motion will not be detected 
	 * @param ev
	 * @return
	 */
	public boolean onTouchEvent(MotionEvent ev){
		return mGesture.onTouchEvent(ev);
	}

	
	/*
	 * 
	 * IMPLEMENTATIONS FROM GestureDetector.OnGestureListener
	 * 
	 * */
	
	@Override
	public boolean onDown(MotionEvent e) {
		return false;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		Log.d(TAG,"start of fling: TOTAL_OFFSET"+TOTAL_OFFSET + "  mMaxX: "+mMaxX);
		mScroller.fling(TOTAL_OFFSET, 0, (int) velocityX/FLING_SCROLL_SCALE, 0, 0, mMaxX, 0, 0);
//		startFlinging();
		mNewFlingPos = TOTAL_OFFSET;
		doFling();
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {
		
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		Log.d( TAG, "Cosmin - scroll distance = "+distanceX );
    	mTheEpgList.scrollBy((int)-distanceX);
    	return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		/*when single tab detected try to select a program*/
		mTheEpgList.selectProgram((int)e.getRawX(), (int)e.getRawY());
		return false;
	}
	
	
	/*
	 * 
	 * END OF
	 * 
	 * IMPLEMENTATIONS FROM GestureDetector.OnGestureListener
	 * 
	 * */
	
	/**
	 * @desc method used to execute the fling event.
	 * it will do scrollTo with the difference got from the fling and will start a handler to 
	 * redo the scroll if the fling is not over
	 */
	private void doFling(){
		if ( flinging() ){
		    mScroller.computeScrollOffset();
			int tempCurrentX = mScroller.getCurrX();
			int flingDelta = tempCurrentX - mNewFlingPos;
			Log.d(TAG,"before racalculation: TOTAL_OFFSET: "+ TOTAL_OFFSET+" past newFlingPos: "+mNewFlingPos + " flingdelta: "+ flingDelta);
			mNewFlingPos = tempCurrentX;
			mTheEpgList.scrollBy(flingDelta);
			Log.d(TAG,"Fling: "+mFlingReads+" TOTAL_OFFSET: "+TOTAL_OFFSET);
			mFLingHandler.post(new Runnable() {
				
				@Override
				public void run() {
					doFling();
				}
			});
		}
		
	}

	/**
	 * @desc when a fling event is detected this method is called. 
	 * Here the layout for the children is requested
	 */
	private void startFlinging(){
		mFlingReads = mTheEpgList.getChildCount();
		mTheEpgList.requestLayoutFromChilds();
		mNewFlingPos = TOTAL_OFFSET;
		
	}
	
	/**
	 * @desc method to check if the scroller is still flinging
	 * @return true if the scroller is still flinging, false otherwise
	 */
	public boolean flinging(){
		return !mScroller.isFinished();
	}
	
	/**
	 * @desc this method will recalculate the TOTAL_OFFSET based on the fling event
	 * it can be read once by all the children of the view and after that will recalculate 
	 * the position from the scroller based on the fling event
	 * @return int - the new fling position
	 */
	public int recalculatePositionFromFling(){
		/*sensitive area. lock before entering*/
		lock.lock();
		/*new read made*/
		mFlingReads--;
		
		if ( mFlingReads <= 0 ){
			/*all the childs read the new fling position*/
			if ( flinging() )
			{
			    mScroller.computeScrollOffset();
			    /*
			     * store the currentX value from the scroller into a temporary variable
			     * This way calculate the difference between last value and this one and pass 
			     * it to the offsetCalculator.
			     * */
				int tempCurrentX = mScroller.getCurrX();
				Log.d(TAG,"before racalculation: TOTAL_OFFSET: "+ TOTAL_OFFSET+" past newFlingPos: "+mNewFlingPos);
				int flindDelta = tempCurrentX - mNewFlingPos;
				mNewFlingPos = tempCurrentX;
				/*calculate the offsets*/
				/*
				 * if the passed fling delta value that is passed is bigger then the TOTAL_OFFSET it will return 0
				 * the returned value is passed to 
				 * */
				int newFlingDelta = mTheEpgList.calculateTotalOffset(flindDelta);

				Log.d(TAG,"Fling: "+mFlingReads+" TOTAL_OFFSET: "+TOTAL_OFFSET + " newFlingPos :  "+mNewFlingPos + " Fling delta: " +flindDelta+" TIme: "+Calendar.getInstance().getTimeInMillis());

				mTheEpgList.moveTheEpgTimeLine(newFlingDelta);
				mFlingReads = mTheEpgList.getChildCount();
			}
			
		}
		lock.unlock();
		return mNewFlingPos;
		
	}

}
