package com.crmsoftware.duview.ui.watchnow;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission


import java.util.Calendar;
import java.util.Date;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Scroller;

import com.actionbarsherlock.internal.nineoldandroids.animation.ValueAnimator;
import com.crmsoftware.duview.R;
import com.crmsoftware.duview.data.MvpMedia;
import com.crmsoftware.duview.data.MvpProgram;
import com.crmsoftware.duview.ui.common.MvpVerticalListView;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;



/**
 * @author Cosmin
 *
 */
/**
 * @author Cosmin
 *
 */
public class EpgChannelsList extends MvpVerticalListView {

	protected static int TIME_LINE_WIDTH = 0;
//	protected static int TOTAL_OFFSET = 0;
	protected static int CURRENT_TIME_POS = -1;
	protected static int EPG_VIEW_LEFT_POS = -1;
	protected static int TIMELINE_VISIBILITY = View.VISIBLE;
	public static String SELECTED_PROGRAM_ID = "";
	//public static String PLAYED_CHANNEL_ID = "";
	
	float memDownX = Float.NaN;
	float memDownY = Float.NaN;
	float memMoveX = Float.NaN;
	float memMoveY = Float.NaN;	
	float memUpX = Float.NaN;
	float memUpY = Float.NaN;	
	
	public static int JUMP = 10;
	
	private EpgTimeHours viewEpgTimeHours;
	private View viewEpgTimeLine;
	private Handler handler = new Handler();;
	private IEpgListener listener;
	public static EpgProgram selectedProgram = null;
	

	protected int mMaxX = Integer.MAX_VALUE;
	
	/**
	 * @desc the property that will help with the scroll, fling and touch detection and process.
	 */
	protected static MvpEpgScrollHelper mScrollHelper;

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public EpgChannelsList(Context context) {
		super(context);
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public EpgChannelsList(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		EPG_VIEW_LEFT_POS = -1; //// not current date
		
		if (EPG_VIEW_LEFT_POS == -1)	
		{
			Resources res = getResources();
			int logoSpace = (int)res.getDimension(R.dimen.epg_channel_height);
			int logoRight = (int)res.getDimension(R.dimen.epg_channel_logo_marginLeftRight) * 2;
			EPG_VIEW_LEFT_POS = logoSpace + logoRight;
			
			
		}
		
		if (TIME_LINE_WIDTH == 0)
		{
			Resources res = getResources();
			int epgCellHourWidth = (int)res.getDimension(R.dimen.epg_hourbar_cellWidth);
			TIME_LINE_WIDTH = ((int)(epgCellHourWidth))*(24*2);
		}
		mMaxX = TIME_LINE_WIDTH;
		
		mScrollHelper = new MvpEpgScrollHelper(context, this);
		
		
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		boolean handled;
		handled = super.dispatchTouchEvent(ev);
		/*dispach the event to the scrollHelper to detect motion events*/
		handled |= mScrollHelper.onTouchEvent(ev);
		//if (INFO) Log.d(TAG, "dispatchTouchEvent return " + handled);
		return handled;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public static void setDefaultConfiguration()
	{
		TIMELINE_VISIBILITY = View.VISIBLE;
		SELECTED_PROGRAM_ID = "";
		MvpEpgScrollHelper.TOTAL_OFFSET = 0;
		CURRENT_TIME_POS = -1;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void setEpgChannelListener(IEpgListener listener)
	{
		this.listener = listener;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public EpgChannelsList(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	public boolean selectProgram(int screenX, int screenY)
	{
		if (VERBOUSE) Log.m(TAG,this, "selectProgram  X = " + screenX + " Y = " + screenY);

		for (int i= 0; i<getChildCount(); i++)
		{
			View channelRow = getChildAt(i);
			
			EpgProgramsList epgProgramsList = (EpgProgramsList)channelRow.findViewById(R.id.epg_channel); 
			
			if (false == Utils.isPointWithinView(screenX, screenY, epgProgramsList))
			{
				continue;
			}
			
			
			int pos =  epgProgramsList.getItemIndex(screenX, screenY);
			
			if (pos  != -1)
			{
				EpgProgram clickedProgram = (EpgProgram) epgProgramsList.getItem(pos);
				MvpProgram program = clickedProgram.getProgram();
				
				if (selectedProgram != null)
				{
					selectedProgram.setSelected(false);
				}
				
				EpgChannelsList.SELECTED_PROGRAM_ID = program.getId();
				selectedProgram = clickedProgram;
				selectedProgram.setSelected(true);
				
				listener.OnProgramClick(channelRow, selectedProgram.getProgram(), program.isCurrent());

				return true;
			}

		}	
		
		return false;
	}
	
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	public boolean selectProgram(String channelId, String programId, boolean play)
	{
		if (VERBOUSE) Log.m(TAG,this, "selectProgram  channelId: " + channelId + " programId: " + programId);

		for (int i= 0; i<getChildCount(); i++)
		{
			View channelRow = getChildAt(i);
			
			EpgProgramsList epgProgramsList = (EpgProgramsList)channelRow.findViewById(R.id.epg_channel); 
			
			if (false == epgProgramsList.getChannelMedia().getId().equals(channelId))
			{
				continue;
			}
			
			EpgProgram programView = epgProgramsList.getProgramView(programId);
			
			if (null == programView)
			{
				return false;
			}

			if (selectedProgram != null)
			{
				selectedProgram.setSelected(false);
			}
	
			MvpProgram program = programView.getProgram();
			
			EpgChannelsList.SELECTED_PROGRAM_ID = program.getId();
			selectedProgram = programView;
			selectedProgram.setSelected(true);
			
			listener.OnProgramClick(channelRow, selectedProgram.getProgram(), play);

			return true;

		}	
		
		return false;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	public void setEpgTimeHoursView(View view)
	{
		if (view instanceof EpgTimeHours)
		{
			viewEpgTimeHours = (EpgTimeHours)view;
			viewEpgTimeHours.createTimeLine();
			
			LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) viewEpgTimeHours.getLayoutParams();
			params.leftMargin = EPG_VIEW_LEFT_POS;
			viewEpgTimeHours.setLayoutParams(params);
		}
	}
	
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void setEpgTimelineView(View view)
	{
		viewEpgTimeLine = view;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void updateTimeline(Date date)
	{	
		long totalSecs = 0;
		Calendar startDayTime = Calendar.getInstance();
		startDayTime.setTime(date);
		totalSecs += startDayTime.get(Calendar.HOUR_OF_DAY) * 3600;
		totalSecs += startDayTime.get(Calendar.MINUTE)*60;
		totalSecs += startDayTime.get(Calendar.SECOND);
		
		CURRENT_TIME_POS =  (int)((double)(totalSecs * TIME_LINE_WIDTH)/(double)(24*3600));
		
		FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) viewEpgTimeLine.getLayoutParams();
		int halfWidth = (int)getResources().getDimension(R.dimen.epg_timeline_bump_width)/2;
				
		int epgPos = CURRENT_TIME_POS + EPG_VIEW_LEFT_POS - mScrollHelper.TOTAL_OFFSET;
		params.leftMargin = epgPos - halfWidth;
		params.rightMargin = epgPos + halfWidth;
		viewEpgTimeLine.setLayoutParams(params);	
		
		if (epgPos <= EPG_VIEW_LEFT_POS)
		{
			viewEpgTimeLine.setVisibility(View.INVISIBLE);
		}
		else
		{
			viewEpgTimeLine.setVisibility(TIMELINE_VISIBILITY);
		}
		
		//if (TIMELINE_VISIBILITY == View.VISIBLE && epgPos >=EPG_VIEW_LEFT_POS && epgPos < getWidth()) // if the line is visible in the screen
		{
			for (int i=0; i<getChildCount(); i++)
			{
				View view = getChildAt(i).findViewById(R.id.epg_channel);;
				
				if (view instanceof ViewGroup)
				{
					ViewGroup viewGroup = (ViewGroup) view;
					for (int j=0; j < viewGroup.getChildCount(); j++)
					{
						viewGroup.getChildAt(j).invalidate();
					}
				}
			}			
		}
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void updateChannelData(Date date)
	{	 
		 for (int i=0; i<getChildCount(); i++)
		 {
			View viewChannel = getChildAt(i).findViewById(R.id.epg_channel);;
			
			if (viewChannel instanceof EpgProgramsList)
			{
				EpgProgramsList channel = (EpgProgramsList) viewChannel; 
				MvpMedia channelMedia = channel.getChannelMedia();
				channel.setData(channelMedia, date);
			}
		 }		 
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public View getChannelView(String channelId)
	{	 
		 for (int i=0; i<getChildCount(); i++)
		 {
			View viewChannel = getChildAt(i).findViewById(R.id.epg_channel);;
			
			if (viewChannel instanceof EpgProgramsList)
			{
				EpgProgramsList channel = (EpgProgramsList) viewChannel; 
				if (channel.getChannelMedia().getId().equals(channelId))
				{
					return getChildAt(i);
				}
					
			}
		 }	
		 
		 return null;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void centerOnCurrentTime()
	{
		handler.post(new Runnable() {
			
			@Override
			public void run() {
				FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) viewEpgTimeLine.getLayoutParams();				
				int halfWidth = (int)getResources().getDimension(R.dimen.epg_timeline_bump_width)/2;
				int scrollX = (params.leftMargin + halfWidth) - getWidth()/2;
				scrollBy(-scrollX);
				
			}
		});

	}
	
	/**
	 * @param deltaX
	 * @return the passed delta or 0 if the delta is bigger then the current mScrollHelper.TOTAL_OFFSET
	 * @desc will recalculate the mScrollHelper.TOTAL_OFFSET given the delta passed as parameter
	 */
	protected int calculateTotalOffset(final int deltaX){
		int newOffset = mScrollHelper.TOTAL_OFFSET - deltaX;
		if (newOffset < 0 )
		{
			newOffset = 0;
		}
		
		int newDeltaX = mScrollHelper.TOTAL_OFFSET - newOffset ;
		
		int epgWidth = getWidth() - EPG_VIEW_LEFT_POS;
		if (TIME_LINE_WIDTH > epgWidth)
		{
			int diffRight = (TIME_LINE_WIDTH - mScrollHelper.TOTAL_OFFSET) + newDeltaX  ;
			if (diffRight < epgWidth )
			{
				newDeltaX += (epgWidth - diffRight) + 2;
			}
		}
		
		MvpEpgScrollHelper.TOTAL_OFFSET -= newDeltaX;
		return newDeltaX;
	}
	
	/**
	 * @desc will request the layout of all the children and the epgTimeHours
	 */
	protected void requestLayoutFromChilds(){
		for (int i= 0; i<getChildCount(); i++)
		{
			View view = getChildAt(i).findViewById(R.id.epg_channel);;
			
			if (view instanceof EpgRow)
			{
				EpgRow wantedView = (EpgRow)view; 
				wantedView.requestLayout();
			}
		}
		
		if (null != viewEpgTimeHours)
		{
			viewEpgTimeHours.requestLayout();
		}
	}
	
	/**
	 * @desc will move the timeLine that marks the current time on the epg with the passed deltaX
	 * @param deltaX
	 */
	protected void moveTheEpgTimeLine(int deltaX){
		if (null != viewEpgTimeLine && deltaX != 0)
		{
			FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) viewEpgTimeLine.getLayoutParams();
			params.leftMargin += deltaX;
			params.rightMargin += deltaX;
			viewEpgTimeLine.setLayoutParams(params);
			
			if (params.leftMargin <= EPG_VIEW_LEFT_POS)
			{
				viewEpgTimeLine.setVisibility(View.INVISIBLE);
			}
			else
			{
				viewEpgTimeLine.setVisibility(TIMELINE_VISIBILITY);
			}
		}
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void scrollBy(final int deltaX)
	{
		handler.post(new Runnable() {
	
		@Override
		public void run() {
			if (VERBOUSE) Log.m(TAG,this, "before TOTAL_OFFSET = " + mScrollHelper.TOTAL_OFFSET);
		
			int newDeltaX = calculateTotalOffset(deltaX);
			
			if (VERBOUSE) Log.m(TAG,this, "after TOTAL_OFFSET = " + mScrollHelper.TOTAL_OFFSET);
			requestLayoutFromChilds();
			moveTheEpgTimeLine(newDeltaX);
			
			
		}
		});

	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void showTimeline(int visibility)
	{
		TIMELINE_VISIBILITY = visibility;
		viewEpgTimeLine.setVisibility(TIMELINE_VISIBILITY);
		viewEpgTimeHours.markCurrentHour(visibility == View.VISIBLE);

	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public static int getTimeLinePosOnChild(View view)
	{
		if (TIMELINE_VISIBILITY != View.VISIBLE)	// not current date
		{
			return -1;
		}
		
		int absViewLeft = view.getLeft() + MvpEpgScrollHelper.TOTAL_OFFSET;
		int absViewRight = view.getRight() + MvpEpgScrollHelper.TOTAL_OFFSET;		
		boolean current = absViewLeft  <= CURRENT_TIME_POS  && CURRENT_TIME_POS<= absViewRight;
		int diff = CURRENT_TIME_POS - absViewLeft;
		return current ? diff : -1;
	}
	

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		/*when detached from windows delete the scroll helper*/
		mScrollHelper = null;
	}
	
}
