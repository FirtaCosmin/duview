package com.crmsoftware.duview.ui.watchnow;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************


import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.crmsoftware.duview.configuration.PlatformSettings;
import com.crmsoftware.duview.core.common.HttpReqParams;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.core.common.TaskPageReqCache;
import com.crmsoftware.duview.core.common.TaskSingleReqCache;
import com.crmsoftware.duview.core.common.TaskSingleReqParse;
import com.crmsoftware.duview.data.MpxPagHeader;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.data.MvpCategory;
import com.crmsoftware.duview.data.MvpFavoritesList;
import com.crmsoftware.duview.data.MvpList;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.data.MvpMedia;
import com.crmsoftware.duview.data.MvpPage;
import com.crmsoftware.duview.data.MvpTag;
import com.crmsoftware.duview.utils.IHttpConst;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;

public class TaskGetTvChannels extends Task implements ITaskReturn{
	public static boolean DEBUG = Log.DEBUG; 
	public static final String TAG = TaskGetTvChannels.class.getSimpleName();
	
	public static final String OBJPREFIX_FILTEREDCHANNELS= "TpMpxFeedUrl.filtered_channels";
	public static final String OBJPREFIX_TVCHANNELS= "TpMpxFeedUrl.channels";
	public static final String OBJPREFIX_FAVORITES= "TpMpxFeedUrl.favorites";
	public static final String OBJPREFIX_TVCHANNELS_MAP= "channels.map";
	

	private TaskPageReqCache taskGetChannels = null;
	private TaskSingleReqParse  taskGetFavorites = null;
	private long dataLifeTime = 0;
	private boolean filterFavorites = true;
	
	private MvpObject mvpFavoritesList;
	private MvpObject mvpReturnData;
	private MvpObject mvpChannelsList;
	private MvpObject mvpChannelsMap;
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public TaskGetTvChannels(ITaskReturn callback, MvpObject mvpReturnData, boolean filterFavorites, long dataLifeTime) 
	{
		super(callback);
		if (DEBUG) Log.m(TAG,this,"ctor()");
		
		this.filterFavorites = filterFavorites;
		this.mvpReturnData = mvpReturnData;
		this.dataLifeTime = dataLifeTime;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	
	@Override
	protected void onResetData() 
	{
		if (DEBUG) Log.m(TAG,this,"resetData");
		
		taskGetChannels = null;

	}
	
	/**
	 *    
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onExecute() 
	{
		if (DEBUG) Log.m(TAG,this,"onExecute");

		MvpUserData authData = MvpUserData.getFromMvpObject();
		
		mvpFavoritesList = MvpObject.newReference(Utils.formatLink(OBJPREFIX_FAVORITES));
		mvpChannelsList = MvpObject.newReference(Utils.formatLink(OBJPREFIX_TVCHANNELS));
		
		mvpChannelsMap = MvpObject.newReference(Utils.formatLink(OBJPREFIX_TVCHANNELS_MAP));
		
		boolean isAvailable = true;
		if (mvpChannelsList.getData() == null)
		{
			isAvailable = false;
			
			HttpReqParams httpReqParams = new HttpReqParams(
					IHttpConst.VALUE_SCHEMA_1_2, 
					IHttpConst.VALUE_CJSON);
			

			httpReqParams.setParam(IHttpConst.PARAM_BY_CATEGORIES, IHttpConst.VALUE_CHANNELS);	
			httpReqParams.setRange(1, 500);
			httpReqParams.setSort(IHttpConst.VALUE_LINE_UP_ID);
			httpReqParams.setParam(IHttpConst.PARAM_TYPES, IHttpConst.VALUE_NONE);
			
			taskGetChannels = new TaskPageReqCache<MvpMedia>(
					this, 
					MvpMedia.class, 
					MpxPagHeader.class,
					PlatformSettings.getInstance().getTpMpxFeedUrl(), 
					httpReqParams, 
					mvpChannelsList, 
					dataLifeTime);

			executeSubTask(taskGetChannels);	
		}
		
		if(!authData.isAnonymous())
		{
		if (mvpFavoritesList.getData() == null)
		{
			isAvailable = false;
			
			HttpReqParams httpReqParams = new HttpReqParams(
					IHttpConst.VALUE_SCHEMA_1_1, 
					IHttpConst.VALUE_CJSON);
			

			httpReqParams.setParam(IHttpConst.ACCOUNT_NAME, PlatformSettings.getInstance().getAccountNameValue());
			httpReqParams.setParam(IHttpConst.METHOD_PARAM, IHttpConst.METHOD_PARAM_GET);
			httpReqParams.setParam(IHttpConst.PARAM_TOKEN, MvpUserData.getFromMvpObject().getToken());
			httpReqParams.setRange(1, 500);
			
			taskGetFavorites = new TaskSingleReqParse<MvpFavoritesList>(
					this, 
					MvpFavoritesList.class,
					PlatformSettings.getInstance().getFavoriteEpgUrl(),
					httpReqParams);

			executeSubTask(taskGetFavorites);				
		}
		}
		
		if (isAvailable)
		{
			deliverList();
			taskCompleted();		
		}

		
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public Object getReturnData() {
		if (DEBUG) Log.m(TAG,this,"getReturnData");
		
		return mvpChannelsList;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private MvpPage<MvpMedia> filterChannels(MvpPage<MvpMedia> listChannels)
	{
		
		Map<String,MvpMedia> mapChannels = (Map<String,MvpMedia>) mvpChannelsMap.getData();
		if (null == mapChannels)
		{
			mapChannels = Collections.synchronizedMap(new HashMap<String, MvpMedia>());
			mvpChannelsMap.setData(mapChannels);
		}
		
		MvpPage<MvpMedia> listReturn = new MvpPage<MvpMedia>(MvpMedia.class, MpxPagHeader.class);
		
		synchronized (mapChannels) 
		{
			mapChannels.clear();			

			MvpList<String> listFavorites = (MvpList<String>) mvpFavoritesList.getData();
			

			
			for (int i=0; i<listChannels.getTotalResults(); i++)
			{
				MvpMedia channel = listChannels.getItem(i);

				if (channel == null)
				{
					continue;
				}
				
				mapChannels.put(channel.getId(), channel);
				
				boolean found = false;
				for (int j=listFavorites.size() - 1; j>=0; j--)
				{
					String channelId = listFavorites.getItem(j);
					if (channel.getId().equals(channelId))
					{
						channel.setSelected(true);
						listReturn.addItem(channel);
						break;
					}
				}
			}
		}
		
		return listReturn;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private MvpPage<MvpMedia> filterChannelsAnon(MvpPage<MvpMedia> listChannels)
	{
		
		Map<String,MvpMedia> mapChannels = (Map<String,MvpMedia>) mvpChannelsMap.getData();
		if (null == mapChannels)
		{
			mapChannels = Collections.synchronizedMap(new HashMap<String, MvpMedia>());
			mvpChannelsMap.setData(mapChannels);
		}
		
		MvpPage<MvpMedia> listReturn = new MvpPage<MvpMedia>(MvpMedia.class, MpxPagHeader.class);
		
		synchronized (mapChannels) 
		{
			mapChannels.clear();			
			
			for (int i=0; i<listChannels.getTotalResults(); i++)
			{
				MvpMedia channel = listChannels.getItem(i);

				if (channel == null)
				{
					continue;
				}
				
				mapChannels.put(channel.getId(), channel);
				listReturn.addItem(channel);
			}
		}
		
		return listReturn;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	private void deliverList() 
	{
		
		MvpPage<MvpMedia> listChannels = (MvpPage<MvpMedia>) mvpChannelsList.getData();
		
		MvpUserData authData = MvpUserData.getFromMvpObject();
		if(!authData.isAnonymous())
		{
		MvpPage<MvpMedia> filterListChannels = filterChannels(listChannels); 
		if (filterFavorites)
		{
			listChannels = filterListChannels;
		}
		}
		else
		{
			MvpPage<MvpMedia> listChannelsAnon = filterChannelsAnon(listChannels); 
			listChannels = listChannelsAnon;
		}
		
		listChannels.setTotalResults(listChannels.size());
		mvpReturnData.setData(listChannels);
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onReturnFromTask(Task subTask, boolean canceled) {

		super.onReturnFromTask(subTask, canceled);
		
		if (subTask.isError())
		{
			taskCompleted(subTask.getError());
			return;
		}
		
		MvpUserData authData = MvpUserData.getFromMvpObject();
		if(authData.isAnonymous())
		{
			if(subTask == taskGetChannels)
			{
				if(mvpChannelsList.getData() == null)
				{
					return;
				}
				
				deliverList();
				taskCompleted();
			}
		}
		else
		{
			if (subTask == taskGetChannels || subTask == taskGetFavorites)
			{
				if (mvpChannelsList.getData() == null || taskGetFavorites.getReturnData() == null)
				{
					return;
				}
				mvpFavoritesList.setData(taskGetFavorites.getReturnData());
				deliverList();
				taskCompleted();	
			}
		}
		
				
	}
}
