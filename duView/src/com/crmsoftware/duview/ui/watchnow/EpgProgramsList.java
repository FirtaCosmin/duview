package com.crmsoftware.duview.ui.watchnow;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.data.MvpChannelPrograms;
import com.crmsoftware.duview.data.MvpMedia;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.data.MvpProgram;
import com.crmsoftware.duview.ui.common.IMvpGenericAdapter;
import com.crmsoftware.duview.ui.common.MvpItemHolder;
import com.crmsoftware.duview.ui.common.MvpSimpleListAdapter;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;

public class EpgProgramsList extends EpgRow implements ITaskReturn {
	public static boolean VERBOUSE = Log.VERBOUSE ; 

	public final static SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

	private TaskGetPrograms taskGetPrograms; 
	private MvpChannelPrograms channelData = null;
	private View viewLoading;
	private View viewBody;
	private MvpMedia channelMedia;
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public EpgProgramsList(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void enableRow(boolean state)
	{
		if (viewLoading == null || viewBody == null)
		{
			return;
		}
		
		if (state == false)
		{
			viewLoading.setVisibility(View.VISIBLE);
			viewBody.setVisibility(View.INVISIBLE);		
		}
		else
		{
			viewLoading.setVisibility(View.GONE);
			viewBody.setVisibility(View.VISIBLE);				
		}
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void setControls(View viewLoading, View viewBody) 
	{
		this.viewLoading = viewLoading;
		this.viewBody = viewBody;		
	}
	

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public MvpMedia getChannelMedia() 
	{
		return channelMedia;
	}


	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void setData(MvpMedia channelMedia, Date date) 
	{
		if (DEBUG) Log.m(TAG,this, "setChannel");

		if (VERBOUSE) Log.m(TAG,this, "\nmTotalOffset = " + mTotalOffset + "\nTOTAL_OFFSET = " + EpgChannelsList.mScrollHelper.TOTAL_OFFSET );
		
		if (null == channelMedia)
		{
			return;
		}
		
		this.channelMedia = channelMedia;
		String channelId = channelMedia.getId();
		
		if (null != taskGetPrograms)
		{
			taskGetPrograms.cancel(true);
			taskGetPrograms = null;
		}

		sTag = channelId;	// only for debugging
		channelData = null;
		setAdapter(null);
		
		initView();
		
		MvpObject mvpReturnData = MvpObject.newReference(Utils.formatLink(TaskGetPrograms.createReference(channelId,date)));
		channelData = (MvpChannelPrograms)mvpReturnData.getData();
		
		if (null != channelData)
		{
			enableRow(true);
			MvpSimpleListAdapter adapter = (MvpSimpleListAdapter)mAdapter;
			if (adapter == null)
			{
				adapter = new MvpSimpleListAdapter(MvpMedia.class, this);
				setAdapter(adapter);
			}
			adapter.setCount(channelData.getCount());
		}
		else
		{
			enableRow(false);
			taskGetPrograms = new TaskGetPrograms(this, channelId, date, EpgChannelsList.TIME_LINE_WIDTH);
			taskGetPrograms.execute();
		}
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onListItemUpdateView(IMvpGenericAdapter adapter,
			MvpItemHolder holder, int position) {
	
		if (DEBUG) Log.m(TAG,this, "onListItemUpdateView");

	    if (null == channelData)
	    {
	    	return;
	    }
		    
	    MvpProgram program = channelData.getProgram(position);
	    if (null == program)
	    {
	    	return;
	    }
	    
	    EpgProgram view = (EpgProgram) holder.getView();
	    
	    view.setProgram(program);
	    
	    if (EpgChannelsList.SELECTED_PROGRAM_ID.equals(program.getId()) && false == EpgChannelsList.SELECTED_PROGRAM_ID.isEmpty())
	    {
	    	EpgChannelsList.selectedProgram = view;
	    	view.setSelected(true);
	    }
	    else
	    {
	    	view.setSelected(false);
	    }
	}
	
	
	

	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected int getItemViewWidth(int pos) {
		
		if (null == channelData)
		{
			return 5;// :)
		}

		return channelData.getCellSize(pos);
	}
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onReturnFromTask(Task task, boolean canceled) {
		if (DEBUG) Log.m(TAG,this, "onReturnFromTask");	
		
		if (task.isError())
		{
			return;
		}
		
		if (task != taskGetPrograms)
		{
			return;
		}
		
		MvpSimpleListAdapter adapter = (MvpSimpleListAdapter)mAdapter;
		if (adapter == null)
		{
			adapter = new MvpSimpleListAdapter(MvpMedia.class, this);
			setAdapter(adapter);
		}

		MvpObject mvpObject = (MvpObject)task.getReturnData();
		channelData = (MvpChannelPrograms)mvpObject.getData();
		if (null != channelData)
		{
			new Handler().post(new Runnable() {
				
				@Override
				public void run() {
					if (null != mAdapter)
					{
						enableRow(true);
						((MvpSimpleListAdapter)mAdapter).setCount(channelData.getCount());
					}
				}
			});
		}
	}
	
	
	@Override
	public int onListItemGetLayoutId(IMvpGenericAdapter adapter, int position) {
		// TODO Auto-generated method stub
		return 	R.layout.epg_program;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public MvpProgram getCurrentProgram() 
	{
		if (null == channelData)
		{
			return null;
		}
		
		return channelData.getCurrentProgram();
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	public EpgProgram getProgramView(String programId)
	{
		if (null == channelData)
		{
			return null;
		}
		
		for (int i= 0; i<getChildCount(); i++)
		{
			View view = getChildAt(i);
			
			if (view instanceof EpgProgram)
			{
				EpgProgram programView = (EpgProgram)view;
				if (programView.getProgram().getId().equals(programId))
				{
					return programView;
				}
			}
		}	
		return null;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	public MvpProgram getProgram(String programId)
	{

		return channelData.getProgram(programId);
	}
	
}
