package com.crmsoftware.duview.ui.watchnow;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************

import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.SearchView;
import android.widget.SearchView.OnCloseListener;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.FragmentChangeActivity;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.configuration.PlatformSettings;
import com.crmsoftware.duview.core.common.HttpReqParams;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.core.common.TaskPageReqCache;
import com.crmsoftware.duview.core.common.TaskSingleReqParse;
import com.crmsoftware.duview.data.MpxPagHeader;
import com.crmsoftware.duview.data.MvpChannelPrograms;
import com.crmsoftware.duview.data.MvpContent;
import com.crmsoftware.duview.data.MvpFavoritesList;
import com.crmsoftware.duview.data.MvpMedia;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.data.MvpPage;
import com.crmsoftware.duview.data.MvpParentalManagement;
import com.crmsoftware.duview.data.MvpProgram;
import com.crmsoftware.duview.data.MvpServerError;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.player.MvpPlayer;
import com.crmsoftware.duview.player.MvpPlayerInfo;
import com.crmsoftware.duview.player.PlayerView;
import com.crmsoftware.duview.ui.MvpFragment;
import com.crmsoftware.duview.ui.common.IMvpGenericAdapter;
import com.crmsoftware.duview.ui.common.IMvpGenericListViewListener;
import com.crmsoftware.duview.ui.common.MvpHorizontalListView;
import com.crmsoftware.duview.ui.common.MvpImageView;
import com.crmsoftware.duview.ui.common.MvpItemHolder;
import com.crmsoftware.duview.ui.common.MvpSimpleListAdapter;
import com.crmsoftware.duview.ui.common.MvpVerticalListView;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.ui.watchnow.ProgramDetailsView.ProgramDetailsListener;
import com.crmsoftware.duview.utils.CustomDialogView;
import com.crmsoftware.duview.utils.IHttpConst;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.ParentalPinDialogView;
import com.crmsoftware.duview.utils.Utils;

public class LiveTvFragment extends MvpFragment implements
ITaskReturn,
IMvpGenericListViewListener,
IEpgListener,
OnQueryTextListener,
OnCloseListener
{
	
	public static boolean DEBUG = Log.DEBUG; 
	public static boolean IS_TESTING = Log.IS_TESTING; //Log.DEBUG; 
	public final static String TAG = LiveTvFragment.class.getSimpleName();

	private static final String SELECTED_DATE = "selectedDate";
	private static final String SELECTED_PROGRAM = "selectedProgram";
	private static final String SELECTED_CHANNEL = "selectedChannel";
	private static final String PLAY_VIDEO = "playvideo";
	
	private static final String DATA_LIVE_DETAILS = "dataLiveDetails";
	private static final String DATA_OTHER_DETAILS = "dataOtherDetails";
	
	public static final String FAVORITES_ENABLED = "FAVORITES_ENABLED";
	
	public static long DATA_LIFE_TIME = 60*60*1000; // 60min life (the data is taken from cache and not updated from server)

	private TaskGetTvChannels taskGetTvChannels = null;

	private MvpObject mvpChannelsObj = null;
	private MvpObject mvpSearchChannelsObj = null;
	

	private String[] listDays;
	private Calendar currentEpgDay = Calendar.getInstance();	
	
	private Calendar debugEpgDay = Calendar.getInstance();
	private Calendar debugCurrentTime = Calendar.getInstance();
	MvpPage<MvpMedia> debugListChannels = new MvpPage<MvpMedia>(MvpMedia.class, MpxPagHeader.class);
	
	private ProgramDetailsView 	viewOtherDetails;
	private ProgramDetailsView 	viewLiveDetails;
	private PlayerView 			viewPlayer;
	private SearchView mSearchView;
	
	TaskPageReqCache<MvpMedia> taskSearchList;
	TaskPageReqCache<MvpMedia> taskFavoriteList;
	private TaskSingleReqParse taskAddDelete;
	private Task taskParentalPin;
	
	private boolean isFavoritesEnabled = false;
	private MenuItem searchItem;
	private MenuItem favoriteItem;
	
	private static View selectedChannelRow;
	
	

	private static Timer timer;
	public static int UPDATEEPG = 10; // 30 secs
	private Handler handler = new Handler();
    
//tells handler to send a message
	class UpdateEpgTask extends TimerTask 
	{
        @Override
        public void run() 
        {
        	handler.post(new Runnable() {
				
				@Override
				public void run() {
					
					updateTimeline();
					updateCurrentProgram();
				}
			});
		}
    };
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	/* (non-Javadoc)
	 * @see com.crmsoftware.duview.ui.MvpFragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		if (DEBUG) Log.m(TAG,this,"onCreateView");
		
		
		View view = setContentView(R.layout.fragment_live_tv,container,inflater);

		MvpViewHolder viewHolder = getHolder();
		
		Bundle data = getData();

		
		Bundle liveDetailsData = data.getBundle(DATA_LIVE_DETAILS);
		if (null == liveDetailsData)
		{
			liveDetailsData = new Bundle();
			data.putBundle(DATA_LIVE_DETAILS,liveDetailsData);
		}
		
		Bundle otherDetailsData = data.getBundle(DATA_OTHER_DETAILS);		
		if (null == otherDetailsData)
		{
			otherDetailsData = new Bundle();
			data.putBundle(DATA_OTHER_DETAILS,otherDetailsData);
		}
		
		boolean fullscreen = liveDetailsData.getBoolean(ProgramDetailsView.PLAYER_FULLSCREEN, false);
		
		setHasOptionsMenu(true);
		getMvpActivity().setMenuEnabled(!fullscreen);
		getMvpActivity().setActionBarVisibility(!fullscreen ? View.VISIBLE : View.GONE);
		getMvpActivity().setHomeButtonActionBar(true);
		getMvpActivity().setTitle(MainApp.getRes().getString(R.string.watch_now));
		
		viewPlayer = (PlayerView)viewHolder.getView(R.id.player_view);
		viewOtherDetails = (ProgramDetailsView) viewHolder.getView(R.id.details_other);
		viewLiveDetails = (ProgramDetailsView) viewHolder.getView(R.id.details_live);
			
		viewOtherDetails.setData(otherDetailsData);
		viewOtherDetails.bindPlayer(null);	
		viewOtherDetails.bindOtherDetails(viewLiveDetails);		
		viewOtherDetails.setEpgListener(this);
		
		viewLiveDetails.setData(liveDetailsData);
		viewLiveDetails.bindPlayer(viewPlayer);
		viewLiveDetails.bindOtherDetails(viewOtherDetails);
		viewLiveDetails.setEpgListener(this);
		/*listen to the changes in the program details to change the options menu buttons*/
		viewLiveDetails.setProgramDetailsListener(new ProgramDetailsListener() {
			
			@Override
			public void minimized(boolean minimized) {
				getActivity().invalidateOptionsMenu();
			}
			
			@Override
			public void closePlayer() {
				getActivity().invalidateOptionsMenu();
				
			}
		});
		
		viewPlayer.bringToFront();
	
		mvpChannelsObj = MvpObject.newReference(Utils.formatLink(TaskGetTvChannels.OBJPREFIX_FILTEREDCHANNELS));
		mvpChannelsObj.addObserver(this);
		

		/*get isFavoritesEnabled from fragment bundle if it is saved
		 * else get it from the saved app instance info*/
//		isFavoritesEnabled = data.getBoolean(FAVORITES_ENABLED, MvpUserData.getFromMvpObject().isOpenTvWithFavorites());
		initIsFavEnabled();
		
		mvpSearchChannelsObj = MvpObject.newReference(Utils.formatLink(PlatformSettings.getInstance().getSearchUrl()));
		mvpSearchChannelsObj.addObserver(this);
		
		if (IS_TESTING)
		{
/*			debugCurrentTime.set(Calendar.HOUR_OF_DAY, 9);
			debugCurrentTime.set(Calendar.MINUTE, 30);
			debugCurrentTime.set(Calendar.SECOND, 0);
			debugCurrentTime.set(Calendar.MILLISECOND, 0);	*/
			
			
			int addDayFromCurrent = 0;
			
			debugEpgDay = Calendar.getInstance();
			debugEpgDay.setTime(currentEpgDay.getTime()); 
			debugEpgDay.add(Calendar.DAY_OF_MONTH, addDayFromCurrent);

			currentEpgDay.setTime(debugEpgDay.getTime());
			
			debugListChannels.addItem(new MvpMedia("Arabya","http://livecdn.tvanywhere.ae/LiveSS/Logos/300/Al-Arabiya.png","http://alu.data.media.theplatform.com/media/data/Media/58145859670"));	// feletah 
			debugListChannels.addItem(new MvpMedia("Arabya hatach","http://livecdn.tvanywhere.ae/LiveSS/Logos/300/Al-Arabiya-Al-Hadath.png","http://alu.data.media.theplatform.com/media/data/Media/58155075686"));	// 
			//debugListChannels.addItem(new MvpMedia("http://alu.data.media.theplatform.com/media/data/Media/166237763881"));	// empty channel
			//debugListChannels.addItem(new MvpMedia("http://alu.data.media.theplatform.com/media/data/Media/90724931558"));	// 4th channel in server list
			//debugListChannels.addItem(new MvpMedia("http://alu.data.media.theplatform.com/media/data/Media/166237763881"));	// No info avaibale 
			debugListChannels.setTotalResults(3);
		}
		// ----- epgchannels, timeline and hourline
		
		
		if (savedInstanceState == null)
		{
			EpgChannelsList.setDefaultConfiguration();
			data.putLong(SELECTED_DATE,currentEpgDay.getTimeInMillis());
		}
		else
		{
			Long time = data.getLong(SELECTED_DATE);
			currentEpgDay.setTimeInMillis(time);
			
			EpgChannelsList.SELECTED_PROGRAM_ID = data.getString(SELECTED_PROGRAM, "");
		}
		
		Resources res = getResources();
		
		EpgChannelsList chanelsList = (EpgChannelsList) viewHolder.getView(R.id.items_pagedlist);
		MvpChannelPrograms.NO_PROG_INFO = getResources().getString(R.string.epg_no_program_info);;
		chanelsList.setEpgTimelineView(viewHolder.getView(R.id.epg_current_time));
		chanelsList.setEpgChannelListener(this); 
		
		ViewGroup hoursBar = (ViewGroup) viewHolder.getView(R.id.items_epg_time_hours);
		chanelsList.setEpgTimeHoursView(hoursBar);

        MvpSimpleListAdapter channelsAdapter = new MvpSimpleListAdapter(MvpMedia.class, this);
        channelsAdapter.receiveClickFrom(R.id.logo_selection);
        
        chanelsList.setAdapter(channelsAdapter);        
        updateTimeline();
		if (MainApp.getRes().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
		{
			chanelsList.scrollBy(0); // in case of screen rotation (portrait > landscape)  and EPG is at 23:30 this is adjusting TOTAL OFFSET by reposition programs to right 
		}
        
        if (EpgChannelsList.mScrollHelper.TOTAL_OFFSET == 0)
        {
        	chanelsList.centerOnCurrentTime();
        }
		//-------- epg days

		int dayWidth = (int)res.getDimension(R.dimen.epg_daybar_cellWidth);
        listDays = res.getStringArray(R.array.epg_days);
        
		double cellWidth = (res.getDisplayMetrics().widthPixels + res.getDimension(R.dimen.epg_item_space)) / 7;
	
		
		if (cellWidth > dayWidth) // exapnding the days timeline over the entire space (tablet usually)
		{
			dayWidth = Utils.roundToInt(cellWidth);
		}
		
		MvpHorizontalListView epgDays = (MvpHorizontalListView) viewHolder.getView(R.id.items_epg_time_days);
		epgDays.setCellWidth(dayWidth);
		
        MvpSimpleListAdapter epgDaysAdapter = new MvpSimpleListAdapter(String.class, this);
        epgDaysAdapter.receiveClickFrom(R.id.epg_day_title);
        
        epgDays.setAdapter(epgDaysAdapter);
        epgDaysAdapter.setCount(7);
        
		
        // orchestrate everything 
		if (!IS_TESTING)
		{

			MvpPage<MvpMedia> listCategories = (MvpPage<MvpMedia>) mvpChannelsObj.getData();
			if  (null == listCategories)
			{
				getMvpActivity().showLoadingDialog();
				taskGetTvChannels = new TaskGetTvChannels(this, mvpChannelsObj, isFavoritesEnabled, DATA_LIFE_TIME);
				taskGetTvChannels.execute();
			}
			else
			{
				mvpChannelsObj.setData(listCategories);
			}
		}
		else
		{
			mvpChannelsObj.setData(debugListChannels);
		}
		
		return view;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onResume() {
		if (DEBUG) Log.m(TAG,this,"onResume");
		
		if (null != viewPlayer)
		{
			viewLiveDetails.onParentResume();
		}

		startUpdateTimer();
		
		super.onResume();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onPause() {
		if (DEBUG) Log.m(TAG,this,"onPause");
		
		if (null != viewPlayer)
		{
			viewLiveDetails.onParentPaused();
		}
		
		EpgChannelsList chanelsList = (EpgChannelsList) viewHolder.getView(R.id.items_pagedlist);
		
		super.onPause();
	}
	
	
	@Override
	public void onDestroy() 
	{
		if (DEBUG) Log.m(TAG,this,"onDestroy");
		
		//stopUpdateTimer();
		
		super.onDestroy();
	}
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void startUpdateTimer()
	{
		if (DEBUG) Log.m(TAG,this,"startUpdateTimer");
		
  		if (timer != null)
  		{
  			stopUpdateTimer();	
  		}

  		timer = new Timer();
  		timer.schedule(new UpdateEpgTask(), 0, UPDATEEPG*1000);	
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void stopUpdateTimer()
	{
		if (DEBUG) Log.m(TAG,this,"stopUpdateTimer");
		
		if (null == timer)
		{
			return;
		}
		
		timer.cancel();
		timer.purge();
		timer = null;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void closeDetails() 
	{
		if (DEBUG) Log.m(TAG,this,"closeDetails");
		
		if (null != viewOtherDetails && viewOtherDetails.isShown())
		{
			viewOtherDetails.show(false);
		}
		
		if (null != viewLiveDetails && viewLiveDetails.isShown())
		{
			viewLiveDetails.show(false);
		}
		
		if (null != viewPlayer)
		{
			viewPlayer.destroyPlayer();
			viewPlayer.setVisibility(View.INVISIBLE);
			viewPlayer = null;	
		}
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onResetData() 
	{
		if (DEBUG) Log.m(TAG,this,"onResetData");
		
		stopUpdateTimer();
		closeDetails();
		
		getData().clear();
		
		
//		if (isFavoritesEnabled)
//		{
//			mvpChannelsObj.setData(null);
//		}

		getData().putBoolean(FAVORITES_ENABLED, isFavoritesEnabled);

		 MvpUserData authData = MvpUserData.getFromMvpObject();
		 authData.setOpenTvWithFavorites(isFavoritesEnabled);
		super.onResetData();
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public boolean onBackPressed()
	{
		if (DEBUG) Log.m(TAG,this,"onBackPressed");
		Boolean ret = true;
		if (null == viewLiveDetails)
		{
			return ret;
		}
		
		if (viewOtherDetails.isShown())
		{	
			ret = viewOtherDetails.onBackPressed();
		}
		
		if (viewLiveDetails.isShown())
		{	
			markAsPlayedChannel("", selectedChannelRow);
			ret = viewLiveDetails.onBackPressed();
		}
		/*redraw the options menu buttons*/
		getActivity().invalidateOptionsMenu();
		return ret;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void updateTimeline()
	{
		if (DEBUG) Log.m(TAG,this,"updateTimeline");
		
		Calendar currentTime = Calendar.getInstance();
		if (IS_TESTING)
		{
			currentTime.setTime(debugCurrentTime.getTime());
		}

		boolean isCurrentDate = 
				currentTime.get(Calendar.DAY_OF_MONTH) == currentEpgDay.get(Calendar.DAY_OF_MONTH) 
				&& currentTime.get(Calendar.MONTH) == currentEpgDay.get(Calendar.MONTH) 
				&& currentTime.get(Calendar.YEAR) == currentEpgDay.get(Calendar.YEAR);
		
		EpgChannelsList chanelsList = (EpgChannelsList) viewHolder.getView(R.id.items_pagedlist);
		chanelsList.showTimeline(isCurrentDate ? View.VISIBLE : View.INVISIBLE);
		chanelsList.updateTimeline(currentTime.getTime());
		
	}
	
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onDestroyView() 
	{
		if (DEBUG) Log.m(TAG,this,"onDestroyView");
		
		mvpChannelsObj.deleteObserver(this);
		
		
		super.onDestroyView();
	}
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onReturnFromTask(Task task, boolean canceled) 
	{
		
		if (task == taskGetTvChannels)
		{
			getMvpActivity().hideLoadingDialog();
			taskGetTvChannels = null;
			return;
		}	
		
		if (task == taskParentalPin)
		{
			getMvpActivity().hideLoadingDialog();
			
			if (task.getError() instanceof MvpServerError)
			{
				int retCode = ((MvpServerError)task.getError()).getResponseCode();
				MvpServerError mvpError = (MvpServerError)task.getError();
				if (retCode != 200)
				{
					if(retCode == 400)
					{
						showToast(MainApp.getRes().getString(R.string.change_pins_settings_wrong));
					}
					else
					{
						showToast(mvpError.getTitle());
					}
					ParentalPinDialogView.getInstance().dismiss();
					ParentalPinDialogView parentalDialogView =   ParentalPinDialogView.getInstance();
					parentalDialogView.setData(getTag(), getData());
					parentalDialogView.show(getChildFragmentManager(), ParentalPinDialogView.TAG);
				}
				else
				{
					ParentalPinDialogView.getInstance().dismiss();
					
					Bundle data = getData();
					gotoProgramDetails(
							data.getString(SELECTED_CHANNEL),
							data.getString(SELECTED_PROGRAM), 
							data.getBoolean(PLAY_VIDEO) );
				}
			}
			taskParentalPin = null;
			return;
		}		

	}
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onListItemViewClick(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder) {
		
		 if (DEBUG) Log.m(TAG,this, "onListItemViewClick");
		 
		 Type type = adapter.getType();
		 int id = view.getId();

		 
		 //------- days adapter
		 if (type == String.class)
		 {
			switch (id) {
			case R.id.epg_day_title:
			{
				 Calendar selectedDate = Calendar.getInstance(); 
				 selectedDate.add(Calendar.DAY_OF_MONTH, holder.getPosition());
				 currentEpgDay.setTime(selectedDate.getTime());
				 
				 getData().putLong(SELECTED_DATE, currentEpgDay.getTimeInMillis());
				 
				 adapter.notifyDataSetChange();
				
				 EpgChannelsList chanelsList = (EpgChannelsList) viewHolder.getView(R.id.items_pagedlist);
				 chanelsList.updateChannelData(currentEpgDay.getTime());	
				 

				 
				 if (holder.getPosition() == 0)
				 {
					 chanelsList.showTimeline(View.VISIBLE);
					 updateTimeline();
					 chanelsList.centerOnCurrentTime();
				 }
				 else
				 {
					 updateTimeline();
				 }
				 

			}	
			break;

			default:
				break;
			}
			return; 
		 }
		 
		 //------ channels adapter
		 if (type == MvpMedia.class)
		 {
			 switch (id) {
			 case R.id.logo_selection:
				 	MvpUserData authData = MvpUserData.getFromMvpObject();
				 	if(authData.isAnonymous())
				 	{
				 		CustomDialogView customDialogView = CustomDialogView.getInstance();
						customDialogView.show(getFragmentManager(), CustomDialogView.TAG);
				 	}
				 	else
				 	{
				
					MvpPage<MvpMedia> listChannels = (MvpPage<MvpMedia>)mvpChannelsObj.getData(); // only categories are comming here 
					MvpMedia media = listChannels.getItem(holder.getPosition());
					
					boolean state = media.getSelected();
					
					media.setSelected(!state);
					 int visibility = !state ?  View.VISIBLE : View.INVISIBLE;
					 String mediaId = media.getId();
					 holder.getView(R.id.channel_favorite).setVisibility(visibility);
					 
					 MvpObject mvpFavoritesList = MvpObject.newReference(Utils.formatLink(TaskGetTvChannels.OBJPREFIX_FAVORITES));
					 MvpFavoritesList list = (MvpFavoritesList) mvpFavoritesList.getData();
					 if (!state)
					 {
						 performAddDeleteFavorites(mediaId,IHttpConst.METHOD_PARAM_POST);
						 list.add(mediaId);
					 }
					 else
					 {
						 performAddDeleteFavorites(mediaId,IHttpConst.METHOD_PARAM_DELETE);
						 list.remove(mediaId);
						 
						 if (isFavoritesEnabled)
						 {
							MvpVerticalListView listView = (MvpVerticalListView) getHolder().getView(R.id.items_pagedlist);
							IMvpGenericAdapter categoriesAdapter = (IMvpGenericAdapter) listView.getAdapter();
								
							 listChannels.removeItem(holder.getPosition());
							 categoriesAdapter.setCount(listChannels.size());  
						 }
					 }
				 	}
					 


				break;

			 default:
				break;
			}
		 }
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public boolean onListItemViewTouch(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onDataUpdate(Observable observable, Object data) 
	{
		 if (DEBUG) Log.m(TAG,this, "onDataUpdate");
		 
		if (observable == mvpChannelsObj)
		{

			getMvpActivity().hideLoadingDialog();
			MvpObject mvpChannelsObj = (MvpObject)observable; // only categories are comming here 
			
			MvpPage<MvpMedia> listCategories = (MvpPage<MvpMedia>) mvpChannelsObj.getData();
			if  (null == listCategories)
			{
				// data might be removed from memory
				return;
			}
			
			MvpVerticalListView listView = (MvpVerticalListView) getHolder().getView(R.id.items_pagedlist);
			IMvpGenericAdapter categoriesAdapter = (IMvpGenericAdapter) listView.getAdapter();
			categoriesAdapter.setCount(listCategories.size());  
		}
		
	}

	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onListItemUpdateView(IMvpGenericAdapter adapter,
			MvpItemHolder holder, int position) 
	{
		 if (DEBUG) Log.m(TAG,this, "onListItemUpdateView");
		 
		 Type type = adapter.getType();
		 if (DEBUG) Log.m(TAG,this, "type= "+ type + " >>>>>>> position = " + position);
		 
		 //------- channels adapter
		 if (type == MvpMedia.class)
		 {
			 EpgProgramsList epgChannel = (EpgProgramsList) holder.getView(R.id.epg_channel);
			 epgChannel.setControls(holder.getView(R.id.epg_channel_loading), holder.getView(R.id.epg_channel));
			 		 		 
			 MvpPage<MvpMedia> listChannels = (MvpPage<MvpMedia>) mvpChannelsObj.getData();
			 
			 if (null != listChannels)
			 { 
				 MvpMedia channel = listChannels.getItem(position);
				 if (null != channel)
				 {
					 epgChannel.setData(channel, currentEpgDay.getTime());
					 MvpImageView channelLogo = (MvpImageView) holder.getView(R.id.channel_logo);
					 
					 String sUrl = channel.getThumbPoster().url;
					 
					 if (sUrl.isEmpty())
					 {
						 sUrl = channel.getThumbLandscape().url; 
					 }
					 
					 channelLogo.setData(sUrl);
					
					 markAsPlayedChannel(channel.getId(), holder.getView());
					 MvpUserData authData = MvpUserData.getFromMvpObject();
					 int visibility = channel.isSelected() && !authData.isAnonymous() ?  View.VISIBLE : View.INVISIBLE;
						
					 holder.getView(R.id.channel_favorite).setVisibility(visibility);
					 
					 return;
				 }
				 
			 }

			 epgChannel.enableRow(false);

			 return;
		 }
		 
		 //----------- days adapter
		 if (type == String.class)
		 {
			 Calendar currentDate = Calendar.getInstance(); 
			 currentDate.add(Calendar.DAY_OF_MONTH, position);
			
			 int index = position;
			 if (position >= 2)
			 {
				index = currentDate.get(Calendar.DAY_OF_WEEK)+1;
			 }	 
			 
			 boolean current = currentEpgDay.get(Calendar.DAY_OF_MONTH) == currentDate.get(Calendar.DAY_OF_MONTH);
			 
			 TextView view = (TextView) holder.getView(R.id.epg_day_title);
			 
			 view.setText(listDays[index]);
			 view.setTextColor(getResources().getColor(current ? R.color.epg_cell_selected_light : R.color.white));
			 holder.setVisibility(R.id.epg_day_selected, current ? View.VISIBLE : View.INVISIBLE);
			 
		 }
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void markAsPlayedChannel(String channelId, View channelRow)
	{
		if (channelRow == null)
		{
			return;
		}
		
		 String playedChanneldId = "";
		 if (MvpPlayer.getInstance().getMedia() != null)
		 {
			 playedChanneldId = MvpPlayer.getInstance().getMedia().getId();
		 }
		 
		 View channelLogo = channelRow.findViewById(R.id.logo_selection);
		 if (false == playedChanneldId.equals(channelId) || Utils.isEmpty(channelId))
		 {
			 channelLogo.setBackgroundColor(getResources().getColor(R.color.epg_channel_logo_background));
		 }
		 else
		 {
			 selectedChannelRow = channelRow;
			 channelLogo.setBackgroundResource(R.drawable.epg_selected_channel); 
		 }
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void updateCurrentProgram() 
	{
		
/*		MvpObject mvpReturnData = MvpObject.newReference(Utils.formatLink(TaskGetPrograms.createReference(channelId,date)));
		MvpChannelPrograms channelData = (MvpChannelPrograms)mvpReturnData.getData();
		
		if (null == channelData)
		{
			 return;
		}
		
		MvpProgram program = channelData.getCurrentProgram();
		 
		if (null == program)
		{
			 return;
		}*/
		
		if (selectedChannelRow == null)
		{
			return;
		}
		
		 EpgProgramsList epgChannel = (EpgProgramsList) selectedChannelRow.findViewById(R.id.epg_channel);
		 
		 MvpProgram program = epgChannel.getCurrentProgram();
		 
		 if (null == program)
		 {
			 return;
		 }
		 
		 updateData(epgChannel.getChannelMedia(), program, MvpPlayer.getPlayerInfo());
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void gotoProgramDetails(String channelId, String programId, boolean play)
	{
		EpgChannelsList chanelsList = (EpgChannelsList) viewHolder.getView(R.id.items_pagedlist);
		View channelRow = chanelsList.getChannelView(channelId);
		
		if (null == channelRow)
		{
			return;
		}
		
		EpgProgramsList list = (EpgProgramsList)channelRow.findViewById(R.id.epg_channel);;
		MvpProgram program = list.getProgram(programId);
		if (null == program)
		{
			return;
		}
		
		gotoProgramDetails(channelRow, program, play);
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void gotoProgramDetails(View channelRow, MvpProgram program, boolean play)
	{
		 EpgProgramsList epgChannel = (EpgProgramsList) channelRow.findViewById(R.id.epg_channel);
		 MvpMedia channel = epgChannel.getChannelMedia();
		 MvpUserData authData = MvpUserData.getFromMvpObject();
		 
		 if (play)
		 {
			 markAsPlayedChannel("", selectedChannelRow);	//  deselecting the current one

			 viewPlayer.destroyPlayer();
			 
			 viewLiveDetails.setPlayerInfo(MvpPlayer.getPlayerInfo());
			 updateData(channel, program,MvpPlayer.getPlayerInfo());
			 
			 
			 if (false == program.isCurrent())	// for programs that are not current 
			 {
				 viewLiveDetails.maximize(true, false);
			 }
				 
			 if (!viewLiveDetails.isShown())
			 { 
				 viewLiveDetails.show(true);
			 }
		 }
		 else
		 {
			 MvpPlayerInfo info = new MvpPlayerInfo();
			 viewOtherDetails.setPlayerInfo(info);	 
			 updateData(channel, program,info);
			 
			 viewOtherDetails.show(true);
		 }
		 	 
		
		 if (play && !authData.isAnonymous())
		 {
			 viewPlayer.attachPlayer();
			 
			 if (IS_TESTING)
			 {

				 viewPlayer.play(Environment.getExternalStorageDirectory() + "/duView/A.mp4");
			 }
			 else
			 {
			 	//viewPlayer.play(Environment.getExternalStorageDirectory() + "/duView/A.mp4");
			 	//viewPlayer.play("http://184.72.239.149/vod/mp4:bigbuckbunny_750.mp4/playlist.m3u8");
				 viewPlayer.play(channel);
			 }
		 }
		 
		 markAsPlayedChannel(channel.getId(), channelRow);
		 startUpdateTimer();
		 getActivity().invalidateOptionsMenu();
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void OnProgramClick(View channelRow, MvpProgram program, boolean play) 
	{
		 if (DEBUG) Log.m(TAG,this, "OnProgramClick");
		 	 
		

		 EpgProgramsList epgChannel = (EpgProgramsList) channelRow.findViewById(R.id.epg_channel);
		 MvpMedia channel = epgChannel.getChannelMedia();
		 Bundle data = getData();
		 data.putString(SELECTED_PROGRAM, program.getId());
		 data.putString(SELECTED_CHANNEL, channel.getId());
		 data.putBoolean(PLAY_VIDEO, play);
	 
		 if (DEBUG) Log.d(TAG, "details url = " + program.getDetailsUrl());
		if(MvpParentalManagement.getInstance().checkLevel(channel.getParentalLevel()) == MvpParentalManagement.LEVEL_NOK)
		{
			ParentalPinDialogView parentalDialogView =   ParentalPinDialogView.getInstance();
			parentalDialogView.setData(LiveTvFragment.TAG, data);
			parentalDialogView.show(getChildFragmentManager(), ParentalPinDialogView.TAG);
		}
		else
		{
			gotoProgramDetails(channelRow, program, play);
		}
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void OnWatchChannelClick(String channelId, String programId) 
	{
		 if (DEBUG) Log.m(TAG,this, "OnWatchChannelClick");
		 
		EpgChannelsList chanelsList = (EpgChannelsList) viewHolder.getView(R.id.items_pagedlist);
		chanelsList.selectProgram(channelId, programId, true);		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		
		inflater.inflate(R.menu.watch_now_menu, menu);
		
		  favoriteItem = menu.findItem(R.id.grid_default_favorites);
		  searchItem = menu.findItem(R.id.grid_default_search);
		  mSearchView = (SearchView) searchItem.getActionView();
		  /*if any of the details views are visible and maximized then hide the action bar buttons*/
		  if ( viewOtherDetails.isMaximized() && viewOtherDetails.isShown() || 
			   viewLiveDetails.isMaximized()  && viewLiveDetails.isShown() ) {
			  searchItem.setVisible(false);
			  mSearchView.setVisibility(View.INVISIBLE);
			  favoriteItem.setVisible(false);
		  }else{
			  searchItem.setVisible(true);
			  mSearchView.setVisibility(View.VISIBLE);
			  favoriteItem.setVisible(true);
			  SearchViewShow(searchItem);
			  favoriteState(isFavoritesEnabled);
		  }
		
		super.onCreateOptionsMenu(menu, inflater);
	}
	/* (non-Javadoc)
	 * @see com.actionbarsherlock.app.SherlockFragment#onPrepareOptionsMenu(android.view.Menu)
	 * @desc will clear the search textbox  
	 * 		 The search textbox keeps the search string even after the fragment switching. 
	 * 		 This will prevent it from doing that.
	 */
	@Override
	public void onPrepareOptionsMenu (com.actionbarsherlock.view.Menu menu){
		 if (DEBUG) Log.m(TAG,this, "onPrepareOptionsMenu");
		 mSearchView.setQuery("", false);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) 
		{
		case R.id.grid_default_favorites:
			MvpUserData authData = MvpUserData.getFromMvpObject();
			if(authData.isAnonymous())
			{
				CustomDialogView customDialogView = CustomDialogView.getInstance();
				customDialogView.show(getFragmentManager(), CustomDialogView.TAG);
			}
			else
			{
				isFavoritesEnabled = !isFavoritesEnabled;
				favoriteState(isFavoritesEnabled);
				getData().putBoolean(FAVORITES_ENABLED, isFavoritesEnabled);
				MvpUserData.getFromMvpObject().setOpenTvWithFavorites(isFavoritesEnabled);
				
				getMvpActivity().showLoadingDialog();
				taskGetTvChannels = new TaskGetTvChannels(this, mvpChannelsObj, isFavoritesEnabled, DATA_LIFE_TIME);
				taskGetTvChannels.execute();
			}
             
		}
		return super.onOptionsItemSelected(item);
	}
	
	@SuppressLint("NewApi")
	public void favoriteState(boolean isSelected){
		if(favoriteItem != null)
		{
			if(isSelected)
			{
				favoriteItem.setIcon(R.drawable.heart_ch);
			}
			else
			{
				favoriteItem.setIcon(R.drawable.heart_bar);
			}
			mSearchView.onActionViewCollapsed();
		}
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void SearchViewShow(MenuItem searchItem) {

	    if (isAlwaysExpanded()) {
	        mSearchView.setIconifiedByDefault(false);
	    } else {
	        searchItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM);
	    }

	    mSearchView.setOnQueryTextListener(this);
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public boolean onClose() {
		mSearchView.clearFocus();
		mSearchView.setQuery("", false);

		 MvpUserData authData = MvpUserData.getFromMvpObject();
		 authData.setOpenTvWithFavorites(isFavoritesEnabled);
	    return true;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected boolean isAlwaysExpanded() {
	    return false;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public boolean onQueryTextSubmit(String query) {
		
		closeDetails();
		stopUpdateTimer();
		
		Bundle data = new Bundle();
		
		data.putInt(ProgramsListViewFragment.DATA_POSITION, 0);
		data.putString(ProgramsListViewFragment.DATA_QUERY, query);
		data.putBoolean(ProgramsListViewFragment.DATA_FAVORITES, isFavoritesEnabled);
		
		FragmentChangeActivity fca = (FragmentChangeActivity) getActivity();
		
		fca.goTo(ProgramsListViewFragment.class,ProgramsListViewFragment.TAG, data, true);
		hideKeyboard(getActivity());
		
		return true;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public boolean onQueryTextChange(String newText) {
		return false;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onListItemsCountChanged(IMvpGenericAdapter adapter, int newCount) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void performAddDeleteFavorites(String favChannelsList, String method)
	{
		HttpReqParams httpReqParams = new HttpReqParams(
				IHttpConst.VALUE_SCHEMA_1_1);

		httpReqParams.setParam(IHttpConst.ACCOUNT_NAME, PlatformSettings.getInstance().getAccountNameValue());
		httpReqParams.setParam(IHttpConst.METHOD_PARAM, method);
		httpReqParams.setParam(IHttpConst.CHANNEL_LIST, favChannelsList);
		httpReqParams.setParam(IHttpConst.PARAM_TOKEN, MvpUserData.getFromMvpObject().getToken());
	
		taskAddDelete = new TaskSingleReqParse
				(this,
				null,
				PlatformSettings.getInstance().getFavoriteEpgUrl(), 
				httpReqParams);

		taskAddDelete.execute();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public static void hideKeyboard(Activity activity) {
	    InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
	    //Find the currently focused view, so we can grab the correct window token from it.
	    View view = activity.getCurrentFocus();
	    //If no view currently has focus, create a new one, just so we can grab a window token from it
	    if(view == null) {
	        view = new View(activity);
	    }
	    inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public int onListItemGetLayoutId(IMvpGenericAdapter adapter, int position) 
	{
		if (adapter.getType() == String.class)
		{
			return R.layout.epg_day;
		}
		
		if (adapter.getType() == MvpMedia.class)
		{
			return R.layout.epg_channel;
		}
		
		return 0;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void updateData(MvpMedia mvpMedia, MvpProgram mvpProgram, MvpPlayerInfo playerInfo)
	{
		if (DEBUG) Log.d(TAG,"updateData");	

		playerInfo.resetData();

		 if(mvpMedia != null)
		 {
			 playerInfo.setChannelId(mvpMedia.getId());
				
			 MvpContent itemImageData = mvpMedia.getThumbPoster();
			 if (null != itemImageData)
			 {
				 playerInfo.setChannelImage(itemImageData.getUrl());
			 }
			 
			 
			 playerInfo.setParentalRatings(mvpMedia.getRatings().getRating());
			 String min = MainApp.getRes().getString(R.string.min);
		 }
		 
		 if(mvpProgram != null)
		 {
			 playerInfo.setProgramId(mvpProgram.getId());
			 playerInfo.setTitle(mvpProgram.getTitle());
			 playerInfo.setShortDesc(mvpProgram.getShortDescription());
			 playerInfo.setStart(mvpProgram.getStartTime());
			 playerInfo.setEnd(mvpProgram.getEndTime());
			 playerInfo.setDuration((((mvpProgram.getEndTime())/1000)/60) - (((mvpProgram.getStartTime())/1000)/60) + " min");
			 playerInfo.setLongDesc(mvpProgram.getShortDescription());			 
			 playerInfo.setShortDesc((Utils.dateToString(mvpProgram.getStartTime(),EpgProgramsList.dateFormat) + "-" + Utils.dateToString(mvpProgram.getEndTime(),EpgProgramsList.dateFormat)));
			 playerInfo.setProgramImage("");
		 }
		 
		 if (Utils.isEmpty(mvpProgram.getTitle()))
		 {
			 playerInfo.setTitle(MvpChannelPrograms.NO_PROG_INFO);
		 }
		 
		 playerInfo.notifyObservers();
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onMvpDlgClose(String dlgTag, Bundle data) {
		
			int viewId = data.getInt(ParentalPinDialogView.RETURN_STATE);
			
			switch (viewId) 
			{
			case R.id.button_pozitive:
				String pin = data.getString(ParentalPinDialogView.PIN_VALUE);
				if(Utils.isEmpty(pin))
				{
					showToast("Field is empty");
				}
				else
				{
					getMvpActivity().showLoadingDialog();
					taskParentalPin = MvpParentalManagement.getInstance().checkPin(pin, this);
				}
				break;
	
			case R.id.button_negative:
				ParentalPinDialogView.getInstance().dismiss();
			default:
				break;
			}
		
		
		super.onMvpDlgClose(dlgTag, data);
	}
	
	
	
	/**
	 * @desc method to initialize the value of isFavoritesEnabled flag.
	 * It will be false if :
	 * - it is passed false from the fragment bundle
	 * - it is passed false from user profile
	 * - it is passed true from bundle or profile but the user has no favorite channels
	 * It will be true if:
	 * - it is passed as true from bundle or profile and the user has favorite channels
	 */
	private void initIsFavEnabled(){
		/*consider that is initially false. This way it will not be necessary 
		 * to change its value too many times*/
		isFavoritesEnabled = false;
		Bundle data = getData();
		Boolean isFavEnabledFromUserProf = MvpUserData.getFromMvpObject().isOpenTvWithFavorites();
		Boolean isFavEnabledFromBundle = data.getBoolean(FAVORITES_ENABLED, isFavEnabledFromUserProf);

		MvpPage<MvpMedia> listCategories = (MvpPage<MvpMedia>) mvpChannelsObj.getData();
		if ( listCategories != null && listCategories.size() != 0 ){
			if ( isFavEnabledFromBundle){
				isFavoritesEnabled = true;
				/*set the data to null to re send the request for the favorite channels */
				mvpChannelsObj.setData(null);
			}
		}

	}
	
}
