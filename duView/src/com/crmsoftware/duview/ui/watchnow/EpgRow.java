package com.crmsoftware.duview.ui.watchnow;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************



import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Scroller;

import com.crmsoftware.duview.ui.common.HorizontalAdapterView;
import com.crmsoftware.duview.ui.common.IMvpGenericAdapter;
import com.crmsoftware.duview.ui.common.IMvpGenericListViewListener;
import com.crmsoftware.duview.ui.common.MvpItemHolder;
import com.crmsoftware.duview.utils.Utils;


public class EpgRow extends HorizontalAdapterView implements IMvpGenericListViewListener {


	private Scroller mScroller;
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	public EpgRow(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		mScroller = new Scroller(context);
	}
	
	public void passFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY){
		mScroller.fling(mCurrentX, 0, (int) velocityX, 0, 0, mMaxX, 0, 0);
		requestLayout();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	public int getItemIndex(int screenX, int screenY)
	{
		for (int i= 0; i<getChildCount(); i++)
		{
			View view = getChildAt(i);
			
			if (Utils.isPointWithinView(screenX, screenY, view))
			{
				return mLeftViewIndex + i + 1;
			}

		}	
		
		return -1;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	public View getItem(int position)
	{
		return getChildAt(position-(mLeftViewIndex+1));
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected synchronized void onLayout(boolean changed, int left, int top, int right, int bottom) {

		super.onLayout(changed, left, top, right, bottom);
		if(mAdapter == null)
		{
			
			return;
		}

		mCurrentX = 0;
		mNextX =   MvpEpgScrollHelper.TOTAL_OFFSET - mTotalOffset;
		arangeViews();

	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public int getTotalWidth()
	{
		if (mAdapter == null)
		{
			return 0;
		}
		
		int total = 0;
		
		for (int i=0;i<mAdapter.getCount(); i++)
		{
			total += (int) getItemViewWidth(i);
		}
		
		return total;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onListItemViewClick(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public boolean onListItemViewTouch(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onListItemUpdateView(IMvpGenericAdapter adapter,
			MvpItemHolder holder, int position) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onListItemsCountChanged(IMvpGenericAdapter adapter, int newCount) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int onListItemGetLayoutId(IMvpGenericAdapter adapter, int position) {
		// TODO Auto-generated method stub
		return 0;
	}





}
