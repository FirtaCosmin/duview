package com.crmsoftware.duview.ui.watchnow;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.crmsoftware.duview.configuration.PlatformSettings;
import com.crmsoftware.duview.core.common.DataType;
import com.crmsoftware.duview.core.common.HttpReqParams;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.core.common.TaskGetDataFromStorage;
import com.crmsoftware.duview.core.common.TaskSingleReqCache;
import com.crmsoftware.duview.data.MvpChannelPrograms;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.data.MvpProgram;
import com.crmsoftware.duview.utils.IHttpConst;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;

public class TaskGetPrograms extends Task implements ITaskReturn{
	public static boolean DEBUG = Log.DEBUG; 
	public static final String TAG = TaskGetPrograms.class.getSimpleName();
	
	public static final String OBJPREFIX_GETCHANNEL= "Over.ch.";
	
	private MvpObject mvpReturnData;
	private MvpObject mvpProcData;
	private TaskSingleReqCache  taskGetPrograms = null;
	private long dataLifeTime = 60*60*1000;
	private long timelineWidth;
	private Date date;
	private String channelId;
	SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public TaskGetPrograms(ITaskReturn callback, String channelId, Date date, long timelineWidth) 
	{
		super(callback);
		
		if (DEBUG) Log.m(TAG,this,"ctor()");
		
		this.timelineWidth = timelineWidth;
		this.date = date;
		this.channelId = channelId;

	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	
	@Override
	protected void onResetData() 
	{
		if (DEBUG) Log.m(TAG,this,"resetData");
		
		taskGetPrograms = null;
		mvpProcData = null;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public static String createReference(String channelId, Date date)
	{
		Calendar day = Calendar.getInstance();
		day.setTime(date); 

		
		return Utils.formatLink(
				OBJPREFIX_GETCHANNEL+
				channelId+
				day.get(Calendar.YEAR) + "" +
				day.get(Calendar.MONTH) +""+
				day.get(Calendar.DAY_OF_MONTH)); 
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onExecute() 
	{
		if (DEBUG) Log.m(TAG,this,"onExecute");
		
		mvpReturnData = MvpObject.newReference(createReference(channelId,date));
		mvpProcData = new MvpObject();
		int offset = Calendar.getInstance().get(Calendar.ZONE_OFFSET) + Calendar.getInstance().get(Calendar.DST_OFFSET);

		
		Calendar startTime = Calendar.getInstance();
		startTime.setTime(date); 
		startTime.set(Calendar.HOUR_OF_DAY, 0);
		startTime.set(Calendar.MINUTE, 0);
		startTime.set(Calendar.SECOND, 0);
		startTime.set(Calendar.MILLISECOND, 1);	

		
		Calendar endTime = Calendar.getInstance();
		endTime.setTime(date); 
		endTime.set(Calendar.HOUR_OF_DAY, 23);
		endTime.set(Calendar.MINUTE, 59);
		endTime.set(Calendar.SECOND, 59);
		endTime.set(Calendar.MILLISECOND, 0);	

		String start = dateFormater.format(new Date(startTime.getTimeInMillis() - offset ));
		String end = dateFormater.format(new Date(endTime.getTimeInMillis() - offset));
		
		
		HttpReqParams httpReqParams = new HttpReqParams();	

		httpReqParams.setParam(IHttpConst.PARAM_CHANNEL_ID, channelId);		
		httpReqParams.setParam(IHttpConst.PARAM_TYPE, IHttpConst.VALUE_TYPE_TV);
		httpReqParams.setParam(IHttpConst.PARAM_START_DATE_TIME, start);
		httpReqParams.setParam(IHttpConst.PARAM_END_DATE_TIME, end);
		
		taskGetPrograms = new TaskSingleReqCache(
				MvpChannelPrograms.class,
				PlatformSettings.getInstance().getEpgOverviewUrl(),
				httpReqParams,mvpReturnData, 
				dataLifeTime);
		
		executeSubTask(taskGetPrograms);
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public Object getReturnData() {
		if (DEBUG) Log.m(TAG,this,"getReturnData");
		
		return mvpReturnData;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onReturnFromTask(Task subTask, boolean canceled) {

		super.onReturnFromTask(subTask, canceled);
		
		if (subTask != taskGetPrograms)
		{
			return;
		}
		
		MvpChannelPrograms channel = null;
		if (taskGetPrograms.isError())
		{
			taskCompleted(taskGetPrograms.getError());
			channel = new MvpChannelPrograms();
			
		}
		else
		{
			MvpObject mvpObject = (MvpObject) taskGetPrograms.getReturnData();
			channel = (MvpChannelPrograms) mvpObject.getData();
		}
		
		channel.processPrograms(date);
		channel.generateTimeline(date,timelineWidth);
		
		mvpReturnData.setData(channel);
		
		taskCompleted();
		
	}
}
