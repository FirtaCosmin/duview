package com.crmsoftware.duview.ui.watchnow;

import java.util.Calendar;
import java.util.Map;
import java.util.Observable;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.data.MvpMedia;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.data.MvpParentalManagement;
import com.crmsoftware.duview.data.MvpProgram;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.player.MvpPlayer;
import com.crmsoftware.duview.player.MvpPlayerInfo;
import com.crmsoftware.duview.player.PlayerActivity;
import com.crmsoftware.duview.player.PlayerView;
import com.crmsoftware.duview.ui.common.IMvpGenericAdapter;
import com.crmsoftware.duview.ui.common.MvpImageView;
import com.crmsoftware.duview.ui.common.MvpItemHolder;
import com.crmsoftware.duview.ui.common.PlayerViewPagerFragment;
import com.crmsoftware.duview.utils.CustomDialogView;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.ParentalPinDialogView;
import com.crmsoftware.duview.utils.Utils;

public class ProgramsViewPagerFragment extends PlayerViewPagerFragment{

	public static boolean DEBUG = Log.DEBUG; 
	public static boolean IS_TESTING = false; 
	
	public final static String TAG = ProgramsViewPagerFragment.class.getSimpleName();

	private Map<String,MvpMedia> mapChannels;
	private int timeZoneOffset; 
	private String listDays[];
	private TextView title;
	private MvpUserData authData;
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		if (DEBUG) Log.m(TAG,this,"onCreateView");

		View view = super.onCreateView(inflater, container, savedInstanceState);
		authData = MvpUserData.getFromMvpObject();
		initList(MvpProgram.class);
		
		timeZoneOffset = Calendar.getInstance().get(Calendar.ZONE_OFFSET) + Calendar.getInstance().get(Calendar.DST_OFFSET);
		
        listDays = getResources().getStringArray(R.array.epg_days);
		
		MvpObject mvpChannelsMap = MvpObject.newReference(Utils.formatLink(TaskGetTvChannels.OBJPREFIX_TVCHANNELS_MAP));
		mapChannels = (Map<String,MvpMedia>) mvpChannelsMap.getData();
		
         
		return view;
	}
	
	@Override
	public void onResume() {
		MvpParentalManagement.getInstance().addObserver(this);
		super.onResume();
	}
	
	
	@Override
	public void onPause() {
		MvpParentalManagement.getInstance().deleteObserver(this);
		super.onPause();
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onListItemViewClick(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder) 
	{
		switch (view.getId()) {
	
			
		case R.id.watch_now_button:
		{
			
			if(null!= authData && authData.isAnonymous())
			{
				CustomDialogView customDialogView = CustomDialogView.getInstance();
				customDialogView.show(getFragmentManager(), CustomDialogView.TAG);
			}
			else
			{
			currentPosition = holder.getPosition();
			
			MvpProgram  mvpProgram = (MvpProgram) adapter.getItem(holder.getPosition());
			
			currentViewPlayer = (PlayerView)holder.getView(R.id.player_view);
			currentViewPlayer.attachPlayer();
			if (IS_TESTING)
			{
				currentViewPlayer.play(Environment.getExternalStorageDirectory() + "/A.mp4");
			}
			else
			{
				synchronized (mapChannels) 
				{
					currentViewPlayer.play(mapChannels.get(mvpProgram.getChannelId()));
				}
			}	
			}
		}
		break;
	
		
		case R.id.player_fullscreen:
		{
			Intent intent = new Intent(getActivity(), PlayerActivity.class);
			intent.putExtra(PlayerActivity.LAYOUT_ID, R.layout.player_program);
			
			MvpProgram  mvpProgram = (MvpProgram) adapter.getItem(holder.getPosition());
			
			MvpMedia channel = mapChannels.get(mvpProgram.getChannelId());
			 
			String sImageURL = "";
			if (null != channel)
			{
				sImageURL = channel.getThumbPoster().url;
				
				if (Utils.isEmpty(sImageURL))
				{
					sImageURL = channel.getThumbLandscape().url;
				}
			}
			
			long startTime = mvpProgram.getStartTime() + timeZoneOffset;
			long endTime = mvpProgram.getEndTime() + timeZoneOffset;
			int day = ProgramDetailsView.getDay(startTime, endTime);	
			String sSchedule = Utils.dateToString(startTime,EpgProgramsList.dateFormat) + "-" + Utils.dateToString(endTime,EpgProgramsList.dateFormat);
			
			MvpPlayerInfo info = MvpPlayer.getPlayerInfo();
			
			info.setDuration(listDays[day] + " " + sSchedule);
			info.setTitle(mvpProgram.getTitle());
			info.setChannelImage(sImageURL);
			info.setStart(startTime);
			info.setEnd(endTime);
			
			startActivity(intent);
		}
		break;
		
		default:
			super.onListItemViewClick(adapter,view,holder);
			break;
		}
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onListItemUpdateView(IMvpGenericAdapter adapter,
			MvpItemHolder holder, int position) {
		
		MvpProgram  mvpProgram = (MvpProgram) adapter.getItem(position);
		
		if (null == mvpProgram)
		{
			holder.setVisibility(R.id.item_loading, View.VISIBLE);
			holder.setVisibility(R.id.body_view, View.INVISIBLE);
			return;
		}
		
		
		View view = holder.getView(R.id.player_body_view);
		adapter.receiveClickFrom(R.id.player_body_view);
		
		PlayerView viewPlayerView = (PlayerView)holder.getView(R.id.player_view);
		viewPlayerView.bringToFront();
		viewPlayerView.setPlayerListener(this);
		viewPlayerView.showBorder(false);
		
		if (position == currentPosition)	// tablet rotation
		{
			currentViewPlayer = viewPlayerView;
			//currentViewPlayer.attachPlayer();
			
			holder.setVisibility(R.id.player_item_image_layout, View.INVISIBLE);
			holder.setVisibility(R.id.player_item_details, View.INVISIBLE);
			holder.setVisibility(R.id.player_item_details_placeholder, View.INVISIBLE);
		}
		
		holder.setVisibility(R.id.item_loading, View.INVISIBLE);
		holder.setVisibility(R.id.body_view, View.VISIBLE);
		
		title = (TextView) holder.getView(R.id.item_title);
		holder.setText(R.id.item_title, mvpProgram.getTitle());
		title.setEllipsize(TextUtils.TruncateAt.MARQUEE);
		title.setSingleLine(true);
		title.setMarqueeRepeatLimit(2);
		title.setSelected(true);
		
		holder.setText(R.id.player_item_title, mvpProgram.getTitle());
		
		holder.setText(R.id.item_description, mvpProgram.getShortDescription());
		
		long duration = mvpProgram.getDurationInMin();
		holder.setText(R.id.item_duration_value, duration + " min");	

				
		long startTime = mvpProgram.getStartTime() + timeZoneOffset;
		long endTime = mvpProgram.getEndTime() + timeZoneOffset;
		
		String sSchedule = Utils.dateToString(startTime,EpgProgramsList.dateFormat) + "-" + Utils.dateToString(endTime,EpgProgramsList.dateFormat);
		holder.setText(R.id.item_schedule, sSchedule);	

		
		LiveProgressBarView progress = (LiveProgressBarView) holder.getView(R.id.media_seekbar);
		progress.setData(startTime, endTime);
		
		int day = ProgramDetailsView.getDay(startTime, endTime);
		
		 holder.setText(R.id.item_date_day, listDays[day]);
		 holder.setText(R.id.player_item_description, listDays[day] + " " + sSchedule);
		
		long currentTime = Calendar.getInstance().getTimeInMillis();
		int visibility;
		if (startTime <= currentTime && currentTime <= endTime)
		{
			
			visibility = View.VISIBLE;
		}
		else
		{
			
			visibility = View.INVISIBLE;
		}
		
		if(null != authData && authData.isAnonymous())
		{
			holder.setVisibility(R.id.watch_now_button, View.VISIBLE);
		}
		else
		{
			holder.setVisibility(R.id.watch_now_button, visibility);
		}
		
		
		
		MvpImageView itemImage = (MvpImageView) holder.getView(R.id.item_image); 
		itemImage.setData("");
		itemImage.bringToFront();
		
		Utils.showChannelImage(mapChannels, mvpProgram, (MvpImageView) holder.getView(R.id.channel_logo));
		Utils.showChannelImage(mapChannels, mvpProgram, (MvpImageView) holder.getView(R.id.player_item_image));
		
		 super.onListItemUpdateView(adapter, holder, position);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public int onListItemGetLayoutId(IMvpGenericAdapter adapter, int position) {
		// TODO Auto-generated method stub
		return 	R.layout.program_details;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	protected int getItemLevel(int position) 
	{
		if (null == adapter)
		{
			return super.getItemLevel(position);
		}
		
		MvpProgram  mvpProgram = (MvpProgram) adapter.getItem(position);
		
		if (null == mvpProgram)
		{
			return super.getItemLevel(position);	
		}
		
		MvpMedia mvpMedia = mapChannels.get(mvpProgram.getChannelId());
		
		if (null == mvpMedia)
		{
			return super.getItemLevel(position);	
		}
		
		return mvpMedia.getParentalLevel(); 
	}
	
	
	@Override
	public void onDataUpdate(Observable observable, Object data) 
	{
		if (DEBUG) Log.m(TAG,this,"onDataUpdate");
		
		if (MvpParentalManagement.getInstance() == observable)
		{
			updateParental();
		}
		
		super.onDataUpdate(observable, data);
	}

	
	@Override
	public void onMvpDlgClose(String dlgTag, Bundle data) {
		if (DEBUG) Log.m(TAG,this,"onMvpDlgClose");
		
		int viewId = data.getInt(ParentalPinDialogView.RETURN_STATE);
		
		switch (viewId) 
		{
		case R.id.button_pozitive:
			String pin = data.getString(ParentalPinDialogView.PIN_VALUE);
			if(Utils.isEmpty(pin))
			{
				showToast("Field is empty");
			}
			else
			{
				getMvpActivity().showLoadingDialog();
				MvpParentalManagement.getInstance().checkPin(pin, this);
				ParentalPinDialogView.getInstance().cancelAction();
			}
			break;

		default:
			break;
		}
		
		
		super.onMvpDlgClose(dlgTag, data);
	}
	
	@Override
	public void onReturnFromTask(Task task, boolean canceled) {
		if (DEBUG) Log.m(TAG,this,"onReturnFromTask");
		super.onReturnFromTask(task, canceled);
	}
	
}
