package com.crmsoftware.duview.ui.duapps;
import java.util.Observable;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.configuration.AppSettings;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.data.DuAppsItem;
import com.crmsoftware.duview.data.MvpCategory;
import com.crmsoftware.duview.data.MvpList;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.ui.BrowserFragment;
import com.crmsoftware.duview.ui.MvpFragment;
import com.crmsoftware.duview.ui.common.IMvpGenericAdapter;
import com.crmsoftware.duview.ui.common.IMvpGenericListViewListener;
import com.crmsoftware.duview.ui.common.MvpImageView;
import com.crmsoftware.duview.ui.common.MvpItemHolder;
import com.crmsoftware.duview.ui.common.MvpSimpleListAdapter;
import com.crmsoftware.duview.ui.common.MvpVerticalListView;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;



public class DuAppsFragment extends MvpFragment implements ITaskReturn, IMvpGenericListViewListener {
	public static boolean DEBUG = Log.DEBUG; 
	public static final int TESTUSER = 1; 
	public static final String TAG_DATA= "data" ; 
	
	public final static String TAG = DuAppsFragment.class.getSimpleName();

	private MvpObject mvpDuApps;
	private MvpSimpleListAdapter adapter;
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public static Bundle buildData(String sText) 
	{
		Bundle bundle = new Bundle();
		bundle.putString(TAG_DATA, sText);
		
		return bundle;
	}
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,

			Bundle savedInstanceState) 
	{		
		if (DEBUG) Log.m(TAG,this,"onCreateView");
		
		View view = setContentView(R.layout.duapps_fragment,container,inflater);
		
		viewHolder = getHolder();
		
		getMvpActivity().setMenuEnabled(true);
		getMvpActivity().setActionBarVisibility(View.VISIBLE);
		getMvpActivity().setHomeButtonActionBar(true);
		getMvpActivity().setTitle(getResources().getString(R.string.apps));
		
		
		MvpVerticalListView listView = (MvpVerticalListView) viewHolder.getView(R.id.items_list);
		
		mvpDuApps = MvpObject.newReference(Utils.formatLink(TaskGetDuApps.OBJPREFIX_DUAPPS_UPDATE));
		mvpDuApps.addObserver(this);
				
        adapter = new MvpSimpleListAdapter(MvpCategory.class, this);
        adapter.receiveClickFrom(R.id.body_view);   
        
        listView.setAdapter(adapter);
		
        
		//viewHolder.setVisibility(R.id.item_loading, View.VISIBLE);
		//viewHolder.setVisibility(R.id.items_list, View.INVISIBLE);	
		
		TaskGetDuApps task = new TaskGetDuApps(this,mvpDuApps,-1);	// 1h timelife
		task.execute();
		
		return view;
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onDestroyView() 
	{
		if (DEBUG) Log.m(TAG,this,"onDestroyView");
				
		if (null != mvpDuApps)
		{
			mvpDuApps.deleteObserver(this);
		}
		super.onDestroyView();
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void update(Observable observable, Object data) 
	{
		if (DEBUG) Log.m(TAG,this,"update");
		
		if(observable == mvpDuApps)
		{
			if (null == viewHolder)
			{
				return;
			}
					
			MvpList<DuAppsItem> listItems = (MvpList<DuAppsItem>) mvpDuApps.getData();
			adapter.setCount(listItems.size());
			
			viewHolder.setVisibility(R.id.item_loading, View.INVISIBLE);
			viewHolder.setVisibility(R.id.items_list, View.VISIBLE);		
		}
		
		super.update(observable, data);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onReturnFromTask(Task task, boolean canceled) 
	{
		if (DEBUG) Log.m(TAG,this,"onReturnFromTask");
		
		if (task.isError())
		{
			showToast(task.getError().toString());
		}
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onListItemUpdateView(IMvpGenericAdapter adapter,
			MvpItemHolder holder, int position) 
	{
		if (DEBUG) Log.m(TAG,this,"onListItemUpdateView");
		
		if (position % 2 == 0) 
		{
			holder.getView(R.id.background).setBackgroundResource(R.color.first_item);
		} 
		else
		{
			holder.getView(R.id.background).setBackgroundResource(R.color.second_item);
		}
		
		MvpList<DuAppsItem> listItems = (MvpList<DuAppsItem>) mvpDuApps.getData();
		 
		if (null == listItems)
		{
			return;
		}
		 
		DuAppsItem itemData = listItems.getItem(position);

		MvpImageView viewImage = (MvpImageView)holder.getView(R.id.item_image);
		viewImage.setData(""/*itemData.getImagePath()*/);
		
		holder.setText(R.id.item_title, itemData.getTitle()[AppSettings.LANG_SELECTED]);
		holder.setText(R.id.item_title_arabic, itemData.getTitle()[AppSettings.LANG_ARABIC]);
		holder.setText(R.id.item_description, itemData.getDescription()[AppSettings.LANG_SELECTED]);
		holder.setText(R.id.item_description_arabic, itemData.getDescription()[AppSettings.LANG_ARABIC]);		
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onListItemViewClick(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder) {
		
		switch (view.getId()) 
		{
		case R.id.body_view:
		{
			
			MvpList<DuAppsItem> listItems = (MvpList<DuAppsItem>) mvpDuApps.getData();
			 
			if (null == listItems)
			{
				return;
			}
			DuAppsItem itemData = listItems.getItem(holder.getPosition());
			
			
			
			String url = itemData.getExternalLink();
			/*try to open PlayStore with the external link*/
			try {
			    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url.replace("http://", "market://"))));
			} catch (android.content.ActivityNotFoundException anfe) {
				/*if error received then open the internal brawser.*/
				Bundle bundle = new Bundle();
				bundle.putString(BrowserFragment.URL, url);
				getMvpActivity().goTo(BrowserFragment.class,BrowserFragment.TAG, bundle, true);
			}
			
			Bundle bundle = new Bundle();
			bundle.putString(BrowserFragment.URL, itemData.getExternalLink());
			getMvpActivity().goTo(BrowserFragment.class,BrowserFragment.TAG, bundle, true);
		}
		break;

		default:
			break;
		}
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public boolean onListItemViewTouch(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onListItemsCountChanged(IMvpGenericAdapter adapter, int newCount) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public int onListItemGetLayoutId(IMvpGenericAdapter adapter, int position) {
		// TODO Auto-generated method stub
		return R.layout.duapps_list_row;
	}
}
