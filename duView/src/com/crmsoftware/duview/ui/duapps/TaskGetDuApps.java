package com.crmsoftware.duview.ui.duapps;


import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.graphics.Canvas.EdgeType;

import com.crmsoftware.duview.configuration.PlatformSettings;
import com.crmsoftware.duview.core.common.HttpReqParams;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.core.common.TaskSingleReqCache;
import com.crmsoftware.duview.data.DuAppsItem;
import com.crmsoftware.duview.data.DuAppsUpdateReq;
import com.crmsoftware.duview.data.DuAppsUpdateResp;
import com.crmsoftware.duview.data.MvpError;
import com.crmsoftware.duview.data.MvpError.ErrorType;
import com.crmsoftware.duview.data.MvpList;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.utils.IHttpConst;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;

public class TaskGetDuApps extends Task {

	public static boolean DEBUG = Log.DEBUG; 
	public final static String TAG = TaskGetDuApps.class.getSimpleName();

	public static final String OBJPREFIX_DUAPPS_UPDATE= "duapps.update";
	public static final String OBJPREFIX_DUAPPS_INTERNAL= "<internal>";
	
	public final static String VALUE_PLATFORM = "ANDROID";
	public final static String VALUE_SECTION = "APP_DETAIL";
	
	private TaskSingleReqCache  taskAppsRequest = null;
	private MvpObject returnData;
	private MvpObject procData;
	private int lifeTime;
	
	public TaskGetDuApps(ITaskReturn callback, MvpObject returnData, int lifeTime) 
	{
		super(callback);
		
		if (DEBUG) Log.m(TAG,this,"ctor()");
		
		this.returnData = returnData;
		this.lifeTime = lifeTime;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public Object getReturnData() {

		return returnData;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onExecute() 
	{
		if (DEBUG) Log.m(TAG,this,"onExecute");
        
		PlatformSettings platfSett = PlatformSettings.getInstance();
		HttpReqParams httpParams = new HttpReqParams();
		
		DuAppsUpdateReq request = new DuAppsUpdateReq();
		request.setPlatform(VALUE_PLATFORM);
		request.setSection(VALUE_SECTION);
		
		Calendar calendar = Calendar.getInstance();
		//request.setUpdatedate(Utils.dateToString(calendar.getTimeInMillis(),new SimpleDateFormat("yyyy-MM-dd")));
		request.setUpdatedate("2014-06-26");
		
		httpParams.setPostData(request.toJsonString());
		
		httpParams.setProperty(IHttpConst.PROP_CONTENT_TYPE, IHttpConst.VALUE_APPJSON);
		int length = httpParams.getPostData().getBytes().length;
		httpParams.setProperty(IHttpConst.PROP_CONTENT_LENGTH, ""+length);
		
		MvpObject procData = MvpObject.newReference(OBJPREFIX_DUAPPS_INTERNAL);
		taskAppsRequest = new TaskSingleReqCache (
				this,
				DuAppsUpdateResp.class,
				PlatformSettings.getInstance().getDuAppsUpdateUrl(), 
				httpParams,
				procData,
				lifeTime);
		
		executeSubTask(taskAppsRequest);	
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onResetData()
	{
		super.onResetData();
		
		taskAppsRequest = null;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onReturnFromTask(Task subTask, boolean canceled) 
	{
		
		if (subTask == taskAppsRequest)
		{
			if (taskAppsRequest.isError())
			{
				taskCompleted(taskAppsRequest.getError());
				return;
			}
			
			MvpObject mvpObject = (MvpObject)subTask.getReturnData();
			
			if (null == mvpObject)
			{
				taskCompleted(new MvpError(ErrorType.Processing));
				return;				
			}
			
			//filtering the list
			DuAppsUpdateResp respUpdate = (DuAppsUpdateResp) mvpObject.getData();
			MvpList<DuAppsItem> listApps = respUpdate.getListApps();
			for (int i=listApps.size() -1; i>=0 ;i--)
			{
				DuAppsItem item = listApps.getItem(i);
				if (item.getIsActive() != 1)
				{
					listApps.remove(i);
				}
			}
			
			MvpObject.removeReference(mvpObject.getId());

			returnData.setData(listApps);
			taskCompleted();
		}
		
		
	}
}
