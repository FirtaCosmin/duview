package com.crmsoftware.duview.ui.apphelp;

import java.io.IOException;
import java.io.InputStream;

import android.app.Dialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.data.AppHelpPageData;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.data.MvpList;
import com.crmsoftware.duview.ui.MvpFragment;
import com.crmsoftware.duview.ui.common.IMvpGenericAdapter;
import com.crmsoftware.duview.ui.common.IMvpGenericListViewListener;
import com.crmsoftware.duview.ui.common.MvpItemHolder;
import com.crmsoftware.duview.ui.common.MvpPagerAdapter;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.ui.common.MvpViewPager;
import com.crmsoftware.duview.utils.Log;

public class AppHelpFragment extends MvpFragment implements IMvpGenericListViewListener, OnPageChangeListener {

	public static boolean DEBUG = Log.DEBUG;
	public final static String TAG = AppHelpFragment.class.getSimpleName();
	
	public final static String DATA_STARTPAGE = "DATA_STARTPAGE";
	
	public static boolean tempDisabled = false;
	
	private Dialog dialog;
	private MvpList<AppHelpPageData> listItems = new MvpList<AppHelpPageData>(AppHelpPageData.class);
	private MvpPagerAdapter adapter;
	private MvpViewHolder viewHolder;
	
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public View onCreateView(final LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) 
	{
		super.onCreateView(inflater, container, savedInstanceState);
		
		if (DEBUG) Log.m(TAG, this, "onCreateView");
		
		View view = setContentView(R.layout.apphelp_fragment, container, inflater);
		
		getMvpActivity().setMenuEnabled(false);
		getMvpActivity().setActionBarVisibility(View.GONE);
		setHasOptionsMenu(true);
		

		//initDialog();
		viewHolder = getHolder();
		
		viewHolder.setClickListener(this, R.id.btn_done);
		viewHolder.setClickListener(this, R.id.dont_show_check);

		buildList();
		
		
		adapter = new MvpPagerAdapter(
				 			AppHelpPageData.class, 
				 			this);
		 
		 MvpViewPager pagerView = (MvpViewPager) viewHolder.getView(R.id.items_list);
		 pagerView.setAdapter(adapter);
		 pagerView.setOnPageChangeListener(this);
		 
		 adapter.setCount(listItems.size());
		 
		 updateCounter(0);
		 
		return view;
	}
	
	
	@Override
	protected void finalize() throws Throwable 
	{
		Log.d("","finalized " + TAG);
		
		super.finalize();
	}
	
	@Override
	public void onDestroyView() {
		super.onDestroyView();
		tempDisabled = true;
		getMvpActivity().setActionBarVisibility(View.VISIBLE);
		getMvpActivity().setMenuEnabled(true);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void buildList() 
	{
		listItems.add(new AppHelpPageData("home-search-menu.png", R.string.help_home_search_menu));//3
		listItems.add(new AppHelpPageData("menu_slide.png", R.string.help_menu));//4
		listItems.add(new AppHelpPageData("home-filters.png", R.string.help_filters));//2
		listItems.add(new AppHelpPageData("home-featured.png", R.string.help_home_featured));//1
		listItems.add(new AppHelpPageData("seeall.png", R.string.help_seeall_navigation));//5
		listItems.add(new AppHelpPageData("product-detail.png", R.string.help_product_detail));//9
		listItems.add(new AppHelpPageData("player_maximized.png", R.string.help_player_maximized));//10
		listItems.add(new AppHelpPageData("epg_navigation.png", R.string.help_epg_navigation));//6
		listItems.add(new AppHelpPageData("epg-click.png", R.string.help_epg_click));//7
		listItems.add(new AppHelpPageData("player-minimized.png", R.string.help_player_minimized));//8
		
		
	}
	
	@Override
	public boolean onBackPressed() 
	{
		getMvpActivity().hide(AppHelpFragment.TAG);
		return false;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onListItemViewClick(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder) 
	{

		
	}
	
	@Override
	public void onResetData() {
		
		getMvpActivity().hide(TAG);
		
		super.onResetData();
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public boolean onListItemViewTouch(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onListItemUpdateView(IMvpGenericAdapter adapter,
			MvpItemHolder holder, int position) {

		AppHelpPageData item = listItems.getItem(position);
		
		holder.setText(R.id.item_description, item.getDescriptionId());
		
		
		try 
		{
	        InputStream ims = (InputStream) getActivity().getAssets().open("help/"+item.getImageFile());
			
	        // load image as Drawable
	        Drawable d = Drawable.createFromStream(ims, null);
	        
	        // set image to ImageView
	        holder.setImage(R.id.item_image, d);
		} 
		catch (IOException e) 
		{

		}
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onListItemsCountChanged(IMvpGenericAdapter adapter, int newCount) {
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public static Bundle getConfig(int startPage)
	{
		Bundle bundle = new Bundle();
		bundle.putInt(DATA_STARTPAGE, startPage);
		
		return bundle;
	}



	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_done:	
			
			tempDisabled = true;
			getMvpActivity().hide(TAG);
			
			break;
			
		case R.id.dont_show_check:	
			
			MvpUserData data = MvpUserData.getFromMvpObject();
			
			boolean state = viewHolder.getCheckState(R.id.dont_show_check) <=0 ? false : true;
			
			data.setHelp(state == false);
			
			
			break;
		default:
			break;
		}
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public int onListItemGetLayoutId(IMvpGenericAdapter adapter, int position) 
	{
		return R.layout.apphelp_page;
	}
	
	private void updateCounter(final int position)
	{
		
		new Handler().post(new Runnable() {
			



			@Override
			public void run() 
			{
				
		        String sCount = String.format(MainApp.getRes().getString(R.string.medial_list_position_information), position+1, adapter.getCount()).toLowerCase();
		        viewHolder.setText(R.id.current_position, sCount +" ");

			}
		});
	}


	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onPageSelected(int arg0) {
		updateCounter(arg0);
		
	}

}