package com.crmsoftware.duview.ui.common;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************

import java.util.LinkedList;
import java.util.Queue;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.utils.Log;

public class MvpPagPagerAdapter<TemplateType> extends MvpPagerAdapter {
	public static boolean DEBUG = Log.DEBUG; 
	public static boolean INFO = true; 
	public final static String TAG = MvpPagPagerAdapter.class.getSimpleName();
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected MvpAdapterCore adapterCore;
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	
	MvpPagContext<TemplateType> paginationContext;
	MvpObject mvpObjContext;
	Queue<View> viewQueue = new LinkedList<View>();

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public MvpPagPagerAdapter(Class  genericType) 
	{		
		super(genericType);
		
	}
	



		
		/**
		 * 
		 *
		 * @param  
		 * @return      
		 * @see         
		 */	
		@Override
		protected void finalize() throws Throwable {
			paginationContext.cancelAllRequests();
			paginationContext = null;
			super.finalize();
		}


	
		
		@Override
		public Object getItem(int position) {

			return paginationContext.getItem(position);
		}
		


		/**
		 * 
		 *
		 * @param  
		 * @return      
		 * @see         
		 */	
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return paginationContext.getCount();
		}


		/**
		 * 
		 *
		 * @param  
		 * @return      
		 * @see         
		 */	
		@Override
		public MvpObject getPaginationContext() 
		{
			return mvpObjContext;
			
		}	
		
		/**
		 * 
		 *
		 * @param  
		 * @return      
		 * @see         
		 */		
		public void setPaginationContext(MvpObject mvpObjContext) 
		{
			this.mvpObjContext = mvpObjContext;
			
			paginationContext = (MvpPagContext) mvpObjContext.getData();
			
			if (null == paginationContext)
			{
				return;
			}
			
			paginationContext.bindAdapter(this);

			int count = paginationContext.getCount();
			setCount(count);
			if (count == -1)
			{
				paginationContext.newPage(0);
			}
		}

}
