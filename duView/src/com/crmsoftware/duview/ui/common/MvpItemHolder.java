package com.crmsoftware.duview.ui.common;
import java.util.List;

import com.crmsoftware.duview.data.MvpTag;

import android.content.Context;
import android.graphics.Typeface;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;

public class MvpItemHolder extends MvpViewHolder
{
	public static final String TAG = MvpItemHolder.class.getSimpleName();
	
	public static Typeface typefaceNormal = null;
	public static Typeface typefaceBold = null;
	
	static String hour =null;
	static String min = null;
	View viewParent;
	int itemPosition;
	int layoutId;
	
	MvpTag mvpTag;
	
	SparseArray<View> viewList = new SparseArray<View>();
	Context context;
	
	public MvpItemHolder(View viewParent, Integer itemPosition, List<Integer> resIds, int layoutId)
	{	
		super(viewParent, resIds);
		
		this.layoutId = layoutId;
		update(viewParent,itemPosition);
	}
	

	public void update(View viewParent, Integer itemPosition)
	{
		this.viewParent = viewParent;
		this.itemPosition = itemPosition;
	}
	
	public int getPosition()
	{
		return itemPosition;
	}
	
	
	public int getLayoutId()
	{
		return layoutId;
	}
	
	public MvpTag getTag()
	{
		return mvpTag;
	}
	
	public MvpTag setTag()
	{
		return mvpTag;
	}
	


}
