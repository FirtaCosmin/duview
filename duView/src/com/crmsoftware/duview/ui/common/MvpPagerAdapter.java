package com.crmsoftware.duview.ui.common;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************

import java.util.LinkedList;
import java.util.Queue;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.utils.Log;

public class MvpPagerAdapter<TemplateType> extends PagerAdapter implements IMvpGenericAdapter {
	public static boolean DEBUG = Log.DEBUG; 
	public static boolean INFO = true; 
	public final static String TAG = MvpPagPagerAdapter.class.getSimpleName();
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected MvpAdapterCore adapterCore;
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	
	MvpPagContext<TemplateType> paginationContext;
	Queue<View> viewQueue = new LinkedList<View>();
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public MvpPagerAdapter(
			Class  genericType) 
	{	
		adapterCore = new MvpAdapterCore(this,genericType);

	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	public MvpPagerAdapter(
			Class  genericType,
			Object parent) 
	{	
		adapterCore = new MvpAdapterCore(this, genericType, parent);

	}
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public boolean isViewFromObject(View view, Object object) 
	{
		if (DEBUG) Log.m(TAG,this,"isViewFromObject");
		
		   return view == object;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public Object instantiateItem(ViewGroup container, int position) 
	{
		if (DEBUG) Log.m(TAG,this,"instantiateItem");	
		
		View view = adapterCore.getView(position, viewQueue.poll(), container);;
		if (null == view)
		{
			view = adapterCore.getView(position, null, container);
		}
		
		ViewParent parent = view.getParent();
		if (parent != null)
		{
			((ViewGroup)parent).removeView(view);
		}
		container.addView(view);
	      
		//view.setBackgroundResource(R.drawable.general_background);

		
		adapterCore.updateView(view);	
		return view;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	  @Override
	  public void destroyItem(ViewGroup container, int position, Object object) 
	  {
		 if (DEBUG) Log.m(TAG,this,"destroyItem");	
		 

			 View view = (View)object;
			 container.removeView(view); 
			 viewQueue.add(view);
		 	
	  }


		
		/**
		 * 
		 *
		 * @param  
		 * @return      
		 * @see         
		 */	
		@Override
		protected void finalize() throws Throwable {

			super.finalize();
		}


		/**
		 * 
		 *
		 * @param  
		 * @return      
		 * @see         
		 */	
		@Override
		public void setCount(Integer totalNbOfItems) 
		{
			adapterCore.setCount(totalNbOfItems);
		}
		
		
		/**
		 * 
		 *
		 * @param  
		 * @return      
		 * @see         
		 */		
		@Override
		public Class getType()
		{
			return adapterCore.getType();
		}
		
		/**
		 * 
		 *
		 * @param  
		 * @return      
		 * @see         
		 */	
		
		@Override
		public Object getItem(int position) {

			return paginationContext.getItem(position);
		}
		
		/**
		 * 
		 *
		 * @param  
		 * @return      
		 * @see         
		 */	
		@Override
		public void notifyDataSetChange() {
			super.notifyDataSetChanged();
		}

		/**
		 * 
		 *
		 * @param  
		 * @return      
		 * @see         
		 */	
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return adapterCore.getCount();
		}

		/**
		 * 
		 *
		 * @param  
		 * @return      
		 * @see         
		 */	
		@Override
		public void setGenericListViewListener(
				IMvpGenericListViewListener callback) 
		{
			adapterCore.setGenericListViewListener(callback);
			
		}

		/**
		 * 
		 *
		 * @param  
		 * @return      
		 * @see         
		 */	
		@Override
		public void receiveClickFrom(int... resIds) {
			adapterCore.receiveClickFrom(resIds);
			
		}

		/**
		 * 
		 *
		 * @param  
		 * @return      
		 * @see         
		 */	
		@Override
		public void receiveTouchFrom(int... resIds) {
			adapterCore.receiveTouchFrom(resIds);
			
		}

		/**
		 * 
		 *
		 * @param  
		 * @return      
		 * @see         
		 */	
		@Override
		public MvpObject getPaginationContext() 
		{
			return null;
			
		}
		
		
		@Override
		public Object getTag() {
			return adapterCore.getTag();
		}


		@Override
		public void setTag(Object data) 
		{
			adapterCore.setTag(data);	
		}


		@Override
		public void addView(MvpItemHolder itemHolder, View view) {
			adapterCore.addView( itemHolder,  view);
			
		}
		
		

}
