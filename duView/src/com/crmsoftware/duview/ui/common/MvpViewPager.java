package com.crmsoftware.duview.ui.common;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;

public class MvpViewPager extends ViewPager {
	/**
	 * 
	 * @desc interface used by objects that want to be announced about any page change event 
	 */
	public interface PageChangeListener{
		/**
		 * @param pager
		 * @param position
		 * @desc callback for the pageChange Event
		 */
		public void pageChanged(MvpViewPager pager, int position);
	}
	
	
	/**
	 * @desc the number of seconds after which the pager will change the current page.
	 */
	private static final int AUTOMATIC_PAGE_CHANGE_PERIOD = 10;
	/**
	 * @desc the number of seconds after which the pager will start changing the current page
	 */
	private static final int AUTOMATIC_PAGE_CHANGE_DELAY = 10;
	private boolean automaticPageChange = false;
	private Timer automaticPageChangeTimer;
	private boolean attachedToWindow = false;
	private boolean timerstarted = false;
	
	public static boolean DEBUG = Log.DEBUG ; 
	public static boolean VERBOUSE = Log.VERBOUSE ; 
	public static final String TAG = MvpViewPager.class.getSimpleName();

	public static int PAGESIZE = 5;
	private int pageRange[] = {-1,-1};
	/**
	 * @desc list that contains the PageChangeListener objects registered to listen 
	 * 		 for the changing of the main page
	 */
	private ArrayList<PageChangeListener> pageChangeListeners = new ArrayList<MvpViewPager.PageChangeListener>();
	

	
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public MvpViewPager(Context context) {
		super(context);
		init();
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public MvpViewPager(Context context, AttributeSet attrset) {
		super(context,attrset);
		init();
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {

		super.onLayout(changed, l, t, r, b);
		
		PagerAdapter adapter = getAdapter();
		
		if (adapter instanceof IMvpGenericAdapter)
		{
			int currentItem = getCurrentItem();
			int firstItem = currentItem - PAGESIZE;
			int lastItem = currentItem + PAGESIZE;
			
			if (firstItem <0 )
			{
				firstItem = 0;
			}
			
			int count = getAdapter().getCount();
			if (lastItem >= count)
			{
				lastItem = count -1;
			}
			
			Utils.checkPageRange((IMvpGenericAdapter)getAdapter(), PAGESIZE, firstItem, lastItem, pageRange, TAG, VERBOUSE);
		}	
	}
	
	/**
	 * @desc using this callback to know when the view is attached to window.
	 *       will not use isAttachedToWindow() because it is new starting with API 19.
	 */
	protected void onAttachedToWindow (){
		super.onAttachedToWindow();
		attachedToWindow = true;
		if ( automaticPageChange ){
			/*if the timer needs to start*/
			/*start it :) */
			startAutomaticPageChange();
		}
	}
	
	protected void onDetachedFromWindow(){
		super.onDetachedFromWindow();
		stopAutomaticPageChange();
	}
	/**
	 * @desc will start the automatic page change timer.
	 * 		 can be stopped with stopAnimation()
	 * @param willHaveAutomaticPageChange
	 */
	public void setAutomaticPageChange(boolean willHaveAutomaticPageChange){
		/*if the new state is different of the one saved*/
		if ( automaticPageChange != willHaveAutomaticPageChange ){
			automaticPageChange = willHaveAutomaticPageChange;
			if (automaticPageChange){
				if ( attachedToWindow ){
					/*if the pager is attached to the windows then the timer page change can start.*/
					startAutomaticPageChange();
				}
				/*
				 * if the pager is not attached then the timer will start when 
				 * the pager will be attached
				 * */
			}else{
				/*stop the timer*/
				stopAutomaticPageChange();
			}
		}
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void setAdapter(PagerAdapter arg0) 
	{
		super.setAdapter(arg0);
	}

	/**
	 * @param listener
	 * @desc will add the passed listener to the listener list
	 */
	public void setPageChangeListener(PageChangeListener listener){
		if ( listener != null ){
			pageChangeListeners.add(listener);
		}
		
	}
	
	
	/**
	 * @desc will make the initial configurations 
	 */
	private void init(){

		/*set the page listener to forward the events to the PageChangeListeners*/
		setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int position) {
				anouncePageChange(position);
			}
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {}
			@Override
			public void onPageScrollStateChanged(int arg0) {}
		});
		/*init the automatic page change timer*/
		automaticPageChangeTimer = new Timer();
	}
	
	
	/**
	 * @param position
	 * @desc will announce all the listeners in the list that the
	 * 		 main page has been changed.
	 */
	private void anouncePageChange(int position){
		for ( PageChangeListener list : pageChangeListeners ){
			list.pageChanged(this, position);
		}
	}
	
	/**
	 * @desc will make the pager to go to the next page. If the last page is hit 
	 *       then it will go to the start.
	 */
	private void changeToNextPage(){
		int currentPos = getCurrentItem();
		int itemCount = getAdapter().getCount();
		if ( currentPos == itemCount - 1 ){
			setCurrentItem(0, true);
		}else{
			setCurrentItem(++currentPos, true);
		}
			
	}
	
	/**
	 * @desc will start a timer that will automatically change the current page 
	 */
	private void startAutomaticPageChange(){
		/*in case that the timer has not started yet start it*/
		if ( !timerstarted ){
			timerstarted = true;
			automaticPageChangeTimer.scheduleAtFixedRate(new TimerTask() {
				
				@Override
				public void run() {
					Handler handler = new Handler(Looper.getMainLooper());
					handler.post(new Runnable() {
					     public void run() {
								changeToNextPage();				     
						}
					});
				}
			}, AUTOMATIC_PAGE_CHANGE_DELAY*1000, AUTOMATIC_PAGE_CHANGE_PERIOD*1000);
		}
		/*if the timer is started then do nothing*/
	
	}

	/**
	 * @desc will stop the timer
	 */
	private void stopAutomaticPageChange(){
		automaticPageChangeTimer.cancel();
		timerstarted = false;
	}
}
