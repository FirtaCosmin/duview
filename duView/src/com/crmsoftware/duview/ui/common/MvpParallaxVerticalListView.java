package com.crmsoftware.duview.ui.common;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListAdapter;

import com.crmsoftware.duview.ui.common.parallax.ParallaxListView;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;

public class MvpParallaxVerticalListView extends ParallaxListView {

	public static boolean DEBUG = Log.DEBUG ; 
	public static boolean VERBOUSE = Log.VERBOUSE ; 
	
	public static final String TAG = MvpVerticalListView.class.getSimpleName();
	
	private int pageRange[] = {-1,-1};
	public static int PAGESIZE = 10;

	
	public MvpParallaxVerticalListView(Context context, AttributeSet attrset) {
		super(context,attrset);

	}
	
	public MvpParallaxVerticalListView(Context context,AttributeSet attrset, int defStyle) {
		super(context,attrset,defStyle);

	}
	
	@Override
	protected void onDetachedFromWindow() {
		if (VERBOUSE) Log.m(TAG,this,"onDetachedFromWindow");
		
		super.onDetachedFromWindow();
	}
	

	
	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		// TODO Auto-generated method stub
		super.onScrollChanged(l, t, oldl, oldt);
		
		ListAdapter adapter = getAdapter();
		
		if (adapter instanceof IMvpGenericAdapter)
		{
			Utils.checkPageRange((IMvpGenericAdapter)getAdapter(), PAGESIZE, getFirstVisiblePosition(), getLastVisiblePosition(),pageRange, TAG, VERBOUSE);
		}
	}

}
