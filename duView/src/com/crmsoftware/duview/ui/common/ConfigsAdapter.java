package com.crmsoftware.duview.ui.common;

import java.util.Vector;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.crmsoftware.duview.data.MvpObject;

public class ConfigsAdapter extends MvpSimpleListAdapter {
	public static final String TAG = ConfigsAdapter.class.getSimpleName();

	
	
	protected Vector<String> model;
	
	
	public ConfigsAdapter(
			Class  genericType,
			Object parent) 
	{	
		super( genericType,parent);
		model = new Vector<String>();
		model.add("1");
		model.add("2");
		model.add("3");
		model.add("4");
		model.add("5");
		model.add("6");
		model.add("7");
		model.add("8");
		model.add("9");
		model.add("10");
		model.add("11");
		model.add("12");
		model.add("13");
		
		setCount(model.size());
		
	}
	
	/*
	 * Implementation of IMvpGenericAdapter
	 * */

	@Override
	public Object getItem(int position) {
		return model.get(position);
	}
	
	
	
	/*
	 * 
	 * PRIVATE SECTION
	 * 
	 * */

	
	/**
	 * @desc will create all the config items and will use them to 
	 * populate the config item list
	 */
	private void createConfigItems(){
		
	}

}
