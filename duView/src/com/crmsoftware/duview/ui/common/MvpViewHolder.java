package com.crmsoftware.duview.ui.common;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewPager;
import android.text.TextWatcher;
import android.util.SparseArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;

import com.crmsoftware.duview.data.MvpObject;


public class MvpViewHolder
{
	public static Typeface typefaceNormal = null;
	public static Typeface typefaceBold = null;
	public static Typeface typefaceLight = null;
	
	private Context context;

	private Resources resources;
	
	public static String hour =null;
	public static String min = null;
	private View viewParent;
	
	private SparseArray<View> viewList = new SparseArray<View>();

	public void setAppContext (Context context) 
	{ 
		this.context = context;
		this.resources = context.getResources(); 
	};

	
	public MvpViewHolder(View viewParent)
	{	
		setAppContext(viewParent.getContext());
			
		if (null == viewParent)
		{
			return;
		}
		
		init(viewParent);
		
		grabAllViewsWithId(viewParent);
		

	}
	
	public MvpViewHolder(View viewParent, List<Integer> resIds)
	{	
		setAppContext(viewParent.getContext());

		init(viewParent);
		
		grabAllViewsWithId(viewParent);
	}
	
	
	public void init(View viewParent)
	{	
		this.viewParent = viewParent;
		
		
		if (null == typefaceNormal)
		{
			typefaceNormal = Typeface.createFromAsset(context.getAssets(),
					"fonts/DuCoHeadlineArabic_Rg.ttf");	
			
			if (null == typefaceNormal)
			{
				Exception exception =  new Exception("can not find fonts/DuCoHeadlineArabic_Rg.ttf");
				exception.printStackTrace();
			}
		}
		
		if (null == typefaceBold)
		{
			typefaceBold = Typeface.createFromAsset(context.getAssets(),
					"fonts/DuCoHeadlineArabic_Bd.ttf");	
			
			if (null == typefaceBold)
			{
				Exception exception =  new Exception("can not find fonts/DuCoHeadlineArabic_Bd.ttf");
				exception.printStackTrace();
				
				typefaceBold = typefaceNormal;
			}
		}
		
		if (null == typefaceLight)
		{
			typefaceLight = Typeface.createFromAsset(context.getAssets(),
					"fonts/DuCoHeadlineArabic_Lt.ttf");	
			
			if (null == typefaceLight)
			{
				Exception exception =  new Exception("can not find fonts/DuCoHeadlineArabic_Lt.ttf");
				exception.printStackTrace();
				
				typefaceLight = typefaceNormal;
			}
		}
		
		

	}
	
	
	/**
	 * scan in deep a view childrens and keeps their IDs in the list for later use with ItemHolder
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return  Object    
	 * @see         
	 */		
	public  void grabAllViewsWithId(View baseView)
	{
		if (baseView instanceof TextView)
		{
			setFont((TextView)baseView);
		}
		
		if (baseView instanceof EditText)
		{
			setFont((EditText)baseView);
		}
	
		if (baseView instanceof ViewGroup)
		{
			ViewGroup viewGroup = viewGroup = (ViewGroup) baseView;
			/*try
			{
				viewGroup = (ViewGroup) baseView;
			}
			catch (Exception e)
			{
				
			}
			
			if (null == viewGroup)
			{
				return;
			}*/
			
			for(int i=0; i<viewGroup.getChildCount();i++) 
			{
				View view = viewGroup.getChildAt(i);
	
				grabAllViewsWithId(view);
			}
		}
		
		int id = baseView.getId();
		
		if (id != View.NO_ID)
		{
			baseView.setTag(this);
			viewList.put(id, baseView);
		}
	}
	
	
	/**
	 * scan in deep a view childrens and keeps their IDs in the list for later use with ItemHolder
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return  Object    
	 * @see         
	 */		
	public static void grabAllViewsWithId(View baseView, List<Integer> listResIds)
	{
		
		int id = baseView.getId();
		
		if (id != View.NO_ID)
		{
			listResIds.add(id);
		}
		
		ViewGroup viewGroup = null;
		try
		{
			viewGroup = (ViewGroup) baseView;
		}
		catch (Exception e)
		{
			
		}
		
		if (baseView instanceof TextView)
		{
			setFont((TextView)baseView);
		}
		
		if (baseView instanceof EditText)
		{
			setFont((EditText)baseView);
		}
		
		if (null == viewGroup)
		{
			return;
		}
		
		for(int i=0; i<viewGroup.getChildCount();i++) 
		{
			View view = viewGroup.getChildAt(i);

			grabAllViewsWithId(view, listResIds);
    
		}
	}
	
	
	
	public static void setFont(TextView view)
	{
		if(false == (view instanceof TextView) )
		{
			return;
		}
		
		int style = Typeface.NORMAL;
		Typeface typeface = view.getTypeface();
		
		if (null != typeface)
		{
			style = typeface.getStyle();
		}
		
		switch (style)
		{
			case Typeface.BOLD:
				
				view.setTypeface(typefaceBold);
				
			break;
				
				
			case Typeface.BOLD_ITALIC:
			
				view.setTypeface(typefaceBold);
				
			break;
			
			case Typeface.ITALIC:		
				view.setTypeface(typefaceLight);
			break;
			
			case Typeface.NORMAL:
			default:
				
				view.setTypeface(typefaceNormal);
				
			break;
				
			
		}
			
	}
	
	public static void setFont(EditText view)
	{
		if(false == (view instanceof EditText) )
		{
			return;
		}
		
		
		int style = Typeface.NORMAL;
		Typeface typeface = view.getTypeface();
		
		if (null != typeface)
		{
			style = typeface.getStyle();
		}
		
		switch (style)
		{
			case Typeface.BOLD:
				
				view.setTypeface(typefaceBold);
				
			break;
				
				
			case Typeface.BOLD_ITALIC:
			
				view.setTypeface(typefaceBold);
				
			break;
			
			case Typeface.ITALIC:		
			case Typeface.NORMAL:
			default:
				
				view.setTypeface(typefaceNormal);
				
			break;
				
			
		}
			
	}
	
	public void setClickListener(OnClickListener listener, int... resIds)
	{		
		if (null == resIds)
		{
			return;
		}
		
		for(int resId : resIds)
		{
			View view = viewList.get(resId);
			if (null != view)
			{
				view.setOnClickListener(listener);
			}
		}
	}
	
	public void addTextChangedListener(TextWatcher listener, int... resIds)
	{		
		if (null == resIds)
		{
			return;
		}
		
		for(int resId : resIds)
		{
			View view = viewList.get(resId);
			if (null != view && view instanceof EditText)
			{
				((EditText)view).addTextChangedListener(listener);
			}
		}
	}

	
	public void setClickListener(OnClickListener listener, List<Integer> resIds)
	{				
		if (null == resIds)
		{
			return;
		}
		
		for(int resId : resIds)
		{
			View view = viewList.get(resId);
			if (null != view)
			{
				view.setOnClickListener(listener);
			}
		}
	}
	
	public void setTouchListener(OnTouchListener listener, int... resIds)
	{
		if (null == resIds)
		{
			return;
		}
		
		for(int resId : resIds)
		{
			View view = viewList.get(resId);
			if (null != view)
			{
				view.setOnTouchListener(listener);
			}	
		}
	}
	
	public void setTouchListener(OnTouchListener listener, List<Integer> resIds)
	{				
		if (null == resIds)
		{
			return;
		}
		
		for(int resId : resIds)
		{
			View view = viewList.get(resId);
			if (null != view)
			{
				view.setOnTouchListener(listener);
			}	
		}
	}
	
	public void setOnRatingBarChangeListener(OnRatingBarChangeListener listener, int... resIds)
	{
		if (null == resIds)
		{
			return;
		}
		
		for(int resId : resIds)
		{
			View view = viewList.get(resId);
			if (null != view && view instanceof RatingBar)
			{
				((RatingBar)view).setOnRatingBarChangeListener(listener);
			}
		}		
	}
	
	public View getView()
	{
		return viewParent;
	}

	public View getView(int childResId)
	{
		return viewList.get(childResId);
	}
	
	public ImageView getViewAsImage(int resId)
	{
		View view= viewList.get(resId);
		if (view instanceof ImageView)
		{
			return (ImageView)view;
		}
		return null;
	}
	
	public ListView getViewAsListView(int resId)
	{
		View view= viewList.get(resId);
		if (view instanceof ImageView)
		{
			return (ListView)view;
		}
		return null;
	}
	
	public void setVisibility(int viewResId, int visibility)
	{
		View view = getView(viewResId);
		if (null == view)
		{
			return;
		}

		view.setVisibility(visibility);			
	}
	
	public void setEnabled(int viewResId, boolean enabled)
	{
		View view = getView(viewResId);
		if (null == view)
		{
			return;
		}

		view.setEnabled(enabled);
	}
	
	public String getText(int viewResId)
	{
		View view = getView(viewResId);
		if (null == view)
		{
			return null;
		}
		
		if (view instanceof TextView)
		{
			return ((TextView)view).getText().toString();
		}
		
		if (view instanceof EditText)
		{
			return ((EditText)view).getText().toString();
		}
		
		return null;
	}
	
	public int getCheckState(int viewResId)
	{
		View view = getView(viewResId);
		if (null == view)
		{
			return -1;
		}
		
		if (view instanceof CompoundButton)
		{
			return ((CompoundButton)view).isChecked() ? 1 : 0;
		}

		return -1;
	}
	
	
	public void setCheckState(int viewResId, boolean state)
	{
		View view = getView(viewResId);
		if (null == view)
		{
			return;
		}
		
		if (view instanceof CompoundButton)
		{
			((CompoundButton)view).setChecked(state);
		}

	}
	
	public void setText(int viewResId, String text)
	{
		setTextWithVisiblityIfEmpty(viewResId,text,View.INVISIBLE);
	}
	
	public void setTextFromStringTemplate(int viewResId, int stringResId, Object... values) 
	{
		String text = String.format(resources.getString(stringResId), values);
		setText(viewResId,text);
	}
	
	public void setTextWithResIdIfEmpty(int viewResId, String text, int resIdIfEmpty) 
	{
		if (null == text)
		{
			setText(viewResId,resIdIfEmpty);
			return;
		}
		
		if (text.length() > 0)
		{
			setText(viewResId,resIdIfEmpty);
			return;
		}		
	}
	
	public void setTextWithVisiblityIfEmpty(int viewResId, String text, int viewVisibilityIfEmpty)
	{
		View view = getView(viewResId);
		if (null == view)
		{
			return;
		}
		
		if (view instanceof TextView)
		{
			TextView viewText = (TextView)view;
			if (null != text)
			{
				if (view instanceof EditText)
				{
					EditText viewEdit = (EditText)view;
					viewEdit.setText(text);	
				}
				else
				{
					viewText.setText(text);							
				}
								
				if (text.length() > 0)
				{
					viewText.setVisibility(View.VISIBLE);
				}
				else
				{
					viewText.setVisibility(viewVisibilityIfEmpty);	
				}

			}
			else
			{
				viewText.setVisibility(viewVisibilityIfEmpty);				
			}
		}	
	}
	
	public void setText(int viewResId, int resId)
	{
		String text = resources.getString(resId);
		setText(viewResId,text);
	}
	
	public void setTextFormatDuration(int viewResId, String seconds)
	{	
		//setText(viewResId, UtilMethods.convertSecToMinutes( Integer.parseInt(seconds), hour, min));
	}
	
	public void setImage(int viewResId, int resId)
	{
		
		View view = getView(viewResId);
		if (null == view)
		{
			return;
		}	
		
		if (false == (view instanceof ImageView))
		{
			return;
		}
		
		
		((ImageView)view).setImageResource(resId);	
	}
	
	
	public void setImage(int viewResId, Drawable draw)
	{
		
		View view = getView(viewResId);
		if (null == view)
		{
			return;
		}	
		
		if (false == (view instanceof ImageView))
		{
			return;
		}
		
		((ImageView)view).setImageDrawable(draw);	
	}
	
	public void setBackground(int viewResId, int resId)
	{
		
		View view = getView(viewResId);
		if (null == view)
		{
			return;
		}	
		
		
		view.setBackgroundResource(resId);	
		
	}
	
	public void setAdapter(int viewResId, BaseAdapter adapter)
	{
		if (null == adapter)
		{
			return;
		}
		
		View view = getView(viewResId);
		if (null == view)
		{
			return;
		}
		

		
		if (view instanceof ListView)
		{		
			((ListView)view).setAdapter(adapter);
			return;
		}
		
		if(view instanceof AbsListView)
		{
			((AbsListView)view).setAdapter(adapter);
			return;
		}
	}
	
	
	public MvpObject getAdapterPagContext(int viewResId)
	{

		
		View view = getView(viewResId);
		if (null == view)
		{
			return null;
		}
		

		Object adapter = null;
		
		if (view instanceof MvpViewPager)
		{		
			 adapter = ((ViewPager)view).getAdapter();
		}
		else
		{
			if (view instanceof MvpHorizontalListView)
			{		
				 adapter = ((MvpHorizontalListView)view).getAdapter();
			}	
			else
			{
			
				if (view instanceof MvpVerticalListView)
				{		
					 adapter = ((MvpVerticalListView)view).getAdapter();
				}
			}
		}
		
		if (null == adapter)
		{
			return null;
		}
		
		if (adapter instanceof IMvpGenericAdapter)
		{
			return ((IMvpGenericAdapter)adapter).getPaginationContext();
		}
		
		return null;
	}
	
	public void setRating(int viewResId, float ratingData)
	{
	
		View view = getView(viewResId);
		if (null == view)
		{
			return;
		}
		
		if (view instanceof RatingBar)
		{		
			((RatingBar)view).setRating(ratingData);
			return;
		}
	}
	
	public float getRating(int viewResId)
	{
	
		View view = getView(viewResId);
		if (null == view)
		{
			return 0;
		}
		
		if (view instanceof RatingBar)
		{		
			return ((RatingBar)view).getRating();
		}
		
		return 0;
	}
	
	public void addView(int viewResId, View view)
	{
		View parent = getView(viewResId);
		if (null == parent)
		{
			return;
		}
		
		if (view instanceof ViewGroup)
		{
			((ViewGroup)parent).addView(view);
		}
	}
	
	public void requestFocus(int viewResId)
	{
		View view = getView(viewResId);
		if (null == view)
		{
			return;
		}
		
		view.requestFocus();
	}
	
	/**
	 *  
	 *
	 * @param  
	 * @param  
	 * @return      
	 * @see          
	 */
	public Resources getResources()
	{
		return resources;
	}
	
	
	/**
	 *  
	 *
	 * @param  
	 * @param  
	 * @return      
	 * @see          
	 */
	public Context getContext()
	{
		return context;
	}
	
	/**
	 *  
	 *
	 * @param  
	 * @param  
	 * @return      
	 * @see          
	 */
	public int[] buildResourcesList(int... params) 
	{	
		return params;
	}
}
