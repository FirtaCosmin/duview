package com.crmsoftware.duview.ui.common;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************


import java.util.Observable;
import java.util.Observer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.core.common.TaskImageReqCache;
import com.crmsoftware.duview.data.MvpImage;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;


public class MvpImageView extends FrameLayout implements ITaskReturn {
	public static boolean DEBUG = Log.DEBUG ; 
	public static final String TAG = MvpImageView.class.getSimpleName();
	
	public static boolean RUN_ON_UI_THREAD = false;
	public static boolean RUN_ON_CURRENT_THREAD = false;
	
	private TaskImageReqCache taskGetImage = null;
	private MvpImage mvpImage = null;
	private String imageUrl = "";
	private static Handler handler = null;
	private int defaultImageResId = R.drawable.du_logo_fade;
	private static Drawable defaultImage = null;
	private ProgressBar progressBarLoading = null;
	private ImageView imageView = null;
	private ScaleType scaleType = ScaleType.FIT_CENTER;
	
	public static long DATA_LIFE_TIME = 12*60*60*1000; // 12h life (the data is taken from cache and not updated from server)
	
	
	/**
	 * 
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
    public MvpImageView(Context context) 
    {
        super(context);
        initControl(context,null);
    }

	/**
	 * 
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
    public MvpImageView(Context context, AttributeSet attrs) 
    {
        super(context, attrs);
        initControl(context,attrs);
    }

	/**
	 * 
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
    @Override
    protected void finalize() throws Throwable {
		if (DEBUG) Log.m(TAG,this,"finalize");
    	super.finalize();
    }
    
	/**
	 * 
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
    public MvpImageView(Context context, AttributeSet attrs, int defStyle) 
    {
        super(context, attrs, defStyle);
        initControl(context,attrs);
    }

    
	/**
	 * 
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
    public void initControl(Context context,AttributeSet attrs)
    {
    	imageView = new ImageView(context,attrs);
    	imageView.setId(0);
    	scaleType = imageView.getScaleType();
    	
    	FrameLayout layout = (FrameLayout) LayoutInflater.from(getContext()).inflate(R.layout.image_loading_bar, null);
    	progressBarLoading = (ProgressBar) layout.getChildAt(0);
    	
    	addView(imageView);
    	addView(layout);
    	layout.bringToFront();

    }  
   
    @Override
    protected void onAttachedToWindow() 
    {
		if (DEBUG) Log.m(TAG,this,"onAttachedToWindow"); 
    	super.onAttachedToWindow();
    }
    
    @Override
    protected void onDetachedFromWindow() 
    {
		if (DEBUG) Log.m(TAG,this,"onDetachedFromWindow"); 
		
		stop();
		
    	super.onDetachedFromWindow();
    }
    
    
    private void stop()
    {
    	imageUrl = "";
    	
    	if (null != taskGetImage)
    	{
    		taskGetImage.cancel(true);
    		taskGetImage = null;
    	}
    	
    	if (null != mvpImage)
    	{
    		mvpImage.dettachView(imageView);
    		mvpImage = null;
    		setDefaultImage(View.VISIBLE);
    	}
    	
    }
    
	/**
	 * 
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
    public void setDefaultImageResId(int resId)
    {
    	defaultImageResId = resId;
    	defaultImage = null;
    }
    
    
	/**
	 * 
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
    public void setData(String sURL) 
    {
    	if (handler == null)
    	{
    		handler = new Handler();
    	}
    	
    	if (imageUrl == sURL && false == Utils.isEmpty(sURL))	// in case that is the same URL
    	{
    		return;
    	}
    	
    	if(sURL.equals("anonAvatar"))
    	{
    		setDefaultImage(View.INVISIBLE);
    		return;
    	}

    	if (false == sURL.startsWith("http"))	// this for local files only
    	{
			imageView.setScaleType(scaleType);	
			imageView.setImageURI(Uri.parse(sURL));
    		return;
    	}
    	
    	stop();	// cancel and/or erase the prev image 
    	
    	if (Utils.isEmpty(sURL))
    	{
    		setDefaultImage(View.INVISIBLE);
    		return;
    	}

    	this.imageUrl = sURL;
    	
		setDefaultImage(View.VISIBLE);
		taskGetImage = new TaskImageReqCache(this, sURL, DATA_LIFE_TIME);
		taskGetImage.execute(); 

    }
      
	public void updateImage() 
	{
		if (null != progressBarLoading)
		{
			progressBarLoading.setVisibility(View.INVISIBLE);
		}
		
		if (null == mvpImage)
		{
			return;
		}
		
		synchronized (mvpImage) 
		{			
			imageView.setScaleType(scaleType);	
			mvpImage.attachView(imageView);
		}
	}
	
	
	/**
	 * @desc set the default image and the visibility of the loading image
	 * @param progressVisibility - the visibility of the loading image
	 */
	public void setDefaultImage(final int progressVisibility) 
	{
    	if (null == defaultImage)
    	{
    		defaultImage = getResources().getDrawable(defaultImageResId);    
    	}
    	
		imageView.setImageBitmap(null);
		imageView.setScaleType(ScaleType.CENTER);
		imageView.setImageDrawable(defaultImage);
		imageView.invalidate();
		
		if (null != progressBarLoading)
		{
			progressBarLoading.setVisibility(progressVisibility);
		}
	}


	@Override
	public void onReturnFromTask(Task task, boolean canceled) {
		
		if (Utils.isEmpty(imageUrl)) // image reset
		{
			return;
		}
		String a = ((TaskImageReqCache)task).getUrl(); 
		if (false == (a.equals(imageUrl) || false == a.equals(imageUrl.replace("%20", " ")))) // there is a new image that was set
		{
			return;
		}
		
		if (task.isError())
		{
			setDefaultImage(View.INVISIBLE);	
			return;
		}
		
		MvpImage retData = (MvpImage) task.getReturnData();
		if (retData == null)
		{
			setDefaultImage(View.INVISIBLE);
			return;
		}
		
		mvpImage = retData;
		
		synchronized (mvpImage) 
		{
			updateImage();
		}
	}
    
}
