package com.crmsoftware.duview.ui.common;



import android.view.MotionEvent;
import android.view.View;

public interface IMvpGenericListViewListener{
	
	public void onListItemViewClick(IMvpGenericAdapter  adapter, View view, MvpItemHolder holder);
	public boolean onListItemViewTouch(IMvpGenericAdapter  adapter,View view, MvpItemHolder holder, MotionEvent event);
	public void onListItemUpdateView(IMvpGenericAdapter  adapter,  MvpItemHolder holder, int position);
	public int onListItemGetLayoutId(IMvpGenericAdapter  adapter, int position);
	public void onListItemsCountChanged(IMvpGenericAdapter  adapter, int newCount);
}
