package com.crmsoftware.duview.ui.common;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionbarsherlock.view.MenuItem;
import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.data.MvpMedia;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.data.MvpParentalManagement;
import com.crmsoftware.duview.data.MvpServerError;
import com.crmsoftware.duview.player.IPlayerView;
import com.crmsoftware.duview.player.PlayerView;
import com.crmsoftware.duview.ui.MvpFragment;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.ParentalPinDialogView;

public class PlayerViewPagerFragment 
extends MvpFragment 
implements 
IMvpGenericListViewListener,
OnPageChangeListener, 
IPlayerView,
ITaskReturn
{

	public static boolean DEBUG = Log.DEBUG; 
	public static boolean IS_TESTING = false; 
	
	public final static String TAG = PlayerViewPagerFragment.class.getSimpleName();
	
	
	public final static String DATA_CONTEXT = "DATA_CONTEXT"; 
	public final static String DATA_POSITION = "DATA_POSITION"; 
	public final static String DATA_VIEWTITLE = "DATA_VIEWTITLE"; 
	public final static String DATA_PREMIUM_LOGO = "DATA_PREMIUM_LOGO";
	/**
	 * @desc will tell the fragment to start playing after loading
	 */
	public final static String DATA_PLAY = "DATA_PLAY";
	
	public static String MIN = ""; 

	protected List<MvpViewHolder> categoriesHolder = new ArrayList<MvpViewHolder>(); 
	protected MvpViewPager pagerView;
	protected MenuItem barTitle;
	
	protected MvpPagPagerAdapter adapter;
	
	protected boolean isFullscreen = false;
	protected PlayerView currentViewPlayer = null;
	/**
	 * @desc flag indicating that after the view is drawn the player must start.
	 */
	protected Boolean mWillPlayAtStart = false;
	
	protected int currentPosition = 0;
	private TextView tvMiTitle;
	private Bundle data;
	private MvpObject mvpObjContext;
	
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		if (DEBUG) Log.m(TAG,this,"onCreateView");

		View view = setContentView(R.layout.fragment_items_viewpager,container,inflater);
		
		setHasOptionsMenu(true);
		getMvpActivity().setMenuEnabled(true);
		getMvpActivity().setActionBarVisibility(View.VISIBLE);
		getMvpActivity().setHomeButtonActionBar(true);
		getMvpActivity().setTitle(getData().getString(DATA_VIEWTITLE, "Details"));
		
		MIN = MainApp.getRes().getString(R.string.min);
		
		return view;
	}
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected void initList(Class itemType)
	{
		if (DEBUG) Log.m(TAG,this,"initList");
		
		Bundle viewData = getData();
		MvpViewHolder holder = getHolder();
		
		mvpObjContext = MvpObject.newReference(viewData.getString(DATA_CONTEXT));
		
		currentPosition = viewData.getInt(DATA_POSITION, 0);
		/*get the bundle information aboout the playing at start*/
		mWillPlayAtStart = viewData.getBoolean(DATA_PLAY, false);
		/*reset it now*/
		viewData.putBoolean(DATA_PLAY, false);
		
		
		
		pagerView = (MvpViewPager) holder.getView(R.id.items_pagedlist);
		
		adapter = new MvpPagPagerAdapter<MvpMedia>(
				itemType);
		
		adapter.receiveClickFrom(R.id.watch_now_button,R.id.player_body_view, R.id.player_play, R.id.player_pause, R.id.player_close, R.id.player_fullscreen);
		adapter.setPaginationContext(mvpObjContext);

		adapter.setGenericListViewListener(this);
		
		pagerView.setOnPageChangeListener(this);
		pagerView.setAdapter(adapter);
		
		if (currentPosition > 0)
		{
			pagerView.setCurrentItem(currentPosition);
		}
		else
		{
			updateCounter(currentPosition);
		}
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onPause() 
	{
		if (DEBUG) Log.m(TAG,this,"onPause");
		
		MvpObject mvpObjContext = MvpObject.newReference(getData().getString(DATA_CONTEXT));
		MvpPagContext ctx = (MvpPagContext) mvpObjContext.getData();
		
		int position = pagerView.getCurrentItem();
		if (ctx != null) 
		{
			ctx.setFirstVisibleItem(position);
		}	
		
		ctx.cancelAllRequests();
		
		Bundle data = getData();
		data.putInt(DATA_POSITION, position);
		
		if (null != currentViewPlayer)
		{
			currentViewPlayer.onParentPaused();
		}

		
		super.onPause();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onResume() 
	{
		if (DEBUG) Log.m(TAG,this,"onResume");
		
		if (null != currentViewPlayer)
		{
			currentViewPlayer.onParentResume();
		}
		
		super.onResume();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onDestroyView() 
	{		
		if (DEBUG) Log.m(TAG,this,"onDestroyView");
			
		if (null == mvpObjContext)
		{
			return;
		}
		
		MvpPagContext paginationContext = (MvpPagContext) mvpObjContext.getData();
		paginationContext.unbindAdapter();
		
		if (null != adapter)
		{
			adapter.setGenericListViewListener(null); // this will release fragment instance
			adapter = null;
		}
		
		super.onDestroyView();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onListItemViewClick(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder) 
	{
		switch (view.getId()) {
		case R.id.player_body_view:
		{
			PlayerView viewPlayer = (PlayerView) view.getParent();
			viewPlayer.showControlBar(!viewPlayer.isControlBarVisible());
		}	
			break;
			
		case R.id.player_close:
		{
			PlayerView viewPlayer = (PlayerView)holder.getView(R.id.player_view);
			viewPlayer.destroyPlayer();
		}	
			break;
		
		case R.id.player_play:
			resumePlayer();
			break;
		case R.id.player_pause:
			pausePlayer();
			break;
			


		
		default:
			break;
		}
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onResetData() 
	{
			
		if (null != currentViewPlayer)
		{
			destroyPlayer();
		}

		super.onResetData();
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu,
			com.actionbarsherlock.view.MenuInflater inflater) {
		inflater.inflate(R.menu.details_menu, menu);
		barTitle = (MenuItem) viewHolder.getView(R.id.title);
		barTitle = menu.findItem(R.id.title);
	    tvMiTitle = new TextView(getActivity());
	    tvMiTitle.setTextColor(Color.WHITE);
	    
        tvMiTitle.setTypeface(MvpViewHolder.typefaceNormal);

	    // MenuItem myMenuItem;
        barTitle.setActionView(tvMiTitle);
	    
		super.onCreateOptionsMenu(menu, inflater);
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void destroyPlayer() 
	{
        if (null != currentViewPlayer)
        {
        	currentViewPlayer.destroyPlayer();
			currentViewPlayer.setVisibility(View.INVISIBLE);
			currentViewPlayer = null;
        }		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onPageScrollStateChanged(int state) {
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onPageSelected(final int position) 
	{
		updateCounter(position);
		updateParental();
		destroyPlayer();
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onListItemsCountChanged(IMvpGenericAdapter fromAdapter, int newCount) 
	{
		if (null == pagerView)
		{
			return;
		}
		
		if (null == adapter)
		{
			return;
		}
		

		MvpPagContext pagContext = (MvpPagContext)fromAdapter.getPaginationContext().getData();
		
		if (pagContext.getCount() != newCount)
		{
			int position = pagerView.getCurrentItem();
			updateCounter(position);
		}
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void updateCounter(final int position)
	{
		
		new Handler().post(new Runnable() {
			
			private Typeface typefaceNormal = null;

			@Override
			public void run() 
			{
				if (null == barTitle || null == tvMiTitle || null == adapter)
				{
					return;
				}
				
		        String sCount = String.format(MainApp.getRes().getString(R.string.medial_list_position_information), position+1, adapter.getCount()).toLowerCase();
		        tvMiTitle.setText(sCount + "  ");

			}
		});
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onVideoSizeChanged(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onVideoCompletion() {

		destroyPlayer();	
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public boolean onBackPressed() 
	{      
		destroyPlayer();
        
		return super.onBackPressed();
	}

	@Override
	public boolean onListItemViewTouch(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public void onListItemUpdateView(IMvpGenericAdapter adapter,
			MvpItemHolder holder, int position) {
		int visibility = MvpParentalManagement.LEVEL_NOK == MvpParentalManagement.getInstance().checkLevel(getItemLevel(position)) ? View.INVISIBLE : View.VISIBLE;
		holder.setVisibility(R.id.body_view, visibility);
		
		
	}


	@Override
	public int onListItemGetLayoutId(IMvpGenericAdapter adapter, int position) {
		// TODO Auto-generated method stub
		return R.layout.media_details;
	}
	
	
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void updateParental() 
	{
		if (DEBUG) Log.m(TAG,this,"updateParental");
		
		if (null == pagerView)
		{
			return;
		}
		
		if (null == adapter)
		{
			return;
		}
		
		if (null != currentViewPlayer)
		{
			return;
		}
		
		int currentPos = pagerView.getCurrentItem();
		for (int i = 0; i < pagerView.getChildCount(); i++) 
		{
			View view = pagerView.getChildAt(i);
			Object tag = view.getTag();
			
			if (tag instanceof MvpItemHolder)
			{
				MvpItemHolder holder = (MvpItemHolder) tag;
				int itemPos = holder.getPosition();
				
				int visibility = MvpParentalManagement.LEVEL_NOK == MvpParentalManagement.getInstance().checkLevel(getItemLevel(itemPos)) ? View.INVISIBLE : View.VISIBLE;
				holder.setVisibility(R.id.body_view, visibility);
				
				if (itemPos == currentPos)
				{
						if(MvpParentalManagement.LEVEL_NOK == MvpParentalManagement.getInstance().checkLevel(getItemLevel(currentPos)))
						{
							Fragment prev = getChildFragmentManager().findFragmentByTag(ParentalPinDialogView.TAG);
							if(prev == null)
							{
								ParentalPinDialogView parentalDialogView =   ParentalPinDialogView.getInstance();
								parentalDialogView.setData(getTag(), fragmentData);
								parentalDialogView.show(getChildFragmentManager(), ParentalPinDialogView.TAG);
							}
						}
				}
			}
		}
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected int getItemLevel(int itemPosition)
	{
		return 0;
	}
	

	@Override
	public void onReturnFromTask(Task task, boolean canceled) {
		if(task.isError())
		{
			Object error = task.getError();
			MvpServerError mvpError = (MvpServerError)error;
			int errorCode = mvpError.getResponseCode();
			getMvpActivity().hideLoadingDialog();
			if(errorCode == 200)
			{
				ParentalPinDialogView.getInstance().cancelAction();
			}
			if (errorCode == 403)
			{
				showToast(mvpError.getTitle());
				return;
			}
			
			if (errorCode == 400)
			{
				showToast(MainApp.getRes().getString(R.string.change_pins_settings_wrong));
				return;
			}
		}
		
	}
	
	
	/**
	 * @desc will resume the player view if it is not null
	 */
	protected void resumePlayer(){

		if (null == currentViewPlayer)
		{
			return;
		}

		currentViewPlayer.play();
	}
	
	/**
	 * @desc will pause the player if it is created
	 */
	protected void pausePlayer(){

		if (null == currentViewPlayer)
		{
			return;
		}

		currentViewPlayer.pause();
	}
	
}
