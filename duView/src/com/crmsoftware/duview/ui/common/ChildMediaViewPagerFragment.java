package com.crmsoftware.duview.ui.common;

import java.util.Observable;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.data.MvpContent;
import com.crmsoftware.duview.data.MvpMedia;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.data.MvpParentalManagement;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.player.MvpPlayer;
import com.crmsoftware.duview.player.MvpPlayerInfo;
import com.crmsoftware.duview.player.PlayerActivity;
import com.crmsoftware.duview.player.PlayerView;
import com.crmsoftware.duview.utils.CustomDialogView;
import com.crmsoftware.duview.utils.IConst;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.ParentalPinDialogView;
import com.crmsoftware.duview.utils.Utils;

public class ChildMediaViewPagerFragment 
extends PlayerViewPagerFragment 
{

	public static boolean DEBUG = Log.DEBUG; 
	public static boolean IS_TESTING = false; 
	
	public final static String TAG = PlayerViewPagerFragment.class.getSimpleName();
	
	
	private MvpObject mvpObjContext;
	private Button mCurrentWatchNowButoon = null;
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) 
	{	
		if (DEBUG) Log.m(TAG,this,"onCreateView");

		View view = super.onCreateView(inflater, container, savedInstanceState);
		
		initList(MvpMedia.class);

		return view;
	}
	
	@Override
	public void onResume() {
		MvpParentalManagement.getInstance().addObserver(this);
		super.onResume();
	}
	
	
	@Override
	public void onPause() {
		MvpParentalManagement.getInstance().deleteObserver(this);
		super.onPause();
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@SuppressLint("NewApi")
	@Override
	public void onListItemUpdateView(IMvpGenericAdapter adapter,
			MvpItemHolder holder, int position) 
	{
		MvpMedia  mvpMedia = (MvpMedia) adapter.getItem(position);
		
		if (null == mvpMedia)
		{
			holder.setVisibility(R.id.item_loading, View.VISIBLE);
			holder.setVisibility(R.id.body_view, View.INVISIBLE);
			return;
		}
		
		
			View view = holder.getView(R.id.player_body_view);
			adapter.receiveClickFrom(R.id.player_body_view);

			PlayerView viewPlayerView = (PlayerView) holder
					.getView(R.id.player_view);
			viewPlayerView.bringToFront();
			viewPlayerView.setPlayerListener(this);
			viewPlayerView.showBorder(false);

			if (position == currentPosition) // tablet rotation
			{
				currentViewPlayer = viewPlayerView;
				// currentViewPlayer.attachPlayer();

				holder.setVisibility(R.id.player_item_image_layout,
						View.INVISIBLE);
				holder.setVisibility(R.id.player_item_details, View.INVISIBLE);
				holder.setVisibility(R.id.player_item_details_placeholder,
						View.INVISIBLE);
			}

			holder.setVisibility(R.id.item_loading, View.INVISIBLE);
			holder.setVisibility(R.id.body_view, View.VISIBLE);

			TextView titleView = (TextView) holder.getView(R.id.item_title);
			if (null != titleView)
			{
				titleView.setText(mvpMedia.getTitle());
				titleView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
				titleView.setSingleLine(true);
				titleView.setMarqueeRepeatLimit(2);
				titleView.setSelected(true);
			}

			holder.setText(R.id.player_item_title, mvpMedia.getTitle());

			holder.setText(R.id.item_description, mvpMedia.getDescription());

			String duration = mvpMedia.getContent().getDuration(MIN);

			holder.setText(R.id.player_item_description, duration);

			setDuration(holder, duration);

			setYear(holder, mvpMedia.getReleaseYear());

			setAgeRating(holder, mvpMedia.getRatings().getRating()
					.toUpperCase());

			setGenre(holder, mvpMedia.getGenres());

			setDirector(holder, mvpMedia.getDirectors());

			setStars(holder, mvpMedia.getActors());

			setButtonContent(holder, mvpMedia);

			MvpImageView itemPremiumImage = (MvpImageView) holder
					.getView(R.id.item_logo);
			String imgPremiumLogo = getData().getString(DATA_PREMIUM_LOGO, IConst.EMPTY_STRING);
			if(!imgPremiumLogo.equals(IConst.EMPTY_STRING))
			{
				itemPremiumImage.setData(imgPremiumLogo.replace("%20", " "));
				itemPremiumImage.setVisibility(View.VISIBLE);
			}
			else
			{
				itemPremiumImage.setVisibility(View.GONE);
			}
			
			MvpImageView itemImage = (MvpImageView) holder
					.getView(R.id.item_image);

			String sImageURL = null;
			MvpContent itemImageData = mvpMedia.getDetailsLandscape();
			if (null != itemImageData) 
			{
				sImageURL = itemImageData.getUrl();

			}
			itemImage.setData(sImageURL);
			// itemImage.setData( Environment.getExternalStorageDirectory() +
			// "/duView/test.png");

			MvpImageView playerImage = (MvpImageView) holder
					.getView(R.id.player_item_image);
			playerImage.setData(sImageURL);
			

			/*if the start player is set then start it*/
			if ( mWillPlayAtStart ){
				setButtonState((Button) holder.getView(R.id.watch_now_button), mWillPlayAtStart);
				/*reset the flag so no other pager element will start the player*/
				mWillPlayAtStart = false;
				/*start the player*/
				startPlayer(holder, mvpMedia);
				
			}
		 
			super.onListItemUpdateView(adapter, holder, position);

	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onListItemViewClick(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder) 
	{
		if (DEBUG) Log.m(TAG,this,"onListItemViewClick");
		
		
		switch (view.getId()) 
		{
		case R.id.watch_now_button:
		{
			MvpUserData authData = MvpUserData.getFromMvpObject();
			if(authData.isAnonymous())
			{
				CustomDialogView customDialogView = CustomDialogView.getInstance();
				customDialogView.show(getFragmentManager(), CustomDialogView.TAG);
			}
			else
			{
			currentPosition = holder.getPosition();
			MvpMedia  mvpMedia = (MvpMedia) adapter.getItem(currentPosition);
			
			startPlayer(holder, mvpMedia);	
			
			mCurrentWatchNowButoon = (Button) holder.getView(R.id.watch_now_button);
			setButtonState(mCurrentWatchNowButoon, true);
			}
		}
		break;
		
		case R.id.player_fullscreen:
		{
			currentPosition = holder.getPosition();
			MvpMedia  mvpMedia = (MvpMedia) adapter.getItem(currentPosition);
			
			MvpPlayerInfo info = MvpPlayer.getPlayerInfo();
			
			info.setShortDesc(mvpMedia.getDescription());
			info.setTitle(mvpMedia.getTitle());
			info.setChannelImage(mvpMedia.getThumbPoster().getUrl());
			
			Intent intent = new Intent(getActivity(), PlayerActivity.class);
			intent.putExtra(PlayerActivity.LAYOUT_ID, R.layout.player_media);
			startActivity(intent);
		}
		break;
		
		case R.id.player_close:
			setButtonState((Button) holder.getView(R.id.watch_now_button), false);
		
		default:
			super.onListItemViewClick(adapter,view,holder);
			break;
		}
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public int onListItemGetLayoutId(IMvpGenericAdapter adapter, int position) {
		if(adapter instanceof MvpPagListAdapter)
		{
			return 	R.layout.media_simple_list_view_row;
		}
		return 	R.layout.media_details;
	}


	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onDataUpdate(Observable observable, Object data) 
	{
		if (DEBUG) Log.m(TAG,this,"onDataUpdate");
		
		if (MvpParentalManagement.getInstance() == observable)
		{
			updateParental();
		}
		
		super.onDataUpdate(observable, data);
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	protected int getItemLevel(int position) 
	{
		if (null == adapter)
		{
			return super.getItemLevel(position);
		}
		MvpMedia mvpMedia = (MvpMedia)adapter.getItem(position);
		
		if (null == mvpMedia)
		{
			return super.getItemLevel(position);	
		}
		
		return mvpMedia.getParentalLevel(); 
	}
	
	@Override
	public void onMvpDlgClose(String dlgTag, Bundle data) {
		if (DEBUG) Log.m(TAG,this,"onMvpDlgClose");
		
		int viewId = data.getInt(ParentalPinDialogView.RETURN_STATE);
		
		switch (viewId) 
		{
		case R.id.button_pozitive:
			String pin = data.getString(ParentalPinDialogView.PIN_VALUE);
			if(Utils.isEmpty(pin))
			{
				showToast("Field is empty");
			}
			else
			{
				getMvpActivity().showLoadingDialog();
				MvpParentalManagement.getInstance().checkPin(pin, this);
				
			}
			break;

		default:
			break;
		}
		
		
		super.onMvpDlgClose(dlgTag, data);
	}
	
	private void setDuration(MvpItemHolder holder, String duration)
	{
		if(!duration.equals("0 min"))
		{
			holder.setVisibility(R.id.item_duration_layout,  View.VISIBLE);
			holder.setText(R.id.item_duration_value, duration);	
		}
		else
		{
			holder.setVisibility(R.id.item_duration_layout,  View.GONE);
		}
	}
	
	private void setYear(MvpItemHolder holder, String year)
	{
		if(!Utils.isEmpty(year))
		{
			holder.setVisibility(R.id.item_year_layout,  View.VISIBLE);
			holder.setText(R.id.item_year_value, year);
		}
		else
		{
			holder.setVisibility(R.id.item_year_layout,  View.GONE);
		}
	}
	
	private void setAgeRating(MvpItemHolder holder, String genre)
	{
		if(!Utils.isEmpty(genre))
		{
			holder.setVisibility(R.id.item_parental_info,  View.VISIBLE);
			holder.setText(R.id.item_parental_info, genre);
		}
		else
		{
			holder.setVisibility(R.id.item_parental_info,  View.GONE);
		}
	}
	
	private void setGenre(MvpItemHolder holder, String genre)
	{
		if(!Utils.isEmpty(genre))
		{
			holder.setVisibility(R.id.item_genre_layout,  View.VISIBLE);
			holder.setText(R.id.item_genre_value, genre);
		}
		else
		{
			holder.setVisibility(R.id.item_genre_layout,  View.GONE);
		}
	}
	
	private void setStars(MvpItemHolder holder, String actors)
	{
		if(!Utils.isEmpty(actors))
		{
			holder.setVisibility(R.id.item_star_layout,  View.VISIBLE);
			holder.setText(R.id.item_stars_value, actors);
		}
		else
		{
			holder.setVisibility(R.id.item_star_layout,  View.GONE);
		}
	}
	
	private void setDirector(MvpItemHolder holder, String director)
	{
		if(!Utils.isEmpty(director))
		{
			holder.setVisibility(R.id.item_director_layout,  View.VISIBLE);
			holder.setText(R.id.item_director_value, director);
		}
		else
		{
			holder.setVisibility(R.id.item_director_layout,  View.GONE);
		}
	}


	
	@SuppressLint("NewApi")
	public void setButtonContent(MvpItemHolder holder, MvpMedia mvpMedia)
	{
		String btnText = "";
		int backId = R.drawable.button_selector_invert;
		int leftDrw = 0;
		Button watchNowButton = (Button) holder
				.getView(R.id.watch_now_button);

		
			btnText = holder.getResources().getString(
					R.string.watch_now_message);
			leftDrw = R.drawable.button_icon_watch_now;
			holder.setVisibility(R.id.item_duration_value, View.VISIBLE);

		watchNowButton.setText(btnText);
		watchNowButton.setBackgroundResource(backId);
		watchNowButton.setCompoundDrawablesRelativeWithIntrinsicBounds(
				leftDrw, 0, 0, 0);
	}
	
	/**
	 * @desc will attach the player and start it with the given media
	 * @param holder
	 * @param mvpMedia
	 */
	private void startPlayer(MvpItemHolder holder,MvpMedia  mvpMedia){

		currentViewPlayer = (PlayerView)holder.getView(R.id.player_view);
		currentViewPlayer.attachPlayer();
		if (IS_TESTING)
		{
			currentViewPlayer.play(Environment.getExternalStorageDirectory() + "/A.mp4");
		}
		else
		{
			currentViewPlayer.play(mvpMedia);
		}		
	}
	
	@SuppressLint("NewApi")
	private void setButtonState(Button button, boolean isSetPlayerActive) {
		if(null == button)
		{
			return;
		}
		if(isSetPlayerActive)
		{
			button.setBackground(MainApp.getRes().getDrawable(R.drawable.button_style_dark_margenta));
		}
		else
		{
			button.setBackground(MainApp.getRes().getDrawable(R.drawable.button_selector_invert));
		}
	}
	
	@Override
	public void destroyPlayer() {
		if ( mCurrentWatchNowButoon != null ){
			setButtonState(mCurrentWatchNowButoon, false);
			mCurrentWatchNowButoon = null;
		}
		super.destroyPlayer();
	}
	
	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels){
		super.onPageScrolled(position,positionOffset, positionOffsetPixels);
		if (mCurrentWatchNowButoon != null){
				/*if the current position is not with a series then reset the button*/
			setButtonState(mCurrentWatchNowButoon, false);
		}
		mCurrentWatchNowButoon = null;
	}
}
