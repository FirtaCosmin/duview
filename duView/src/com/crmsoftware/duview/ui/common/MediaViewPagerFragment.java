package com.crmsoftware.duview.ui.common;

import java.util.HashMap;
import java.util.Observable;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.FragmentChangeActivity;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.configuration.PlatformSettings;
import com.crmsoftware.duview.core.common.HttpReqParams;
import com.crmsoftware.duview.data.MpxPagHeader;
import com.crmsoftware.duview.data.MvpContent;
import com.crmsoftware.duview.data.MvpMedia;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.data.MvpParentalManagement;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.player.MvpPlayer;
import com.crmsoftware.duview.player.MvpPlayerInfo;
import com.crmsoftware.duview.player.PlayerActivity;
import com.crmsoftware.duview.player.PlayerView;
import com.crmsoftware.duview.utils.CustomDialogView;
import com.crmsoftware.duview.utils.IConst;
import com.crmsoftware.duview.utils.IHttpConst;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.ParentalPinDialogView;
import com.crmsoftware.duview.utils.Utils;

public class MediaViewPagerFragment 
extends PlayerViewPagerFragment 
implements 
OnScrollListener
{

	public static boolean DEBUG = Log.DEBUG; 
	private static String ACTIVE_EPISODE_POS_LSIT = "SERIES_LIST";
	public static boolean IS_TESTING = false; 
	
	public final static String TAG = PlayerViewPagerFragment.class.getSimpleName();
	
	public static long SERIES_LIFE_TIME = 60*60*1000; // 60min life (the data is taken from cache and not updated from server)
	
	private MvpObject mvpObjContext;
	
	/**
	 * @desc map to hold the positions that have the episodes list displayed.
	 */
	private HashMap<Integer, Boolean> mSeriesWithActiveEpisodeList;
	
	/**
	 * @desc the watch now button that was last clicked. It is used to 
	 * change its state when the pager changes the page.
	 */
	private Button mCurrentWatchNowButoon = null;
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) 
	{	
		if (DEBUG) Log.m(TAG,this,"onCreateView");

		View view = super.onCreateView(inflater, container, savedInstanceState);
		
		initList(MvpMedia.class);
		
		Bundle data = getData();
		try{
			/*get the data regarding the active series from the fragment bundle.*/
			mSeriesWithActiveEpisodeList = (HashMap<Integer, Boolean>)data.get(ACTIVE_EPISODE_POS_LSIT);
			/*if no data is defined in the bundle then create the object and add it to the bundle*/
			if ( mSeriesWithActiveEpisodeList == null ){
				mSeriesWithActiveEpisodeList = new HashMap<Integer, Boolean>();
				data.putSerializable(ACTIVE_EPISODE_POS_LSIT, mSeriesWithActiveEpisodeList);
			}
		}
		catch (ClassCastException e) {
			/*if a cast exception is catch then create the object and add it to the bundle.*/
			mSeriesWithActiveEpisodeList = new HashMap<Integer, Boolean>();
			data.putSerializable(ACTIVE_EPISODE_POS_LSIT, mSeriesWithActiveEpisodeList);
		}
		

		return view;
	}
	
	@Override
	public void onResume() {
		MvpParentalManagement.getInstance().addObserver(this);
		super.onResume();
	}
	
	
	@Override
	public void onPause() {
		MvpParentalManagement.getInstance().deleteObserver(this);
		super.onPause();
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@SuppressLint("NewApi")
	@Override
	public void onListItemUpdateView(IMvpGenericAdapter adapter,
			MvpItemHolder holder, int position) 
	{
		if (DEBUG) Log.d(TAG, "onListItemUpdateView");
		MvpMedia  mvpMedia = (MvpMedia) adapter.getItem(position);
		
		if (null == mvpMedia)
		{
			holder.setVisibility(R.id.item_loading, View.VISIBLE);
			holder.setVisibility(R.id.body_view, View.INVISIBLE);
			return;
		}
		
		if(adapter instanceof MvpPagListAdapter)
		{
			if ( adapter.getCount() <= 0 ){
				return;
			}
			mLoadingView.setVisibility(View.GONE);
			mEpisodeLit.setVisibility(View.VISIBLE);
			holder.setVisibility(R.id.item_loading, View.INVISIBLE);
			holder.setVisibility(R.id.body_view, View.VISIBLE);
			
			holder.setText(R.id.item_title, mvpMedia.getTitle());
			/*if the device is in portrait mode the even/odd rows will have different background */
			/*if the device is in landscape mode the 0-1/2-3 rows will have different background */
			if ( getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ){
				if ((position+2) % 4 == 0 || (position + 1) % 4 == 0) 
				{
					holder.getView(R.id.body_view).setBackgroundResource(R.color.first_item);
				} 
				else
				{
					holder.getView(R.id.body_view).setBackgroundResource(R.color.second_item);
				}
			}else{
				if (position % 2 == 0 ) 
				{
					holder.getView(R.id.body_view).setBackgroundResource(R.color.first_item);
				} 
				else
				{
					holder.getView(R.id.body_view).setBackgroundResource(R.color.second_item);
				}
			}
		}
		else
		{
		
			View view = holder.getView(R.id.player_body_view);
			adapter.receiveClickFrom(R.id.player_body_view);

			PlayerView viewPlayerView = (PlayerView) holder
					.getView(R.id.player_view);
			viewPlayerView.bringToFront();
			viewPlayerView.setPlayerListener(this);
			viewPlayerView.showBorder(false);

			if (position == currentPosition) // tablet rotation
			{
				currentViewPlayer = viewPlayerView;
				currentViewPlayer.attachPlayer();

				holder.setVisibility(R.id.player_item_image_layout,
						View.INVISIBLE);
				holder.setVisibility(R.id.player_item_details, View.INVISIBLE);
				holder.setVisibility(R.id.player_item_details_placeholder,
						View.INVISIBLE);
			}

			holder.setVisibility(R.id.item_loading, View.INVISIBLE);
			holder.setVisibility(R.id.body_view, View.VISIBLE);

			TextView titleView = (TextView) holder.getView(R.id.item_title);
			if (null != titleView)
			{
				titleView.setText(mvpMedia.getTitle());
				titleView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
				titleView.setSingleLine(true);
				titleView.setMarqueeRepeatLimit(2);
				titleView.setSelected(true);
			}

			holder.setText(R.id.player_item_title, mvpMedia.getTitle());

			holder.setText(R.id.item_description, mvpMedia.getDescription());

			String duration = mvpMedia.getContent().getDuration(MIN);

			holder.setText(R.id.player_item_description, duration);

			setDuration(holder, duration);

			setYear(holder, mvpMedia.getReleaseYear());

			setAgeRating(holder, mvpMedia.getRatings().getRating()
					.toUpperCase());

			setGenre(holder, mvpMedia.getGenres());

			setDirector(holder, mvpMedia.getDirectors());

			setStars(holder, mvpMedia.getActors());

			Button watchNowButton = (Button) holder.getView(R.id.watch_now_button);
			setButtonState(watchNowButton, false);
			
			MvpImageView itemPremiumImage = (MvpImageView) holder
					.getView(R.id.item_logo);
			String imgPremiumLogo = getData().getString(DATA_PREMIUM_LOGO, IConst.EMPTY_STRING);
			if(!imgPremiumLogo.equals(IConst.EMPTY_STRING))
			{
				itemPremiumImage.setData(imgPremiumLogo.replace("%20", " "));
				itemPremiumImage.setVisibility(View.VISIBLE);
			}
			else
			{
				itemPremiumImage.setVisibility(View.GONE);
			}
			
			/*display the series episodes*/
			setLayoutVisibility(holder, mvpMedia, position);

			MvpImageView itemImage = (MvpImageView) holder
					.getView(R.id.item_image);
			String sImageURL = null;
			MvpContent itemImageData = mvpMedia.getDetailsLandscape();
			if (null != itemImageData) 
			{
				sImageURL = itemImageData.getUrl();

			}
			itemImage.setData(sImageURL);

			MvpImageView playerImage = (MvpImageView) holder
					.getView(R.id.player_item_image);
			playerImage.setData(sImageURL);

			
			/*if the start player is set then start it*/
			if ( mWillPlayAtStart ){
				setButtonState((Button) holder.getView(R.id.watch_now_button), mWillPlayAtStart);
				/*reset the flag so no other pager element will start the player*/
				mWillPlayAtStart = false;
				/*start the player*/
				startPlayer(holder, mvpMedia);
				
			}
			/*if using parallax then scroll to top again*/
			if (holder.getView(R.id.parallaxScrollVew) != null){
				ScrollView scrollView = (ScrollView)holder.getView(R.id.parallaxScrollVew);
				scrollView.scrollTo(0, 0);
			}
		}
		 
			super.onListItemUpdateView(adapter, holder, position);

	}
	
	/**
	 * 
	 *
	 * @param 
	 * @param  
	 * @return      
	 * @see         
	 */
	public void goToEpisodes(MvpItemHolder holder, MvpMedia  mvpMedia)
	{
		if (DEBUG) Log.m(TAG,this,"goToEpisodes");
		
		String categoryId =  IHttpConst.SCHEME_CATEGORY_GENRE + IHttpConst.CHAR_DOTS + mvpMedia.getSeriesName();
		
		String mvpObjContextId = "TpMpxFeedUrl."+categoryId;
		MvpObject mvpPagContext = MvpObject.getReference(Utils.formatLink(mvpObjContextId));
		
		HttpReqParams httpReqParams = new HttpReqParams(
				IHttpConst.VALUE_SCHEMA_1_2, 
				IHttpConst.VALUE_CJSON);
		
		httpReqParams.setSort(IHttpConst.VALUE_FIELD_SORTTITLE);
		httpReqParams.setParam(IHttpConst.PARAM_BY_CATEGORIES, categoryId);	
		
		if (mvpPagContext == null)
		{				
			mvpPagContext = MvpPagContext.getContext(
					MpxPagHeader.class,
					MvpPagContext.STARTWITH_ONE,						
					PlatformSettings.getInstance().getTpMpxFeedUrl(),
		 			httpReqParams,
		 			mvpObjContextId,
		 			SERIES_LIFE_TIME);
		}
		
		 MvpPagListAdapter adapter = new MvpPagListAdapter<MvpMedia>(
		 			MvpMedia.class,
		 			1);
		 
		 mvpObjContext = MvpObject.newReference(mvpPagContext.getId());
			
		adapter.setPaginationContext(mvpObjContext);
		adapter.setGenericListViewListener(this);
		adapter.receiveClickFrom(R.id.item_row, R.id.simple_list_watch);
		
		View episodesView = holder.getView(R.id.episodes_list);
		if ( episodesView instanceof AbsListView ){
			AbsListView listView = (AbsListView) episodesView;
			listView.setOnScrollListener(this);
			listView.setAdapter(adapter);
		}else if ( episodesView instanceof MvpAdaptiveLinearLayout ){
			((MvpAdaptiveLinearLayout )episodesView).setAdapter(adapter);
		}
		
//		Bundle data = new Bundle();
//		
//		data.putInt(MediaViewPagerFragment.DATA_POSITION, 0);
//		data.putString(MediaViewPagerFragment.DATA_CONTEXT, mvpPagContext.getId());
//		data.putString(MediaViewPagerFragment.DATA_VIEWTITLE, mvpMedia.getTitle());
//		FragmentChangeActivity fca = (FragmentChangeActivity) getActivity();
//		fca.goTo(MediaListViewFragment.class,MediaListViewFragment.TAG, data, true);
	}
	
	@Override
	public boolean onListItemViewTouch(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder, MotionEvent event) {
		return false;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@SuppressLint("NewApi")
	@Override
	public void onListItemViewClick(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder) 
	{
		if (DEBUG) Log.m(TAG,this,"onListItemViewClick");
		
		switch (view.getId()) 
		{
		case R.id.watch_now_button:
		{
			try
			{
			
				MvpUserData authData = MvpUserData.getFromMvpObject();
				currentPosition = holder.getPosition();
				MvpMedia  mvpMedia = (MvpMedia) adapter.getItem(currentPosition);
				
				if(authData.isAnonymous())
				{
					if (false == mvpMedia.getSeriesName().isEmpty())
					{
						/*if the current item is a series one*/
						/*display or hide the episodes list*/
						switchEpisodeListVisibility(holder,mvpMedia,currentPosition);
					}
					else
					{
						CustomDialogView customDialogView = CustomDialogView.getInstance();
						customDialogView.show(getFragmentManager(), CustomDialogView.TAG);
					}
				}
				else
				{
				
				
					if (false == mvpMedia.getSeriesName().isEmpty())
					{
						/*if the current item is a series one*/
						/*display or hide the episodes list*/
						switchEpisodeListVisibility(holder,mvpMedia,currentPosition);
					}
					else
					{
						startPlayer(holder,mvpMedia);
						mCurrentWatchNowButoon = (Button) holder.getView(R.id.watch_now_button);
						setButtonState(mCurrentWatchNowButoon, true);
					}
				}
			}
			catch( NullPointerException ex )
			{
				if(DEBUG) Log.d(TAG, "::onListItemViewClick NullPointerException probably caused by data not loaded yet");
			}

			
		  
		}
		break;
		case R.id.item_row:
		{
			if(holder.getPosition() == -1)
			{
				return;
			}
				Bundle dataForChild = new Bundle();
				
				dataForChild.putInt(MediaViewPagerFragment.DATA_POSITION, holder.getPosition());
				dataForChild.putString(MediaViewPagerFragment.DATA_CONTEXT, mvpObjContext.getId());
				
				//ChildMediaViewPagerFragment fragment = new ChildMediaViewPagerFragment();
				MediaViewPagerFragment fragment = new MediaViewPagerFragment();
				fragment.setData(dataForChild);
				
				FragmentChangeActivity fca = (FragmentChangeActivity) getActivity();
				fca.goTo(MediaViewPagerFragment.class,MediaViewPagerFragment.TAG, dataForChild, true);
		}
		break;
		
		case R.id.player_fullscreen:
		{
			currentPosition = holder.getPosition();
			MvpMedia  mvpMedia = (MvpMedia) adapter.getItem(currentPosition);
			
			MvpPlayerInfo info = MvpPlayer.getPlayerInfo();
			
			info.setShortDesc(mvpMedia.getDescription());
			info.setTitle(mvpMedia.getTitle());
			info.setChannelImage(mvpMedia.getThumbPoster().getUrl());
			
			Intent intent = new Intent(getActivity(), PlayerActivity.class);
			intent.putExtra(PlayerActivity.LAYOUT_ID, R.layout.player_media);
			startActivity(intent);
		}
		break;
		case R.id.simple_list_watch:
			
			if(holder.getPosition() == -1)
			{
				return;
			}
			
			/*If click on play btn in the episodes list*/
			MvpUserData authData = MvpUserData.getFromMvpObject();
			if(authData.isAnonymous())
			{
			CustomDialogView customDialogView = CustomDialogView.getInstance();
			customDialogView.show(getFragmentManager(), CustomDialogView.TAG);
			}
			else
			{
			Bundle dataForChild = new Bundle();
			
			dataForChild.putInt(MediaViewPagerFragment.DATA_POSITION, holder.getPosition());
			dataForChild.putString(MediaViewPagerFragment.DATA_CONTEXT, mvpObjContext.getId());
			/*tell child to start playing*/
			dataForChild.putBoolean(MediaViewPagerFragment.DATA_PLAY, true);
			
			
			FragmentChangeActivity fca = (FragmentChangeActivity) getActivity();
			fca.goTo(MediaViewPagerFragment.class,MediaViewPagerFragment.TAG, dataForChild, true);
			}
	
			
		break;
		
		case R.id.player_close:
				setButtonState((Button) holder.getView(R.id.watch_now_button), false);
		default:
			super.onListItemViewClick(adapter,view,holder);
			break;
		}
		
	}

	@SuppressLint("NewApi")
	private void setButtonState(Button button, boolean isSetPlayerActive) {
		if(null == button)
		{
			return;
		}
		if(isSetPlayerActive)
		{
			button.setBackground(MainApp.getRes().getDrawable(R.drawable.button_style_dark_margenta));
		}
		else
		{
			button.setBackground(MainApp.getRes().getDrawable(R.drawable.button_selector_invert));
		}
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public int onListItemGetLayoutId(IMvpGenericAdapter adapter, int position) {
		if(adapter instanceof MvpPagListAdapter)
		{
			return 	R.layout.media_simple_list_view_row;
		}
		return 	R.layout.media_details;
	}


	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onDataUpdate(Observable observable, Object data) 
	{
		if (DEBUG) Log.m(TAG,this,"onDataUpdate");
		
		if (MvpParentalManagement.getInstance() == observable)
		{
			updateParental();
		}
		
		super.onDataUpdate(observable, data);
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	protected int getItemLevel(int position) 
	{
		if (null == adapter)
		{
			return super.getItemLevel(position);
		}
		MvpMedia mvpMedia = (MvpMedia)adapter.getItem(position);
		
		if (null == mvpMedia)
		{
			return super.getItemLevel(position);	
		}
		
		return mvpMedia.getParentalLevel(); 
	}
	
	@Override
	public void onMvpDlgClose(String dlgTag, Bundle data) {
		if (DEBUG) Log.m(TAG,this,"onMvpDlgClose");
		
		int viewId = data.getInt(ParentalPinDialogView.RETURN_STATE);
		
		switch (viewId) 
		{
		case R.id.button_pozitive:
			String pin = data.getString(ParentalPinDialogView.PIN_VALUE);
			if(Utils.isEmpty(pin))
			{
				showToast("Field is empty");
			}
			else
			{
				getMvpActivity().showLoadingDialog();
				MvpParentalManagement.getInstance().checkPin(pin, this);
				
			}
			break;

		default:
			break;
		}
		
		
		super.onMvpDlgClose(dlgTag, data);
	}
	
	private void setDuration(MvpItemHolder holder, String duration)
	{
		if(!duration.equals("0 min"))
		{
			holder.setVisibility(R.id.item_duration_layout,  View.VISIBLE);
			holder.setText(R.id.item_duration_value, duration);	
		}
		else
		{
			holder.setVisibility(R.id.item_duration_layout,  View.GONE);
		}
	}
	
	private void setYear(MvpItemHolder holder, String year)
	{
		if(!Utils.isEmpty(year))
		{
			holder.setVisibility(R.id.item_year_layout,  View.VISIBLE);
			holder.setText(R.id.item_year_value, year);
		}
		else
		{
			holder.setVisibility(R.id.item_year_layout,  View.GONE);
		}
	}
	
	private void setAgeRating(MvpItemHolder holder, String genre)
	{
		if(!Utils.isEmpty(genre))
		{
			holder.setVisibility(R.id.item_parental_info,  View.VISIBLE);
			holder.setText(R.id.item_parental_info, genre);
		}
		else
		{
			holder.setVisibility(R.id.item_parental_info,  View.GONE);
		}
	}
	
	private void setGenre(MvpItemHolder holder, String genre)
	{
		if(!Utils.isEmpty(genre))
		{
			holder.setVisibility(R.id.item_genre_layout,  View.VISIBLE);
			holder.setText(R.id.item_genre_value, genre);
		}
		else
		{
			holder.setVisibility(R.id.item_genre_layout,  View.GONE);
		}
	}
	
	private void setStars(MvpItemHolder holder, String actors)
	{
		if(!Utils.isEmpty(actors))
		{
			holder.setVisibility(R.id.item_star_layout,  View.VISIBLE);
			holder.setText(R.id.item_stars_value, actors);
		}
		else
		{
			holder.setVisibility(R.id.item_star_layout,  View.GONE);
		}
	}
	
	private void setDirector(MvpItemHolder holder, String director)
	{
		if(!Utils.isEmpty(director))
		{
			holder.setVisibility(R.id.item_director_layout,  View.VISIBLE);
			holder.setText(R.id.item_director_value, director);
		}
		else
		{
			holder.setVisibility(R.id.item_director_layout,  View.GONE);
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		
		if (scrollState == SCROLL_STATE_IDLE)
		{
			int pos = 0;
			if (null != mvpObjContext)
			{
				MvpPagContext context = (MvpPagContext)mvpObjContext.getData();
			
				if (null != context)
				{
					pos = context.getFirstVisibleItem();
				}
			}
			getData().putInt(MediaViewPagerFragment.DATA_POSITION, pos);
		}
		
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub
		
	}
	
	
	/**
	 * @desc will display or hide the list with the episodes
	 * @param holder
	 * @param mvpMedia
	 * @param position
	 */
	private View mLoadingView = null;
	private View mEpisodeLit = null;
	@SuppressLint("NewApi")
	private void setLayoutVisibility(MvpItemHolder holder, MvpMedia mvpMedia, int position)
	{
		String btnText = "";
		int backId = R.drawable.button_selector_invert;
		int leftDrw = 0;
		Button watchNowButton = (Button) holder
				.getView(R.id.watch_now_button);
		
		
		
		if (false == mvpMedia.getSeriesName().isEmpty())
		{
			/*if the series does not have an entry for the current position*/
			/*add one with false value - consider the episodes list as not visible at first*/
			if ( mSeriesWithActiveEpisodeList.get(position) == null ){
				mSeriesWithActiveEpisodeList.put(position, false);
			}
			if(mSeriesWithActiveEpisodeList.get(position))
			{
				btnText = holder.getResources().getString(R.string.hide_episodes);
				holder.setVisibility(R.id.description_layout, View.GONE);
				holder.setVisibility(R.id.episodes_list, View.INVISIBLE);
				holder.setVisibility(R.id.episode_list_layout, View.VISIBLE);
				holder.setVisibility(R.id.episodes_item_loading, View.VISIBLE);
				mLoadingView = holder.getView(R.id.episodes_item_loading);
				mEpisodeLit = holder.getView(R.id.episodes_list);
				goToEpisodes(holder, mvpMedia);

				setButtonState(watchNowButton, true);
			}
			else
			{
				btnText = String.format(
						holder.getResources().getString(R.string.episodes),
						mvpMedia.getEpisodesCount());
				holder.setVisibility(R.id.episode_list_layout, View.GONE);
				holder.setVisibility(R.id.description_layout, View.VISIBLE);
				
			}

			
			backId = R.drawable.button_selector;
			leftDrw = R.drawable.icon_pages;
		} 
		else
		{
			btnText = holder.getResources().getString(
					R.string.watch_now_message);
			leftDrw = R.drawable.button_icon_watch_now;
			holder.setVisibility(R.id.episode_list_layout, View.GONE);
			holder.setVisibility(R.id.description_layout, View.VISIBLE);
		}

		watchNowButton.setText(btnText);
		watchNowButton.setBackgroundResource(backId);
		watchNowButton.setCompoundDrawablesRelativeWithIntrinsicBounds(
				leftDrw, 0, 0, 0);
		
		
	}
	
	/**
	 * @desc will attach the player and start it with the given media
	 * @param holder
	 * @param mvpMedia
	 */
	private void startPlayer(MvpItemHolder holder,MvpMedia  mvpMedia){

		currentViewPlayer = (PlayerView)holder.getView(R.id.player_view);
		currentViewPlayer.attachPlayer();
		if (IS_TESTING)
		{
			currentViewPlayer.play(Environment.getExternalStorageDirectory() + "/A.mp4");
		}
		else
		{
			currentViewPlayer.play(mvpMedia);
		}	
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}
	
	/**
	 * @desc method to which the visibility of the list of episodes for the passed position.
	 * @param holder
	 * @param mvpMedia
	 * @param position
	 */
	private void switchEpisodeListVisibility(MvpItemHolder holder, MvpMedia mvpMedia, int position){
		/*
		 * initialize the new state with true.
		 * If no element is placed in the map then this is the first time the button is pressed 
		 * for this element and will need to be revealed.
		 * */
		Boolean newVisibleState = true;
		if ( mSeriesWithActiveEpisodeList.get(position) != null ){
			newVisibleState = !mSeriesWithActiveEpisodeList.get(position);
		}
		mSeriesWithActiveEpisodeList.put(position, newVisibleState);
		setLayoutVisibility(holder, mvpMedia, position);
	}
	
	@Override
	public void destroyPlayer() {
		if ( mCurrentWatchNowButoon != null ){
			setButtonState(mCurrentWatchNowButoon, false);
			mCurrentWatchNowButoon = null;
		}
		super.destroyPlayer();
	}

	
	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels){
		super.onPageScrolled(position,positionOffset, positionOffsetPixels);
		if (mCurrentWatchNowButoon != null){

			int currentPosition = pagerView.getCurrentItem();
			if( mSeriesWithActiveEpisodeList.get(currentPosition) == null){
				/*if the current position is not with a series then reset the button*/
				setButtonState(mCurrentWatchNowButoon, false);
			}
		}
		mCurrentWatchNowButoon = null;
	}
}
