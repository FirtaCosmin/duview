package com.crmsoftware.duview.ui.common;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************



import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.utils.Log;

public class MvpPagListAdapter<TemplateType> extends BaseAdapter implements IMvpGenericAdapter  {
	public static boolean DEBUG = Log.DEBUG; 
	public static final String TAG = MvpPagListAdapter.class.getSimpleName();

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	
	MvpPagContext<TemplateType> paginationContext;
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected MvpAdapterCore adapterCore;
	MvpObject mvpObjContext;
	int subRowsNumber = 1;
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public MvpPagListAdapter(Class  genericType,
			int subRowsNumber) 
	{
		adapterCore = new MvpAdapterCore(this,genericType);
		this.subRowsNumber = subRowsNumber;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void setPaginationContext(MvpObject mvpObjContext) 
	{
		this.mvpObjContext = mvpObjContext;
		
		paginationContext = (MvpPagContext) mvpObjContext.getData();
		paginationContext.bindAdapter(this);
		
		//paginationContext.newPage(0);
		int count = paginationContext.getCount();
		setCount(count);
		if (count == -1)
		{
			paginationContext.newPage(0);
		}
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	protected void finalize() throws Throwable {
		if (DEBUG) Log.m(TAG,this,"finalize");
		if (null == paginationContext)
		{
			return;
		}
		
		paginationContext.unbindAdapter();
		paginationContext.cancelAllRequests();
		paginationContext = null;
		
		super.finalize();
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public Object getItem(int position) {

		return paginationContext.getItem(position);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		View view = null;

		if (subRowsNumber > 1) // multirow
		{
			LayoutInflater inflater = adapterCore.getInflater();
			if (null == inflater)
			{
				adapterCore.setInflater(parent.getContext());
				inflater =  adapterCore.getInflater();
			}
			
			LinearLayout tl = null;
			if (null != convertView)
			{
				tl = (LinearLayout)convertView;
			}
			else
			{
				tl = (LinearLayout)adapterCore.getInflater().inflate(R.layout.horizontal_list_view_table_item,null);				
			}
			
			view  = tl;
			
			for (int i=0; i< subRowsNumber; i++)
			{
				int itemPos = position * subRowsNumber  + i;
				View convertChild = null;
				View viewRow = null;
				
				RelativeLayout row = (RelativeLayout) tl.getChildAt(i);
				
				if (row.getChildCount() > 0) // convert the exisitng child
				{
					convertChild = row.getChildAt(0);
					viewRow = adapterCore.getView(itemPos, convertChild, parent);
				}
				else
				{
					viewRow = adapterCore.getView(itemPos, null, parent);
					row.addView(viewRow);
				}
				
				if (itemPos < adapterCore.getCount())
				{
					viewRow.setVisibility(View.VISIBLE);
					adapterCore.updateView(viewRow);
					viewRow.invalidate();
				}
				else
				{
					viewRow.setVisibility(View.INVISIBLE);
				}
			}
		}
		else
		{
			view = adapterCore.getView(position, convertView, parent);
			adapterCore.updateView(view);
		}
		 
		return view;
	}
	
	
	@Override
	public int getCount() 
	{
		int totalItems = adapterCore.getCount();
		
		if (totalItems <=0 )
		{
			return totalItems;
		}
		
		int columns = totalItems;
		
		if (subRowsNumber > 1)
		{
			columns = columns / subRowsNumber + 1;
		}
		
		//if (DEBUG) Log.d(TAG, "layout: " + rowLayoutResId +  "   totalItems=" + totalItems);
		
		return columns;
	}
	
	
	@Override
	public void setCount(Integer totalNbOfItems) 
	{
		
		adapterCore.setCount(totalNbOfItems);				
	}
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public MvpObject getPaginationContext() {

		return mvpObjContext;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void setGenericListViewListener(
			IMvpGenericListViewListener callback) 
	{
		adapterCore.setGenericListViewListener(callback);
		
	}
		
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public Class getType()
	{
		return adapterCore.getType();
	}
	
	/**
	 * 
	 * @desc will apply the listeners before onListItemUpdateView()
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void receiveClickFrom(int... resIds) {
		adapterCore.receiveClickFrom(resIds);
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void receiveOnItemClick(AbsListView view) 
	{
		adapterCore.receiveOnItemClick(view);
	}


	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void receiveTouchFrom(int... resIds) {
		adapterCore.receiveTouchFrom(resIds);
		
	}

	@Override
	public void notifyDataSetChange() {
		super.notifyDataSetChanged();
		
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	public int getRowsNumber() {
		return subRowsNumber;
	}
	
	@Override
	 public boolean areAllItemsEnabled() {
	     return false;
	 }

	 @Override
	 public boolean isEnabled(int position) {
	     return false;
	 }


	@Override
	public Object getTag() {
		return adapterCore.getTag();
	}


	@Override
	public void setTag(Object data) 
	{
		adapterCore.setTag(data);	
	}


	@Override
	public void addView(MvpItemHolder itemHolder, View view) {
		adapterCore.addView( itemHolder,  view);
		
	}

}
