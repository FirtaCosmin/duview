package com.crmsoftware.duview.ui.common;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.ui.MvpFragment;
import com.crmsoftware.duview.utils.Log;



public class UnderConstrFragment extends MvpFragment  {
	public static boolean DEBUG = Log.DEBUG; 
	public static final int TESTUSER = 1; 
	public static final String TAG_DATA= "data" ; 
	
	public final static String TAG = UnderConstrFragment.class.getSimpleName();

	View holowView;
	private Animation fadeInAnimation;
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public static Bundle buildData(String sText) 
	{
		Bundle bundle = new Bundle();
		bundle.putString(TAG_DATA, sText);
		
		return bundle;
	}
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,

			Bundle savedInstanceState) 
	{		
		if (DEBUG) Log.m(TAG,this,"onCreateView");
		
		View view = setContentView(R.layout.underconstr_view,container,inflater);
		
		MvpViewHolder holder = getHolder();
		
		getMvpActivity().setMenuEnabled(true);
		getMvpActivity().setActionBarVisibility(View.VISIBLE);
		getMvpActivity().setHomeButtonActionBar(true);
		getMvpActivity().setTitle(MainApp.getRes().getString(R.string.under_construction));
		
		Bundle data = getData();
		
		
		if (data != null)
		{
			String sText= data.getString(TAG_DATA);
			holder.setText(R.id.feature_name, sText);
		}
		
		
		holowView = holder.getView(R.id.image_back);
		
		fadeInAnimation = AnimationUtils.loadAnimation(getActivity(), R.animator.view_fade_out);
        fadeInAnimation.setAnimationListener(new AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                        // TODO Auto-generated method stub

                }
                @Override
                public void onAnimationRepeat(Animation animation) {
                        // TODO Auto-generated method stub
                }
                @Override
                public void onAnimationEnd(Animation animation) {
               	 holowView.startAnimation(fadeInAnimation);

                }
        });
        holowView.startAnimation(fadeInAnimation);
		
		return view;
	}
}
