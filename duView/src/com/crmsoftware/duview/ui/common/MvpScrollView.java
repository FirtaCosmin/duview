package com.crmsoftware.duview.ui.common;

import com.crmsoftware.duview.utils.Log;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;

public class MvpScrollView extends View{

	/**/
	private static String TAG = MvpScrollView.class.getSimpleName();
	private static Boolean DEBUG = Log.DEBUG;
	
	
	/**
	 * @desc the current position where the scroll must be drawn to
	 */
	private int mCurrentPosition = 0;
	/**
	 * @desc the physical with in pixels that the entire scroll bar will have
	 */
	private int mScrollBarWidth = 0;
	/**
	 * @desc the physical height i pixels of the entire scroll bar
	 */
	private int mScrollBarHeight = 0;
	/**
	 * @desc the physical width in pixels of the scroll. 
	 * 		 The height of the scroll will be the same as the scroll bar height
	 */
	private int mScrollWidth = 0;
	/**
	 * @desc the rectangle that represents the scroll
	 */
	private Rect mScrollRect =  new Rect();
	/**
	 * @desc the rectangle that represents the entire scroll bar
	 */
	private Rect mScrollBarRect = new Rect();
	/**
	 * @desc the number of scrolled pixels from the fullPxNo that 
	 * will make the scroll move one pixel
	 */
	private float mScrollPx = 1;
	
	
	/**
	 * @desc the number of pixels to scroll
	 */
	private int mFullPxNo = 0;
	
	/**
	 * @desc flag set if the scroll needs to be hidden
	 */
	private boolean mScrollHidden = true;
	
	private static final int DELAY_TO_HIDE_VIEW = 500;
	final private Handler mHandlerToHideView = new Handler();
	final private Runnable mRunnableToHideView = new Runnable() {
		
		@Override
		public void run() {
			hide();
		}
	};
	
	
	
	/**
	 * @desc this is the paint objects that will characterize the scroll drawing
	 */
	private Paint mScrollBarPaint = new Paint();
	private Paint mScrollPaint = new Paint();
	private static final int DEFAULT_SCROLL_BAR_COLOR = Color.TRANSPARENT;
	private static final int DEFAULT_SCROLL_COLOR = 0xBB888888;/*GREY with transparency*/
	
	public MvpScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		init();
	}
	
	
	/*
	 * OVERRIDES FROM View
	 */
	/* (non-Javadoc)
	 * @see android.view.View#onSizeChanged(int, int, int, int)
	 * @desc will calculate the position of the rectangles for the scroll bar and the scroll
	 */
	@Override
	protected void  onSizeChanged (int w, int h, int oldw, int oldh) {
		/*get scroll bar width compensating for the padding*/
		mScrollBarWidth = w - getPaddingRight() - getPaddingLeft();
		/*get scroll bar height conpensating for the padding*/
		mScrollBarHeight = h - getPaddingBottom() - getPaddingTop();
		/*calculate scroll bar rectangle*/
		mScrollBarRect.top = getPaddingTop();
		mScrollBarRect.left = getPaddingLeft();
		mScrollBarRect.bottom = mScrollBarRect.top + mScrollBarHeight;
		mScrollBarRect.right = mScrollBarRect.left + mScrollBarWidth;

		calculateScrollSize();

	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if ( !mScrollHidden ){
			canvas.drawRect(mScrollBarRect, mScrollBarPaint);
			canvas.drawRect(mScrollRect, mScrollPaint);
		}
		
		   
	}
	
	
	
	/*
	 * PUBLIC METHODS
	 * */
	/**
	 * @desc this method will set the number of pixel to scroll. 
	 * This value will be scaled to the width of the scroll bar
	 * @param scrollPx
	 */
	public void setScrollPx(int scrollPx){
		mFullPxNo = scrollPx;
		calculateScrollSize();
	}
	
	/**
	 * @desc will scroll to the passed position.
	 * The passed position is relative to the number of pixels to scroll 
	 * not to the scroll bar width. The real distance that the scroll will move to will
	 *  be scaled to the scroll bar width using mScrollPx value. 
	 * @param newPosition
	 */
	public void scrollTo(int newPosition){
		if (DEBUG) Log.d(TAG, "Scroll To new Position: "+ newPosition);
		if ( mScrollPx != 0 ){
			int newPos = (int)(newPosition*mScrollPx);
			if (newPos != mCurrentPosition){
				mCurrentPosition = newPos;
				redraw();
			}else{
				/*do not update the scroll just display it*/
				showScroll(true);
			}
		}
	}
	
	/**
	 * @desc will move the scroll with the passed number of pixels.
	 * The passed value is relative to the number of pixels to scroll not to the scroll bar width.
	 * The real distance that the scroll will move will be scaled to the scroll bar 
	 * width using mScrollPx value. 
	 * @param dx
	 */
	public void scrollBy(int dx){
		if (DEBUG) Log.d(TAG, "Scroll by new Position: "+ dx);
		if ( mScrollPx != 0 ){
			int newPos = mCurrentPosition + (int)(dx*mScrollPx);
			if (newPos != mCurrentPosition){
				mCurrentPosition = newPos;
				redraw();
			}else{
				/*do not update the scroll just display it*/
				showScroll(true);
			}
		}
	}

	
	/*
	 *       PROTECTED 
	 * */
	
	/**
	 * @desc will initialize the scroll bar Paints
	 * @param scrollBarColor
	 * @param ScrollColor
	 */
	protected void init(){
		
		
		mScrollPaint.setStyle(Paint.Style.FILL);
		mScrollPaint.setColor(DEFAULT_SCROLL_COLOR);
		
		mScrollBarPaint.setStyle(Paint.Style.FILL);
		mScrollBarPaint.setColor(DEFAULT_SCROLL_BAR_COLOR);
	}
	
	
	/*
	 *       PRIVATE 
	 * */
	
	/**
	 * @desc will calculate the scroll width, scroll passe and rectangle
	 */
	private void calculateScrollSize(){
		/*calculate the scroll width*/
		if ( mFullPxNo <= mScrollBarWidth ){
			mScrollWidth = mScrollBarWidth;
			mScrollPx = 0;
		}else{
			mScrollWidth = mScrollBarWidth * mScrollBarWidth / mFullPxNo;
			mScrollPx = (float)mScrollBarWidth / mFullPxNo;
		}
		/*calculate the scroll rectangle*/
		calculateScrollRect();
	}
	
	
	/**
	 * @desc will calculate the rectangle that represents the scroll of the scroll bar depending on 
	 * 				the number of positions 
	 * 				the total width of the scroll bar and 
	 * 				the current position of the scroll
	 */
	private void calculateScrollRect(){
		/*calculate scroll rectangle*/
		/*scroll will be the same height of the entire scroll bar*/
		mScrollRect.top = mScrollBarRect.top;
		mScrollRect.left = mScrollBarRect.left + mCurrentPosition;
		mScrollRect.bottom = mScrollBarRect.bottom;
		mScrollRect.right = mScrollRect.left + mScrollWidth;
		/*if the scroll exceeds the scroll bar */
		if ( mScrollRect.right > mScrollBarRect.right){
			mScrollRect.right = mScrollBarRect.right;
		}
		
		if (DEBUG) Log.d(TAG, "recalculated scrollrect: "+mScrollRect);
	}
	
	/**
	 *  @desc method used to recalculate the Scroll rectangle and trigger the scroll to be redrawn 
	 */
	private void redraw(){
		/*recalculate the rectangle of the scroll*/
		calculateScrollRect();
		/*show the view*/
		showScroll(false);
		/*trigger the view to be redrawn*/
		draw();
	}
	
	private void showScroll(boolean triggerRedraw){
		mScrollHidden = false;
		/*start or restart the thread to hide the scroll*/
		mHandlerToHideView.removeCallbacks(mRunnableToHideView);
		mHandlerToHideView.postDelayed(mRunnableToHideView, DELAY_TO_HIDE_VIEW);
		if ( triggerRedraw ){
			draw();
		}
	}
	
	/**
	 * @desc will trigger the view to redraw its self
	 */
	private void draw(){
		invalidate();
		requestLayout();
	}
	
	/**
	 * @desc will hide the scroll view and redraw the view
	 */
	private void hide(){
		mScrollHidden = true;
		draw();
	}

}
