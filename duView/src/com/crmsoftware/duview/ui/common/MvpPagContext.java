package com.crmsoftware.duview.ui.common;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import android.os.Handler;

import com.crmsoftware.duview.core.common.HttpReqParams;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.core.common.TaskPageReqCache;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.data.MvpPage;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;

public class MvpPagContext<TemplateType>  implements Observer{
	public static boolean DEBUG = Log.DEBUG; 
	public static final String TAG = MvpPagContext.class.getSimpleName();

	public static int PAGESIZE = 10;
	public static boolean STARTWITH_ONE = false;	
	public static boolean STARTWITH_ZERO = true;	
	
	private long updateTime;
	private long PERIOD = 500;
	
	public static Handler handler;
	private Runnable update = null;
	
	public class PageContext implements ITaskReturn
	{
		public MvpObject mvpObject;
		public Task task;
		
		/**
		 * 
		 *
		 * @param  
		 * @return      
		 * @see         
		 */		
		PageContext(MvpObject mvpObject)
		{
			this.mvpObject = mvpObject;
		}
		
		/**
		 * 
		 *
		 * @param  
		 * @return      
		 * @see         
		 */		
		@Override
		protected void finalize() throws Throwable {
			
			if (null != task)
			{
				task.cancel(false);
				task = null;
			}

			super.finalize();
		}
		
		/**
		 * 
		 *
		 * @param  
		 * @return      
		 * @see         
		 */		
		public MvpPage getPageData()
		{
			if (null == mvpObject)
			{
				return null;
			}
			
			try
			{
				Object data = mvpObject.getData();
				if (null == data)
				{
					return null;
				}
				return (MvpPage) data;
			}
			catch (Exception e)
			{
				
			}
			return null;
		}
		
		
		/**
		 * 
		 *
		 * @param  
		 * @return      
		 * @see         
		 */		
		public void execute(Task task)
		{
			if (null != this.task)
			{
				return;
			}
			
			this.task = task;
			this.task.execute();
		}
		
		/**
		 * 
		 *
		 * @param  
		 * @return      
		 * @see         
		 */		
		public void cancelTask()
		{
			if (null == task)
			{
				return;
			}
			task.cancel(false);
			task = null;
		}
		
		/**
		 * 
		 *
		 * @param  
		 * @return      
		 * @see         
		 */		
		public Object getItem(int index)
		{
			MvpPage page = getPageData();
			if (null == page)
			{
				return null;
			}
			
			return page.getItem(index);
		}
		
		
		/**
		 * 
		 *
		 * @param  
		 * @return      
		 * @see         
		 */
		@Override
		public void onReturnFromTask(Task task, boolean canceled) 
		{
			if (DEBUG) Log.m(TAG,this,"onReturnFromTask");
			
			if (task.isError())
			{
				return;		//TODO  notify UI
			}
			
			this.task = null;
		}
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */
	public Map<Integer, PageContext> mapPages =  Collections.synchronizedMap(new HashMap<Integer, PageContext>());
	
	private int firstVisibleItem = 0;
	private String sURL="";
	private Class pagHeaderType;
	private HttpReqParams httpParams;
	private String sCacheSuffix="";
	private IMvpGenericAdapter adapter;
	private int totalResults=-1;
	private long dataLifeTime = 0;
	private boolean firstIndexZero = false;	
	
	/**
	 * @param pagHeaderType
	 * @param firstIndexZero
	 * @param sURL
	 * @param httpParams
	 * @param sCacheSuffix - id for the mvpObject that holds the paginator
	 * @param dataLifeTime - the time the cashed file cand be used.
	 * @return
	 */
	public static MvpObject getContext(
			Class pagHeaderType, 
			boolean firstIndexZero,
			String sURL,
			HttpReqParams httpParams,
			String sCacheSuffix,
			long dataLifeTime)
	{
		MvpObject mvpObjContext = MvpObject.newReference(sCacheSuffix);
		MvpPagContext context = (MvpPagContext) mvpObjContext.getData();
		if (null == context)
		{
			context = new MvpPagContext();
			mvpObjContext.setData(context);
		}
		
		context.setData(pagHeaderType,firstIndexZero, sURL, httpParams, sCacheSuffix, dataLifeTime);
		
		return mvpObjContext;
	}
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public MvpPagContext()
	{
		
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void setData(
			Class pagHeaderType,
			boolean firstIndexZero,
			String sURL,
			HttpReqParams httpParams,
			String sCacheSuffix,
			long dataLifeTime) 
	{
		this.pagHeaderType = pagHeaderType;
		this.firstIndexZero = firstIndexZero;
		this.sURL = sURL;
		this.httpParams = httpParams;
		this.sCacheSuffix = sCacheSuffix;
		this.dataLifeTime = dataLifeTime;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	protected void finalize() throws Throwable {
		if (DEBUG) Log.m(TAG,this,"finalize");
		
		cancelAllRequests();
		super.finalize();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public Object getItem(int index)
	{
		if (DEBUG) Log.m(TAG,this,"getItem");
		
		int page = index / PAGESIZE;
		int position = index % PAGESIZE;
		
		PageContext pageContext = mapPages.get(page);
		if (null == pageContext)
		{
			newPage(page); // case when the item is requesting the page before pagination
			return  null;
		}
				
		return pageContext.getItem(position);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void bindAdapter(IMvpGenericAdapter adapter)
	{
		this.adapter = adapter;
		this.adapter.setCount(-1); // this is reseting the adapter (in case that list is reused)
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public IMvpGenericAdapter unbindAdapter()
	{

		IMvpGenericAdapter retAdapter = this.adapter;
		this.adapter = null;
		
		return retAdapter;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public int getCount()
	{
		return totalResults;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void newPage(int index)
	{
		if (DEBUG) Log.m(TAG,this,"newPage");
		
		if (DEBUG) Log.d(TAG,"page " + index);
		
		if (null == adapter)
		{
			if (DEBUG) Log.d(TAG,"adapter is null ");
			return;
		}
		
		synchronized (mapPages) 
		{
			PageContext pageContext = mapPages.get(index);
			
			if (null != pageContext)
			{
				if (DEBUG) Log.d(TAG,"page requested already " + index);
				return;
			}
		}
				
		int start = index * PAGESIZE + (firstIndexZero ? 0 : 1);
		int end = (index+1)*PAGESIZE - (firstIndexZero ? 1 : 0) ;
		MvpObject mvpObject = MvpObject.newReference(Utils.formatLink(sCacheSuffix+"."+start+"_"+end+".txt"));

		HttpReqParams reqParams = httpParams.clone();
		reqParams.setRange(start, end);
		
		PageContext pageContext = new PageContext(mvpObject);		
		TaskPageReqCache<TemplateType> task = new TaskPageReqCache<TemplateType>(pageContext, adapter.getType(), pagHeaderType, sURL, reqParams, mvpObject, dataLifeTime);

		pageContext.execute(task);
		
		synchronized (mapPages) 
		{			
			mapPages.put(index, pageContext);
		}
		
		mvpObject.addObserver(this);
		task.execute();
	}

	

	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void cancelAllRequests()
	{
		if (DEBUG) Log.m(TAG,this,"cancelAllRequests");
		
		synchronized (mapPages) 
		{
		    Iterator it = mapPages.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pairs = (Map.Entry)it.next();
		        
		        PageContext pageContext = (PageContext) pairs.getValue();
		        pageContext.cancelTask();
		        
				if (null != pageContext.mvpObject)
				{
					pageContext.mvpObject.deleteObserver(this);
				}
		    }
		}
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void setFirstVisibleItem(int firstVisibleItem)
	{
		this.firstVisibleItem = firstVisibleItem;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public int getFirstVisibleItem()
	{
		return firstVisibleItem;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void deletePage(int page)
	{
		if (DEBUG) Log.m(TAG,this,"deletePage");
		
		if (DEBUG) Log.d(TAG,"page " + page);
		
		synchronized (mapPages) 
		{
			PageContext pageContext  = mapPages.get(page);
			boolean removePage = true;
			if (null != pageContext)
			{
				if (pageContext.getPageData() != null)
				{
					removePage = false;
				}
				pageContext.mvpObject.deleteObserver(this);
				pageContext.cancelTask();
			}
			
			if (removePage)
			{
				mapPages.remove(page);
			}
		}	
	}
	
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void update(Observable observable, Object data) {
		
		if (DEBUG) Log.m(TAG,this,"update");
		
		 if (observable instanceof MvpObject)
		 {
				MvpObject mvpObject = (MvpObject) observable;
				
				final MvpPage<TemplateType> mvpPage = (MvpPage) mvpObject.getData();
				if (null == mvpPage)
				{
					return;
				}
				
				handler.post(new Runnable() {
					
					@Override
					public void run() {
						totalResults = mvpPage.getTotalResults();
						if (DEBUG) Log.d(TAG,"\t totalResults: " + totalResults);
						if (null != adapter)
						{
							adapter.setCount(totalResults);
							//refresh();				
						}
						
					}
				});

		 }
	}

}

