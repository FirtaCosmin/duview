package com.crmsoftware.duview.ui.common;

import android.view.View;

import com.crmsoftware.duview.data.MvpObject;



public interface IMvpGenericAdapter {

	public void notifyDataSetChange();
	public void setCount(Integer totalNbOfItems);
	public int  getCount();
	public Class getType();
	public Object getItem(int position);
	
	public void receiveClickFrom(int... resIds);
	public void receiveTouchFrom(int... resIds);	
	
	public void addView(MvpItemHolder itemHolder, View view);	

	public MvpObject getPaginationContext();
	public Object getTag();
	public void setTag(Object data);
	
	public void setGenericListViewListener(IMvpGenericListViewListener callback);

}
