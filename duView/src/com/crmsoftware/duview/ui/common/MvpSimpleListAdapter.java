package com.crmsoftware.duview.ui.common;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************


import com.crmsoftware.duview.data.MvpObject;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;


public class MvpSimpleListAdapter extends BaseAdapter implements IMvpGenericAdapter {
	public static final String TAG = MvpSimpleListAdapter.class.getSimpleName();

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected MvpAdapterCore adapterCore;
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public MvpSimpleListAdapter(
			Class  genericType,
			int rowLayoutResId) 
	{	
		adapterCore = new MvpAdapterCore(this,genericType,rowLayoutResId);

	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	public MvpSimpleListAdapter(
			Class  genericType,
			Object parent) 
	{	
		adapterCore = new MvpAdapterCore(this, genericType, parent);

	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View view = adapterCore.getView(position, convertView, parent);
		adapterCore.updateView(view);
		
		return view;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public int getCount() {
		return adapterCore.getCount();
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public Object getItem(int position) {
		return adapterCore.getItem(position);
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public long getItemId(int position) {
		return adapterCore.getItemId(position);
	}
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void setCount(Integer totalNbOfItems) 
	{
		adapterCore.setCount(totalNbOfItems);
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void setGenericListViewListener(
			IMvpGenericListViewListener callback) 
	{
		adapterCore.setGenericListViewListener(callback);
		
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public Class getType()
	{
		return adapterCore.getType();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void receiveClickFrom(int... resIds) {
		adapterCore.receiveClickFrom(resIds);
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void receiveOnItemClick(AbsListView view) 
	{
		adapterCore.receiveOnItemClick(view);
	}


	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void receiveTouchFrom(int... resIds) {
		adapterCore.receiveTouchFrom(resIds);
		
	}

	@Override
	public void notifyDataSetChange() {
		super.notifyDataSetChanged();
		
	}


	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public MvpObject getPaginationContext() 
	{
		return null;
		
	}
	
	@Override
	public Object getTag() {
		return adapterCore.getTag();
	}


	@Override
	public void setTag(Object data) 
	{
		adapterCore.setTag(data);	
	}

	@Override
	public void addView(MvpItemHolder itemHolder, View view) 
	{
		adapterCore.addView( itemHolder,  view);
		
	}


}
