
package com.crmsoftware.duview.ui.common;

import java.util.LinkedList;
import java.util.Queue;

import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;

import android.content.Context;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;

public class HorizontalAdapterView extends AdapterView<ListAdapter> {
	public static boolean DEBUG = Log.DEBUG ; 
	public static boolean VERBOUSE = Log.VERBOUSE ; 
	public static final String TAG = HorizontalAdapterView.class.getSimpleName();
	
	protected static final String ITEM_WIDTH = "item_width";
	private static final String DIVIDER_WIDTH = "divider_width";
	protected ListAdapter mAdapter;
	protected int mLeftViewIndex = -1;
	protected int mRightViewIndex = 0;
	protected int mCurrentX = 0;
	protected int mNextX;
	protected int mMaxX = Integer.MAX_VALUE;
	protected int mDisplayOffset = 0;
	protected Queue<View> mRemovedViewQueue = new LinkedList<View>();
	protected int pageRange[] = {-1,-1};
	public static int PAGESIZE = 10;

	protected boolean mDataChanged = false;
	protected float cellWidth = 0;
	protected int memCount = 0;
	protected int mTotalOffset = 0;
	

	protected float separatorWidth = 0;
	protected int rowsNumber = 1;
	
	protected String sTag = "";
	
	protected OnItemSelectedListener mOnItemSelected;
	protected OnItemClickListener mOnItemClicked;
	protected OnItemLongClickListener mOnItemLongClicked;
	

	private MvpScrollView mScrollView;

	public HorizontalAdapterView(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (DEBUG) Log.m(TAG,this, "ctor()");
		initView();
		initParam(attrs);
	}
	
	@Override
	protected void finalize() throws Throwable {
		if (DEBUG)  Log.m(TAG,this, "finalize ");
		
		super.finalize();
	}
	
	protected void initParam(AttributeSet attrs) {
		Resources r = getResources();
		int value = attrs.getAttributeIntValue(null, DIVIDER_WIDTH, 0);
		separatorWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, r.getDisplayMetrics());
		
		value = attrs.getAttributeIntValue(null, ITEM_WIDTH, 0);
		cellWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, r.getDisplayMetrics());
	}

	public synchronized void initView() {
		if (VERBOUSE) Log.d(TAG, "initView ");
		
		mLeftViewIndex = -1;
		mRightViewIndex = 0;
		mDisplayOffset = 0;
		mCurrentX = 0;
		mNextX = 0;
		mMaxX = Integer.MAX_VALUE;
		mTotalOffset = 0;
		mDataChanged = false;
		
		removeAndKeepViewsFromLayout();
	}
	
	protected void removeAndKeepViewsFromLayout()
	{
		int count = getChildCount();
		for (int i=0;i<count; i++)
		{
			View view = getChildAt(i);
			
			mRemovedViewQueue.offer(view);
		}
		
		removeAllViewsInLayout();
	}
	
	
	
	protected DataSetObserver mDataObserver = new DataSetObserver() {

		@Override
		public void onChanged() {
			synchronized(HorizontalAdapterView.this){
				if (DEBUG) Log.d(TAG, "DataSetObserver::onChanged ");
				mDataChanged = true;
			}
			/*update the scroll view's dimension*/
			updateScrollViewDimension();
			//invalidate();
			requestLayout();
		}

		@Override
		public void onInvalidated() {
			if (DEBUG) Log.d(TAG, "DataSetObserver::onInvalidated ");
			reset();
		}
		
	};

	@Override
	public ListAdapter getAdapter() {
		return mAdapter;
	}

	@Override
	public View getSelectedView() {
		//TODO: implement
		return null;
	}

	@Override
	public void setAdapter(ListAdapter adapter) 
	{
		initView();
		
		if(mAdapter != null) 
		{
			mAdapter.unregisterDataSetObserver(mDataObserver);
		}
		
		if (adapter instanceof MvpPagListAdapter)
		{		
			rowsNumber = ((MvpPagListAdapter)adapter).getRowsNumber();
		}
		
		mAdapter = adapter;
		if (mAdapter != null)
		{
			mAdapter.registerDataSetObserver(mDataObserver);
		}
	}
	
	protected synchronized void reset(){
		initView();
        requestLayout();
	}

	@Override
	public void setSelection(int position) {
		int pos = position;
		if (pos < 0)
		{
			pos = 0;
		}
		
		int childWidth = getItemViewWidth(pos);

		int x =(int) (pos * (childWidth+separatorWidth));
		if (x > 0)
		{
			scrollBy(x);
		}
	} 
	
	private void addAndMeasureChild(final View child, int viewPos, int width) {
		

		
		LayoutParams params = child.getLayoutParams();
		if(params == null) {
			params = new LayoutParams( LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		}
		
		addViewInLayout(child, viewPos, params, true);
		child.measure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.AT_MOST),
				MeasureSpec.makeMeasureSpec(getHeight(), MeasureSpec.AT_MOST));
		child.invalidate();
		
	}

	
	protected void arangeViews()
	{
		int dx = mCurrentX - mNextX;
		

		if (VERBOUSE) Log.m(TAG,this, "\n\n############ ARANGE VIEWS ##################" +"\n"+sTag +"\n before mTotalOffset = " + mTotalOffset + "\n dx = " + dx );

		mTotalOffset -= dx;

		if (VERBOUSE) Log.d(TAG, "\n\tmNextX=" + mNextX + "\n\tmCurrentX=" + mCurrentX + "\n\tmTotalOffset = " + mTotalOffset + "\n\tmDisplayOffset" + mDisplayOffset);
			
		if (offsetOutOfView(dx))
		{	
			if (VERBOUSE) Log.m(TAG,this, "\n*************** resposition items !!!");
			repositionItems();
		}
		else
		{
			if (VERBOUSE) Log.m(TAG,this, "\n*************** move items !!!");
			removeNonVisibleItems(dx);
			fillList(dx);
			positionItems(dx);
		}
		
		if (VERBOUSE) Log.m(TAG,this, "\t\t\t child count = " + getChildCount() + "\tqueued = " + mRemovedViewQueue.size());
		
		if (mAdapter instanceof IMvpGenericAdapter)
		{
			Utils.checkPageRange((IMvpGenericAdapter) mAdapter, PAGESIZE, (mLeftViewIndex +1)*rowsNumber, (mRightViewIndex)*rowsNumber -1 ,pageRange, TAG, VERBOUSE);
		}
		mCurrentX = mNextX;
		
		if (VERBOUSE) Log.d(TAG, "##############################");
		/*update the scroll view with the new position*/
		moveScrollView(mCurrentX);
	}
	
	protected boolean offsetOutOfView(int deltaX)
	{
		int count = getChildCount();
		if (count > 0)
		{
			View viewStart = getChildAt(0);
			View viewEnd = getChildAt(count-1);
			
			if (viewStart.getLeft()-deltaX >= getWidth() ||
					viewEnd.getRight()-deltaX <=0)
			{
				return true;
			}	
		}
		else
		{
			return true;
		}
		
		return false;
	}
	
	protected int getItemViewWidth(int pos)
	{	
		return (int)cellWidth;
	}
	
	protected void repositionItems()
	{
		//if (VERBOUSE) Log.d(TAG, "fillList dx= " + dx + " \n mCurrentX= " + mCurrentX);
		removeAndKeepViewsFromLayout();
		
		if (null == mAdapter)
		{
			return;
		}
		
		if (mAdapter.getCount() ==0)
		{
			return;
		}
		

		IMvpGenericAdapter adapter = (IMvpGenericAdapter)mAdapter;

		mLeftViewIndex = -1;
		mRightViewIndex = 0;
		int offset = 0;
		
		int i=0;
		for (i=0; i<mAdapter.getCount(); i++)
		{
			int childWidth = getItemViewWidth(i) + (int)separatorWidth;
			offset += childWidth;
			int diff = mTotalOffset - offset;
			if (diff < 0)
			{
				mDisplayOffset = -(diff + childWidth);
				mLeftViewIndex = i-1;
				mRightViewIndex = i+1;
				break;
			}	
		}
		
		if (offset < mTotalOffset + getWidth())
		{
			i++;
			for (; i < mAdapter.getCount(); i++)
			{
				mRightViewIndex = i;
				
				int childWidth = getItemViewWidth(i) + (int)separatorWidth;
				offset += childWidth;
				
				if (offset > mTotalOffset + getWidth())
				{
					break;
				}
				
			}	
			mRightViewIndex++;
		}
		
		if (mRightViewIndex>mAdapter.getCount())
		{
			mRightViewIndex = mAdapter.getCount();
		}
		
		if (VERBOUSE) Log.d(TAG, "mLeftViewIndex= " + mLeftViewIndex + " \n mRightViewIndex= " + mRightViewIndex + " \n mDisplayOffset= " + mDisplayOffset + " adapter.count()="+mAdapter.getCount());
		int left = mDisplayOffset;
		for (i=mLeftViewIndex+1; i<mRightViewIndex; i++)
		{
			View updateView = mRemovedViewQueue.poll();
			View child = mAdapter.getView(i, updateView, this);
			
			int childWidth = getItemViewWidth(i) + (int)separatorWidth;
			addAndMeasureChild(child,-1,childWidth);
			
			child.layout(left, 0, left + childWidth, child.getMeasuredHeight());
			
			left += childWidth ;
		}	

	}

	
	protected void fillList(final int dx) {
	

		int edge = 0;
		View child = getChildAt(getChildCount()-1);
		if(child != null) {
			edge = child.getRight();
		}
		fillListRight(edge, dx);
		
		edge = 0;
		child = getChildAt(0);
		if(child != null) {
			edge = child.getLeft();
		}
		fillListLeft(edge, dx);
	}
	

	
	protected void fillListRight(int rightEdge, final int dx) {
		
		
		int width = getWidth();
		
		if (VERBOUSE) Log.d(TAG, "\nfillListRight dx= " + dx + " \n rightEdge= " + rightEdge +   "\n width = " + width);
		
		
		while(rightEdge + dx < getWidth() && mRightViewIndex < mAdapter.getCount()) {
			
			
			int childWidth = getItemViewWidth(mRightViewIndex) + (int)separatorWidth;
			View updateView = mRemovedViewQueue.poll();
			View child = mAdapter.getView(mRightViewIndex, updateView, this);
			addAndMeasureChild(child, -1, childWidth);
			if (VERBOUSE) Log.d(TAG, "mRightViewIndex = " + mRightViewIndex  + "\n width = " + width  + " \n rightEdge= " + rightEdge);



			rightEdge += childWidth;
			if(mRightViewIndex == mAdapter.getCount()-1){
				mMaxX = mCurrentX + rightEdge - getWidth();
			}
			if (mMaxX < 0) {
				mMaxX = 0;
			}
			mRightViewIndex++;
		}

		/*
		 * if the number of elements in the listview is the same as the number of elements in the adapter 
		 * then calculate the maximum x number of pixels
		 * */
		if ( mRightViewIndex ==  mAdapter.getCount() && mRightViewIndex == getChildCount() ){
			mMaxX = mCurrentX + rightEdge - getWidth();
		}
		
	}
	
	protected void fillListLeft(int leftEdge, final int dx) {
		if (VERBOUSE) Log.d(TAG, "\nfillListLeft dx= " + dx + " \n leftEdge= " + leftEdge);
		
		while(leftEdge + dx > 0 && mLeftViewIndex >= 0) {
			int childWidth = getItemViewWidth(mLeftViewIndex) + (int)separatorWidth;
			View updateView = mRemovedViewQueue.poll();
			View child = mAdapter.getView(mLeftViewIndex,updateView , this);
			addAndMeasureChild(child, 0, childWidth);
			
			leftEdge -= childWidth;
			mLeftViewIndex--;
			mDisplayOffset -= childWidth;
		}
	}
	
	protected void removeNonVisibleItems(final int dx) {
		View child = getChildAt(0);
		while(child != null && child.getRight() + dx <= 0) {
			mLeftViewIndex++;
			int childWidth = getItemViewWidth(mLeftViewIndex) + (int)separatorWidth;
			mDisplayOffset += childWidth;
			mRemovedViewQueue.offer(child);
			removeViewInLayout(child);
			child = getChildAt(0);		
		}
		
		child = getChildAt(getChildCount()-1);
		while(child != null && child.getLeft() + dx >= getWidth()) {
			mRemovedViewQueue.offer(child);
			removeViewInLayout(child);
			mRightViewIndex--;
			child = getChildAt(getChildCount()-1);
		}
	}
	
	protected void positionItems(final int dx) {
		if(getChildCount() > 0){
			mDisplayOffset += dx;
			int left = mDisplayOffset;
			int pos = mLeftViewIndex+1;
			for(int i=0;i<getChildCount();i++,pos++){
				View child = getChildAt(i);
				int childWidth = getItemViewWidth(pos);
				child.layout(left, 0, left + childWidth, child.getMeasuredHeight());
				left += childWidth + separatorWidth;
			}
		}
	}
	
	public synchronized void scrollBy(final int x) 
	{
		
		if (VERBOUSE) Log.m(TAG,this, "\n"+sTag + "\n scrollBy dx = " + x + "\nmTotalOffset = " + mTotalOffset + "mDisplayOffset = \n "+ mDisplayOffset);
		
		mCurrentX = 0;
		mNextX = -x;
		requestLayout();
	}

	public float getCellWidth() {
		return cellWidth;
	}
	
	public void setCellWidth(int width) {
		cellWidth = width;
	}
	
	public void setCellSpace(int width) {
		separatorWidth = width;
	}
	
	
	/**
	 * @desc method to link the adapter view to a scroll View
	 * @param theScrollView
	 */
	public void linkScrollView(MvpScrollView theScrollView){
		mScrollView = theScrollView;
	}
	
	/**
	 * @desc will pass the new position to the scroll view
	 * @param newX
	 */
	private void moveScrollView(int newX){
		if( mScrollView != null ){
			mScrollView.scrollTo(newX);
		}
	}
	/**
	 * @desc will set the maximum size to scroll 
	 */
	private void updateScrollViewDimension(){
		if ( mScrollView != null ){
			/*
			 * the maximum size is element count of cell width and 
			 * element count -1 of cell separators
			 * */
			mScrollView.setScrollPx( mAdapter.getCount()      * (int)cellWidth + 
					                (mAdapter.getCount() - 1) * (int)separatorWidth);
		}
	}
}
