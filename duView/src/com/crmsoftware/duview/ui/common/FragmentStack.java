package com.crmsoftware.duview.ui.common;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************



import java.util.Stack;

import com.crmsoftware.duview.utils.Log;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public class FragmentStack implements Parcelable {
	/**
	 * 
	 */
	
	public static boolean DEBUG = Log.DEBUG; 
	public final static String TAG = FragmentStack.class.getSimpleName();
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public class FragmentData implements Parcelable
	{
		/**
		 * 
		 */
		
		public Bundle 	fragmentData = new Bundle();
		public String 	fragmentTag = "";
		public Class 	fragmentType;
		
		public FragmentData( Class fragmentType,String fragmentTag, Bundle fragmentData) {
			this.fragmentTag = fragmentTag;
			this.fragmentType = fragmentType;
			this.fragmentData = fragmentData;
			
			if (this.fragmentData == null)
			{
				this.fragmentData = new Bundle();
			}
		}

		public FragmentData(Parcel in)
		{
			fragmentTag = in.readString();
			
			String className = in.readString();
			try {
				fragmentType = Class.forName(className);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			fragmentData = in.readParcelable(Bundle.class.getClassLoader());
		}
		
		@Override
		public int describeContents() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeString(fragmentTag);
			dest.writeString(fragmentType.getName());
			fragmentData.writeToParcel(dest, flags);
		}
	}
	
	Stack<FragmentData> stackList = new Stack<FragmentData>();

	public FragmentStack()
	{
		
	}
	
	public FragmentStack(Parcel in)
	{
		in.readList(stackList, FragmentData.class.getClassLoader());
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		
			dest.writeList(stackList);
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void clear()
	{
		stackList.clear();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void push(Class fragmentType,String fragmentTag,Bundle data)
	{
		FragmentData fd = new FragmentData(fragmentType,fragmentTag,data);
		stackList.push(fd);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public FragmentData pop()
	{
		FragmentData fd = (FragmentData) stackList.pop();
		
		return fd;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public int getBackStackCount()
	{
		
		return stackList.size();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public FragmentData getLastEntry()
	{
		int last = stackList.size()-1;
		
		if (last < 0)
		{
			return null;
		}
		FragmentData fd = (FragmentData)stackList.get(last);
		
		return fd;
	}


	
	
}
