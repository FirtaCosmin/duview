package com.crmsoftware.duview.ui.common;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;

import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.utils.Log;


public class MvpAdapterCore implements OnItemClickListener, OnClickListener, OnTouchListener, IMvpGenericAdapter  
{
	public static boolean DEBUG = Log.DEBUG; 
	public static final String TAG = MvpAdapterCore.class.getSimpleName();	
	
	private LayoutInflater inflater; 
	
	private static final long MIN_CLICK_INTERVAL = 600;
	private long mLastClickTime;
	private Object tagData;
	
	public void setInflater (Context context) 
	{ 
		inflater = LayoutInflater.from(context); 
	};
	
	private  Class genericType;

	
	/**
	 * it is used to call getId from TemplateItem
	 *
	 * @param  TemplateItem
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected Method methodGetId;
	
	
	
	/**
	 * keeps all resource IDs from the current layout
	 *
	 * @param  TemplateItem
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected List<Integer> listResIds ;
	
	/**
	 * keeps all resource IDs that will register for click listener
	 *
	 * @param  TemplateItem
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected List<Integer> listResIdsForClick = Collections.synchronizedList(new ArrayList<Integer>());
	
	/**
	 * keeps all resource IDs that will register for touch listener
	 *
	 * @param  TemplateItem
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected List<Integer> listResIdsForTouch = Collections.synchronizedList(new ArrayList<Integer>());
	
	
	/**
	 * keeps the context (eq. used for fonts)
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
	private  Context parentContext;
	
	/**
	 * keeps the totalnb of items that can be bigger than listData size
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected Integer totalNbOfItems = 0 ;
	
	/**
	 * keeps the parent reference for notifications
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected IMvpGenericListViewListener parentCallback;
	
	/**
	 * keeps the parent reference for notifications
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected IMvpGenericAdapter parentAdapter;
	
	
	/**
	 * keeps the Id of this adapter in case that multiple adapters are used in the same time
	 *
	 * @param  TemplateItem
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected String id;
	
	Handler handler;


	/**
	 * ctor
	 *
	 * @param  rowLayoutResId - the layout resource ID used tom inflate list item
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
	MvpAdapterCore(IMvpGenericAdapter parentAdapter, Class genericType)
	{
		this.parentAdapter = parentAdapter;
		this.genericType = genericType;
		handler = new Handler();	// UI thread
		
	}
	
	
	/**
	 * ctor
	 *
	 * @param  rowLayoutResId - the layout resource ID used tom inflate list item
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
	MvpAdapterCore(IMvpGenericAdapter parentAdapter, Class genericType,  Object parent)
	{
		if (DEBUG) Log.m(TAG, this, "MvpAdapterCore");		
		
		this.parentAdapter = parentAdapter;
		this.genericType = genericType;
		parentCallback = (IMvpGenericListViewListener) parent;
		handler = new Handler();	// UI thread
		
	}
	
	MvpAdapterCore()
	{

	}
	
	@Override
	protected void finalize() throws Throwable {
		if (DEBUG) Log.m(TAG, this, "finalize");
		
		super.finalize();
	}
	
	
	/**
	 * sets the adapter data and total number of items
	 *
	 * @param  listItems - adapter data
	 * @param  totalNbOfItems - the total number of items, this is necessary when the  listItems was not retrieved completly
	 * @param  
	 * @return      
	 * @see         
	 */	

	public void setCount(final Integer newTotalItems) 
	{
		if (Thread.currentThread().getId() != 1)
		{
		handler.post(new Runnable() {
				
				@Override
				public void run() {
					

					if (null != parentCallback && totalNbOfItems != newTotalItems)
					{
						parentCallback.onListItemsCountChanged(parentAdapter, newTotalItems); 
					}	
					
					totalNbOfItems = newTotalItems ;
					parentAdapter.notifyDataSetChange();
				}
			});
		}
		else
		{

			
			if (null != parentCallback && totalNbOfItems != newTotalItems)
			{
				parentCallback.onListItemsCountChanged(parentAdapter, newTotalItems); 
			}
			
			totalNbOfItems = newTotalItems;	
			parentAdapter.notifyDataSetChange();
		}
		

	}
	


	
	/**
	 * get total number of rows in the list. The size of the list can be bigger than list of items.
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return  the number of items in the list   
	 * @see         
	 */	
	public int getCount() {

		return totalNbOfItems;
	}

	
	/**
	 * returns a data item from a specific position
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return  Object    
	 * @see         
	 */	
	@Override
	public Object getItem(int position) {
		return null;
	}

	
	/**
	 * returns a  item id from a specific position
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return  Object    
	 * @see         
	 */	
	public long getItemId(int position) {
		return 0;
	}
	
	
	/**
	 * returns the view for a specific item
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return  View    
	 * @see         
	 */	
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		if (position < 0)	// case of count = -1
		{
			return new View(parent.getContext());
		}
		
		int layoutId = 0;
		if (null != parentCallback)
		{
			layoutId = parentCallback.onListItemGetLayoutId(parentAdapter, position);
		}

		MvpItemHolder holder = null;
		if(null != convertView)
		{
			Object objTag = convertView.getTag();
			
			if (null != objTag)
			{		
				holder = (MvpItemHolder)objTag;
				
				if (layoutId == holder.getLayoutId())
				{
					holder.update(convertView, position);
					return convertView;
				}
			}
		}
		
		//Log.d(TAG, "\t\t Holder recreated: " + getType().getSimpleName() + "  pos: " + position);
		if (null == inflater)
		{
			setInflater(parent.getContext());
		}
		
		
		if (layoutId == 0)	// this should not happen but frame animation can trigger it
		{
			return new View(parent.getContext());
		}
		
		View view = inflater.inflate(layoutId, null);
		

		listResIds = new ArrayList<Integer>();
		MvpViewHolder.grabAllViewsWithId(view,listResIds); //TODO  this part has to be optimized with a map<layouid>

		
		holder = new MvpItemHolder(view, position, listResIds, layoutId);
		
		holder.setClickListener(this,listResIdsForClick);
		holder.setTouchListener(this,listResIdsForTouch);
		
		view.setTag(holder);

		
		return view;
	}
	/**
	 *  
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return  View    
	 * @see         
	 */	
	public void updateView(View view) 
	{
		if (null != parentCallback && null != view.getTag())
		{
			//if (Log.IS_DEBUG) Log.d(DBGTAG, "\t\t getView listItems " + System.identityHashCode(listItems));
			//if (Log.IS_DEBUG) Log.d(DBGTAG, "\t\t getView item " + System.identityHashCode(itemHolder.getData()));
			//if (Log.IS_DEBUG) Log.d(DBGTAG, "\t\t getView holder " + System.identityHashCode(itemHolder));
			
			MvpItemHolder holder = (MvpItemHolder)view.getTag();
			
			if (holder.getPosition() < 0)
			{
				return ;
			}
			
			parentCallback.onListItemUpdateView(parentAdapter, holder, holder.getPosition());
		}	
	}
	
	/**
	 * register views in order to get click on them
	 *
	 * @param  resId - the views resource id
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void receiveClickFrom(int... resIds)
	{
		for (Integer resId : resIds)
		{
			listResIdsForClick.add(resId);		
		}
	}
	
	/**
	 * register views in order to get click on them
	 *
	 * @param  list - the views resource id
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void receiveClickFrom(List<Integer> list)
	{
			listResIdsForClick.addAll(list);
	}
	
	/**
	 * register views in order to get touch on them
	 *
	 * @param  resId - the views resource id
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void receiveTouchFrom(int... resIds)
	{
		for (int resId : resIds)
		{
			listResIdsForTouch.add(resId);		
		}
	}
	
	/**
	 * sets the adapter id
	 *
	 * @param  id - unique id 
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void setId(String id)
	{
		this.id = id;	
	}
	
	/**
	 * gets the adapter id
	 *
	 * @param  id - unique id 
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */		
	public String getId()
	{
		return id;	
	}
	
	
	/**
	 * register views in order to get touch on them
	 *
	 * @param  list - the views resource id
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */		
	public void receiveTouchFrom(List<Integer> list)
	{
		listResIdsForTouch.addAll(list);		
	}

	
	@Override
	public void onClick(View view) 
	{
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;

        if(elapsedTime <= MIN_CLICK_INTERVAL)
        {
            return;
        }
        mLastClickTime = currentClickTime;
        
		MvpItemHolder itemHolder  = (MvpItemHolder)view.getTag();	
		
		if (null != parentCallback)
		{
			parentCallback.onListItemViewClick(parentAdapter, view, itemHolder);
		}
	}

	@Override
	public boolean onTouch(View view, MotionEvent event) 
	{
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;

        mLastClickTime = currentClickTime;

        if(elapsedTime <= MIN_CLICK_INTERVAL)
        {
            return false;
        }
        
		MvpItemHolder itemHolder  = (MvpItemHolder)view.getTag();	
		
		if (null != parentCallback)
		{
			return parentCallback.onListItemViewTouch(parentAdapter, view, itemHolder, event);
		}
		return false;
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) 
	{
		MvpItemHolder itemHolder  = (MvpItemHolder)view.getTag();		
		
		if (null != parentCallback)
		{
			parentCallback.onListItemViewClick(parentAdapter, view, itemHolder);
		}
	}

	public void receiveOnItemClick(AbsListView view) 
	{
		view.setOnItemClickListener(this);
	}


	public Class getType()
	{
		return genericType;
	}


	@Override
	public void notifyDataSetChange() {
		

	}


	@Override
	public void setGenericListViewListener(IMvpGenericListViewListener callback) 
	{
		this.parentCallback = callback;
		
	}


	@Override
	public MvpObject getPaginationContext() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Object getTag() {
		return tagData;
	}


	@Override
	public void setTag(Object data) 
	{
		tagData = data;	
	}

	
	public LayoutInflater getInflater() {

		return inflater;
	}


	@Override
	public void addView(MvpItemHolder holder, View view) 
	{	

		holder.grabAllViewsWithId(view);
		holder.setClickListener(this,listResIdsForClick);
		holder.setTouchListener(this,listResIdsForTouch);
	}

}
