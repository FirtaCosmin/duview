package com.crmsoftware.duview.ui.common;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************


import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListAdapter;
import android.widget.ListView;

public class MvpVerticalListView extends ListView{
	public static boolean DEBUG = Log.DEBUG ; 
	public static boolean VERBOUSE = Log.VERBOUSE ; 
	
	public static final String TAG = MvpVerticalListView.class.getSimpleName();
	
	private int pageRange[] = {-1,-1};
	public static int PAGESIZE = 10;

	
	public MvpVerticalListView(Context context) {
		super(context);

	}
	
	public MvpVerticalListView(Context context, AttributeSet attrset) {
		super(context,attrset);

	}
	
	public MvpVerticalListView(Context context,AttributeSet attrset, int defStyle) {
		super(context,attrset,defStyle);

	}
	
	@Override
	protected void onDetachedFromWindow() {
		if (VERBOUSE) Log.m(TAG,this,"onDetachedFromWindow");
		
		super.onDetachedFromWindow();
	}
	

	
	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		// TODO Auto-generated method stub
		super.onScrollChanged(l, t, oldl, oldt);
		
		ListAdapter adapter = getAdapter();
		
		if (adapter instanceof IMvpGenericAdapter)
		{
			Utils.checkPageRange((IMvpGenericAdapter)getAdapter(), PAGESIZE, getFirstVisiblePosition(), getLastVisiblePosition(),pageRange, TAG, VERBOUSE);
		}
	}

	
}
