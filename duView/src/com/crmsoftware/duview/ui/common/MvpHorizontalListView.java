
package com.crmsoftware.duview.ui.common;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Scroller;

import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;

public class MvpHorizontalListView extends HorizontalAdapterView {
	public static boolean DEBUG = Log.DEBUG ; 
	public static boolean VERBOUSE = Log.VERBOUSE ; 
	public static final String TAG = MvpHorizontalListView.class.getSimpleName();
	
	public boolean mAlwaysOverrideTouch = true;

	protected Scroller mScroller;
	private GestureDetector mGesture;
	private int childWidth = -1;

	public MvpHorizontalListView(Context context, AttributeSet attrs) {
		super(context, attrs);

	}

	private OnGestureListener mOnGesture;

	public synchronized void initView() {
		if (VERBOUSE) Log.d(TAG, "initView ");
		super.initView();
		
		mOnGesture = new GestureDetector.SimpleOnGestureListener() {

				@Override
				public boolean onDown(MotionEvent e) {
					return MvpHorizontalListView.this.onDown(e);
				}

				@Override
				public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
						float velocityY) {
					return MvpHorizontalListView.this.onFling(e1, e2, velocityX, velocityY);
				}

				@Override
				public boolean onScroll(MotionEvent e1, MotionEvent e2,
						float distanceX, float distanceY) {
					
					synchronized(MvpHorizontalListView.this){
						mNextX += (int)distanceX;
					}
					requestLayout();
					
					return true;
				}

				@Override
				public boolean onSingleTapConfirmed(MotionEvent e) {
					for(int i=0;i<getChildCount();i++){
						View child = getChildAt(i);
						if (Utils.isPointWithinView((int) e.getRawX(), (int) e.getRawY(), child)) {
							if(mOnItemClicked != null){
								mOnItemClicked.onItemClick(MvpHorizontalListView.this, child, mLeftViewIndex + 1 + i, mAdapter.getItemId( mLeftViewIndex + 1 + i ));
							}
							if(mOnItemSelected != null){
								mOnItemSelected.onItemSelected(MvpHorizontalListView.this, child, mLeftViewIndex + 1 + i, mAdapter.getItemId( mLeftViewIndex + 1 + i ));
							}
							break;
						}
						
					}
					return true;
				}
				
				@Override
				public void onLongPress(MotionEvent e) {
					int childCount = getChildCount();
					for (int i = 0; i < childCount; i++) {
						View child = getChildAt(i);
						if (Utils.isPointWithinView((int) e.getRawX(), (int) e.getRawY(), child)) {
							if (mOnItemLongClicked != null) {
								mOnItemLongClicked.onItemLongClick(MvpHorizontalListView.this, child, mLeftViewIndex + 1 + i, mAdapter.getItemId(mLeftViewIndex + 1 + i));
							}
							break;
						}

					}
				}

			};
		
		mScroller = new Scroller(getContext());
		mGesture = new GestureDetector(getContext(), mOnGesture);
	}


	@Override
	protected synchronized void onLayout(boolean changed, int left, int top, int right, int bottom) {
		super.onLayout(changed, left, top, right, bottom);

		if(mAdapter == null){
			return;
		}
		
		if(mDataChanged)
		{
			//mScroller.forceFinished(true);
/*			int totalOffset = mTotalOffset;
			initView();
			mNextX = totalOffset;
			mDataChanged = false;*/
			
			for (int i=mLeftViewIndex+1,j=0; i<mRightViewIndex; i++,j++)
			{
				View child = mAdapter.getView(i, getChildAt(j), this);
			}
			mDataChanged = false;
		}
		
		
		if(mScroller.computeScrollOffset())
		{
			int scrollx = mScroller.getCurrX();
			mNextX = scrollx;
		}
		
		if(mNextX <= 0){
			mNextX = 0;
		}
		
		if(mNextX >= mMaxX) {
			mNextX = mMaxX;
		}
		
		boolean performArange = true;
		int count = getChildCount();
		if (count > 0)
		{
			View viewRight = getChildAt(count-1);
			View viewLeft = getChildAt(0);
			int dx = mCurrentX - mNextX;
			int rightCorner = viewRight.getRight() + dx;
			if (rightCorner < getWidth() && mRightViewIndex == mAdapter.getCount())
			{
				dx = getWidth() - rightCorner;
				mNextX = mCurrentX - dx;
				mScroller.forceFinished(true);

			}
			
			int leftCorner = viewLeft.getLeft() + dx;
			if (leftCorner > 0 && mLeftViewIndex == -1)
			{
				dx = leftCorner;
				mNextX +=  dx;
			}

		}
		
		if (performArange)
		{
			arangeViews();
			if(!mScroller.isFinished()){
				post(new Runnable(){
					@Override
					public void run() {
						requestLayout();
					}
				});
			}
		}
	}
	
	
	
	@Override
	protected int getItemViewWidth(int pos) 
	{
		if (childWidth == -1)	// optimization
		{
			childWidth = super.getItemViewWidth(0);
		}
		return childWidth;
	}
	
	public void setCellWidth(int width) 
	{
		childWidth  = -1;
		super.setCellWidth(width);
	}

	
	public synchronized void scrollTo(final int x, boolean bAsync) {
		
		if (!bAsync)
		{
			mCurrentX = 0;
			mNextX = x;
			requestLayout();
			return;
		}
		
		final Handler execHandler = new Handler();
		final Runnable runnable = new Runnable() 
		{
				@Override
				public void run() 
				{
					mCurrentX = 0;
					mNextX = x;
					requestLayout();
				}
			};
			
			execHandler.postDelayed(runnable, 1);	
			

		
		final Runnable runnable2 = new Runnable() 
		{
				@Override
				public void run() 
				{
					requestLayout();
				}
			};
			
			execHandler.postDelayed(runnable2, 200);	
			

	}
	
	private float memX = 0;
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		
		boolean isVerticalMovement = mGestureDetector.onTouchEvent(ev);
		if (isVerticalMovement){
			this.getParent().requestDisallowInterceptTouchEvent(false);
			return false;
		}
		
		boolean handled;
		
		switch (ev.getAction()) {
	    case MotionEvent.ACTION_DOWN:
	    	if (VERBOUSE) Log.d(TAG, "ACTION_DOWN ");
	    	memX = ev.getX();
	    	this.getParent().requestDisallowInterceptTouchEvent(true);
	    	break;
	    	
	    case MotionEvent.ACTION_UP:
	    	//if (INFO) Log.d(TAG, "ACTION_UP");
	    	this.getParent().requestDisallowInterceptTouchEvent(false);
	    	if (VERBOUSE) Log.d(TAG, "ACTION_UP ");
	    	// if scrolling before UP event, don't dispatch the event to children views
	    	if (Math.abs(ev.getX() - memX) > 10) {
	    		handled = mGesture.onTouchEvent(ev);
	    		if (VERBOUSE) Log.d(TAG, "handled ");
	    		return handled;
	    	}
	    	
	    	
	    	break;
	    	
	    case MotionEvent.ACTION_MOVE:
	    	//if (INFO) Log.d(TAG, "ACTION_MOVE");
	    	break;
	    }
		
		handled = super.dispatchTouchEvent(ev);
		handled |= mGesture.onTouchEvent(ev);
		//if (INFO) Log.d(TAG, "dispatchTouchEvent return " + handled);
		return handled;
	}
	
	private GestureDetector mGestureDetector = new GestureDetector(this.getContext(), new YScrollDetector());
	
	 // Return false if we're scrolling in the x direction  
    class YScrollDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return (Math.abs(distanceY) > Math.abs(distanceX));
        }
    }
	
	protected boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
		synchronized(MvpHorizontalListView.this){
			mScroller.fling(mNextX, 0, (int)-velocityX, 0, 0, mMaxX, 0, 0);
		}
		requestLayout();
		
		return true;
	}
	
	protected boolean onDown(MotionEvent e) {
		mScroller.forceFinished(true);
		return true;
	}
	
/*	
	@Override
	public void setAdapter(ListAdapter adapter) {
		super.setAdapter(adapter);
		reset();
	}*/


}
