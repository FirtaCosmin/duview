package com.crmsoftware.duview.ui.common;

import android.os.Bundle;

public interface OnMvpDlgClose {
	public void onMvpDlgClose(String dlgTag, Bundle data);
}
