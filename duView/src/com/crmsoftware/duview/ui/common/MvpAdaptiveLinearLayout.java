package com.crmsoftware.duview.ui.common;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Adapter;
import android.widget.LinearLayout;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.utils.Log;

/**
 * @author Cosmin
 * @desc This class implements a LinearLayout that will refresh it's elements 
 * based on an adapter data change.
 *
 */
public class MvpAdaptiveLinearLayout extends LinearLayout {
	private static  String TAG = MvpAdaptiveLinearLayout.class.getSimpleName();
	private static int DEFAULT_COLUMN_NO = 1;
	/**
	 * @desc the adapter that will give the views to draw
	 */
    private Adapter adapter;
    
    /* ***************************
     *     XML Parameters
     * defined with MvpAdaptiveLinearLayout style
     * *************************** */
    
    /**
     * @desc the number of colums that the layout will have
     */
    private int mColumnNo = DEFAULT_COLUMN_NO;
    private boolean mDifferentRowCollor = false;
    private int mEvenRowColor = 0x000000;
    private int mOddRowColor = 0x000000;
    private boolean mCompleteLastRow = false;
    
    
    private View mEmptyViewID;
    /**
     * @desc the dataobserver that is used to respond to the changes in the adapter
     */
    private final DataSetObserver observer = new DataSetObserver() {

        /* (non-Javadoc)
         * @see android.database.DataSetObserver#onChanged()
         */
        @Override
        public void onChanged() {
        	/*when the observed data is changing refresh the views from the adapter.*/
            refreshViewsFromAdapter();
        }

        @Override
        public void onInvalidated() {
            removeAllViews();
        }
    };

    public MvpAdaptiveLinearLayout(Context context) {
        super(context);
    }

    public MvpAdaptiveLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public MvpAdaptiveLinearLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    


    
	/**
	 * @desc initializes the layout's parameter from the xml params and adds 
	 * the columns depending on the column_no attribute
	 * @param context
	 * @param attrs
	 */
	protected void init(Context context, AttributeSet attrs) {
		TypedArray typeArray = context.obtainStyledAttributes(attrs, R.styleable.MvpAdaptiveLinearLayout);
		this.mColumnNo = typeArray.getInt(R.styleable.MvpAdaptiveLinearLayout_column_no, DEFAULT_COLUMN_NO);
		this.mDifferentRowCollor = typeArray.getBoolean(R.styleable.MvpAdaptiveLinearLayout_different_row_color, false);
			boolean noColors = false;
		if ( mDifferentRowCollor ){
			/*if flag for different colors is provided then get the colors*/
			if ( typeArray.hasValue(R.styleable.MvpAdaptiveLinearLayout_even_row_color) ){
				this.mEvenRowColor = typeArray.getInt(R.styleable.MvpAdaptiveLinearLayout_even_row_color, 0x000000);
			}else{
				noColors = true;
			}
			
			if ( typeArray.hasValue(R.styleable.MvpAdaptiveLinearLayout_odd_row_color) ){
				this.mOddRowColor = typeArray.getInt(R.styleable.MvpAdaptiveLinearLayout_odd_row_color, 0x000000);
			}else{
				noColors = true;
			}
		}
		if ( noColors ){
			/*if any of the colors are not provided then reset mDifferentRowCollor flag */
			mDifferentRowCollor = false;
		}
		mCompleteLastRow = typeArray.getBoolean(R.styleable.MvpAdaptiveLinearLayout_complete_last_row, false);
		typeArray.recycle();
		setOrientation(HORIZONTAL);
//		addColumns();
	}

    /**
     * @desc getter for the linked adapter
     * @return the linked adapter
     */
    public Adapter getAdapter() {
        return adapter;
    }

    /**
     * @desc setter method to link the adapter to the layout
     * @param adapter
     */
    public void setAdapter(Adapter adapter) {
    	/*
    	 * first unregister as an observer from the current adapter, 
    	 * if there has been one added before.
    	 * */
        if (this.adapter != null) {
            this.adapter.unregisterDataSetObserver(observer);
        }
        /*then add the adapter and register as observer.*/
        this.adapter = adapter;
        if (this.adapter != null) {
            this.adapter.registerDataSetObserver(observer);
        }
        /*and init the view from it*/
		initViewsFromAdapter();
    }

    /**
     * @desc method to add the initial view from the adapter.
     */
    protected void initViewsFromAdapter() {
    	/*first remove all the existing views*/
        removeAllViews();
        /* then add the ones from the adapter*/
        if (adapter != null) {
        	Log.d(TAG,TAG+" initViewsFromAdapter childCount: "+getChildCount());
    		addColumns();
        	Log.d(TAG,TAG+" initViewsFromAdapter childCount: "+getChildCount());
        	addViewFromAdapter(0, adapter.getCount());
        	completeLastRow();

        }
    }

    /**
     *  @desc method to update the views from the adapter.
     */
    protected void refreshViewsFromAdapter() {
    	Log.d(TAG,TAG+" refreshViewsFromAdapter childCount: "+getChildCount());
    	/*check if the columns have been added*/
    	if ( getChildCount() == 0 ){
    		/*add them if they have not been added*/
    		addColumns();
    	}
    	
        int childCount =  getChildViewCount();
        int adapterSize = adapter.getCount();
        int reuseCount = Math.min(childCount, adapterSize);
        /*
         * first will update the already existing views.
         * these are all the existing ones or, 
         * if the adapter contains less view now, the ones from the adapter.
         * */
        for (int i = 0; i < reuseCount; i++) {
            View theView = adapter.getView(i, getChildViewAt(i), this);
        	setViewBackground(theView, i);
        }

        /*
         * after updating the existing ones
         * */
        if (childCount < adapterSize) {
        	/*
        	 * add the new ones if the adapter has more views 
        	 * then the ones that already exist
        	 * */
        	addViewFromAdapter(childCount, adapterSize);
        } else if (childCount > adapterSize) {
        	/*
        	 * or remove the view counted over the adapter's count of views
        	 * */
            removeChildren(adapterSize, childCount - adapterSize);
        }
        completeLastRow();
    }
    
    
    /**
     * @desc will add mColumnNo of LinearLayouts with horizontal orientations to the this layout
     */
    private void addColumns(){
    	Log.d(TAG,TAG + "AddColums:" + mColumnNo + " total children: " + getChildCount());
    	for ( int i=0; i<mColumnNo; i++ ){
    		LinearLayout layout = new LinearLayout(getContext());
    		layout.setLayoutParams(new LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1));
    		layout.setOrientation(LinearLayout.VERTICAL);
    		addView(layout, i);
    	}
    }
    
    
    /**
     * @desc will add a 
     * @param position
     * @param view
     */
    private void addChildView(View view, int position){
    	Log.d(TAG,TAG + "addChildView:Position: "+ position + " Column No:" + mColumnNo + " total children: " + getChildCount());
    	LinearLayout theLayout = (LinearLayout)getChildAt(position%mColumnNo);
    	theLayout.addView(view, position/mColumnNo);
    }
    
    /**
     * @desc will return the child view calculating it's position in the 
     * vertical layouts depending on it's position from the adapter that is passed as parameter 
     * @param position - the position in the adapter
     * @return
     */
    private View getChildViewAt(int position){
    	
    	LinearLayout theLayout = (LinearLayout)getChildAt(position%mColumnNo);
    	View theView = theLayout.getChildAt(position/mColumnNo);
    	return theView;
    }
    
    /**
     * @desc will remove count number of view from the startPos position
     * @param startPos
     * @param count
     */
    private void removeChildren(int startPos, int count){
    	for ( int i=startPos; i<startPos + count; i++ ){
        	LinearLayout theLayout = (LinearLayout)getChildAt(i%mColumnNo);
        	theLayout.removeViewAt(i/mColumnNo);
    	}
    }
    
    /**
     * @return
     */
    private int getChildViewCount(){
    	int count = 0 ;
    	for ( int i=0; i<mColumnNo; i++ ){
    		count += ((LinearLayout)getChildAt(i)).getChildCount();
    	}
    	return count;
    }
    /**
     * @desc will set the view background 2 colors on on two rows
     * the colors are provided in the xml configuration using 
     * different_row_color, even_row_color, odd_row_color parameters
     * @param theView
     * @param position
     */
    private void setViewBackground(View theView, int position){
    	if ( !mDifferentRowCollor ){
    		/*if xml configuration states different row colors not to be used*/
    		return;
    	}
    	
    	if ((position/mColumnNo)%2 == 0) 
		{
    		theView.setBackgroundColor(mEvenRowColor);
		} 
		else
		{
			theView.setBackgroundColor(mOddRowColor);
		}
    }
    
    /**
     * @desc will add the views from adapter from startPos to endPos
     * @param startPos
     * @param endPos
     */
    private void addViewFromAdapter(int startPos, int endPos){
    	View theView = null;
        for (int i = startPos; i < endPos; i++) {
        	theView = adapter.getView(i, null, this);
        	addChildView(theView, i);
        	setViewBackground(theView, i);
        }
        mEmptyViewID = theView;
    }	
    
    /**
     * @desc if the xml parameter complete_last_row is set then it will register a 
     * layout listener to the last cell, when the layout is set it will get the height 
     * of the last view and will use it to create empty views for the empty cells in 
     * the last row.
     */
    private void completeLastRow(){
    	if ( !mCompleteLastRow ){
    		/*if in the xml lastRowCompletion is not activated*/
    		/*do nothing*/
    		return;
    	}

    	if ( getChildViewCount()%mColumnNo == 0){
    		/*if the last row is filled*/
    		/*do nothing*/
    		return;
    	}
    	
    	final View lastView = getChildViewAt(adapter.getCount() - 1);

		ViewTreeObserver vto = lastView.getViewTreeObserver();
		vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
		
		    @SuppressLint("NewApi") @Override
		    public void onGlobalLayout() {
		        ViewTreeObserver obs = lastView.getViewTreeObserver();
		        addCompletionView(lastView.getHeight());
		        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
		            obs.removeOnGlobalLayoutListener(this);
		        } else {
		            obs.removeGlobalOnLayoutListener(this);
		        }
		    }
		
		});
    	
    	

    }
    
    /**
     * @desc will create an empty view with the passed height and match parent width 
     * for every empty last cells. 
     * @param height
     */
    private void addCompletionView(int height){
    	int totalChildCount = getChildViewCount();
    	int rowNo = totalChildCount / mColumnNo;
    	if ( totalChildCount%mColumnNo != 0){
    		/*if the last row is not filled*/
    		for ( int i = totalChildCount; i < (rowNo+1) * mColumnNo; i++ ){

        		View v = new View(getContext());
        		v.setLayoutParams(new LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, height));
    			setViewBackground(v, i);
    			addChildView(v , i);
    		}
    	}
    }
}