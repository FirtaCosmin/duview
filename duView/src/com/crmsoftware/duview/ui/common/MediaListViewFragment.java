package com.crmsoftware.duview.ui.common;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.FragmentChangeActivity;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.data.MvpContent;
import com.crmsoftware.duview.data.MvpMedia;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.data.MvpParentalManagement;
import com.crmsoftware.duview.data.MvpServerError;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.ui.MvpFragment;
import com.crmsoftware.duview.utils.CustomDialogView;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.ParentalPinDialogView;
import com.crmsoftware.duview.utils.Utils;
import com.insidesecure.drmagent.v2.internal.nativeplayer.TSSegmentInfo;

public class MediaListViewFragment 
extends MvpFragment 
implements 
IMvpGenericListViewListener, 
OnClickListener, 
OnScrollListener,
ITaskReturn
{

	public static boolean DEBUG = Log.DEBUG; 
	public final static String TAG = MediaViewPagerFragment.class.getSimpleName();
	
	
	public final static String DATA_CONTEXT = "DATA_CONTEXT"; 
	public final static String DATA_POSITION = "DATA_POSITION"; 
	public final static String DATA_VIEWTITLE = "DATA_VIEWTITLE"; 
	
	private MvpObject mvpCategoriesObj = null;
	private List<MvpViewHolder> categoriesHolder = new ArrayList<MvpViewHolder>(); 
	private MvpObject mvpObjContext;
	private Bundle data;
	private MvpPagListAdapter adapter;
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		if (DEBUG) Log.m(TAG,this,"onCreateView");

		View view = setContentView(R.layout.fragment_circular_parallax_vertical_list_view,container,inflater);
//		View view = setContentView(R.layout.fragment_vertical_list_view,container,inflater);
		MvpViewHolder holder = getHolder();
		
		getMvpActivity().setMenuEnabled(true);
		getMvpActivity().setActionBarVisibility(View.VISIBLE);
		getMvpActivity().setHomeButtonActionBar(true);
		getMvpActivity().setTitle(getData().getString(DATA_VIEWTITLE, ""));
		
		Bundle viewData = getData();
		
		mvpObjContext = MvpObject.newReference(viewData.getString(DATA_CONTEXT));
		
		final int position = viewData.getInt(DATA_POSITION,0);

		final ListView listView = (ListView) holder.getView(R.id.items_pagedlist);
		listView.setOnScrollListener(this);
		
		adapter = new MvpPagListAdapter<MvpMedia>(
	 			MvpMedia.class,
	 			1);
		
		adapter.setGenericListViewListener(this);
		adapter.setPaginationContext(mvpObjContext);
		adapter.receiveClickFrom(R.id.description_layout, R.id.play_media_btn);

		
		listView.setAdapter(adapter);
		
		new Handler().post(new Runnable() {
			
			@Override
			public void run() {
				listView.setSelection(position);
			}
		});
		 
		return view;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onResetData() {
		if (null == mvpObjContext)
		{
			return;
		}
		
		MvpPagContext paginationContext = (MvpPagContext) mvpObjContext.getData();
		paginationContext.unbindAdapter();
		
		if (null != adapter)
		{
			adapter.setGenericListViewListener(null); // this will release fragment instance
			adapter = null;
		}
		
		super.onResetData();
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onListItemViewClick(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder) {

		 data = getData();
			
		data.putInt(MediaViewPagerFragment.DATA_POSITION, holder.getPosition());
		data.putString(MediaViewPagerFragment.DATA_CONTEXT, mvpObjContext.getId());
		
		/* if the click was on the play button */
		if (view.getId() == R.id.play_media_btn) 
		{

			data.putBoolean(MediaViewPagerFragment.DATA_PLAY, true);

		}
		else 
		{
			data.putBoolean(MediaViewPagerFragment.DATA_PLAY, false);
		}
		
		
		gotoDetailsWithParentalCheck(holder);
	
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public boolean onListItemViewTouch(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onListItemUpdateView(IMvpGenericAdapter adapter,
			MvpItemHolder holder, int position) {
		
		MvpMedia  mvpMedia = (MvpMedia) adapter.getItem(position);
		
		if (null == mvpMedia)
		{
			holder.setVisibility(R.id.item_loading, View.VISIBLE);
			holder.setVisibility(R.id.body_view, View.INVISIBLE);
			return;
		}
		
		holder.setClickListener(this, R.id.watch_later_button, R.id.watch_now_button);
		holder.setVisibility(R.id.item_loading, View.INVISIBLE);
		holder.setVisibility(R.id.body_view, View.VISIBLE);
		
		holder.setText(R.id.item_title, mvpMedia.getTitle());
		holder.setText(R.id.item_description, mvpMedia.getDescription());
		holder.setText(R.id.item_parental_info, mvpMedia.getRatings().getRating().toUpperCase());
		if (false == mvpMedia.getSeriesName().isEmpty())
		{
			holder.setText(R.id.item_episodes, String.format(holder.getResources().getString(R.string.episodes_list), mvpMedia.getEpisodesCount()));
			holder.setVisibility(R.id.item_episodes_icon, View.VISIBLE);
			holder.setVisibility(R.id.play_media_btn, View.GONE);
		}
		else
		{	
			holder.setVisibility(R.id.item_episodes, View.GONE);
			holder.setVisibility(R.id.item_episodes_icon, View.GONE);
			holder.setVisibility(R.id.play_media_btn, View.VISIBLE);
		}
		
		 MvpImageView itemImage = (MvpImageView) holder.getView(R.id.item_image); 

		if (position % 2 == 0) 
		{
			holder.getView(R.id.body_view).setBackgroundResource(R.color.first_item);
		} 
		else
		{
			holder.getView(R.id.body_view).setBackgroundResource(R.color.second_item);
		}
		 String sImageURL = null;
		 MvpContent itemImageData = mvpMedia.getThumbPoster();
		 if (null != itemImageData)
		 {
			 sImageURL = itemImageData.getUrl();
		 }
		 itemImage.setData(sImageURL);
	}

	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.watch_later_button:
			getMvpActivity().goTo(UnderConstrFragment.class, UnderConstrFragment.TAG, UnderConstrFragment.buildData("Wish list feature"), true);
			break;
			
		case R.id.watch_now_button:
			getMvpActivity().goTo(UnderConstrFragment.class, UnderConstrFragment.TAG, UnderConstrFragment.buildData("Player feature"), true);			
			break;
		case R.id.play_media_btn:
			
			break;
		default:
			break;
		}
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onListItemsCountChanged(IMvpGenericAdapter adapter, int newCount) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		
		if (scrollState == SCROLL_STATE_IDLE)
		{
			int pos = 0;
			if (null != mvpObjContext)
			{
				MvpPagContext context = (MvpPagContext)mvpObjContext.getData();
			
				if (null != context)
				{
					pos = context.getFirstVisibleItem();
				}
			}
			getData().putInt(MediaViewPagerFragment.DATA_POSITION, pos);
		}
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public int onListItemGetLayoutId(IMvpGenericAdapter adapter, int position) {
		// TODO Auto-generated method stub
		return R.layout.media_see_all_list_view_row;
	}
	
	@Override
	public void onMvpDlgClose(String dlgTag, Bundle data) {
		
		int viewId = data.getInt(ParentalPinDialogView.RETURN_STATE);
		
		switch (viewId) 
		{
		case R.id.button_pozitive:
			String pin = data.getString(ParentalPinDialogView.PIN_VALUE);
			if(Utils.isEmpty(pin))
			{
				showToast("Field is empty");
			}
			else
			{
				getMvpActivity().showLoadingDialog();
				MvpParentalManagement.getInstance().checkPin(pin, this);
			}
			break;

		default:
			break;
		}
		
		
		super.onMvpDlgClose(dlgTag, data);
	}


	@Override
	public void onReturnFromTask(Task task, boolean canceled) {
		if(task.isError())
		{
			Object error = task.getError();
			MvpServerError mvpError = (MvpServerError)error;
			int errorCode = mvpError.getResponseCode();
			getMvpActivity().hideLoadingDialog();
			if(errorCode == 200)
			{
				gotoDetails();
			}
			if (errorCode == 403)
			{
				showToast(mvpError.getTitle());
				ParentalPinDialogView.getInstance().setEmptyField();
				return;
			}
			
			if (errorCode == 400)
			{
				showToast(MainApp.getRes().getString(R.string.change_pins_settings_wrong));
				ParentalPinDialogView.getInstance().setEmptyField();
				return;
			}
		}
		
	}
	
	
	/**
	 * @desc will check for parental and if parental ok then will go to deatails screen
	 * @param holder
	 */
	private void gotoDetailsWithParentalCheck(MvpItemHolder holder){
		
		MediaViewPagerFragment fragment = new MediaViewPagerFragment();
		fragment.setData(data);
		
		MvpMedia  mvpMedia = (MvpMedia) adapter.getItem(holder.getPosition());
		MvpUserData authData = MvpUserData.getFromMvpObject();
		if (authData.isAnonymous()
				&& data.getBoolean(MediaViewPagerFragment.DATA_PLAY)) 
		{
			CustomDialogView customDialogView = CustomDialogView.getInstance();
			customDialogView.show(getFragmentManager(), CustomDialogView.TAG);
		} 
		else 
		{
			if(null == mvpMedia)
			{
				return;
			}
			if (MvpParentalManagement.getInstance().checkLevel(
					mvpMedia.getParentalLevel()) == MvpParentalManagement.LEVEL_NOK) 
			{

				ParentalPinDialogView parentalDialogView = ParentalPinDialogView
						.getInstance();
				parentalDialogView.setData(getTag(), data);
				parentalDialogView.show(getChildFragmentManager(),
						ParentalPinDialogView.TAG);
			} 
			else 
			{
				gotoDetails();
			}
		}
	}
	
	/**
	 * @desc will change the current fragment with the details screen fragment
	 */
	private void gotoDetails(){
		FragmentChangeActivity fca = (FragmentChangeActivity) getActivity();
		fca.goTo(MediaViewPagerFragment.class,MediaViewPagerFragment.TAG, data, true);
	}


}
