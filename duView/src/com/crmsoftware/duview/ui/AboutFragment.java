package com.crmsoftware.duview.ui;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************



import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.configuration.PlatformSettings;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.ui.apphelp.AppHelpFragment;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.ui.vodcategories.VodCategoriesFragment;
import com.crmsoftware.duview.utils.IConst;
import com.crmsoftware.duview.utils.Log;



public class AboutFragment extends MvpFragment implements OnClickListener  {
	public static boolean DEBUG = Log.DEBUG; 
	
	public final static String TAG = AboutFragment.class.getSimpleName();

	View holowView;
	private Animation fadeInAnimation;

	private ImageView crmLogo;


	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,

			Bundle savedInstanceState) 
	{		
		if (DEBUG) Log.m(TAG,this,"onCreateView");
		
		View view = setContentView(R.layout.fragment_about,container,inflater);
		
		MvpViewHolder viewHolder = getHolder();
		
		viewHolder.setClickListener(this, R.id.dont_show_check,R.id.crm_text);
		
		getMvpActivity().setMenuEnabled(true);
		getMvpActivity().setActionBarVisibility(View.VISIBLE);
		getMvpActivity().setHomeButtonActionBar(true);
		getMvpActivity().setTitle(MainApp.getRes().getString(R.string.about));

		
		
		holowView = viewHolder.getView(R.id.image_back);
//		crmLogo = (ImageView)viewHolder.getView(R.id.crm_image);
//		
//		TranslateAnimation animation = new TranslateAnimation( 1000.0f, 0.0f,
//	            0.0f, 0.0f);          //  new TranslateAnimation(xFrom,xTo, yFrom,yTo)
//		animation.setInterpolator(new LinearInterpolator());
//		animation.setRepeatCount(0);
//		animation.setDuration(1400);
//
//	    // Start animating the image
//	    crmLogo.startAnimation(animation);
		
		
		fadeInAnimation = AnimationUtils.loadAnimation(getActivity(), R.animator.view_fade_out);
        fadeInAnimation.setAnimationListener(new AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                        // TODO Auto-generated method stub

                }
                @Override
                public void onAnimationRepeat(Animation animation) {
                        // TODO Auto-generated method stub
                }
                @Override
                public void onAnimationEnd(Animation animation) {
               	 holowView.startAnimation(fadeInAnimation);

                }
        });
        holowView.startAnimation(fadeInAnimation);
		
    	String versionName = IConst.EMPTY_STRING;
 		try 
 		{
 			versionName = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
 		} 
 		catch (NameNotFoundException e) 
 		{
 			
 		}
 		
 		String ver = String.format(getResources().getString(R.string.app_ver), versionName);
 		viewHolder.setText(R.id.app_ver,ver);
 		
		MvpUserData data = MvpUserData.getFromMvpObject();
		viewHolder.setCheckState(R.id.dont_show_check, !AppHelpFragment.tempDisabled);
		
		viewHolder.setText(R.id.app_device,String.format(getResources().getString(R.string.app_device), Build.MANUFACTURER));
		viewHolder.setText(R.id.app_model,String.format(getResources().getString(R.string.app_model), Build.MODEL));		
		viewHolder.setText(R.id.app_os,String.format(getResources().getString(R.string.app_os), Build.VERSION.RELEASE));			
		return view;
	}
	
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {

			
		case R.id.dont_show_check:	
			
			MvpUserData data = MvpUserData.getFromMvpObject();
			
			boolean state = viewHolder.getCheckState(R.id.dont_show_check) <=0 ? false : true;
			
			data.setHelp(state);
			AppHelpFragment.tempDisabled = !state;
			
			if (state)
			{
				
				getMvpActivity().getMenu().selectMenuItem(VodCategoriesFragment.class, VodCategoriesFragment.TAG, true);
				//getMvpActivity().show(AppHelpFragment.class, AppHelpFragment.TAG, AppHelpFragment.getConfig(0));
			}

			
			break;
		case R.id.crm_text:
			getMvpActivity().showLoadingDialog();
			Bundle bundle = new Bundle();
			bundle.putBoolean(BrowserFragment.DISABLE_MENU,true);
			bundle.putString(BrowserFragment.URL, PlatformSettings.getInstance().getCRMSoftwareUrl());
			getMvpActivity().goTo(BrowserFragment.class,BrowserFragment.TAG, bundle, true);
			break;
		default:
			break;
		}	
	}
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onResetData() {
		getMvpActivity().hide(AppHelpFragment.TAG);
		super.onResetData();
	}
}
