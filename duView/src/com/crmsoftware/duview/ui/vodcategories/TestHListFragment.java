package com.crmsoftware.duview.ui.vodcategories;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************


import java.util.Observable;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.FragmentChangeActivity;
import com.crmsoftware.duview.configuration.PlatformSettings;
import com.crmsoftware.duview.core.common.HttpReqParams;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.data.MpxPagHeader;
import com.crmsoftware.duview.data.MvpCategory;
import com.crmsoftware.duview.data.MvpContent;
import com.crmsoftware.duview.data.MvpList;
import com.crmsoftware.duview.data.MvpMedia;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.ui.MvpFragment;
import com.crmsoftware.duview.ui.common.IMvpGenericListViewListener;
import com.crmsoftware.duview.ui.common.IMvpGenericAdapter;
import com.crmsoftware.duview.ui.common.MediaViewPagerFragment;
import com.crmsoftware.duview.ui.common.MvpHorizontalListView;
import com.crmsoftware.duview.ui.common.MvpImageView;
import com.crmsoftware.duview.ui.common.MvpItemHolder;
import com.crmsoftware.duview.ui.common.MvpPagContext;
import com.crmsoftware.duview.ui.common.MvpPagListAdapter;
import com.crmsoftware.duview.ui.common.MvpSimpleListAdapter;
import com.crmsoftware.duview.ui.common.MvpVerticalListView;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.utils.IHttpConst;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.ParentalPinDialogView;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.ProgressBar;



public class TestHListFragment extends MvpFragment implements IMvpGenericListViewListener, ITaskReturn {
	
	public static boolean DEBUG = Log.DEBUG; 
	public final static String TAG = TestHListFragment.class.getSimpleName();
	
	protected MvpObject mvpCategoriesObj = null;
	


	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		if (DEBUG) Log.m(TAG,this,"onCreateView");
		
		
		setHasOptionsMenu(true);
		
		View view = setContentView(R.layout.fragment_vertical_list_view,container,inflater);
			
		getMvpActivity().setMenuEnabled(true);
		getMvpActivity().setActionBarVisibility(View.VISIBLE);
		getMvpActivity().setHomeButtonActionBar(true);
		
		MvpViewHolder holder = getHolder();
				
		MvpVerticalListView listView = (MvpVerticalListView) holder.getView(R.id.items_pagedlist);
        
        MvpSimpleListAdapter categoriesAdapter = new MvpSimpleListAdapter(MvpCategory.class,this);
        listView.setAdapter(categoriesAdapter);


        MvpObject mvpCategoriesObj = MvpObject.newReference(TaskGetVodCategories.OBJPREFIX_CATEGORIES);
        mvpCategoriesObj.addObserver(this);
        
        TaskGetVodCategories whatsNew = new TaskGetVodCategories(this, mvpCategoriesObj); // this has to be replaced with specific request task
        whatsNew.execute();
        
        
        
        
		return view;
	}
	
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		super.onCreateContextMenu(menu, v, menuInfo);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onDestroy() {
		if (DEBUG) Log.m(TAG,this,"onDestroy");
		
		mvpCategoriesObj.deleteObserver(this);
		
		super.onDestroy();
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onListItemViewClick(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder) {
		 Log.m(TAG,this, "onListItemViewClick");
		
		 
		 if (adapter.getType() == MvpMedia.class)
		 {
			 switch (view.getId()) {
			case R.id.item_title:
				//showLoadingDialog();
				goToDetailsView(adapter,holder);
				break;

			default:
				break;
			}			 
			 

			 return;
		 }
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public boolean onListItemViewTouch(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder, MotionEvent event) {
		
		 Log.m(TAG,this, "onListItemViewTouch");
		 
		return false;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void updateCategoryItemView (IMvpGenericAdapter adapter,
			MvpItemHolder holder, int position)
	{
		 	Log.d(TAG,"MvpCategory position: " + position );	

			if (null == mvpCategoriesObj)
			{
				holder.setVisibility(R.id.item_loading, View.VISIBLE);
				holder.setVisibility(R.id.body_view, View.INVISIBLE);
				return;
			}

			MvpList<MvpCategory> listCategories = (MvpList<MvpCategory>) mvpCategoriesObj.getData();
			if  (null == listCategories)
			{
				// data might be removed from memory so we need to retake it
				holder.setVisibility(R.id.item_loading, View.VISIBLE);
				holder.setVisibility(R.id.body_view, View.INVISIBLE);
				return;
			}
			
			holder.setVisibility(R.id.item_loading, View.INVISIBLE);
			holder.setVisibility(R.id.body_view, View.VISIBLE);

			
			MvpCategory mvpCategory = listCategories.getItem(2);
			String categoryId = mvpCategory.getId();
			holder.setText(R.id.row_title, mvpCategory.getTitle());
			 
			 MvpHorizontalListView listView = (MvpHorizontalListView) holder.getView(R.id.items_pagedlist);
			 listView.setCellSpace(0);
			 listView.setCellWidth(200);
			 MvpPagListAdapter<MvpMedia> horizontalListAdapter = (MvpPagListAdapter<MvpMedia>) listView.getAdapter();	
			 if (horizontalListAdapter == null)
			 {
				 
				 
					HttpReqParams httpReqParams = new HttpReqParams(
							IHttpConst.VALUE_SCHEMA_1_2, 
							IHttpConst.VALUE_CJSON);
					
					httpReqParams.setParam(IHttpConst.PARAM_BY_CATEGORIES, categoryId);	
					httpReqParams.setSort(IHttpConst.VALUE_FIELD_ADDED);
					httpReqParams.setThubmnailFilter("Thumbnail");		
					
					MvpObject mvpObjContext = MvpPagContext.getContext(
							MpxPagHeader.class,
							MvpPagContext.STARTWITH_ONE,
							PlatformSettings.getInstance().getTpMpxFeedUrl(),
				 			httpReqParams,
				 			"TpMpxFeedUrl."+categoryId, 0);
					
					MvpPagContext ctx = (MvpPagContext) mvpObjContext.getData();
					
					
				 	horizontalListAdapter = new MvpPagListAdapter<MvpMedia>(
				 			MvpMedia.class, 
				 			1);
				 	
				 	horizontalListAdapter.setGenericListViewListener(this);
				 	horizontalListAdapter.setPaginationContext(mvpObjContext);
				 	horizontalListAdapter.receiveClickFrom(R.id.item_title);
					listView.setAdapter(horizontalListAdapter);
					
					if  (horizontalListAdapter.getCount() == 0)
					{
						horizontalListAdapter.setCount(1000);
					}
						
					listView.setSelection(ctx.getFirstVisibleItem());
			 }
			 else
			 {
				 //((IMvpGenericAdapter) horizontalListAdapter).getPaginationContext().cancelAllRequests();
			 }	
	}
	
	public void goToDetailsView(IMvpGenericAdapter adapter, MvpItemHolder holder)
	{
		
		MvpObject mvpPagContext = adapter.getPaginationContext();
		
		
		
		Bundle data = new Bundle();
		
		data.putInt(MediaViewPagerFragment.DATA_POSITION, holder.getPosition());
		data.putString(MediaViewPagerFragment.DATA_CONTEXT, mvpPagContext.getId());
		
		MediaViewPagerFragment fragment = new MediaViewPagerFragment();
		fragment.setData(data);
		
			FragmentChangeActivity fca = (FragmentChangeActivity) getActivity();
			fca.goTo(MediaViewPagerFragment.class,MediaViewPagerFragment.TAG, data, true);
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onListItemUpdateView(IMvpGenericAdapter adapter,
			MvpItemHolder holder, int position) {
		
		 Log.m(TAG,this, "onListItemUpdateView");
		 
		 Log.d(TAG,"type" + adapter.getType().getSimpleName() +  " position: " + position );	
		 
		 if (adapter.getType() == MvpCategory.class)
		 {
			 updateCategoryItemView(adapter, holder, position);
			 
			return;
		 }
		 
		 if (adapter.getType() == MvpMedia.class)
		 {
			 Log.d(TAG,"MvpMedia position: " + position );	
			 
			IMvpGenericAdapter horizontalListAdapter = (IMvpGenericAdapter) adapter;
			 MvpMedia mvpMedia = (MvpMedia) horizontalListAdapter.getItem(position);
			 
				if (null == mvpMedia)
				{
					holder.setVisibility(R.id.item_loading, View.VISIBLE);
					holder.setVisibility(R.id.body_view, View.INVISIBLE);
					return;
				}
				
				holder.setVisibility(R.id.item_loading, View.INVISIBLE);
				holder.setVisibility(R.id.body_view, View.VISIBLE);


				holder.setText(R.id.item_title,"[" +position+"]" +  mvpMedia.getTitle());
				MvpImageView view = (MvpImageView)holder.getView(R.id.item_image);
				view.setData(mvpMedia.getThumbPoster().getUrl());
			 return;
		 }
		
		 
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onReturnFromTask(Task task, boolean canceled) 
	{
		 Log.m(TAG,this, "onReturnFromTask");
		 
		if (task instanceof TaskGetVodCategories)
		{
			if (task.isError())
			{
				// error message
				return;
			}
			
	
		}
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onDataUpdate(Observable observable, Object data) 
	{
		 Log.m(TAG,this, "onDataUpdate");
		 
		if (observable instanceof MvpObject)
		{
			mvpCategoriesObj = (MvpObject)observable;
			
			MvpList<MvpCategory> listCategories = (MvpList<MvpCategory>) mvpCategoriesObj.getData();
			if  (null == listCategories)
			{
				// data might be removed from memory
				return;
			}
			
			MvpVerticalListView listView = (MvpVerticalListView) getHolder().getView(R.id.items_pagedlist);
			IMvpGenericAdapter categoriesAdapter = (IMvpGenericAdapter) listView.getAdapter();
			categoriesAdapter.setCount(1); 

		}
		
	}


	@Override
	public void onListItemsCountChanged(IMvpGenericAdapter adapter, int newCount) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public int onListItemGetLayoutId(IMvpGenericAdapter adapter, int position) {
		
		if (adapter.getType() == MvpCategory.class)
		{
			return R.layout.media_category_horizontal_list_row;
		}
		
		if (adapter.getType() == MvpMedia.class)
		{
			return R.layout.test_item;
		}
		
		return 0;
	}

}
