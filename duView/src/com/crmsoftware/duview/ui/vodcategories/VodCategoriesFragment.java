package com.crmsoftware.duview.ui.vodcategories;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.SearchView;
import android.widget.SearchView.OnCloseListener;
import android.widget.SearchView.OnQueryTextListener;

import com.actionbarsherlock.view.MenuItem;
import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.FragmentChangeActivity;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.configuration.PlatformSettings;
import com.crmsoftware.duview.core.common.HttpReqParams;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.data.MpxPagHeader;
import com.crmsoftware.duview.data.MvpCategory;
import com.crmsoftware.duview.data.MvpContent;
import com.crmsoftware.duview.data.MvpList;
import com.crmsoftware.duview.data.MvpMedia;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.data.MvpParentalManagement;
import com.crmsoftware.duview.data.MvpServerError;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.ui.MvpFragment;
import com.crmsoftware.duview.ui.apphelp.AppHelpFragment;
import com.crmsoftware.duview.ui.common.IMvpGenericAdapter;
import com.crmsoftware.duview.ui.common.IMvpGenericListViewListener;
import com.crmsoftware.duview.ui.common.MediaListViewFragment;
import com.crmsoftware.duview.ui.common.MediaViewPagerFragment;
import com.crmsoftware.duview.ui.common.MvpHorizontalListView;
import com.crmsoftware.duview.ui.common.MvpImageView;
import com.crmsoftware.duview.ui.common.MvpItemHolder;
import com.crmsoftware.duview.ui.common.MvpPagContext;
import com.crmsoftware.duview.ui.common.MvpPagListAdapter;
import com.crmsoftware.duview.ui.common.MvpPagPagerAdapter;
import com.crmsoftware.duview.ui.common.MvpScrollView;
import com.crmsoftware.duview.ui.common.MvpSimpleListAdapter;
import com.crmsoftware.duview.ui.common.MvpVerticalListView;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.ui.common.MvpViewPager;
import com.crmsoftware.duview.ui.common.MvpViewPager.PageChangeListener;
import com.crmsoftware.duview.utils.IConst;
import com.crmsoftware.duview.utils.IHttpConst;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.ParentalPinDialogView;
import com.crmsoftware.duview.utils.Utils;



public class VodCategoriesFragment extends MvpFragment implements 
IMvpGenericListViewListener,
ITaskReturn,
IFilterView,
OnQueryTextListener,
OnCloseListener,
PageChangeListener,
OnDismissListener
{
	
	private static final String UIMODE_LANDSCAPE = "Landscape";
	private static final String UIMODE_PREMIUM = "Premium";
	
	public static boolean DEBUG = Log.DEBUG; 
	public final static String TAG = VodCategoriesFragment.class.getSimpleName();
	
	 public static final String 			FRAGMENT_DATA 									= "fragmentData";
	
	protected MvpObject mvpCategoriesObj = null;
	/**
	 * @desc property that represents the task that will request the categories
	 */
	protected TaskGetVodCategories whatsNew ;
	protected MvpList<MvpCategory> listCategories = null;
	List<MvpViewHolder> categoriesHolder = new ArrayList<MvpViewHolder>(); 
	
	public static String MVP_OBJECT_ID = "MVP_OBJECT_ID";
	
	public static String ROOT_CATEGORY_ID = "ROOTCATEGORY_ID";
	public static String ROOT_CATEGORY_TITLE = "ROOTCATEGORY_TITLE";
	
	public static String SEARCH_TEXT = "SEARCH_TEXT";
	public static String FILTER_CATEGORY_ID = "FILTER_CATEGORY_ID";
	public static String SELECTED_ITEM = "SELECTED_ITEM";
	
	public static long DATA_LIFE_TIME = 60*60*1000; // 60min life (the data is taken from cache and not updated from server)


	private SearchView mSearchView;
	private MenuItem searchItem;
	private MenuItem filters;
	private String categoryTitle;
	private String categoryId;
	private Bundle data;
	private int selectedItem;
	/**
	 * @desc flag activated when an event is received and no other needs to be received.
	 */
	private boolean mProcessViewClick = true;
	/**
	 * @desc lock used to secure the touch event processing
	 */
	private ReentrantLock mTouchEventLock = new ReentrantLock();
	/**
	 * @desc the instance to the view containing the category filters. 
	 * */
	FilterView filterview = null;

	
	int posterWidth;
	int pageWidth;
	
	int posterSpace;
	int pageSpace;
	private Bundle dataDetails;
	private static boolean isAppHelpTemp = true;
	private MvpSimpleListAdapter adapter;
	private Task taskParentalPin;
	
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		if (DEBUG) Log.m(TAG,this,"onCreateView");
		
		View view = setContentView(R.layout.fragment_vertical_list_view,container,inflater);

		
		data = getData();
		
		categoryTitle = data.getString(ROOT_CATEGORY_TITLE, "");
		categoryId = data.getString(ROOT_CATEGORY_ID, "");	
		selectedItem = data.getInt(SELECTED_ITEM);	
		
		getMvpActivity().setMenuEnabled(true);
		getMvpActivity().setActionBarVisibility(View.VISIBLE);
		getMvpActivity().setHomeButtonActionBar(true);

		setHasOptionsMenu(true);
       
		mvpCategoriesObj = MvpObject.newReference(TaskGetVodCategories.OBJPREFIX_CATEGORIES);
        
		Resources res = getResources();
		posterWidth = (int) res.getDimension(R.dimen.horizontal_poster_item_width);
		posterSpace = (int) res.getDimension(R.dimen.horizontal_poster_item_space);
		
		pageWidth = (int) res.getDimension(R.dimen.horizontal_pager_item_width);
		pageSpace = (int) res.getDimension(R.dimen.horizontal_pager_item_space);
		

		MvpUserData authData = MvpUserData.getFromMvpObject();
        initCategoriesList();
        if (savedInstanceState == null)
        {
        	if (false == AppHelpFragment.tempDisabled)
        	{
	        	
	        	if (authData.getHelp())
	        	{
	        		isAppHelpTemp = AppHelpFragment.tempDisabled;
	        		
	        		getMvpActivity().show(AppHelpFragment.class, AppHelpFragment.TAG, AppHelpFragment.getConfig(1));
	        	}
        	}
        	resetFilteredData();
        }
        else
        {
             dataDetails = savedInstanceState.getBundle(FRAGMENT_DATA);
        }
        
		return view;
	}
	
	@Override  
	
	public void onResume(){
		super.onResume();

		/*will activate the touch interception*/
		mProcessViewClick = true;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void initCategoriesList()
	{		
		MvpVerticalListView listView = (MvpVerticalListView) getHolder().getView(R.id.items_pagedlist);
		listView.removeAllViewsInLayout();
        
		/*show loading*/
		getHolder().setVisibility(R.id.item_loading, View.VISIBLE);
		
		selectedItem = 0;
		
        adapter = new MvpSimpleListAdapter(MvpCategory.class, this);
        adapter.receiveClickFrom(R.id.see_all);
        adapter.receiveClickFrom(R.id.body_view);   
        
        listView.setAdapter(adapter);
        
        updateActionbar();
        
        whatsNew = new TaskGetVodCategories(this, mvpCategoriesObj);
        whatsNew.execute();
	}
	
	
	/**
	 * 
	 * @desc called when the fragment is switched from the menu.
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onResetData() {
		
		getMvpActivity().hide(AppHelpFragment.TAG);
		resetFilteredData();
		
		//Utils.clearMemory();

		super.onResetData();
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onDestroyView() {
		if (DEBUG) Log.m(TAG,this,"onDestroyView");
		
		
		isAppHelpTemp = true;
		if (null != mSearchView)	// if you got other fragment searchbox closed
		{
			mSearchView.clearFocus();
		}
		
		if (null != adapter)
		{
			adapter.setGenericListViewListener(null); // this will release fragment instance
			adapter = null;
		}
		
		super.onDestroyView();
	}
	

	@Override
	public void onDestroy() {
		if (DEBUG) Log.m(TAG,this,"onDestroyView");

		/*cancel the what's new request without announcing the callback*/
		whatsNew.cancel(true);
		super.onDestroy();
	}
	
	
	@Override
    public boolean onBackPressed()
    {
		if (DEBUG) Log.m(TAG,this,"onBackPressed");
		/*
		 * Cancel filter from back
		 * 
		 * */
		boolean isSearch = (false == Utils.isEmpty(getData().getString(SEARCH_TEXT,"")));
		boolean isFilter = (false == Utils.isEmpty(data.getString(FILTER_CATEGORY_ID,"")));
		if(isFilter || isSearch)
		{
			MvpUserData authData = MvpUserData.getFromMvpObject();
			authData.setFilter(false);
			performFilter(null);
			return false;
		}
		
		
		/*
		 * Cancel search from back
		 * commented because it is a new feature. If this feature is needed un-comment
		 * 
		 * */
		
//		boolean isSearch = (false == Utils.isEmpty(getData().getString(SEARCH_TEXT,"")));
//		if ( isSearch ){
//			/*if a search is implemented then reset it*/
//			applyQuery("");
//			/*and cancel the back press*/
//			return false;
//		}
		
    	return true;
    }
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	

	public void goToListView(IMvpGenericAdapter adapter, MvpItemHolder holder)
	{
	
		MvpObject mvpPagContext = holder.getAdapterPagContext(R.id.items_pagedlist);
		
		Bundle data = new Bundle();
		
		MvpPagContext context = (MvpPagContext) mvpPagContext.getData();
		if(null != listCategories )
		{
			MvpCategory mvpCategory = listCategories.getItem(holder.getPosition());
			if(mvpCategory.getUiMode().equals(UIMODE_PREMIUM))
			{
				data.putString(MediaViewPagerFragment.DATA_PREMIUM_LOGO, mvpCategory.getPremiumLogo());
			}
		}
		data.putInt(MediaListViewFragment.DATA_POSITION, context.getFirstVisibleItem());
		data.putString(MediaListViewFragment.DATA_CONTEXT, mvpPagContext.getId());
		data.putString(MediaListViewFragment.DATA_VIEWTITLE, holder.getText(R.id.row_title));
		
		FragmentChangeActivity fca = (FragmentChangeActivity) getActivity();
		fca.goTo(MediaListViewFragment.class,MediaListViewFragment.TAG, data, true);
		
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see          
	 */	

	public void goToDetailsView(IMvpGenericAdapter adapter, MvpItemHolder holder)
	{
		MvpMedia  mvpMedia = (MvpMedia) adapter.getItem(holder.getPosition());
		if ( mvpMedia == null ){
			/*if clicked on the item but the mvpMedia is not loaded from server then do nothing*/
			if (DEBUG) {
				Log.d(TAG,"::goToDetailsView mvpMedia not loaded yet will not go to details");
			}
			/*reactivate the click processing*/
			mProcessViewClick = true;
			return;
		}
		MvpVerticalListView listView = (MvpVerticalListView) getHolder().getView(R.id.items_pagedlist);
		selectedItem = listView.getFirstVisiblePosition();
		getData().putInt(SELECTED_ITEM, selectedItem);
		
		
		MvpItemHolder holderParent = (MvpItemHolder) adapter.getTag();

		MvpObject mvpPagContext = adapter.getPaginationContext();
		
		dataDetails = new Bundle();
		
		if(null != listCategories )
		{
			MvpCategory mvpCategory = listCategories.getItem(holderParent.getPosition());
			if(mvpCategory.getUiMode().equals(UIMODE_PREMIUM))
			{
				dataDetails.putString(MediaViewPagerFragment.DATA_PREMIUM_LOGO, mvpCategory.getPremiumLogo());
			}
		}
		dataDetails.putInt(MediaViewPagerFragment.DATA_POSITION, holder.getPosition());
		dataDetails.putString(MediaViewPagerFragment.DATA_CONTEXT, mvpPagContext.getId());
		dataDetails.putString(MediaListViewFragment.DATA_VIEWTITLE, "Details");
		
		MediaViewPagerFragment fragment = new MediaViewPagerFragment();
		fragment.setData(data);
		
		if (MvpParentalManagement.getInstance().checkLevel(mvpMedia.getParentalLevel()) == MvpParentalManagement.LEVEL_NOK)
		{
			ParentalPinDialogView parentalDialogView =   ParentalPinDialogView.getInstance();
			parentalDialogView.setData(getTag(), dataDetails);
			parentalDialogView.setOnDismissListener(this);
			parentalDialogView.show(getChildFragmentManager(), ParentalPinDialogView.TAG);
		}
		else
		{
			
			FragmentChangeActivity fca = (FragmentChangeActivity) getActivity();
			fca.goTo(MediaViewPagerFragment.class,MediaViewPagerFragment.TAG, dataDetails, true);
		}	
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onListItemViewClick(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder) 
	{
		mTouchEventLock.lock();
		
		if ( !mProcessViewClick ){

			mTouchEventLock.unlock();
			return;
		}
		
		
		 if (DEBUG) Log.m(TAG,this, "onListItemViewClick");
		
		 int position = holder.getPosition();
		 if (DEBUG) Log.d(TAG,"Position: " + position );	
		 
		 if (adapter.getType() == MvpCategory.class)
		 {
			switch (view.getId()) {
			case R.id.body_view:
				break;
			case R.id.see_all:
			{
				mProcessViewClick = false;
				getData().putInt(SELECTED_ITEM, position);
				goToListView(adapter,holder);
			}
				break;

			default:
				break;
			}
			 
		 }
		 
		 if (adapter.getType() == MvpMedia.class)
		 {
			 switch (view.getId()) {
			case R.id.item_image:
				//showLoadingDialog();
				try{
					mProcessViewClick = false;
					goToDetailsView(adapter,holder);
				}catch( NullPointerException ex ){
					mProcessViewClick = true;
					if(DEBUG) Log.d(TAG, "::onListItemViewClick NullPointerException probably caused by data not loaded yet");
				}
				break;

			default:
				break;
			}			 
			 

		 }

		mTouchEventLock.unlock();
		 
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public boolean onListItemViewTouch(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder, MotionEvent event) {
		
		mTouchEventLock.lock();
		
		if ( !mProcessViewClick ){

			mTouchEventLock.unlock();
			return false;
		}

		 if (DEBUG) Log.m(TAG,this, "onListItemViewTouch");

		 int position = holder.getPosition();
		 if (DEBUG) Log.d(TAG,"Position: " + position );	
		 
		 if (adapter.getType() == MvpCategory.class)
		 {

			 
		 }
		 
		 if (adapter.getType() == MvpMedia.class)
		 {
			 switch (view.getId()) 
			{
			case R.id.item_image:
			{
				mProcessViewClick = false;
				//showLoadingDialog();
				goToDetailsView(adapter,holder);
				mTouchEventLock.unlock();
				return true;
			}

			default:
				break;
			}			 
			 
		 }

		mTouchEventLock.unlock();
		 return false;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void updateCategoryHorizontalListRow (MvpCategory mvpCategory,
			MvpItemHolder holder, MvpObject mvpObjContext, int position)
	{	 
		 MvpHorizontalListView listView = (MvpHorizontalListView) holder.getView(R.id.items_pagedlist);
		 /*link the scroll to the list view*/
		 MvpScrollView theScroll = (MvpScrollView) holder.getView(R.id.scrollView);
		 listView.linkScrollView(theScroll);
		 
		 MvpPagListAdapter<MvpMedia> horizontalListAdapter = (MvpPagListAdapter<MvpMedia>) listView.getAdapter();	
		 if (horizontalListAdapter == null)
		 {
				horizontalListAdapter = new MvpPagListAdapter<MvpMedia>(
			 			MvpMedia.class, 
			 			1);
			 	
			 	horizontalListAdapter.receiveClickFrom(R.id.item_image);
			 	horizontalListAdapter.setGenericListViewListener(this);	
			 	
		 }
 
	 	String uiMode = mvpCategory.getUiMode();
	 	
		if (uiMode.equals(UIMODE_LANDSCAPE) || uiMode.equals(UIMODE_PREMIUM))
		{
	 		listView.setCellWidth(pageWidth);
	 		listView.setCellSpace(pageSpace);	
		}
		else
		{
	 		listView.setCellWidth(posterWidth);
	 		listView.setCellSpace(posterSpace);		
		}
		
		horizontalListAdapter.setTag(holder);	// keep the holder of the list row item for later usage (eq. seeAll counter)			

		listView.setAdapter(horizontalListAdapter);	// this will reset the list view 
		
		horizontalListAdapter.setPaginationContext(mvpObjContext);

	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void updateCategoryPagerRow (MvpCategory mvpCategory, 
			MvpItemHolder holder, MvpObject mvpObjContext, int position)
	{	
		MvpViewPager pagerView = (MvpViewPager) holder.getView(R.id.items_pagedlist);
		MvpPagPagerAdapter pagerAdapter =  (MvpPagPagerAdapter) pagerView.getAdapter();
		
		 if (pagerAdapter == null)
		 {
			pagerAdapter = new MvpPagPagerAdapter<MvpMedia>(
		 			MvpMedia.class);
			
			pagerAdapter.receiveClickFrom(R.id.item_image);
			pagerAdapter.setGenericListViewListener(this);
		 }
		 
        pagerAdapter.setTag(holder);	// keep the holder of the row item for later usage (eq. seeAll counter)		   

        pagerAdapter.setPaginationContext(mvpObjContext);
        
		pagerView.setAdapter(pagerAdapter);
	
		MvpPagContext ctx = (MvpPagContext) mvpObjContext.getData();
		/*initialize the viewpager*/
		initViewPager(pagerView, holder, ctx);
        pagerView.setCurrentItem(ctx.getFirstVisibleItem());
	}
	
	/**
	 * @desc method for configuring the viewPager on the VodCategory fragment.
	 * @param pagerView
	 * @param holder
	 * @param mvpObjContext 
	 */
	private void initViewPager(MvpViewPager pagerView, MvpViewHolder holder, MvpPagContext mvpObjContext){
		/*register for onpageChange to the ViewPager*/
		pagerView.setPageChangeListener(this);
		/*start the automatic page changer*/
		pagerView.setAutomaticPageChange(false);
		PagerAdapter pagerAdapter =  (PagerAdapter) pagerView.getAdapter();
		
//		pageChanged(pagerView, 0);
	}
	
	/**
	 * 
	 * @param  
	 * @return      
	 * @see         
	 */	
	@SuppressLint("NewApi") 
	private void updateCategoryItemView (IMvpGenericAdapter adapter,
			MvpItemHolder holder, int position)
	{
		 	if (DEBUG) Log.d(TAG,"MvpCategory position: " + position );	

			holder.setVisibility(R.id.item_loading, View.VISIBLE);
			holder.setVisibility(R.id.body_view, View.INVISIBLE);
			
			if (null == listCategories)
			{
				return;
			}

			MvpCategory mvpCategory = listCategories.getItem(position);
			
			
			String title = mvpCategory.getTitle();
			String categoryId = mvpCategory.getScheme() + IHttpConst.CHAR_DOTS + mvpCategory.getFullTitle();
			
			holder.setText(R.id.row_title, title);
			if (DEBUG) Log.d(TAG,"MvpCategory title: " + title);	
			
			Bundle data = getData();
			long dataLifeTime = DATA_LIFE_TIME;
			String searchString  = data.getString(SEARCH_TEXT,"");
			String filerCategoryId  = data.getString(FILTER_CATEGORY_ID,"");
			HttpReqParams httpReqParams = new HttpReqParams(
					IHttpConst.VALUE_SCHEMA_1_2, 
					IHttpConst.VALUE_CJSON);
			
			//httpReqParams.setThubmnailFilter("Thumbnail");	

			boolean isSearch = (false == Utils.isEmpty(searchString));
			boolean isFilter = (false == Utils.isEmpty(filerCategoryId));
			if (!isSearch)	// norma and filtered
			{
				httpReqParams.setSort(IHttpConst.VALUE_FIELD_ADDED);
			}

			if (isSearch)	// search case
			{
				dataLifeTime = -1;  // -1  = no cache -> will not get data from cache but from server
				httpReqParams.setFilter(searchString);
			}	
			
			if (isFilter)	// filter case	(this will be with cache)
			{
				dataLifeTime = -1;  // -1  = no cache
				categoryId += IHttpConst.CHAR_COMMA + PlatformSettings.getInstance().getFilterScheme() + IHttpConst.CHAR_DOTS + filerCategoryId;
			}

			httpReqParams.setParam(IHttpConst.PARAM_BY_CATEGORIES, categoryId);	
			
			String mvpObjContextId = buildCategoryMvpObjId(categoryId, searchString);
			
			MvpObject mvpObjContext = MvpObject.getReference(mvpObjContextId);
			
			
			if (mvpObjContext == null)
			{				
				mvpObjContext = MvpPagContext.getContext(
						MpxPagHeader.class,
						MvpPagContext.STARTWITH_ONE,						
						PlatformSettings.getInstance().getTpMpxFeedUrl(),
			 			httpReqParams,
			 			mvpObjContextId,
			 			dataLifeTime);
			}
			MvpPagContext context = (MvpPagContext)mvpObjContext.getData();
			
			updateCounter(holder, context.getCount());
			
			
			if ( context.getCount() != 0 ){
				 if (listCategories.getItem(position).getUiMode().equals(UIMODE_PREMIUM)){
					 holder.getView(R.id.body_view).setBackground(getResources().getDrawable(R.drawable.premium_category_row_bk));
				 }
				switch (holder.getLayoutId())
				{
				
				case R.layout.media_category_pager_row:
					updateCategoryPagerRow(mvpCategory, holder, mvpObjContext, position);
					break;
				
				case R.layout.media_category_horizontal_list_row:
					updateCategoryHorizontalListRow(mvpCategory, holder, mvpObjContext, position);
					break;
				
				case R.layout.media_category_horizontal_list_row_3rows:
					updateCategoryHorizontalListRow(mvpCategory, holder, mvpObjContext, position);
					break;
				
				default:
				
					break;
				}
			}else{
				holder.getView().setVisibility(View.INVISIBLE);
			}
/*			
 * 
			
			if (null != mvpObjContext)
			{
				MvpPagContext ctx = (MvpPagContext) mvpObjContext.getData();
				updateCounter(holder, ctx.getCount());
			}*/
	}
	
	
	private String buildCategoryMvpObjId(String categoryId, String searchString)
	{
		return  Utils.formatLink("TpMpxFeedUrl."+ (searchString.isEmpty() ? "" : "q") +"." + categoryId);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void updateMediaPagerItem (MvpMedia mvpMedia,
			MvpItemHolder holder, int position)
	{
		if (null == mvpMedia)
		{
			holder.setVisibility(R.id.item_loading, View.VISIBLE);
			holder.setVisibility(R.id.body_view, View.INVISIBLE);
			
			return;
		}
		Log.d(TAG, "updateMediaPagerItem: "+position);
		
		holder.setVisibility(R.id.item_loading, View.INVISIBLE);
		holder.setVisibility(R.id.body_view, View.VISIBLE);
		 
		 MvpImageView itemImage = (MvpImageView) holder.getView(R.id.item_image); 
		 
		 String sImageURL = "";
		 MvpContent itemImageData = mvpMedia.getThumbLandscape();
		 if (null != itemImageData)
		 {
			 sImageURL = itemImageData.getUrl();
		 }
		 itemImage.setData(sImageURL);
		 
		 holder.setText(R.id.item_title, mvpMedia.getTitle());
		 holder.setText(R.id.item_genre, mvpMedia.getGenres());
		 
	}
	
	
	/**
	 * 
	 *�
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void updateMediaHorizontalItem (MvpMedia mvpMedia,
			MvpItemHolder holder, int position)
	{
		MvpImageView itemImage = (MvpImageView) holder.getView(R.id.item_image); 
		if (null == mvpMedia)
		{
			holder.setVisibility(R.id.item_loading, View.VISIBLE);
			holder.setVisibility(R.id.body_view, View.INVISIBLE);
			return;
		}
		
		holder.setVisibility(R.id.item_loading, View.INVISIBLE);
		holder.setVisibility(R.id.body_view, View.VISIBLE);
		 
		 String sImageURL = "";
		 MvpContent itemImageData = mvpMedia.getThumbPoster();
		 if (null != itemImageData)
		 {
			 sImageURL = itemImageData.getUrl();
		 }
		 itemImage.setData(sImageURL);		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void updateMediaItemView (IMvpGenericAdapter adapter,
			MvpItemHolder holder, int position)
	{
		MvpMedia  mvpMedia = (MvpMedia) adapter.getItem(position);
		if (null == mvpMedia)
		{
			MvpObject obj = adapter.getPaginationContext();
					
			MvpPagContext ctx = (MvpPagContext) obj.getData();
			
/*			if (ctx != null)
			{
				ctx.refresh();
			}*/
			return;
		}
		switch (holder.getLayoutId())
		{	
		case R.layout.media_category_pager_row_item:
			updateMediaPagerItem(mvpMedia, holder, position);
			break;
		
		case R.layout.media_category_horizontal_list_row_item:
			updateMediaHorizontalItem(mvpMedia, holder, position);
			break;
		
		default:
		
			break;
		}	
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@SuppressLint("NewApi") @Override
	public void onListItemUpdateView(IMvpGenericAdapter adapter,
			MvpItemHolder holder, int position) {
		
		 if (DEBUG) Log.m(TAG,this, "onListItemUpdateView");
		 
		 if (DEBUG) Log.d(TAG,"type " + adapter.getType().getSimpleName() +  " position: " + position );
		 
		 
		 if (adapter.getType() == MvpCategory.class)
		 {
			 updateCategoryItemView(adapter, holder, position);
			 
			return;
		 }
		 
		 if (adapter.getType() == MvpMedia.class)
		 {
			 if (DEBUG) Log.d(TAG,"MvpMedia position: " + position );	
			 
			 updateMediaItemView(adapter, holder, position);
			 
			 return;
		 }
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onReturnFromTask(Task task, boolean canceled) 
	{
		 if (DEBUG) Log.m(TAG,this, "onReturnFromTask");
		
		if(task.isError())
		{
			Object errorasd = task.getError();
			MvpServerError mvpErroras = (MvpServerError)errorasd;
			int errorCodeasd = mvpErroras.getResponseCode();
			/*workaround in case of parental errors*/
			if(task == taskParentalPin)
			{
			Object error = task.getError();
			MvpServerError mvpError = (MvpServerError)error;
			int errorCode = mvpError.getResponseCode();
			getMvpActivity().hideLoadingDialog();
			if(errorCode == 200)
			{
				FragmentChangeActivity fca = (FragmentChangeActivity) getActivity();
				fca.goTo(MediaViewPagerFragment.class, MediaViewPagerFragment.TAG, dataDetails, true);
			}
			
			if (errorCode == 403)
			{
				showToast(mvpError.getTitle());
				ParentalPinDialogView.getInstance().setEmptyField();
				return;
			}
			
			if (errorCode == 400)
			{
				showToast(MainApp.getRes().getString(R.string.change_pins_settings_wrong));
				ParentalPinDialogView.getInstance().setEmptyField();
				return;
			}
			taskParentalPin = null;
			}
			else
			{
				showToast(MainApp.getRes().getString(R.string.failed_to_recive_data));
			}
			
			getMvpActivity().hideLoadingDialog();
			checkToken(task.getError());
			return;
		}
			
		if (task instanceof TaskGetVodCategories)
		{
			Map<String, List<MvpCategory>> mapCategories = (Map<String, List<MvpCategory>>) mvpCategoriesObj.getData();
			if  (null == mapCategories)
			{
				return;
			}
			
			listCategories = (MvpList<MvpCategory>) mapCategories.get(categoryId);
			if (null == listCategories)
			{
				return;
			}
			
			/*hide loading*/
			getHolder().setVisibility(R.id.item_loading, View.GONE);
			
			MvpVerticalListView listView = (MvpVerticalListView) getHolder().getView(R.id.items_pagedlist);
			IMvpGenericAdapter categoriesAdapter = (IMvpGenericAdapter) listView.getAdapter();
			/*the requests will be done for setCount() and cancelled because of the new requests done for setSelection()*/
			categoriesAdapter.setCount(listCategories.size()); 
			listView.setSelection(selectedItem);
		}
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */

/*	@Override
	public void onDataUpdate(Observable observable, Object data) 
	{
		 if (DEBUG) Log.m(TAG,this, "onDataUpdate");
		 
		if (observable instanceof MvpObject)
		{
			mvpCategoriesObj = (MvpObject)observable; // only categories are comming here 
			
			MvpList<MvpCategory> listCategories = (MvpList<MvpCategory>) mvpCategoriesObj.getData();
			if  (null == listCategories)
			{
				// data might be removed from memory
				return;
			}
			
			
			MvpVerticalListView listView = (MvpVerticalListView) getHolder().getView(R.id.items_pagedlist);
			IMvpGenericAdapter categoriesAdapter = (IMvpGenericAdapter) listView.getAdapter();
			categoriesAdapter.setCount(listCategories.size()); 

		}
		
	}*/
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu,
			com.actionbarsherlock.view.MenuInflater inflater) {
		 if (DEBUG) Log.m(TAG,this, "onCreateOptionsMenu");
		 
		inflater.inflate(R.menu.whats_new_menu, menu);
		searchItem = menu.findItem(R.id.grid_default_search);
		filters = menu.findItem(R.id.grid_default_filter);
		
		mSearchView = (SearchView) searchItem.getActionView();
	    mSearchView.setOnQueryTextListener(this);
	    mSearchView.setOnCloseListener(this);
	    searchItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		updateActionbar();
		  
		super.onCreateOptionsMenu(menu, inflater);
	}

	/* (non-Javadoc)
	 * @see com.actionbarsherlock.app.SherlockFragment#onPrepareOptionsMenu(android.view.Menu)
	 * @desc will clear the search textbox if there is not search text saved. 
	 * 		 The search textbox keeps the search string even after the fragment switching. 
	 * 		 This will prevent it from doing that.
	 */
	@Override
	public void onPrepareOptionsMenu (com.actionbarsherlock.view.Menu menu){
		 if (DEBUG) Log.m(TAG,this, "onPrepareOptionsMenu");
		 String query = getData().getString(SEARCH_TEXT,"");
		boolean isSearch = (false == Utils.isEmpty(query));
		if ( !isSearch ){
			mSearchView.setQuery("", false);
		}else{
			mSearchView.setIconified(false);	
			mSearchView.setQuery(query, false);
		}
		
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@SuppressLint("NewApi")
	private void updateActionbar()
	{
		 if (DEBUG) Log.m(TAG,this, "updateActionbar");
		 
		if (null == mSearchView)
		{
			return;
		}
		
		Bundle data = getData();
		String searchString  = data.getString(SEARCH_TEXT,"");
		String filerCategoryId  = data.getString(FILTER_CATEGORY_ID,"");
		
		if (false == Utils.isEmpty(searchString))
		{
			//put text
		}
		else
		{
			mSearchView.onActionViewCollapsed();
		}
		
		if (false == Utils.isEmpty(filerCategoryId))
		{
			filters.setIcon(R.drawable.filters_on);
		}
		else
		{
			filters.setIcon(R.drawable.filters);
		}
		
		if(Utils.isEmpty(categoryTitle) && data != null)
		{
			categoryTitle = data.getString(ROOT_CATEGORY_TITLE, "");	
		}
		getMvpActivity().setTitle(getData().getString(ROOT_CATEGORY_TITLE, ""));
	}
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public boolean onQueryTextChange(String newText) {

//		applyQuery(newText);
		
	    return false;
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public boolean onQueryTextSubmit(String query) 
	{
		 if (DEBUG) Log.m(TAG,this, "onQueryTextSubmit");
		 

		 applyQuery(query);

		
	    return true;
	}
	/**
	 * @param query
	 * @desc this method will apply the passed string as the search text.
	 * 		 If the passed string is empty will clear the search
	 */
	private void applyQuery(String query){
		resetFilteredData();
		
		mSearchView.clearFocus();
		
		getData().putString(SEARCH_TEXT, query); // save the search (in case of tablet rotation etc.)	
		
		initCategoriesList();
		hideKeyboard(getActivity());
	}


	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public boolean onClose() {

		getData().putString(SEARCH_TEXT, ""); // reset the search 
		mSearchView.clearFocus();
		mSearchView.setQuery("", false);

		initCategoriesList();
			
	    return true;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void resetFilteredData() 
	{
		Bundle data = getData();
		String searchString  = data.getString(SEARCH_TEXT,"");
		data.remove(SEARCH_TEXT);
		data.remove(FILTER_CATEGORY_ID);
		data.remove(SELECTED_ITEM);
		
		if (Utils.isEmpty(searchString))
		{
			return;
		}
		
		if (null == listCategories)	
		{
			return;
		}
		
		// if is search then remove the pag contexts

		for (int i=0;i<listCategories.size(); i++)
		{
			MvpCategory mvpCategory = listCategories.getItem(i);
					
			String title = mvpCategory.getTitle();
			String categoryId = mvpCategory.getScheme() + IHttpConst.CHAR_DOTS + mvpCategory.getFullTitle();
		
			String mvpObjContextId = buildCategoryMvpObjId(categoryId, searchString);			
			MvpObject mvpObjContext = MvpObject.removeReference(mvpObjContextId);
			
			if (null == mvpObjContext)
			{
				continue;
			}
			
			 MvpPagContext pagContext =  (MvpPagContext) mvpObjContext.getData();
			 pagContext.unbindAdapter();
			 mvpObjContext = null;
			 MvpObject.removeReference(mvpObjContextId);
		}    

	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected boolean isAlwaysExpanded() {
	    return false;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.grid_default_filter:
			if(AppHelpFragment.tempDisabled !=false)
			{
				isAppHelpTemp = AppHelpFragment.tempDisabled;
			}
			if (true == isAppHelpTemp)
        	{
			setDropDown();
        	}
              return true;  
             }
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void setDropDown()
	{
		 if ( filterview == null )
		 {
			filterview = FilterView.newInstance();
		 }
		 /*re check in case the new instances is not created.*/
		 if(filterview != null) 
		 {
			filterview.show(getFragmentManager(), IConst.DIALOG_CATEGORY_TAG);
		 }
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void updateCounter(MvpItemHolder holder , int newCount) 
	{		
		String sCount = String.format(MainApp.getRes().getString(R.string.content_number), newCount);
		
		holder.setText(R.id.content_number, sCount);
		
		holder.setVisibility(R.id.content_number, newCount>0 ? View.VISIBLE : View.INVISIBLE);
		holder.setVisibility(R.id.see_all_arrow, newCount>0 ? View.VISIBLE : View.INVISIBLE);
		holder.setVisibility(R.id.see_all_text, newCount>0 ? View.VISIBLE : View.INVISIBLE);
		
		holder.setVisibility(R.id.item_loading, newCount>=0 ? View.INVISIBLE : View.VISIBLE);
		holder.setVisibility(R.id.body_view, newCount>=0 ? View.VISIBLE : View.INVISIBLE);	
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onListItemsCountChanged(IMvpGenericAdapter adapter, int newCount) 
	{
		 if (DEBUG) Log.m(TAG,this, "onListItemsCountChanged");
		 
		 if (adapter.getType() == MvpMedia.class)
		 {
			MvpItemHolder holder = (MvpItemHolder) adapter.getTag();
			
			updateCounter(holder, newCount);
			
		 }
	}
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void performFilter(MvpCategory category) 
	{
		resetFilteredData();

		if(category != null)
		{			
			Bundle data = getData();
			data.putString(FILTER_CATEGORY_ID, category.getFullTitle());
		}

		initCategoriesList();
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public static void hideKeyboard(Activity activity) {
	    InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
	    //Find the currently focused view, so we can grab the correct window token from it.
	    View view = activity.getCurrentFocus();
	    //If no view currently has focus, create a new one, just so we can grab a window token from it
	    if(view == null) {
	        view = new View(activity);
	    }
	    inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public int onListItemGetLayoutId(IMvpGenericAdapter adapter, int position) {
		
		if (adapter.getType() == MvpCategory.class)
		{
			if (MainApp.isTablet())
			{
				return R.layout.media_category_horizontal_list_row;
			}
			
			if (null == listCategories)
			{
				return R.layout.media_category_horizontal_list_row;
			}

			MvpCategory mvpCategory = listCategories.getItem(position);
			String uiMode = mvpCategory.getUiMode();
			
			if (uiMode.equals(UIMODE_LANDSCAPE) || uiMode.equals(UIMODE_PREMIUM))
			{
				return R.layout.media_category_pager_row;
			}
					
			return R.layout.media_category_horizontal_list_row; // default
		}
		
		
		if (adapter.getType() == MvpMedia.class)
		{		
			if (adapter instanceof MvpPagPagerAdapter)
			{
				return R.layout.media_category_pager_row_item;
			}
			
			if (adapter instanceof MvpPagListAdapter)
			{
				MvpItemHolder holder = (MvpItemHolder) adapter.getTag();
				
				MvpCategory mvpCategory = listCategories.getItem(holder.getPosition());
				String uiMode = mvpCategory.getUiMode();
				
				if (uiMode.equals(UIMODE_LANDSCAPE) || uiMode.equals(UIMODE_PREMIUM))
				{
					return R.layout.media_category_pager_row_item;
				}
				return R.layout.media_category_horizontal_list_row_item;
			}
		}
		
		return 0;
	}
	
	@Override
	public void onMvpDlgClose(String dlgTag, Bundle data) {
		
		
		int viewId = data.getInt(ParentalPinDialogView.RETURN_STATE);
		
		switch (viewId) 
		{
		case R.id.button_pozitive:
			String pin = data.getString(ParentalPinDialogView.PIN_VALUE);
			if(Utils.isEmpty(pin))
			{
				showToast("Field is empty");
			}
			else
			{
				getMvpActivity().showLoadingDialog();
				taskParentalPin = MvpParentalManagement.getInstance().checkPin(pin, this);
			}
			break;

		default:
			break;
		}
		
		super.onMvpDlgClose(dlgTag, data);
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putBundle(FRAGMENT_DATA, dataDetails);
		
		super.onSaveInstanceState(outState);
	}


	/***************************************
	 * 
	 * Implementations from PageChangeListener
	 * 
	 ***************************************/
	



	/* (non-Javadoc)
	 * @see com.crmsoftware.duview.ui.common.MvpViewPager.PageChangeListener#pageChanged(com.crmsoftware.duview.ui.common.MvpViewPager, int)
	 */
	@Override
	public void pageChanged(MvpViewPager viewPager, int position) {
		int currentPosition = viewPager.getCurrentItem();
		int elementCount = viewPager.getAdapter().getCount();
		MvpViewHolder parentHolder = (MvpViewHolder)viewPager.getTag();
		
		if( Log.DEBUG ) Log.d(TAG, "updateMediaItemView pager current item:" + currentPosition + " childCount="+elementCount);
		
		
		if ( currentPosition == 0  || elementCount <= 1){
			
			/*if the first item is displayed*/
			/*hide the left arrow*/
			parentHolder.setVisibility(R.id.left_arrow, View.GONE);
		}else{
			parentHolder.setVisibility(R.id.left_arrow, View.VISIBLE);
		}
		if ( currentPosition == elementCount - 1 || elementCount <= 1){
			/*if the last item is displayed*/
			/*hide the right arrow*/
			parentHolder.setVisibility(R.id.right_arrow, View.GONE);
		}else{
			parentHolder.setVisibility(R.id.right_arrow, View.VISIBLE);
		}
	}

	/* (non-Javadoc)
	 * @see android.content.DialogInterface.OnDismissListener#onDismiss(android.content.DialogInterface)
	 * @desc used to reactivate the click processing when the dialog closes.
	 */
	@Override
	public void onDismiss(DialogInterface dialog) {
		mTouchEventLock.lock();
		/*if a parental pin dialog will be shown activate the click processing 
		 * in case the user selects cancel to still be able to click*/
		mProcessViewClick = true;
		mTouchEventLock.unlock();
	}

}
