package com.crmsoftware.duview.ui.vodcategories;

import com.crmsoftware.duview.data.MvpCategory;

public interface IFilterView 
{

	public void performFilter(MvpCategory category);
}
