package com.crmsoftware.duview.ui.vodcategories;

import android.R.color;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.IMvpActivity;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.configuration.PlatformSettings;
import com.crmsoftware.duview.core.common.HttpReqParams;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.core.common.TaskPageReqCache;
import com.crmsoftware.duview.data.MpxPagHeader;
import com.crmsoftware.duview.data.MvpCategory;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.data.MvpPage;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.ui.MvpFragment;
import com.crmsoftware.duview.ui.common.IMvpGenericAdapter;
import com.crmsoftware.duview.ui.common.IMvpGenericListViewListener;
import com.crmsoftware.duview.ui.common.MvpItemHolder;
import com.crmsoftware.duview.ui.common.MvpSimpleListAdapter;
import com.crmsoftware.duview.ui.common.MvpVerticalListView;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.utils.IHttpConst;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;

public class FilterView 
extends DialogFragment 
implements
ITaskReturn, 
IMvpGenericListViewListener,
OnClickListener
{

	public static boolean DEBUG = Log.DEBUG;
	public final static String TAG = FilterView.class.getSimpleName();
	public static final String DATA_POSITION = "DATA_POSITION";
	private Dialog dialog;
	public MvpObject mvpCategoriesObj;
	private MvpSimpleListAdapter adapter;
	MvpViewHolder viewHolder;
	TaskPageReqCache<MvpCategory> taskGenres;
	private MvpItemHolder memHolder;
	private TextView text;
	private MvpFragment fragment;
	private Button btnNoFilter;
	private MvpVerticalListView listView;
	private IMvpActivity mvpActivity = null;
	private static Bundle data = new Bundle();
	
	private int memPosition = -1;
	private MvpUserData authData;
	
	private static int y;

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public static FilterView newInstance() {
		return new FilterView();
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public View onCreateView(final LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) 
	{
		super.onCreateView(inflater, container, savedInstanceState);
		
		if (DEBUG) Log.m(TAG, this, "onCreateView");
		View containerView = inflater.inflate(R.layout.filter_list_view,
				container, false);
		mvpActivity = (IMvpActivity) getActivity();
		if(getActivity().getActionBar().getHeight() != 0)
		{
			y = getActivity().getActionBar().getHeight();
		}
		viewHolder = new MvpViewHolder(containerView);
		
		btnNoFilter = (Button) viewHolder.getView(R.id.btn_no_filter);
		viewHolder.setClickListener(this, R.id.btn_no_filter);

		listView = (MvpVerticalListView)viewHolder.getView(R.id.items_pagedlist);
		 adapter = new MvpSimpleListAdapter(
				 			MvpCategory.class, 
				 			this);

		 adapter.receiveClickFrom(R.id.filter_layout);
		 
		 viewHolder.setAdapter(R.id.items_pagedlist, adapter);

		 mvpCategoriesObj = MvpObject.newReference(Utils.formatLink(PlatformSettings.getInstance().getFilterVoDList()));
		
		MvpPage<MvpCategory> page = (MvpPage<MvpCategory>) mvpCategoriesObj.getData();
		if (page != null)
		{
			adapter.setCount(page.size());
		}
		else
		{
			performCategoryListReq();
		}
		
		initDialog();
		final int position = getData().getInt(DATA_POSITION, -1);
		if(savedInstanceState != null)
		{
			memPosition = position;
			
		}
		authData = MvpUserData.getFromMvpObject();
		
		if(!authData.isFilter())
		{
			memPosition = -1;
			memHolder = null;
			showNoFilterBtn();
		}
		
		new Handler().post(new Runnable() {
			
			@Override
			public void run() {
				listView.setSelection(position);
			}
		});
		 

		return containerView;
	}

	public Bundle getData() {
		return data;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	private void initDialog() {
		if (DEBUG)
			Log.m(TAG, this, "initLoadingFragment");
		dialog = getDialog();
		if (dialog != null) {
			dialog.setCanceledOnTouchOutside(true);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			
			WindowManager.LayoutParams params = new WindowManager.LayoutParams();
//			params.copyFrom(dialog.getWindow().getAttributes());
			params.width = (int) MainApp.getRes().getDimension(R.dimen.filter_list_width);
			params.gravity = Gravity.TOP | Gravity.RIGHT;
			dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
			dialog.getWindow().setAttributes(params);
			dialog.getWindow().getAttributes().y = y;
			dialog.getWindow().getAttributes().x =  (y / 3) ;
			dialog.getWindow().setBackgroundDrawableResource(color.transparent);
			dialog.show();
		}

	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onDestroy() 
	{
		if (DEBUG) Log.m(TAG, this, "onDestroy");
		
		if (null != taskGenres)
		{
			taskGenres.cancel(true);
		}
		super.onDestroy();
	}


	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onReturnFromTask(Task task, boolean canceled) 
	{
		if (DEBUG) Log.m(TAG, this, "onReturnFromTask");
		
		if (task.isError()) 
		{
			return;
		}
		
		if (null == viewHolder)
		{
			return;
		}
	
		MvpPage<MvpCategory> page = (MvpPage<MvpCategory>) mvpCategoriesObj.getData();
		adapter.setCount(page.size());
		
		viewHolder.setVisibility(R.id.item_loading, View.INVISIBLE);
		viewHolder.setVisibility(R.id.items_pagedlist, View.VISIBLE);
		return;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected void performCategoryListReq() 
	{
		if (DEBUG) Log.m(TAG, this, "performCategoryListReq");
		
		viewHolder.setVisibility(R.id.item_loading, View.VISIBLE);
		viewHolder.setVisibility(R.id.items_pagedlist, View.INVISIBLE);
		
		
		HttpReqParams httpReqParams = new HttpReqParams();

		httpReqParams.setParam(IHttpConst.BY_SCHEME, PlatformSettings.getInstance().getFilterScheme());
		httpReqParams.setRange(1, 500);
		
		taskGenres = new TaskPageReqCache<MvpCategory>(
				this, 
				MvpCategory.class, 
				MpxPagHeader.class,
				PlatformSettings.getInstance().getFilterVoDList(),
				httpReqParams,
				mvpCategoriesObj,
				60 * 1000);

		taskGenres.execute();

	}


	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onListItemViewClick(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder) 
	{
		switch(view.getId())
		{
			case R.id.filter_layout:
			{
			 fragment = ((IMvpActivity) getActivity()).getCurrentScreen();
				
				if (null == fragment)
				{
					return;
				}
				
				if (fragment instanceof IFilterView)
				{
					dismiss();
					
					MvpPage<MvpCategory> page = (MvpPage<MvpCategory>) mvpCategoriesObj.getData();
					((IFilterView)fragment).performFilter(page.getItem(holder.getPosition())); 
					if(memHolder != null)
					{
						if(memHolder.getPosition() == memPosition)
						{
							
							text = (TextView) memHolder.getView(R.id.filter_text);
							text.setTextColor((memHolder.getResources().getColor(R.color.white)));
						}
						
					}
					
					memHolder = holder;
					memPosition = holder.getPosition();
					
					text = (TextView) memHolder.getView(R.id.filter_text);
					text.setTextColor((memHolder.getResources().getColor(R.color.magenta)));
					showNoFilterBtn();
					getData().putInt(DATA_POSITION, memPosition);
					authData.setFilter(true);
					
					
				}
			}
			break;
		
		}
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public boolean onListItemViewTouch(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onListItemUpdateView(IMvpGenericAdapter adapter,
			MvpItemHolder holder, int position) {
 		 
	 MvpPage<MvpCategory> listChannels = (MvpPage<MvpCategory>) mvpCategoriesObj.getData();
	 
	 if (null != listChannels)
	 { 
		 text = (TextView) holder.getView(R.id.filter_text);
		 if(position == memPosition)
		 {
				text.setTextColor((holder.getResources().getColor(R.color.magenta)));
		 }
		 else
		 {
				text.setTextColor((holder.getResources().getColor(R.color.white)));
		 }
		 
		 MvpCategory mvpCategory = listChannels.getItem(position);
		 if (null != mvpCategory)
		 {
			holder.setVisibility(R.id.filter_text, View.VISIBLE);
			holder.setVisibility(R.id.item_loading, View.GONE);
			holder.setText(R.id.filter_text, mvpCategory.getTitle());
		}
		else
		{
			holder.setVisibility(R.id.item_loading, View.VISIBLE);
			holder.setVisibility(R.id.filter_text, View.GONE);
		}
		 
		 showNoFilterBtn();
	 }
		
	}

	@Override
	public void onListItemsCountChanged(IMvpGenericAdapter adapter, int newCount) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(View v) {
		
		int id = v.getId();
		switch (id) {
		case R.id.btn_no_filter:
			fragment = ((IMvpActivity) getActivity()).getCurrentScreen();
			if(memHolder != null)
			{
				 text = (TextView) memHolder.getView(R.id.filter_text);
				 text.setTextColor((memHolder.getResources().getColor(R.color.white)));
			}
			memPosition = -1;
			memHolder = null;
			getData().putInt(DATA_POSITION, memPosition);
			dismiss();
			
			((IFilterView)fragment).performFilter(null);
			showNoFilterBtn();
			break;

		default:
			break;
		}
	}
	
	
	private void showNoFilterBtn(){
		if(!(memPosition == -1)) 
		{
			btnNoFilter.setVisibility(View.VISIBLE);
		} 
		else
		{
			btnNoFilter.setVisibility(View.GONE);
		}
	}

	@Override
	public int onListItemGetLayoutId(IMvpGenericAdapter adapter, int position) 
	{

		return R.layout.fiter_row;
	}

}