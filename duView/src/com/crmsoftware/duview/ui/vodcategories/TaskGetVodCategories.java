package com.crmsoftware.duview.ui.vodcategories;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.widget.ArrayAdapter;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.configuration.PlatformSettings;
import com.crmsoftware.duview.core.common.HttpReqParams;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.core.common.TaskPageReqCache;
import com.crmsoftware.duview.data.MpxPagHeader;
import com.crmsoftware.duview.data.MvpCategory;
import com.crmsoftware.duview.data.MvpList;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.data.MvpPage;
import com.crmsoftware.duview.utils.IHttpConst;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.app.MainApp;

public class TaskGetVodCategories extends Task{
	public static boolean DEBUG = Log.DEBUG; 
	public static boolean IS_TESTING = Log.IS_TESTING; 
	public static final String TAG = TaskGetVodCategories.class.getSimpleName();
	
	public static final String OBJPREFIX_CATEGORIES = "whatsnew";
	public static final String OBJPREFIX_ALLCATEGORIES = "allcategories";
	
	public static final String PLACEMENT_SCHEME = "placement";
	public static final String CONTENT_SCHEME = "content";
	public static final String ON_DEMAND_CLUB_CATEG = PlatformSettings.IS_PRODUCTION? "On demand club" : "on demand club" ;
	
	public static final String MOST_WATCHED_CATEGORY_ID = "MostWatched";
	public static final  String RECENT_ADITION_CATEGORY_ID = "RecentAddition";
	public static final  String ALL_CATEGORY_ID = "VoD";
	

	private MvpObject mvpReturnData;
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	public TaskGetVodCategories(ITaskReturn callback, MvpObject mvpReturnData) 
	{
		super(callback);
		
		this.mvpReturnData = mvpReturnData;
	}
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public Object getReturnData() {
		if (DEBUG) Log.m(TAG,this,"getReturnData");
		
		return mvpReturnData;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	protected void buildLegacyCategories() {
		if (DEBUG) Log.m(TAG,this,"onExecute");
		
		//-------- legacy
		MvpList<MvpCategory> mvpCategoryList = new MvpList<MvpCategory>(MvpCategory.class);

		MvpCategory category;
		
    	category = new MvpCategory();
    	category.setTitle(MainApp.getRes().getString(R.string.category_Most_Watched));
    	category.setFullTitle(MOST_WATCHED_CATEGORY_ID);
    	category.setScheme(PLACEMENT_SCHEME);
    	mvpCategoryList.add(category);
    	
    	category = new MvpCategory();
		category.setTitle(MainApp.getRes().getString(R.string.category_Recent_Adition));
		category.setFullTitle(RECENT_ADITION_CATEGORY_ID);
    	category.setScheme(PLACEMENT_SCHEME);
		mvpCategoryList.add(category);
  	
    	category = new MvpCategory();
    	category.setTitle(MainApp.getRes().getString(R.string.category_All));
    	category.setFullTitle(ALL_CATEGORY_ID);
    	category.setScheme(CONTENT_SCHEME);		    	
    	mvpCategoryList.add(category);
    	Map<String, MvpList<MvpCategory>> mapCategories = (Map<String, MvpList<MvpCategory>>) mvpReturnData.getData();
		mapCategories.put(ON_DEMAND_CLUB_CATEG + "/Highlights", mvpCategoryList);
    	
		//-------- legacy
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	@Override
	protected void onExecute() {
		if (DEBUG) Log.m(TAG,this,"onExecute");
		
		
		
		Map<String, MvpList<MvpCategory>> mapCategories = (Map<String, MvpList<MvpCategory>>) mvpReturnData.getData();
		
		if (null == mapCategories)
		{
			mapCategories = Collections.synchronizedMap(new HashMap<String, MvpList<MvpCategory>>());
			mvpReturnData.setData(mapCategories);
		}
    	

		
		mapCategories.clear();
		
		
		MvpObject objCategories = MvpObject.newReference(OBJPREFIX_ALLCATEGORIES);
		
		HttpReqParams httpReqParams = new HttpReqParams();

		httpReqParams.setParam(IHttpConst.BY_SCHEME, IHttpConst.SCHEME_CATEGORY_GENRE);
		httpReqParams.setParam(IHttpConst.BY_FULL_TITLE_PREFIX, IHttpConst.VALUE_CATEGORY_ROOT_ONDEMANDCLUB);
		httpReqParams.setSort(IHttpConst.VALUE_FIELD_ORDER);
		httpReqParams.setRange(1, 500);
		
		TaskPageReqCache taskCategs = new TaskPageReqCache<MvpCategory>(
				this, 
				MvpCategory.class, 
				MpxPagHeader.class,
				PlatformSettings.getInstance().getFilterVoDList(),
				httpReqParams,
				objCategories,
				60 * 1000);

		executeSubTask(taskCategs);	
	}
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onReturnFromTask(Task subTask, boolean canceled) 
	{
		super.onReturnFromTask(subTask, canceled);
		
		//buildLegacyCategories(); // this has to be commented
		
		TaskPageReqCache taskCategs = (TaskPageReqCache) subTask;
		
		if (taskCategs.isError())
		{
			taskCompleted(taskCategs.getError());
			
			return;
		}
		
		Map<String, MvpList<MvpCategory>> mapCategories = (Map<String, MvpList<MvpCategory>>) mvpReturnData.getData();
		
		MvpObject objCateg = (MvpObject) taskCategs.getReturnData();
		MvpPage page = (MvpPage)objCateg.getData();
		for (int i=0; i<page.getTotalResults(); i++)
		{
			MvpCategory categ = (MvpCategory) page.getItem(i);
			String fullTitle = categ.getFullTitle();
			
			int pos = fullTitle.lastIndexOf("/");
			
			String parentCateg = fullTitle;
			
			if (pos != -1)
			{
				parentCateg = parentCateg.substring(0, pos);
			}
			
			MvpList<MvpCategory> listCategories = mapCategories.get(parentCateg);
			if (null == listCategories)
			{
				listCategories = new MvpList<MvpCategory>(MvpCategory.class);
				mapCategories.put(parentCateg, listCategories);
			}
			
			listCategories.add(categ);
		}
		
		taskCompleted();

	}
}
