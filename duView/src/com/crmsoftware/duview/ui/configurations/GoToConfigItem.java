package com.crmsoftware.duview.ui.configurations;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.IMvpActivity;
import com.crmsoftware.duview.ui.MvpFragment;
import com.crmsoftware.duview.ui.common.MvpItemHolder;
import com.crmsoftware.duview.utils.Log;

/**
 * @author Cosmin
 * @desc Class that represents a GoTo item in a configuration list.
 * This class will create an item with a title and when it will be pressed it 
 * will open a fragment that will do the necessary configuration changes.
 */
@SuppressWarnings("rawtypes")
public class GoToConfigItem implements IConfigItem{

	public static boolean DEBUG = Log.DEBUG; 
	public final static String TAG = GoToConfigItem.class.getSimpleName(); 

	private String mtitle;
	private String mdesc;
	/**
	 * @desc When pressed the user will be forwarded to this frament
	 */
	private IMvpActivity mActivity;
	
	/**
	 * @desc the class the will represent the fragment that will be opened when 
	 * the item is pressed. 
	 */
	private Class<? extends MvpFragment> mDestinationFragmentClass;

	private String mdestinationTAG;
	
	/**
	 * @param title
	 * @param description - the descriptions text that will be displayed
	 * @param iMvpActivity
	 * @param destination - this is the fragment that will be opened when the user touches the item.
	 * 						the opened fragment will do the necessary changes in configuration.
	 * @param tag2 
	 */
	public GoToConfigItem(String title, 
			              String description, 
			              IMvpActivity iMvpActivity, 
			              Class<? extends MvpFragment> destination, 
			              String destinationTAG){
		mtitle = title;
		mdesc = description;
		mActivity = iMvpActivity;
		mDestinationFragmentClass = destination;
		mdestinationTAG = destinationTAG;
	}
	
	
	
	
	/*
	 * Implementations from IConfigItem
	 * */
	
	@Override
	public void updateHolder(MvpItemHolder holder) {

		 if (DEBUG) Log.m(TAG,this, "updateHolder");
		holder.setText(R.id.config_goto_item_title, mtitle);
		holder.setText(R.id.config_goto_item_info, mdesc);
	}

	@Override
	public int getViewId() {
		return R.layout.config_goto_item;
	}

	public void doClickFor(MvpItemHolder holder, int resId){
		Log.d(TAG, "doClickFor");
		if ( resId == R.id.config_goto_item_layout ){
			/*button pressed*/
			/*goto the passed fragment*/
			mActivity.goTo(mDestinationFragmentClass, 
						   mdestinationTAG, 
					       null, 
					       true);
		}
	}

	@Override
	public void setValue(Object value) {
		/*no value to set because the changes are made into the opened fragment*/
	}

	@Override
	public Object getValue() {
		return null;
	}

	@Override
	public void saveTheData(){
		/*Nothing to save because all the saving is done into the fragment*/
	}

}
