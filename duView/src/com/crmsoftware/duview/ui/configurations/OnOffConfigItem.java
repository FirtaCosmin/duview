package com.crmsoftware.duview.ui.configurations;

import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.ui.MvpFragment;
import com.crmsoftware.duview.ui.common.MvpItemHolder;

public class OnOffConfigItem implements IConfigItem, OnCheckedChangeListener {

	protected String mTitle;
	protected String mDesc;
	protected Boolean mValue = false;
	protected MvpFragment _holderFragment;
	
	public OnOffConfigItem(String text, String description, MvpFragment holderFragment){
		mTitle = text;
		mDesc = description;
		_holderFragment = holderFragment;
	}

	
	/*
	 * Implementations from IConfigItem
	 * */
	
	@Override
	public void updateHolder(MvpItemHolder holder) {
		holder.setText(R.id.config_onoff_item_title, mTitle);
		holder.setText(R.id.config_onoff_item_desc, mDesc);
		holder.setCheckState(R.id.config_onoff_item_toggleButton, mValue);
		((CompoundButton)holder.getView(R.id.config_onoff_item_toggleButton)).setOnCheckedChangeListener(this);
//		((ToggleButton)holder.getView(R.id.config_onoff_item_toggleButton)).setChecked(mValue);
	}

	@Override
	public int getViewId() {
		return R.layout.config_onoff_item;
	}
	

	public void doClickFor(MvpItemHolder holder, int resId){
//		mValue = holder.getCheckState(resId) == 1?true:false;
//		saveTheData();
	}
	

	public void setValue(Object value){
		if (value instanceof Boolean){
			mValue = (Boolean)value;
		}
	}
	public Object getValue(){
		return mValue;
	}


	@Override
	public void saveTheData() {
		/*will not do any saving*/
		/*the saving must be done by a specific extension of this object*/
	}


	@Override
	public void onCheckedChanged(CompoundButton arg0, boolean newState) {
		mValue = newState;
		saveTheData();
	}
}
