package com.crmsoftware.duview.ui.configurations;

import com.crmsoftware.duview.ui.common.MvpItemHolder;

public interface IConfigItem {

	public void updateHolder(MvpItemHolder holder);
	public int getViewId();
	public void doClickFor(MvpItemHolder holder, int resId);
	public void setValue(Object value);
	public Object getValue();
	public void saveTheData();
	
}
