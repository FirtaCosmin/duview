package com.crmsoftware.duview.ui.configurations;

import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.ui.MvpFragment;
import com.crmsoftware.duview.ui.apphelp.AppHelpFragment;
import com.crmsoftware.duview.ui.vodcategories.TaskGetVodCategories;
import com.crmsoftware.duview.ui.vodcategories.VodCategoriesFragment;

public class OnOffConfigItem_Help extends OnOffConfigItem{
	private boolean lastValue;
	
	public OnOffConfigItem_Help(String text, String desc, MvpFragment holderFragment) {
		super(text, desc, holderFragment);
		MvpUserData data = MvpUserData.getFromMvpObject();
		lastValue = data.getHelp();
	}

	@Override
	public void saveTheData(){
		Boolean value = (Boolean)getValue();
		if ( value != lastValue ){
			lastValue = value;
			MvpUserData data = MvpUserData.getFromMvpObject();
			data.setHelp(value);
			if ( value ){
				AppHelpFragment.tempDisabled = false;
				_holderFragment.getMvpActivity().getMenu().selectMenuItem(VodCategoriesFragment.class, TaskGetVodCategories.ON_DEMAND_CLUB_CATEG + "/Highlights", true);
			}
		
		}
		
		
	}
}
