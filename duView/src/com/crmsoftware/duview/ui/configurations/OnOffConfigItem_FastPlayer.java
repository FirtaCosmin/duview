package com.crmsoftware.duview.ui.configurations;


import com.crmsoftware.duview.R;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.ui.MvpFragment;
import com.crmsoftware.duview.ui.common.MvpItemHolder;

public class OnOffConfigItem_FastPlayer extends OnOffConfigItem{

	public OnOffConfigItem_FastPlayer(String text, String description, MvpFragment holderFragment) {
		super(text,description, holderFragment);
	}
	

	@Override
	public void updateHolder(MvpItemHolder holder) {
		super.updateHolder(holder);
	}
	
	@Override
	public void saveTheData(){

		MvpUserData data = MvpUserData.getFromMvpObject();
		data.setFastPlayer((Boolean)getValue());
	}

}
