package com.crmsoftware.duview.ui;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.FragmentChangeActivity;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.configuration.PlatformSettings;
import com.crmsoftware.duview.core.common.HttpReqParams;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.core.common.TaskSingleReqParse;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.ui.configurations.GoToConfigItem;
import com.crmsoftware.duview.ui.vodcategories.VodCategoriesFragment;
import com.crmsoftware.duview.utils.IConst;
import com.crmsoftware.duview.utils.IHttpConst;
import com.crmsoftware.duview.utils.Log;

public class AuthFragment extends MvpFragment implements OnClickListener, ITaskReturn {
	public static boolean DEBUG = Log.DEBUG; 
	public static final int TESTUSER = Log.INFO ? 1 : -1; 
	public final static String TAG = AuthFragment.class.getSimpleName();
	
	public final static String MVP_AUTH_DATA = "MvpAuthData";
	public final static String IS_BACK_ALLOWED = "IS_BACK_ALLOWED";
	public final static String IS_NEED_TO_REMAKE = "is_need_to_remake";
	public final static String USERNAME_VALUE = "username_value";
	public final static String PASSWORD_VALUE = "password_value";

	
	String users[] = {"mkanlica","mkanlica","ali.dernaika","aperatif"};
	String pwds[] = {"dumvp2013_","dumvp2013","LetMeIn!","srs5h6y3"};
	
	private String sUserName = "";
	private String sPwd = "";
	private int rememberMe = 0;
	private AlertDialog alertDialog;
	View holowView;
	private Animation fadeInAnimation;
	private boolean isAlloweBack = true;
	private TaskSingleReqParse<MvpUserData> taskAuth;
	private boolean isNeedToRemake = false;
	
	public static AuthFragment newInstance() {
		
		
		AuthFragment instance = new AuthFragment(); 
		
		if (DEBUG) Log.m(TAG,instance,"newInstance");
		
        return instance;
    }
	
	@Override
	public void onSaveInstanceState(Bundle outState)
	{
	   super.onSaveInstanceState(outState);

		if (DEBUG) Log.m(TAG,this,"onSaveInstanceState");
		
		/*save the focused child to refocus on it on resume*/
	    View focusedChild = getActivity().getCurrentFocus();
	    if (focusedChild != null)
	    {
	       int focusID = focusedChild.getId();
	       outState.putInt("focusID", focusID);
	    }
	    if(null != taskAuth)
	    {
	    	outState.putBoolean(IS_NEED_TO_REMAKE, true);
	    	outState.putString(USERNAME_VALUE, sUserName);
	    	outState.putString(PASSWORD_VALUE, sPwd);
	    }
	    else
	    {
	    	outState.putBoolean(IS_NEED_TO_REMAKE, false);
	    }
	}
	/**
	 * @param inState
	 * @desc this method will restore the state of the application in when an instance state is saved
	 */
	private void onRestoreInstanceState(Bundle inState)
	{

		if (DEBUG) Log.m(TAG,this,"onRestoreInstanceState");
		/*restore the focus on the saved field*/
	    int focusID = inState.getInt("focusID", View.NO_ID);
	    View focusedChild = getHolder().getView(focusID);
	    if (focusedChild != null)
	    {
	       focusedChild.requestFocus();
	    }
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) 
	{		
		if (DEBUG) Log.m(TAG,this,"onCreateView");
		
		View view = setContentView(R.layout.auth_view,container,inflater);
		
		MvpViewHolder holder = getHolder();

		getMvpActivity().setMenuEnabled(false);
		getMvpActivity().setActionBarVisibility(View.VISIBLE);
		getMvpActivity().setHomeButtonActionBar(false);
		getMvpActivity().setTitle(MainApp.getRes().getString(R.string.auth));
		
		holder.setClickListener(this, R.id.login_button , R.id.registration_message,  R.id.log_in_txt, R.id.skip_button);  

		
		MvpUserData authData = MvpUserData.getFromMvpObject();
		if (TESTUSER != -1) 
		{
				holder.setText(R.id.user_name,users[TESTUSER]);
				holder.setText(R.id.user_password,pwds[TESTUSER]);
		}
		else
		{
			String sUsr = authData.getAuthUserName();
			
			if (!sUsr.isEmpty())
			{
				holder.setText(R.id.user_name,sUsr);
			}
		}
		
		holowView = holder.getView(R.id.image_back);
		
		isAlloweBack = getData().getBoolean(IS_BACK_ALLOWED, true);
		
		 fadeInAnimation = AnimationUtils.loadAnimation(getActivity(), R.animator.view_fade_out);
         fadeInAnimation.setAnimationListener(new AnimationListener() {
                 @Override
                 public void onAnimationStart(Animation animation) {
                         // TODO Auto-generated method stub

                 }
                 @Override
                 public void onAnimationRepeat(Animation animation) {
                         // TODO Auto-generated method stub
                 }
                 @Override
                 public void onAnimationEnd(Animation animation) {
                	 holowView.startAnimation(fadeInAnimation);

                 }
         });
         holowView.startAnimation(fadeInAnimation);
		
         if ( savedInstanceState != null ){
        	 onRestoreInstanceState(savedInstanceState);
        	 isNeedToRemake = savedInstanceState.getBoolean(IS_NEED_TO_REMAKE);
        	 if(isNeedToRemake)
        	 {
        		 sUserName = savedInstanceState.getString(USERNAME_VALUE);
        		 sPwd = savedInstanceState.getString(PASSWORD_VALUE);
        		 performForAuthentication(authData);
        	 }
         }
         
		return view;
	}
	
	@Override
	public boolean onBackPressed() {
//		MvpUserData authData = MvpUserData.getFromMvpObject();
//		if (authData.getToken().equals(IConst.EMPTY_STRING) || false == isAlloweBack)
//		{
//			getActivity().showDialog(IConst.QUIT_DIALOG);
//			return false;
//		}
//		
		return true;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onClick(View v) {
		if (DEBUG) Log.m(TAG,this,"onClick");
		
		MvpUserData authData = MvpUserData.getFromMvpObject();
		
		switch (v.getId())
		{
		case R.id.login_button:
		{
			performForAuthentication(authData);
			break;
		}
		case R.id.skip_button:
		{
			authData.removeFromPrefAll();
			authData.setAnonymous(true);
			authData.saveToMvpObject();
			authData.saveAuthDataToPref();
			authData.initFromPref();
			
			getMvpActivity().getMenu().refreshMenu();
			getMvpActivity().getMenu().update();
			getMvpActivity().getMenu().selectDefaultItem();
			break;
		}
		case R.id.registration_message:
		case R.id.util_message:
		case R.id.log_in_txt:
		
			getMvpActivity().showLoadingDialog();
			Bundle bundle = new Bundle();
			bundle.putBoolean(BrowserFragment.DISABLE_MENU,true);
			bundle.putString(BrowserFragment.URL, PlatformSettings.getInstance().getDuSelfcareUrl());
			getMvpActivity().goTo(BrowserFragment.class,BrowserFragment.TAG, bundle, true);
		}
	}
	
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected void performAuthentication(String user, String pwd, int rememberMe)
	{
		if (DEBUG) Log.m(TAG,this,"performAuthentication");
		
		HttpReqParams httpReqParams = new HttpReqParams(
				IHttpConst.VALUE_SCHEMA_1_0, 
				IHttpConst.VALUE_JSON);
		

		
		String sMpxUser = PlatformSettings.getInstance().getTpMpxDirPid() + IHttpConst.CHAR_SLASH + user;
		httpReqParams.setParam(IHttpConst.PARAM_USERNAME, sMpxUser);
		httpReqParams.setParam(IHttpConst.PARAM_PASSWORD, pwd);
		
		if (rememberMe == 1)
		{
			httpReqParams.setParam(IHttpConst.PARAM_IDLE_TIMEOUT, IHttpConst.VALUE_IDLE_TIMEOUT);
		}
		
		
		taskAuth = new TaskSingleReqParse<MvpUserData>(
				this,
				MvpUserData.class,
				PlatformSettings.getInstance().getUserAuthUrl(), 
				httpReqParams);	
		
		taskAuth.execute();
		
	}
	
	public void performForAuthentication(MvpUserData authData)
	{
		authData.removeFromPrefAll();
		authData.setAnonymous(false);
		MvpViewHolder holder = getHolder();
		if(sUserName.equals("") && sPwd.equals(""))
		{
			sUserName = holder.getText(R.id.user_name);
			sPwd = holder.getText(R.id.user_password);
		}
		rememberMe = holder.getCheckState(R.id.remember_check);
		if(!sUserName.equals(IConst.EMPTY_STRING) && !sPwd.equals(IConst.EMPTY_STRING))
		{
			getMvpActivity().showLoadingDialog();
			performAuthentication(sUserName,sPwd,rememberMe);
		}
		else
		{
			showToast("Fields are not filled");
		}
		getMvpActivity().getMenu().refreshMenu();
		getMvpActivity().getMenu().update();
		hideKeyboard(getActivity());
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onReturnFromTask(Task task, boolean canceled) {
		
		if (task.isError())
		{
			getMvpActivity().hideLoadingDialog();

			showErrorDialog(task.getError().displayError());			
			
			return;
		} // error

		MvpUserData authData = (MvpUserData) task.getReturnData();

		if (null != authData)
		{	
			authData.setAuthUserName(sUserName);
			authData.setRemember(rememberMe == 1);
			authData.setAnonymous(false);
			authData.setTokenRetrivalTime();
			
			authData.saveToMvpObject();
			authData.saveAuthDataToPref();
			authData.initFromPref();
			
			if (null == getMvpActivity())
			{
				return;
			}
			
			
			((FragmentChangeActivity) getMvpActivity()).doPostLogin();
			

			
		}
	}
	
	public static void signOut()
	{
		MvpUserData authData = MvpUserData.getFromMvpObject();
		if (null != authData)
		{
			authData.signOut();
		}
	}

	
	public  AlertDialog showErrorDialog(String message) {
		if (DEBUG) Log.m(TAG,this,"showLoadingDialog");
		AlertDialog.Builder errorBuilder = new AlertDialog.Builder(getDialogContext());
		errorBuilder.setMessage(message).setCancelable(false)
		.setPositiveButton(R.string.pozitive_button_text_second, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int viewId) {
				dialog.dismiss();
			}
		});
		alertDialog = errorBuilder.create();
		alertDialog.show();
		return alertDialog;
	}
	
	
	
	private Context getDialogContext() {
		Context context = null;
		Activity activityParent = getActivity().getParent();
		if (activityParent != null) {
			context = activityParent;
		} else {
			context = getActivity();
		}
		return context;
	}
	
	public static void hideKeyboard(Activity activity) {
	    InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
	    //Find the currently focused view, so we can grab the correct window token from it.
	    View view = activity.getCurrentFocus();
	    //If no view currently has focus, create a new one, just so we can grab a window token from it
	    if(view == null) {
	        view = new View(activity);
	    }
	    inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}
	@Override
	public void onDestroy() {
		if(null != taskAuth)
		{
			getMvpActivity().hideLoadingDialog();
			taskAuth.cancel(true);
		}
		super.onDestroy();
	}
	
}
