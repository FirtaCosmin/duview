package com.crmsoftware.duview.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.ui.common.MvpViewHolder;


public class TermsAndConditionsFragment extends MvpFragment  {

	private WebView webView;
	String htmlText;
	
	
	public static TermsAndConditionsFragment getInstance() {
		return new TermsAndConditionsFragment();
	}
	
	
	
	@Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		View view = setContentView(R.layout.terms_and_conditions_view, container, inflater);
		
		MvpViewHolder holder = getHolder();
		
		getMvpActivity().setMenuEnabled(true);
		getMvpActivity().setActionBarVisibility(View.VISIBLE);
		getMvpActivity().setHomeButtonActionBar(true);
		getMvpActivity().setTitle(MainApp.getRes().getString(R.string.terms_and_cond));
		
		webView = (WebView) holder.getView(R.id.webview);
    	webView.setBackgroundColor(0x00000000);
    	
//    	htmlText = getResources().getString(R.string.test);
    	webView.setWebViewClient(new WebViewClient());
    	initWebView();
 	 	
   	 	return view;
    }

	

    @Override
    public void onResume() {
    	super.onResume();
    	
    }
    
    private void initWebView() {
//    	webView.loadDataWithBaseURL("file:///android_asset/", htmlText, "text/html", "utf-8", null);
    	webView.loadUrl("file:///android_asset/Terms&Cond.html");
	}

	@Override
    public void onPause() {
    	super.onPause();

    } 

}