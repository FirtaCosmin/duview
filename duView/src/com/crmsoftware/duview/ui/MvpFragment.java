package com.crmsoftware.duview.ui;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************


import java.util.Observable;
import java.util.Observer;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.MenuItem;
import com.crmsoftware.duview.app.IMvpActivity;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.data.MvpError;
import com.crmsoftware.duview.data.MvpServerError;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.ui.common.OnMvpDlgClose;
import com.crmsoftware.duview.utils.IConst;
import com.crmsoftware.duview.utils.Log;

public class MvpFragment extends SherlockFragment implements Observer, OnClickListener, OnMvpDlgClose
{
	public static boolean DEBUG = Log.DEBUG; 
	public final static String TAG = MvpFragment.class.getSimpleName();
	
	protected MvpViewHolder viewHolder;
	protected Bundle fragmentData = new Bundle();
	private IMvpActivity mvpActivity = null;


	private boolean hasDoneSavedInstace = false;
	private FrameLayout frameLayout;
	
	/**
	 * 
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
    @Override
    protected void finalize() throws Throwable {
		if (DEBUG) Log.m(TAG,this,"finalize");
    	super.finalize();
    }
    
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected View setContentView(int layoutId, ViewGroup container, LayoutInflater inflater)
	{
		if (DEBUG) Log.m(TAG,this, "setContentView");
		 
		mvpActivity = (IMvpActivity) getActivity();
		
		if (frameLayout == null)
		{
			frameLayout = new FrameLayout(getActivity());
			frameLayout.setOnClickListener(this);	// no click throgh fragment
		}
		else
		{
			frameLayout.removeAllViews();
		}
		
		View containerView = LayoutInflater.from(getActivity()).inflate(layoutId, null);
		frameLayout.addView(containerView);
		viewHolder = new MvpViewHolder(containerView);
		
		return frameLayout;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onConfigurationChanged(Configuration newConfig) 
	{
		super.onConfigurationChanged(newConfig);
		
		if (MainApp.IS_MANAGING_CONFIGURATION)
		{
			onCreateView(LayoutInflater.from(getActivity()), null, getData());
		}
	}
	
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onCreate(Bundle savedInstanceState) {
		if (DEBUG) Log.m(TAG,this, "onCreate");
		
		System.gc();
		if (savedInstanceState != null)
		{
			fragmentData = new Bundle();
			fragmentData.putAll(savedInstanceState);
		}
		super.onCreate(savedInstanceState);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		
		if (DEBUG) Log.m(TAG,this, "onSaveInstanceState");
		
		hasDoneSavedInstace = true;
		
		if (fragmentData != null)
		{
			outState.putAll(fragmentData);
		}
		super.onSaveInstanceState(outState);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (DEBUG) Log.m(TAG,this, "onCreateView");
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	

	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	
	@Override
	public void onResume() {
		if (DEBUG) Log.m(TAG,this, "onResume");
		super.onResume();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onPause() {
		if (DEBUG) Log.m(TAG,this, "onPause");
		super.onPause();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onDestroy() {
		if (DEBUG) Log.m(TAG,this, "onDestroy");
		super.onDestroy();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void onResetData()
	{
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	
	@Override
	public void onDetach() {
		if (DEBUG) Log.m(TAG,this, "onDetach");
		
		super.onDetach();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onAttach(Activity activity) {
		if (DEBUG) Log.m(TAG,this, "onAttach");
		super.onAttach(activity);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		if (DEBUG) Log.m(TAG,this, "onActivityCreated");		
		super.onActivityCreated(savedInstanceState);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (DEBUG) Log.m(TAG,this, "onActivityResult");		
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public void onDestroyView() {
		if (DEBUG) Log.m(TAG,this, "onDestroyView");
		
		if (false == hasDoneSavedInstace)
		{
			
		}
		
		super.onDestroyView();
	}
	
	@Override
	public void onStart() {
		if (DEBUG) Log.m(TAG,this, "onStart");	
		super.onStart();
	}
	
	@Override
	public void onStop() {
		if (DEBUG) Log.m(TAG,this, "onStop");	
		super.onStop();
	}
	
	@Override
	public void onLowMemory() {
		if (DEBUG) Log.m(TAG,this, "onLowMemory");	
		super.onLowMemory();
	}
	
	@Override
	public void onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu,
			com.actionbarsherlock.view.MenuInflater inflater) {
		// TODO Auto-generated method stub
		super.onCreateOptionsMenu(menu, inflater);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onDestroyOptionsMenu() {
		if (DEBUG) Log.m(TAG,this, "onDestroyOptionsMenu");	
		super.onDestroyOptionsMenu();
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected MvpViewHolder getHolder()
	{
		return viewHolder;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */
	protected void onDataUpdate(Observable observable, Object data) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @param  
	 * @param  
	 * @return      
	 * @see         
	 */	
    public boolean onBackPressed()
    {
		if (DEBUG) Log.m(TAG,this,"onBackPressed");
		
    	return true;
    }
    
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */
	public void setData(Bundle fragmentData) 
	{
		this.fragmentData = fragmentData;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */
	public Bundle getData() 
	{
		return fragmentData;
	}
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */
	@Override
	public void update(Observable observable, Object data) 
	{
		if (DEBUG) Log.m(TAG,this, "update");
		
		final Observable newObservable = observable;
		final Object newData = data;
		getActivity().runOnUiThread(new Runnable() {
			
			@Override
			public void run() 
			{
				onDataUpdate(newObservable,newData);
			}
		});
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void showToast(String text)
	{
		Toast toast = Toast.makeText(getActivity(), text, Toast.LENGTH_LONG);
		toast.show();
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void checkToken(MvpError error)
	{
		if (error instanceof MvpServerError)
		{
			MvpServerError srvError = (MvpServerError)error;
			if (srvError.getResponseCode() == 401)
			{
				if (srvError.getTitle().contains(IConst.AUTHENTICATION_EXCEPTION_TITLE))
				{
					Bundle data = new Bundle();
					data.putBoolean(AuthFragment.IS_BACK_ALLOWED, false);
					getMvpActivity().goTo(AuthFragment.class, AuthFragment.TAG,data, false);
				}
			}
		}
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public IMvpActivity getMvpActivity() 
	{
		return mvpActivity;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onMvpDlgClose(String dlgTag, Bundle data) {
		// TODO Auto-generated method stub
		
	}
}
