package com.crmsoftware.duview.ui.settings;

import java.util.Observer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.configuration.PlatformSettings;
import com.crmsoftware.duview.core.common.HttpReqParams;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.core.common.TaskSingleReqParse;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.data.MvpError;
import com.crmsoftware.duview.data.MvpParentalManagement;
import com.crmsoftware.duview.data.MvpServerError;
import com.crmsoftware.duview.ui.MvpFragment;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.utils.IConst;
import com.crmsoftware.duview.utils.IHttpConst;
import com.crmsoftware.duview.utils.Log;


public class ParentalPinFragment extends MvpFragment implements OnEditorActionListener, OnClickListener,ITaskReturn {

	public static boolean DEBUG = Log.DEBUG;
	public final static String TAG = ParentalPinFragment.class.getSimpleName();
	
	private EditText oldParentalPin;
	private EditText newParentalPin;
	private EditText reNewParentalPin;
	private AlertDialog alertDialog;
	private Button btnDone;
	
	@Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		if (DEBUG) Log.m(TAG,this,"onCreateView");
		View view = setContentView(R.layout.parental_pin_view,container,inflater);
		
		MvpViewHolder holder = getHolder();

		getMvpActivity().setMenuEnabled(true);
		getMvpActivity().setActionBarVisibility(View.VISIBLE);
		getMvpActivity().setHomeButtonActionBar(true);
		getMvpActivity().setTitle(MainApp.getRes().getString(R.string.parental_pin));
		
		oldParentalPin = (EditText) holder.getView(R.id.old_parental_pin);
		newParentalPin = (EditText) holder.getView(R.id.new_parental_pin);
		reNewParentalPin = (EditText) holder.getView(R.id.re_new_parental_pin);
		
		oldParentalPin.setRawInputType(Configuration.KEYBOARD_12KEY);
		newParentalPin.setRawInputType(Configuration.KEYBOARD_12KEY);
		reNewParentalPin.setRawInputType(Configuration.KEYBOARD_12KEY);
		
		
		btnDone = (Button)holder.getView(R.id.btn_done);
		holder.setClickListener(this, R.id.btn_done);
		
    	return view;
    }
	
	private boolean areFieldsFilled() {
		if(!IConst.EMPTY_STRING.equals(oldParentalPin.getText().toString()) 
				&& !IConst.EMPTY_STRING.equals(newParentalPin.getText().toString())
				&& !IConst.EMPTY_STRING.equals(reNewParentalPin.getText().toString())) {
			return true; 
		} else {
			return false;
		}
	}
     
	public boolean areInfosValid() {
		String newPass = newParentalPin.getText().toString();
		String reNewPass = reNewParentalPin.getText().toString();
		if (newPass.equals(reNewPass)) {
			return true;
		}
		showToast(MainApp.getRes().getString(R.string.change_pins_settings_missmatch));
		return false;
	}
	
	public String getOldParentalPin() {
		return oldParentalPin.getText().toString();
	}
	
	public String getNewParentalPin() {
		return newParentalPin.getText().toString();
	}

	public void resetEditTexts() {
		oldParentalPin.setText(IConst.EMPTY_STRING);
		newParentalPin.setText(IConst.EMPTY_STRING);
		reNewParentalPin.setText(IConst.EMPTY_STRING);
	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if(actionId == EditorInfo.IME_ACTION_GO){
			if (DEBUG) Log.m(TAG,this,"onEditorAction");
			if(!areInfosValid())
			{
				return false;
			}
			if( areFieldsFilled()){
				btnDone.setEnabled(true);
				MvpParentalManagement.getInstance().changePin(
						getNewParentalPin(), getOldParentalPin(), this);
				resetEditTexts();
				getMvpActivity().showLoadingDialog();
			}
			else
			{
				showToast("Fields are not filled");
			}
		}
		return false;
	}

	@Override
	public void onClick(View v) {
		if (DEBUG) Log.m(TAG,this,"onClick");
		switch (v.getId())
		{
		case R.id.btn_done:
		{
			if(!areInfosValid())
			{
				return;
			}
			if (areFieldsFilled())
			{
				MvpParentalManagement.getInstance().changePin(
						getNewParentalPin(), getOldParentalPin(), this);
				resetEditTexts();
				getMvpActivity().showLoadingDialog();
			}
			else
			{
				showToast("Fields are not filled");
			}
			hideKeyboard(getActivity());
		}
		}
	}
		
	@Override
	public void onReturnFromTask(Task task, boolean canceled) {
		if (task.isError())
		{
			Object error = task.getError();
			if (error instanceof MvpServerError)
			{
				MvpServerError mvpError = (MvpServerError)error;
				getMvpActivity().hideLoadingDialog();
				
				int erroCode = mvpError.getResponseCode();
				if (mvpError.isTokenError())
				{
					getMvpActivity().getMenu().performLogOut();
					return;
				}
				
				if (erroCode == 403)
				{
					showErrorDialog("Current PIN is wrong. Please type the correct PIN !");
					return;
				}
				
				if (erroCode == 200)
				{
					showErrorDialog("Pin successfully changed !");
					return;
				}
				
				return;
			}
			
			
			if (error instanceof MvpError)
			{
				getMvpActivity().hideLoadingDialog();
				showErrorDialog(task.getError().displayError());
				return;
			}
			
			
			return;
		}
	}
	public  AlertDialog showErrorDialog(String message) {
		if (DEBUG) Log.m(TAG,this,"showLoadingDialog");
		AlertDialog.Builder errorBuilder = new AlertDialog.Builder(getDialogContext());
		errorBuilder.setMessage(message).setCancelable(false)
		.setPositiveButton(R.string.pozitive_button_text_second, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int viewId) {
				dialog.dismiss();
			}
		});
		alertDialog = errorBuilder.create();
		alertDialog.show();
		return alertDialog;
	}
	
	private Context getDialogContext() {
		Context context = null;
		Activity activityParent = getActivity().getParent();
		if (activityParent != null) {
			context = activityParent;
		} else {
			context = getActivity();
		}
		return context;
	}
	
	public static void hideKeyboard(Activity activity) {
	    InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
	    //Find the currently focused view, so we can grab the correct window token from it.
	    View view = activity.getCurrentFocus();
	    //If no view currently has focus, create a new one, just so we can grab a window token from it
	    if(view == null) {
	        view = new View(activity);
	    }
	    inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}

	
}