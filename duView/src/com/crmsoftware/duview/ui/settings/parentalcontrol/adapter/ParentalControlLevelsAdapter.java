package com.crmsoftware.duview.ui.settings.parentalcontrol.adapter;

import java.util.ArrayList;
import java.util.List;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.ui.MvpFragment;
import com.crmsoftware.duview.ui.settings.parentalcontrol.controls.ParentalItemLevelsController;
import com.crmsoftware.duview.ui.settings.parentalcontrol.controls.ParentalItemLevelsInterface;
import com.crmsoftware.duview.ui.settings.parentalcontrol.controls.ParentalLevelItem;
import com.crmsoftware.duview.utils.IConst;

public class ParentalControlLevelsAdapter  extends BaseAdapter implements  IConst{
	private List<ParentalLevelItem> levelsList = new ArrayList<ParentalLevelItem>();
	private MvpFragment fragment;
	private Holder holder;
	
	public ParentalControlLevelsAdapter(MvpFragment fragment) {
		super();
		this.fragment = fragment;
		ParentalItemLevelsInterface itemLevelsInstance = ParentalItemLevelsController.getInstance().getItemLevelsInstance();
		levelsList.addAll(itemLevelsInstance.getParentalLevelItemList());
	}
	
	@Override
	public int getCount() {
		return levelsList.size();
	}
	
	@Override
	public ParentalLevelItem getItem(int position) {
		return levelsList.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return levelsList.get(position).getItemName().hashCode();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
//		if(null != convertView)
//		{
//			viewHolder = (MvpViewHolder)convertView.getTag();	
//		}
//		else
//		{
//			View view = MainApp.getInflater().inflate(R.layout.parental_control_level_row, parent, false);
//			viewHolder = new MvpViewHolder(view);
//			view.setTag(viewHolder);
//			ParentalLevelItem itemLevel = levelsList.get(position);
//			viewHolder.setVisibility(R.id.check_image, itemLevel.isSelected() ? View.VISIBLE : View.GONE);
//			
//			viewHolder.setText(R.id.level_name, itemLevel.getItemName());
//			
//			if(EMPTY_STRING.equals(itemLevel.getItemShortName()))
//			{
//				viewHolder.setText(R.id.level_short_name, itemLevel.getItemShortName());
//			}
//			else
//			{
//				viewHolder.setText(R.id.level_short_name, itemLevel.getItemShortName());
//			}
//			
//			viewHolder.setBackground(R.id.level_name, itemLevel.isSelected() ? MainApp.getRes().getColor(R.color.margenta) :
//																				MainApp.getRes().getColor(R.color.regular_du_color));
		if(convertView == null){
			convertView = MainApp.getInflaterService().inflate(R.layout.parental_control_level_row, null);
			holder = new Holder();
			holder.levelName = (TextView) convertView.findViewById(R.id.level_name);
			holder.levelShortName = (TextView) convertView.findViewById(R.id.level_short_name);
			holder.mImageView = (ImageView) convertView.findViewById(R.id.check_image);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		ParentalLevelItem itemLevel = levelsList.get(position);
		if (itemLevel.isSelected()){
			holder.mImageView.setVisibility(View.VISIBLE);	
		}
		else {
			holder.mImageView.setVisibility(View.INVISIBLE);
		}
		if(EMPTY_STRING.equals(itemLevel.getItemShortName())){
			holder.levelShortName.setText(itemLevel.getItemName());
			holder.levelName.setVisibility(View.GONE);
		}
		else
		{
			holder.levelShortName.setText(itemLevel.getItemShortName());
			
		}
		holder.levelName.setText(itemLevel.getItemName());
		holder.levelName.setSelected(itemLevel.isSelected());
		return convertView;
	}
	
	final class Holder 
	{
    	public TextView levelShortName;
		private TextView levelName;
    	private ImageView mImageView;
    }

	public void modelChange() {
		ParentalItemLevelsInterface itemLevelsInstance = ParentalItemLevelsController.getInstance().getItemLevelsInstance();
//		int parentalLevel = ParentalLevelModel.getInstance().getParentalLevel(fragment);
//		ParentalLevelItem level = itemLevelsInstance.getParentalLevelByLevel(parentalLevel);
//		itemLevelsInstance.manageSelection(level);
//		notifyDataSetChanged();
	}
	




		
}
