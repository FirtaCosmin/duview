package com.crmsoftware.duview.ui.settings.parentalcontrol.controls;

public class ParentalItemLevelsController {
	private static ParentalItemLevelsController instance = new ParentalItemLevelsController();
	private ParentalItemLevelsInterface itemLevelInstance;
	
	private ParentalItemLevelsController() {
		itemLevelInstance = new ParentalItemLevels();
		itemLevelInstance.initItemWrapperList();
	}
	
	public static ParentalItemLevelsController getInstance() {
		return instance;
	}
	
	public ParentalItemLevelsInterface getItemLevelsInstance() {
		return itemLevelInstance;
	}
	
	public int getTheLowestLevel() {
		return itemLevelInstance.getTheLowestParentalLevel();
	}
	
}
