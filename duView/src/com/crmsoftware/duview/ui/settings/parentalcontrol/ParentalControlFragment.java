package com.crmsoftware.duview.ui.settings.parentalcontrol;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.configuration.PlatformSettings;
import com.crmsoftware.duview.core.common.HttpReqParams;
import com.crmsoftware.duview.core.common.ITaskReturn;
import com.crmsoftware.duview.core.common.Task;
import com.crmsoftware.duview.core.common.TaskSingleReqParse;
import com.crmsoftware.duview.data.MvpUserData;
import com.crmsoftware.duview.data.MvpError;
import com.crmsoftware.duview.data.MvpParentalManagement;
import com.crmsoftware.duview.data.MvpServerError;
import com.crmsoftware.duview.ui.MvpFragment;
import com.crmsoftware.duview.ui.common.IMvpGenericAdapter;
import com.crmsoftware.duview.ui.common.IMvpGenericListViewListener;
import com.crmsoftware.duview.ui.common.MvpItemHolder;
import com.crmsoftware.duview.ui.common.MvpSimpleListAdapter;
import com.crmsoftware.duview.ui.common.MvpVerticalListView;
import com.crmsoftware.duview.ui.common.MvpViewHolder;
import com.crmsoftware.duview.ui.settings.parentalcontrol.controls.ParentalItemLevelsController;
import com.crmsoftware.duview.ui.settings.parentalcontrol.controls.ParentalItemLevelsInterface;
import com.crmsoftware.duview.ui.settings.parentalcontrol.controls.ParentalLevelItem;
import com.crmsoftware.duview.utils.IConst;
import com.crmsoftware.duview.utils.IHttpConst;
import com.crmsoftware.duview.utils.Log;

public class ParentalControlFragment
extends MvpFragment 
implements OnItemClickListener,
IMvpGenericListViewListener, 
OnEditorActionListener, 
OnClickListener, 
ITaskReturn,
Observer
{

	public static boolean DEBUG = Log.DEBUG;
	public final static String TAG = ParentalControlFragment.class.getSimpleName();
	
	public final static String DATA_POSITION = "DATA_POSITION";
	
	private List<ParentalLevelItem> levelsList = new ArrayList<ParentalLevelItem>();
	
	private LinearLayout parentalPinContainer;
	private EditText parentalPinEditText;
	
	private int currentParentalLevel = Integer.MIN_VALUE;
	private static int selectedPosition = 0; 
	private int parentalLevel;
	private AlertDialog alertDialog;
	private MvpVerticalListView levelsListView;
	private ParentalLevelItem itemLevel;
	private ParentalItemLevelsInterface itemLevelsInstance;
	private MvpSimpleListAdapter adapter;
	private Task taskParentalLevel;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		if (DEBUG) Log.m(TAG,this,"onCreateView");
		
		MvpParentalManagement.getInstance().addObserver(this);
		 adapter = new MvpSimpleListAdapter(ParentalLevelItem.class, this);
		
		View view = setContentView(R.layout.parental_control_view,container,inflater);
		
		MvpViewHolder holder = getHolder();
		
		ParentalItemLevelsInterface itemLevelsInstance = ParentalItemLevelsController.getInstance().getItemLevelsInstance();
		levelsList.addAll(itemLevelsInstance.getParentalLevelItemList());
		parentalLevel = MvpParentalManagement.getInstance().getServerLevel();
		selectedPosition = levelsList.indexOf(itemLevelsInstance.getParentalLevelByLevel(parentalLevel));
//		selectedPosition = getData().getInt(DATA_POSITION,0);
		
		getMvpActivity().setMenuEnabled(true);
		getMvpActivity().setActionBarVisibility(View.VISIBLE);
		getMvpActivity().setHomeButtonActionBar(true);
		getMvpActivity().setTitle(MainApp.getRes().getString(R.string.parental_level));
		
		levelsListView = (MvpVerticalListView) holder.getView(R.id.parental_list_view);
		holder.setAdapter(R.id.parental_list_view, adapter);
		adapter.setCount(6);
		levelsListView.setOnItemClickListener(this);
		holder.setClickListener(this, R.layout.parental_control_level_row);
		
		levelsListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		
		parentalPinEditText = (EditText) holder.getView(R.id.parental_pin);
		parentalPinEditText.setOnEditorActionListener(this);
		parentalPinEditText.setRawInputType(Configuration.KEYBOARD_12KEY);
			
		
		parentalPinContainer = (LinearLayout) holder.getView(R.id.parental_pin_container);
		
		holder.setClickListener(this, R.id.btn_done);
		
		updateLevel();
		
		return view;
	}


	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if(actionId == EditorInfo.IME_ACTION_GO){
			if(areFieldsFilled()) {
        		applyChanges();
        	} else 
        		return true;
		}
		return false;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
			if(parent.getId() == R.id.parental_list_view ) 
			{
			selectedPosition = position;
			itemLevelsInstance = ParentalItemLevelsController.getInstance().getItemLevelsInstance();
			showParentalPinController(itemLevelsInstance.getParentalLevelItemList().get(selectedPosition));
			resetEditTexts();
		}
	}
	
	private void showParentalPinController(
			ParentalLevelItem selectedParentalLevel) 
	{
		currentParentalLevel = selectedParentalLevel.getLevel();
		if (currentParentalLevel == parentalLevel) {
			parentalPinContainer.setVisibility(View.GONE);
		} else {
			parentalPinContainer.setVisibility(View.VISIBLE);
		}
	}
	
	protected void resetEditTexts() {
		parentalPinEditText.setText(IConst.EMPTY_STRING);	
	}
	
	protected boolean areFieldsFilled() {
		if(!IConst.EMPTY_STRING.equals(parentalPinEditText.getText().toString()))
		{
			return true; 
		}
		else
		{
			return false;
		}
	}
	
	protected void applyChanges() {

		hideKeyboard(getActivity());
		
		if(areFieldsFilled())
		{
			ParentalItemLevelsInterface itemLevelsInstance = ParentalItemLevelsController.getInstance().getItemLevelsInstance();
			MvpParentalManagement.getInstance().setServerParentalLevel(parentalPinEditText.getText().toString(), itemLevelsInstance.getParentalLevelItemList().get(selectedPosition).getLevel(), this);
		}
		else 
		{	
			if(parentalPinContainer.isShown())
			{
				showToast("Field is empty");
			}
			else
			{
				showToast("Current level is already selected");
			}
		}
		
		resetEditTexts();

}

	private void updateLevel() 
	{
		if (selectedPosition != -1) 
		{
			ParentalItemLevelsInterface itemLevelsInstance = ParentalItemLevelsController
					.getInstance().getItemLevelsInstance();
			ParentalLevelItem currentSelectedParentalLevel = itemLevelsInstance
					.getParentalLevelItemList().get(selectedPosition);
			showParentalPinController(currentSelectedParentalLevel);
		}
	}



	@Override
	public void onClick(View v) {
		switch (v.getId())
		{
		case R.id.btn_done:
		{
			applyChanges();
		}
		}
	}
	

	@Override
	public void onReturnFromTask(Task task, boolean canceled) 
	{
		getMvpActivity().hideLoadingDialog();
		
		if (task.isError())
		{
			Object error = task.getError();
			if (error instanceof MvpServerError)
			{
				MvpServerError mvpError = (MvpServerError)error;
				int erroCode = mvpError.getResponseCode();
				getMvpActivity().hideLoadingDialog();
				
				if (erroCode == 403)
				{
					showErrorDialog(mvpError.getTitle());
					return;
				}
				
				if (erroCode == 200)
				{
					
					showErrorDialog(mvpError.getTitle());
					parentalLevel = MvpParentalManagement.getInstance().getServerLevel();
					levelsListView.setSelection(selectedPosition);
					selectedPosition = levelsList.indexOf(itemLevelsInstance.getParentalLevelByLevel(parentalLevel));
					adapter.notifyDataSetChange();
										return;
				}
				
				if (mvpError.getResponseCode() == 0)
				{
					showErrorDialog(MainApp.getRes().getString(R.string.internet_no_connection));
					return;
				}
				
			}
			
			
			if (error instanceof MvpError)
			{
				showErrorDialog(task.getError().displayError());
				return;
			}
			
			return;
		}
		else
		{
			showErrorDialog("New parental level is set.");
		}
	}
	
	public  AlertDialog showErrorDialog(String message) {
		if (DEBUG) Log.m(TAG,this,"showLoadingDialog");
		AlertDialog.Builder errorBuilder = new AlertDialog.Builder(getDialogContext());
		errorBuilder.setMessage(message).setCancelable(false)
		.setPositiveButton(R.string.pozitive_button_text_second, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int viewId) {
				dialog.dismiss();
			}
		});
		alertDialog = errorBuilder.create();
		alertDialog.show();
		return alertDialog;
	}
	
	private Context getDialogContext() {
		Context context = null;
		Activity activityParent = getActivity().getParent();
		if (activityParent != null) {
			context = activityParent;
		} else {
			context = getActivity();
		}
		return context;
	}


	@Override
	public void onListItemViewClick(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder) {
		selectedPosition = holder.getPosition();
		
	}


	@Override
	public boolean onListItemViewTouch(IMvpGenericAdapter adapter, View view,
			MvpItemHolder holder, MotionEvent event) {
		
		return false;
	}


	@Override
	public void onListItemUpdateView(IMvpGenericAdapter adapter,
			MvpItemHolder holder, int position) {
		 if (adapter.getType() == ParentalLevelItem.class)
		 {
			 updateItemView(adapter, holder, position);
			 
			return;
		 }
		
	}


	private void updateItemView(IMvpGenericAdapter adapter,
			MvpItemHolder holder, int position) {
		ParentalLevelItem itemLevel = levelsList.get(position);
		if(parentalLevel == itemLevel.getLevel())
		{
			itemLevel.setSelected(true);
			holder.setVisibility(R.id.check_image, View.VISIBLE);
		}
		else
		{
			itemLevel.setSelected(false);
			holder.setVisibility(R.id.check_image,  View.GONE);
		}
		
		holder.setText(R.id.level_name, itemLevel.getItemName());
		
		if(IConst.EMPTY_STRING.equals(itemLevel.getItemShortName()))
		{
			holder.setText(R.id.level_short_name, itemLevel.getItemShortName());
		}
		else
		{
			holder.setText(R.id.level_short_name, itemLevel.getItemShortName());
		}
	}


	@Override
	public void onListItemsCountChanged(IMvpGenericAdapter adapter, int newCount) {
		// TODO Auto-generated method stub
		
	}
	
	public static void hideKeyboard(Activity activity) {
	    InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
	    //Find the currently focused view, so we can grab the correct window token from it.
	    View view = activity.getCurrentFocus();
	    //If no view currently has focus, create a new one, just so we can grab a window token from it
	    if(view == null) {
	        view = new View(activity);
	    }
	    inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}


	@Override
	public int onListItemGetLayoutId(IMvpGenericAdapter adapter, int position) {
		// TODO Auto-generated method stub
		return R.layout.parental_control_level_row;
	}
	
	@Override
	public void update(Observable observable, Object data) {
		

	}
	
}
