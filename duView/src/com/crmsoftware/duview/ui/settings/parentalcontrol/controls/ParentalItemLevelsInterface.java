package com.crmsoftware.duview.ui.settings.parentalcontrol.controls;

import java.util.List;

public interface ParentalItemLevelsInterface {

	public void initItemWrapperList();
	public void reset();
	public List<ParentalLevelItem> getParentalLevelItemList();
	public void manageSelection (ParentalLevelItem level);
	public ParentalLevelItem getParentalLevelByLevel(int level);
	public void resetSelections();
	public int getTheLowestParentalLevel();

}
