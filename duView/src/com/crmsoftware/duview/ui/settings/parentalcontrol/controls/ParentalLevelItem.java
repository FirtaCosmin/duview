package com.crmsoftware.duview.ui.settings.parentalcontrol.controls;

import java.io.Serializable;

import com.crmsoftware.duview.utils.IConst;

public class ParentalLevelItem  implements Serializable{
	private static final long serialVersionUID = 230364335161052243L;
	
	private boolean				selected			= false;
	private String				itemShortName		= IConst.EMPTY_STRING;
	private String				itemName			= IConst.EMPTY_STRING;
	private int level = -1;
	
	public void setSelected(boolean isSelected) {
		this.selected = isSelected;
	}
	
	public boolean isSelected() {
		return selected;
	}
	
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	public String getItemName() {
		return itemName;
	}

	public void setItemShortName(String itemShortName) {
		this.itemShortName = itemShortName;
	}

	public String getItemShortName() {
		return itemShortName;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getLevel() {
		return level;
	}

}