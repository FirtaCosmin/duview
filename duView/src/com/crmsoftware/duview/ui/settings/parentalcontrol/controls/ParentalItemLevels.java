package com.crmsoftware.duview.ui.settings.parentalcontrol.controls;

import java.util.ArrayList;
import java.util.List;

import com.crmsoftware.duview.R;
import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.utils.IConst;

public class ParentalItemLevels implements ParentalItemLevelsInterface, IConst {
	private List<ParentalLevelItem>	levelList	= new ArrayList<ParentalLevelItem>();
	
	@Override
	public void initItemWrapperList() {
		ParentalLevelItem itemWrapper = new ParentalLevelItem();
		itemWrapper.setItemName(MainApp.getRes().getString(R.string.level_G));
		itemWrapper.setItemShortName(MainApp.getRes().getString(R.string.level_short_G));
		itemWrapper.setLevel(LEVEL_G);
		levelList.add(itemWrapper);
		
		itemWrapper = new ParentalLevelItem();
		itemWrapper.setItemName(MainApp.getRes().getString(R.string.level_PG));
		itemWrapper.setItemShortName(MainApp.getRes().getString(R.string.level_short_PG));
		itemWrapper.setLevel(LEVEL_PG);
		levelList.add(itemWrapper);
		
		itemWrapper = new ParentalLevelItem();
		itemWrapper.setItemName(MainApp.getRes().getString(R.string.level_PG_13));
		itemWrapper.setItemShortName(MainApp.getRes().getString(R.string.level_short_PG_13));
		itemWrapper.setLevel(LEVEL_PG_13);
		levelList.add(itemWrapper);
		
		itemWrapper = new ParentalLevelItem();
		itemWrapper.setItemName(MainApp.getRes().getString(R.string.level_R));
		itemWrapper.setItemShortName(MainApp.getRes().getString(R.string.level_short_R));
		itemWrapper.setLevel(LEVEL_R);
		levelList.add(itemWrapper);
		
		itemWrapper = new ParentalLevelItem();
		itemWrapper.setItemName(MainApp.getRes().getString(R.string.level_NC_17));
		itemWrapper.setItemShortName(MainApp.getRes().getString(R.string.level_short_NC_17));
		itemWrapper.setLevel(LEVEL_NC_17);
		levelList.add(itemWrapper);
		
		itemWrapper = new ParentalLevelItem();
		itemWrapper.setItemShortName(MainApp.getRes().getString(R.string.no_level));
		itemWrapper.setLevel(NO_LEVEL);
		levelList.add(itemWrapper);
	}
	
	@Override
	public void reset() {
		levelList.clear();
		initItemWrapperList();
	}
	
	@Override
	public List<ParentalLevelItem> getParentalLevelItemList() {
		return levelList;
	}
	
	@Override
	public void manageSelection (ParentalLevelItem level) {
		if(level != null){
			for (ParentalLevelItem parentalLevel : levelList) {
				if(level.getLevel()==parentalLevel.getLevel())
					parentalLevel.setSelected(true);
				else
					parentalLevel.setSelected(false);
			}
		} 
	}
	
	public void resetSelections () {
			for (ParentalLevelItem parentalLevel : levelList) {
					parentalLevel.setSelected(false);
			}
	}
	
	@Override
	public ParentalLevelItem getParentalLevelByLevel(int level){
		for (ParentalLevelItem parentalLevel : levelList) {
			if(level == parentalLevel.getLevel())
				return parentalLevel;
		}
		return null;
	}
	
	public int getTheLowestParentalLevel() {
		if(!levelList.isEmpty())
			return levelList.get(0).getLevel();
		return 0;
	}
	
}