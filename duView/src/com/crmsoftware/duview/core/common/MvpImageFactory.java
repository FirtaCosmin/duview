package com.crmsoftware.duview.core.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.crmsoftware.duview.data.MvpImage;

public class MvpImageFactory {

	private static Map<String, MvpImage> mapImages =  Collections.synchronizedMap(new HashMap<String, MvpImage>());
	private static List<MvpImage> listImages = Collections.synchronizedList(new ArrayList<MvpImage>());
	
	public static long TotalSpace = 0;
	private static long OccupiedSpace = 0;
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public static Bitmap convertToBitmap(byte[] data) 
	{
		if (null == data)
		{
			return null;
		}
		
		try 
		{
	        BitmapFactory.Options o2 = new BitmapFactory.Options();
	        o2.inSampleSize = 2;

	        return BitmapFactory.decodeByteArray(data, 0, data.length,o2); 
		} catch (Exception e) {
			e.printStackTrace();
		}


        return null;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public static MvpImage cache(String sURL, byte[] data) 
	{
		MvpImage image = null;
		synchronized (mapImages) 
		{
			image = mapImages.get(sURL);
			
			if (null == image)
			{
				image  = new MvpImage();
				mapImages.put(sURL, image);
			}
			else
			{
				listImages.remove(image);
				OccupiedSpace -= image.getSize();
			}
			
			Bitmap bmp = convertToBitmap(data);
			Long newSize = (long) (bmp.getHeight() * bmp.getWidth());
			
			OccupiedSpace += newSize;
			
			if (OccupiedSpace > TotalSpace) // if there is no space then clean the unreferenced images
			{
				int totalErased = 0;
				
				Collections.sort(listImages); //bigger at the end
				
				for (int i=listImages.size() - 1; i>=0; i--)	// we are starting to check the bigger images 
				{
					MvpImage mvpImage = listImages.get(i);
					
					if (false == mvpImage.isReferenced())
					{
						totalErased += mvpImage.getSize();
						
						listImages.remove(i);
						mapImages.remove(mvpImage.getUrl());
					}
					
					if (totalErased > TotalSpace/3) // we clean 1/3 of unused images (if there is the case)
					{
						break;
					}
				}
				
				OccupiedSpace -= totalErased;
			}
			
			listImages.add(image);
			image.update(sURL, bmp);
			

		}
		
		return image;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public static MvpImage remove(String sURL) 
	{
		synchronized (mapImages) 
		{
			return mapImages.remove(sURL);	
		}
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public static MvpImage getImage(String sURL) 
	{
		synchronized (mapImages) 
		{
			return mapImages.get(sURL);
		}
	}
}
