package com.crmsoftware.duview.core.common;
// ******************************************************************************************
//   Description:  file created for duView - CRM project
//        Author: Catalin Chitu (cata_chitu@yahoo.com)
// Creation date: 01/05/2014
// Modified data: 
// (c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URL;
import java.net.UnknownHostException;

import com.crmsoftware.duview.app.MainApp;
import com.crmsoftware.duview.data.MvpError;
import com.crmsoftware.duview.data.MvpServerError;
import com.crmsoftware.duview.data.MvpError.ErrorType;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;


public class TaskHttpRequest extends Task
{	
	public static boolean DEBUG = Log.DEBUG; 
	public static boolean IS_RELEASE = Log.IS_RELEASE; 
	public static final String TAG = TaskHttpRequest.class.getSimpleName();	
	
	protected HttpReqParams httpParams = null;

	public static final String HTTP_GET              	= "GET";
	public static final String HTTP_POST             	= "POST";
	
	
	protected String requestType = HTTP_GET;
	protected String sURL = null;
	protected InputStream requestStream = null;
	protected DataType dataType;
	
	protected int responseCode = -1;
	protected int requestTimeout = 30 * 1000; // msecs
	protected HttpURLConnection connection = null;
	
	protected byte[] byteData = null;
	protected String sData = "";
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public TaskHttpRequest(String requestType, String sURL, HttpReqParams httpParams, DataType dataType)
	{	
		if (DEBUG) Log.m(TAG,this,"ctor()");
		
		this.requestType = requestType;
		this.httpParams = httpParams;
		this.sURL = sURL;
		this.dataType = dataType;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public TaskHttpRequest(ITaskReturn callback, String requestType, String sURL, HttpReqParams httpParams, DataType dataType)
	{	
		super(callback);
		if (DEBUG) Log.m(TAG,this,"ctor()");
		
		this.requestType = requestType;
		this.httpParams = httpParams;
		this.sURL = sURL;
		this.dataType = dataType;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public int getResponseCode() 
	{
		return responseCode;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public byte[] getByteData()
	{
		return byteData;
	}
	
	public String getStringData()
	{
		return sData;
	}
	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onExecute() 
	{
		if (DEBUG) Log.m(TAG,this,"onExecute");
		
		Chrono crono = new Chrono();
		String tempURL = this.sURL;
		
		MvpError error = null;
		try
		{
			boolean hasPostData = false;
			if (httpParams != null)
			{
				tempURL = sURL+httpParams.asString();
				hasPostData = (false == Utils.isEmpty(httpParams.getPostData()));
			}			
			URI uri = new URI(tempURL);
				
			URL requestURL = uri.toURL();
			if (DEBUG) Log.d(TAG,">>>>>>>>");
			if (DEBUG) Log.d(TAG,tempURL);
			if (DEBUG) Log.d(TAG,">>>>>>>>");
			
			if (false == IS_RELEASE) 
			{
				Log.i(TAG,tempURL);
				//MainApp.getInstance().writetoLogFile("\n" + tempURL);
			}
			
			connection = (HttpURLConnection) requestURL.openConnection(); 		
			
			connection.setRequestMethod(requestType);
			
			connection.setConnectTimeout(requestTimeout);
			
			if (null != httpParams)
			{
				httpParams.applyProperties(connection);
			}
			
			connection.setDoOutput(hasPostData);
			
			connection.connect();
			
			if (hasPostData)
			{
				DataOutputStream wr = new DataOutputStream (
		                  connection.getOutputStream ());
			      wr.writeBytes (httpParams.getPostData());
			      wr.flush ();
			      wr.close ();								
			}
			
			
			responseCode = connection.getResponseCode();
			
			tempURL = connection.getURL().toString();
			
			if (HttpURLConnection.HTTP_OK == responseCode)
			{
					if (dataType == DataType.String)
					{				
				        sData = Utils.convertBufferToString(connection.getInputStream());
				    }
					else
					{
						byteData = Utils.convertBufferToByteArray(connection.getInputStream());
					}
			}
			else
			{
					
				sData = Utils.convertBufferToString(connection.getErrorStream());
				
		        setError(new MvpError(ErrorType.Http, responseCode));
			}
	
		}
		catch (SocketTimeoutException e)
		{
			e.printStackTrace();
			setError(new MvpServerError(connection));			
		}
		catch (UnknownHostException e)
		{
			e.printStackTrace();
			setError(new MvpServerError(connection));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			setError(new MvpError(ErrorType.Http, e));
		}

		
		if (null != connection)
		{
			connection.disconnect();
			connection = null;
		}
		

		if (DEBUG) Log.d(TAG,"[URL]: " + tempURL);
		if (DEBUG) Log.d(TAG,"data: " + sData);
		
		if (DEBUG) Log.d(TAG,"Http req: " + crono.print());
		
		taskCompleted();
	}

}

