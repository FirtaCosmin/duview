package com.crmsoftware.duview.core.common;

public interface ITaskReturn 
{
	void onReturnFromTask(Task task, boolean canceled);
}
