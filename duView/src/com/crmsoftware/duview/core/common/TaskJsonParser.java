package com.crmsoftware.duview.core.common;
//******************************************************************************************
//Description:  file created for duView - CRM project
// Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************

import org.json.JSONObject;

import com.crmsoftware.duview.data.IJsonParser;
import com.crmsoftware.duview.data.MvpError;
import com.crmsoftware.duview.data.MvpError.ErrorType;
import com.crmsoftware.duview.utils.Log;

public class TaskJsonParser extends Task {

	public static boolean DEBUG = Log.DEBUG; 
	public static final String TAG = TaskJsonParser.class.getSimpleName();	
	
	private String sJSON;
	private IJsonParser object;
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public TaskJsonParser(IJsonParser object, String sJSON) 
	{		
		this.object = object;
		this.sJSON = sJSON;
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public Object getReturnData()
	{		
		return object;
	}

	

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onExecute() {
		if (DEBUG) Log.m(TAG,this,"onExecute");
		
		try 
		{
	         JSONObject jsonObj = new JSONObject(sJSON);
             object.fromJson(jsonObj,null);
		} 
		catch (Exception e) 
		{
	         e.printStackTrace();
	         setError(new MvpError(ErrorType.Parser, e));
	    }
		
		taskCompleted();
	}
}
