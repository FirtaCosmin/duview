package com.crmsoftware.duview.core.common;

//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************


import java.util.Date;

import android.graphics.Canvas.EdgeType;

import com.crmsoftware.duview.data.IJsonParser;
import com.crmsoftware.duview.data.MvpError;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.data.MvpError.ErrorType;
import com.crmsoftware.duview.utils.Log;

public class TaskSingleReqCache<TemplateType> extends Task{

	public static boolean DEBUG = Log.DEBUG; 
	public final static String TAG = "TaskSingleReqCache"; 
	
	private Class classType;
	private String sURL;
	private HttpReqParams httpParams;
	private MvpObject mvpReturnData;
	
	private TaskGetDataFromStorage taskCacheGetData = null;
	private TaskPutDataToStorage taskCachePutData = null;
	private TaskSingleReqParse taskServerDataReqParser = null;
	private TaskJsonParser taskCacheDataParsing = null;
	private long dataLifeTime;
	
	public TaskSingleReqCache(Class classType, String sURL, HttpReqParams httpParams, MvpObject mvpReturnData, long dataLifeTime) 
	{
		if (DEBUG) Log.m(TAG,this,"ctor()");
		
		this.classType = classType;

		this.sURL = sURL;
		this.httpParams = httpParams;	
		
		this.mvpReturnData = mvpReturnData;
		this.dataLifeTime = dataLifeTime;
	}
	
	public TaskSingleReqCache(ITaskReturn callback, Class classType, String sURL, HttpReqParams httpParams, MvpObject mvpReturnData, long dataLifeTime) 
	{
		super(callback);
		if (DEBUG) Log.m(TAG,this,"ctor()");
		
		this.classType = classType;

		this.sURL = sURL;
		this.httpParams = httpParams;	
		
		this.mvpReturnData = mvpReturnData;
		this.dataLifeTime = dataLifeTime;
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onExecute() 
	{
		if (DEBUG) Log.m(TAG,this,"onExecute");
		
		taskCacheGetData = new TaskGetDataFromStorage(mvpReturnData.getId(), DataType.String);
		executeSubTask(taskCacheGetData);
		
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public Object getReturnData() {
		// TODO Auto-generated method stub
		return mvpReturnData;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onResetData()
	{
		super.onResetData();
		
		taskCacheGetData = null;
		taskCachePutData = null;
		taskServerDataReqParser = null;
		taskCacheDataParsing = null;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onReturnFromTask(Task subTask, boolean canceled) 
	{
		if (DEBUG) Log.m(TAG,this,"onReturnFromTask");
		
		super.onReturnFromTask(subTask, canceled);

		if (canceled || isCanceled())
		{
			return;
		}
		
		//----------------------------------------
		if (subTask == taskCacheGetData) // return from getting  data from disk
		{	
			if (taskCacheGetData.isError())	// no cache
			{
				taskServerDataReqParser = new TaskSingleReqParse(
						this,
						classType,
						sURL, 
						httpParams);
				
				executeSubTask(taskServerDataReqParser);
				return;
			}
			
			String sDataFromCache = taskCacheGetData.getStringData();
			
			if (null != sDataFromCache)
			{
				IJsonParser mvpData = null;
				try 
				{
					mvpData= (IJsonParser) classType.newInstance();
				} 
				catch (InstantiationException e) 
				{
					setError(new MvpError(ErrorType.Processing, e));
					
				} 
				catch (IllegalAccessException e) 
				{
					setError(new MvpError(ErrorType.Processing, e));
				}
				
				if (null == mvpData)
				{
					taskCompleted();
					return;
				}
				
				taskCacheDataParsing = new TaskJsonParser(mvpData, sDataFromCache);
				executeSubTask(taskCacheDataParsing);				
			}
			
			return;
		}
		
		//----------------------------------------		
		if (subTask == taskCacheDataParsing) // return from parsing cache data 
		{
			Date date = new Date() ;  
			long delta =  date.getTime() - taskCacheGetData.getFileLastUpdate();
			
			boolean requestFromServer = delta > dataLifeTime;
				
			if (false == taskCacheDataParsing.isError())
			{
				mvpReturnData.setData(taskCacheDataParsing.getReturnData());	// update & notify the peers about the data change
			}
			else
			{
				requestFromServer = true;
			}
			

			if (requestFromServer)
			{
				taskServerDataReqParser = new TaskSingleReqParse(
						this,
						classType,
						sURL, 
						httpParams);
				
				executeSubTask(taskServerDataReqParser);
				return;
			}
			
			taskCompleted();
		}
			
		
		//----------------------------------------	
		if (subTask == taskServerDataReqParser) // return from parsing data from server
		{
			if (taskServerDataReqParser.isError())
			{
				taskCompleted(taskServerDataReqParser.getError());
				return;
			}
			
			if (null == taskCacheGetData)	// in case that cache worker is  executed after server response
			{
				taskCompleted(new MvpError(ErrorType.Processing));
				return;
			}
				
			IJsonParser mvpData = (IJsonParser) taskServerDataReqParser.getReturnData();
			
			String newData = mvpData.toJsonString();
			String oldData = taskCacheGetData.getStringData();
			
			if (false == newData.equals(oldData)) 
			{	
				taskCachePutData = new TaskPutDataToStorage(taskCacheGetData.getFileId(), newData);
				executeSubTask(taskCachePutData);
			}
			else
			{
				taskCompleted();
			}
			
			return;
		}
		
		//----------------------------------------	
		if (subTask == taskCachePutData) // return from saving data
		{
			mvpReturnData.setData(taskServerDataReqParser.getReturnData());	// update & notify the peers about the data change
			
			taskCompleted();
			return;
		}

		
	}
	

	
}
