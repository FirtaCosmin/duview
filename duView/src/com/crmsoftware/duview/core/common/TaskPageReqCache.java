package com.crmsoftware.duview.core.common;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************



import java.util.Date;

import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.data.MvpPage;
import com.crmsoftware.duview.data.MvpServerError;
import com.crmsoftware.duview.utils.Log;



public class TaskPageReqCache<TemplateType> extends Task{

	public static boolean DEBUG = Log.DEBUG; 
	public static final String TAG = TaskPageReqCache.class.getSimpleName();	
	
	private Class classObjType;
	private Class classPagHeader;
	private String sURL;
	private HttpReqParams httpParams;
	private MvpObject mvpReturnData;
	private long dataLifeTime = 0;
	
	private TaskGetDataFromStorage taskCacheGetData = null;
	private TaskPutDataToStorage taskCachePutData = null;
	private TaskJsonRequest taskServerDataReq = null;
	private TaskJsonParser taskCacheDataParsing = null;
	private TaskJsonParser taskServerDataParsing = null;
	
	public TaskPageReqCache(ITaskReturn callback, Class classObjType, Class classPagHeader, String sURL, HttpReqParams httpParams, MvpObject mvpReturnData, long dataLifeTime) 
	{
		
		super(callback);
		
		if (DEBUG) Log.m(TAG,this,"ctor()");
		
		this.classObjType = classObjType;
		this.classPagHeader = classPagHeader;

		this.sURL = sURL;
		this.httpParams = httpParams;
		this.mvpReturnData = mvpReturnData;
		this.dataLifeTime = dataLifeTime;
		
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onExecute() 
	{
		if (DEBUG) Log.m(TAG,this,"onExecute");
		
		if (dataLifeTime >= 0)
		{
			taskCacheGetData = new TaskGetDataFromStorage(mvpReturnData.getId(), DataType.String);
			executeSubTask(taskCacheGetData);
		}
		else
		{
			taskServerDataReq = new TaskJsonRequest(sURL, httpParams);
			executeSubTask(taskServerDataReq);			
		}
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public Object getReturnData() 
	{
		
		return mvpReturnData;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onResetData()
	{
		super.onResetData();
		
		taskCacheGetData = null;
		taskCachePutData = null;
		taskServerDataReq = null;
		taskCacheDataParsing = null;
		taskServerDataParsing = null;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onReturnFromTask(Task subTask, boolean canceled) 
	{
		if (DEBUG) Log.m(TAG,this,"onReturnFromTask");
		
		super.onReturnFromTask(subTask, canceled);

		if (canceled || isCanceled())
		{
			return;
		}
		
		//----------------------------------------
		if (subTask == taskCacheGetData) // return from getting  data from disk
		{	
			if (taskCacheGetData.isError())	// no cache
			{
				taskServerDataReq = new TaskJsonRequest(
						sURL, 
						httpParams);
				executeSubTask(taskServerDataReq);
				return;
			}
				
			String sDataFromCache = ((TaskGetDataFromStorage)subTask).getStringData();
			
			if (null != sDataFromCache && sDataFromCache.length() > 0)
			{
				MvpPage<TemplateType> cachedPaged = new MvpPage<TemplateType>(classObjType, classPagHeader);
				
				taskCacheDataParsing = new TaskJsonParser(cachedPaged, sDataFromCache);
				executeSubTask(taskCacheDataParsing);				
			}
			
			
			return;
		}
		
		//----------------------------------------		
		if (subTask == taskCacheDataParsing) // return from parsing cache data 
		{
			Date date = new Date() ;  
			long delta =  date.getTime() - taskCacheGetData.getFileLastUpdate();
			
			boolean requestFromServer = delta > dataLifeTime;
				
			if (false == taskCacheDataParsing.isError())
			{
				mvpReturnData.setData(taskCacheDataParsing.getReturnData());	// update & notify the peers about the data change
			}
			else
			{
				requestFromServer = true;
			}
			

			if (requestFromServer)
			{
				taskServerDataReq = new TaskJsonRequest(
						sURL, 
						httpParams);
				executeSubTask(taskServerDataReq);
				return;
			}
			
			taskCompleted();
			
			return;
		}
		
		//----------------------------------------		
		if (subTask ==  taskServerDataReq) // return from getting data from server
		{		
			if (taskServerDataReq.isError())
			{
				taskCompleted(new MvpServerError(taskServerDataReq.getStringData()));
				return;
			}
			
			if (taskServerDataReq.getStringData().contains(MvpServerError.TAG_RESPONSECODE))
			{
				taskCompleted(new MvpServerError(taskServerDataReq.getStringData()));
				return;				
			}
			
			MvpPage<TemplateType> serverPage = new MvpPage<TemplateType>(classObjType, classPagHeader);			
			
			taskServerDataParsing = new TaskJsonParser(serverPage, ((TaskHttpRequest)subTask).getStringData());
			executeSubTask(taskServerDataParsing);	
			
			return;
		}
		
		//----------------------------------------	
		if (subTask == taskServerDataParsing) // return from parsing data from server
		{
			if (taskServerDataParsing.isError())
			{
				taskCompleted(taskServerDataParsing.getError());
				return;
			}
			
			MvpPage<TemplateType> mvpPage = (MvpPage<TemplateType>) taskServerDataParsing.getReturnData();
			String saveData = mvpPage.toJsonString();
			
			if (null != taskCacheGetData)	// in case that cache  was  executed
			{
				String oldData = taskCacheGetData.getStringData();
				if (true == saveData.equals(oldData)) 
				{	
					taskCompleted();
					return;
				}
			}
			
			if (false == saveData.isEmpty())
			{
				taskCachePutData = new TaskPutDataToStorage(mvpReturnData.getId(), saveData);
				executeSubTask(taskCachePutData);
			}
			else
			{
				taskCompleted();
			}
			return;
		}
		
		//----------------------------------------	
		if (subTask == taskCachePutData) // return from saving data
		{
			mvpReturnData.setData(taskServerDataParsing.getReturnData());	// update & notify the peers about the data change
			
			taskCompleted();
			return;
		}

		
	}
	

	
}
