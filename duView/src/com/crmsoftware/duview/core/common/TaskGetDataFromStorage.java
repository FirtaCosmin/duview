package com.crmsoftware.duview.core.common;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import com.crmsoftware.duview.data.MvpError;
import com.crmsoftware.duview.data.MvpError.ErrorType;
import com.crmsoftware.duview.utils.AppSettings;
import com.crmsoftware.duview.utils.Log;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************
import com.crmsoftware.duview.utils.Utils;


public class TaskGetDataFromStorage extends Task{

	public static boolean DEBUG = Log.DEBUG; 
	public static final String TAG = TaskGetDataFromStorage.class.getSimpleName();	

	
	public static String PATH = AppSettings.getInstance().getCacheDirectory();
	
	private String fileId;
	private DataType dataType;
	
	protected byte[] byteData = null;
	protected String sData = "";
	
	protected long fileLastUpdate = 0; //sec
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public TaskGetDataFromStorage(String fileId, DataType dataType) 
	{
		if (DEBUG) Log.m(TAG,this,"ctor()");
		
		this.fileId = fileId;
		this.dataType = dataType;
		fileLastUpdate= 0;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	public byte[] getByteData()
	{
		return byteData;
	}

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public String getStringData()
	{
		return sData;
	}

	@Override
	protected void finalize() throws Throwable {
		
		byteData = null;
		sData = null;
		
		super.finalize();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public String getFileId()
	{
		return fileId;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public long getFileLastUpdate()
	{
		return fileLastUpdate;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	@Override
	protected void onExecute() 
	{
		if (DEBUG) Log.m(TAG,this,"onExecute");

		String fullPath = PATH + "/" + fileId;
		
	   InputStream in = null;
		try 
		{
			if (DEBUG) Log.d(TAG,"reading file: " + fullPath);
			
			
			File file = new File(fullPath);
			if (file.exists())
			{
				fileLastUpdate = file.lastModified();
			}
			
			FileInputStream fin = new FileInputStream(file);
			if (DEBUG) Log.d(TAG,"Size (bytes): " + fin.available());
			
			BufferedInputStream bis = new BufferedInputStream(fin);
			if (dataType == DataType.String)
			{				
		        sData = Utils.convertBufferToString(bis);
		    }
			else
			{
				byteData = Utils.convertBufferToByteArray(bis);
			}

		} 
		catch (FileNotFoundException e) 
		{
			if (DEBUG) Log.d(TAG,"error:  " + fullPath);
			setError(new MvpError(ErrorType.Processing, e));
		} catch (IOException e) 
		{
			if (DEBUG) Log.d(TAG,"error:  " + fullPath);
			e.printStackTrace();
		}
		finally 
		{
			if (in != null) 
			{
				try 
				{
					in.close();
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
				
		taskCompleted();
	}



}
