package com.crmsoftware.duview.core.common;
//******************************************************************************************
//Description:  file created for duView - CRM project
// Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************

import com.crmsoftware.duview.utils.Log;


public class TaskDataDownload extends TaskHttpRequest {
	public static boolean DEBUG = Log.DEBUG; 
	public static final String TAG = TaskDataDownload.class.getSimpleName();	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	public TaskDataDownload(String sURL) 
	{
		super(HTTP_GET, sURL, null, DataType.Byte);
		
		if (DEBUG) Log.m(TAG,this,"TaskImageDownload");
	}
}

