package com.crmsoftware.duview.core.common;
//******************************************************************************************
//Description:  file created for duView - CRM project
// Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Future;

import android.os.Handler;

import com.crmsoftware.duview.data.MvpError;
import com.crmsoftware.duview.data.MvpError.ErrorType;
import com.crmsoftware.duview.utils.Log;


public class Task implements Runnable, ITaskReturn{

	public static boolean DEBUG = Log.DEBUG; 
	public static boolean VERBOUSE = Log.VERBOUSE; 
	public static boolean INFO = Log.INFO; 
	public static final String TAG = Task.class.getSimpleName();	
	
	public static final boolean SYNC_MODE = false; 
	public static final boolean ASYNC_MODE = true; 	
	
	
	public static Long idCounter = new Long(0);
	
	protected Object taskId = null;
	
	private boolean executeMode = ASYNC_MODE;
	
	protected Thread thread = null;
	private MvpError error = null;
	private Handler handlerParent = null;
	
	protected Future taskHandler;
	
	public enum State {Created, Pooled , Running , Completed, Canceled, CanceledWithNotif} ;
	protected State executionState = State.Created;
 
	protected ITaskReturn callback = null;
	protected List<Task> listSubTasks = Collections.synchronizedList(new ArrayList<Task>());
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public Task()
	{
		if (DEBUG) Log.m(TAG,this,"ctor");
		setState(State.Created);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	protected void finalize() throws Throwable {
		if (DEBUG) Log.m(TAG,this,"finalize");
		super.finalize();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public Task(ITaskReturn callback)
	{
		if (DEBUG) Log.m(TAG,this,"ctor(parent)");
		this.callback = callback;
		setState(State.Created);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void run() 
	{
		if (DEBUG) Log.m(TAG,this,"run");
		
		thread = Thread.currentThread();
		
		if (isCanceled())
		{
			return;
		}
		
		setState(State.Running);
		
		if (INFO) Log.m(TAG,this,"run: ");
		
		synchronized (this)
		{
			try 
			{
				onResetData();
				onExecute();					
			} 
			catch (Exception e) 
			{
				if (DEBUG) e.printStackTrace();
			}
		
		} 
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected  void onExecute()
	{	
		if (DEBUG) Log.m(TAG,this,"onExecute");
		taskCompleted();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected  void onResetData()
	{	
		if (DEBUG) Log.m(TAG,this,"onResetData");

	}
	
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected  void taskCompleted(MvpError error)
	{
		if (DEBUG) Log.m(TAG,this,"taskCompleted(e)");
		
		if (DEBUG) Log.d(TAG,"	**Error** " + error.toString());
		this.error = error;
		taskCompleted();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected  void taskCompleted()
	{
		if (DEBUG) Log.m(TAG,this,"taskCompleted");
		
		if (isCanceled())
		{
			return;
		}
		
		if (executionState == State.Running)
		{
			setState(State.Completed);
		}
		
		if (null != callback)
		{
			final boolean canceled = executionState == State.CanceledWithNotif || executionState == State.Canceled;
			if (null != handlerParent)
			{
				handlerParent.post(new Runnable() 
				{
					
					@Override
					public void run() {
						callback.onReturnFromTask(Task.this, canceled);
					}
				});
			}
			else
			{
				callback.onReturnFromTask(Task.this, canceled);	
			}	
		}	
		
		onResetData();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public MvpError getError()
	{
		if (DEBUG) Log.m(TAG,this,"getError");
		
		return error;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public boolean isError()
	{
		if (VERBOUSE) Log.m(TAG,this,"isError");
		
		return error != null;
	}
	
	 /**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected  void setError(MvpError error)
	{
		if (DEBUG) Log.m(TAG,this,"setError");
		
		this.error = error;
	}
	

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	protected void executeSubTask(Task subTask)
	{
		if (DEBUG) Log.m(TAG,this,"executeSubTask");
		
		if (isCanceled())
		{
			return;
		}
		
		synchronized (listSubTasks) 
		{
			listSubTasks.add(subTask);
		}
		
		if (DEBUG) Log.d(TAG,"worker id = " + taskId);
		
		subTask.callback = this;
		subTask.execute();
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void execute()
	{
		if (DEBUG) Log.m(TAG,this,"execute");
		
		synchronized (idCounter)
		{
			++idCounter;
			execute(idCounter,ASYNC_MODE);
		} 
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void execute(Object taskId, boolean executeMode)
	{
		if (DEBUG) Log.m(TAG,this,"execute");
		
		this.executeMode = executeMode;
		
		if (Thread.currentThread().getId() == 1)
		{
			handlerParent = new Handler(); // for UI thread only
		}
		
		if (null == taskId)
		{
			return;
		}
		
		this.taskId = taskId;
		
		if (DEBUG) Log.d(TAG,"worker id = " + taskId);
		
		setState(State.Pooled);
		
		if (executeMode)
		{
			taskHandler = TasksPoolExecutor.getInstance().submit(this);
		}
		else
		{
			onExecute();
		}
	}

	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public Object getTaskId()
	{
		return taskId;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public void cancel(boolean noParentNotif)
	{
		if (DEBUG) Log.m(TAG,this,"cancel");
		
		setState(State.Canceled);

		
		boolean bCancel = TasksPoolExecutor.getInstance().remove(this);
		if (DEBUG) Log.d(TAG, "TasksPoolExecutor canceling "+ taskId+ ": " + bCancel);
		if (null != taskHandler)
		{
			bCancel = taskHandler.cancel(false);
			if (DEBUG) Log.d(TAG, "Future canceling "+ taskId+ ": " + bCancel);
		}
		
		synchronized (listSubTasks) 
		{
			for (int i=0; i<listSubTasks.size();i++)
			{
				listSubTasks.get(i).cancel(noParentNotif);
			}
			listSubTasks.clear();
		}
		
		if (false == noParentNotif)
		{
			if (null != handlerParent)
			{
				handlerParent.post(new Runnable() 
				{
					
					@Override
					public void run() {
						callback.onReturnFromTask(Task.this, true);
					}
				});
			}
			else
			{
				callback.onReturnFromTask(Task.this, true);	
			}			
		}

	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	
	public Object getReturnData()
	{
		return  null;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	public void onReturnFromTask(Task subTask, boolean canceled)
	{
		if (DEBUG) Log.m(TAG,this,"onReturnFromTask");
		
		if (DEBUG) Log.d(TAG,"return from worker id = " + subTask.taskId);
		
		synchronized (listSubTasks) 
		{
			listSubTasks.remove(subTask);
		}
	}
	
	
	public boolean isCanceled()
	{
		boolean bState = true;
		synchronized (executionState) 
		{
			bState = (executionState == State.Canceled);
		}
		return bState;
	}
	
	public State getState()
	{
		return executionState;
	}
	
	protected void setState(State state)
	{
		if (DEBUG) Log.m(TAG,this,"setState");
		
		synchronized (executionState) 
		{
			 executionState = state;
				if (DEBUG) Log.d(TAG,"State: " + executionState);
		}
	}

}
