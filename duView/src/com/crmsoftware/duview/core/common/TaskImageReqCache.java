package com.crmsoftware.duview.core.common;

//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import com.crmsoftware.duview.data.MvpError;
import com.crmsoftware.duview.data.MvpError.ErrorType;
import com.crmsoftware.duview.data.MvpImage;
import com.crmsoftware.duview.utils.Log;
import com.crmsoftware.duview.utils.Utils;

public class TaskImageReqCache extends Task{

	public static boolean DEBUG = Log.DEBUG; 
	public static final String TAG = TaskImageReqCache.class.getSimpleName();	

	private String sURL;
	private long dataLifeTime = 0;

	
	private TaskGetDataFromStorage taskCacheGetData = null;
	private TaskPutDataToStorage taskCachePutData = null;
	private TaskDataDownload taskServerDataReq = null;
	

	private MvpImage returnData = null;
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public TaskImageReqCache(ITaskReturn callback, String sURL, long dataLifeTime) 
	{
		super(callback);
		
		if (DEBUG) Log.m(TAG,this,"ctor()");
		
		this.sURL = sURL;	
		this.dataLifeTime = dataLifeTime;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	public String getUrl() 
	{
		return sURL;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
    private String encodeUrl(String sURL) 
    {
		if (DEBUG) Log.m(TAG,this,"encodeUrl()");
		
    	int lastSlashIndex = sURL.lastIndexOf('/');
    	if (lastSlashIndex <= 0) 
    	{
    		return sURL;
    	}
    	
        String urlAnte = sURL.substring(0, lastSlashIndex);
        String urlAfter = sURL.substring(lastSlashIndex + 1);
        
        try 
        {
			return urlAnte + "/" + URLEncoder.encode(urlAfter, "ISO-8859-1").replace("+", "%20");
		} 
        catch (UnsupportedEncodingException e) 
        {
			e.printStackTrace();
		}
		return "";
    }
    

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	@Override
	protected void onExecute() 
	{
		if (DEBUG) Log.m(TAG,this,"onExecute");
		
		
		
		String newURL = encodeUrl(sURL);
		if (newURL.isEmpty())
		{
			taskCompleted(new MvpError(ErrorType.Http));
			return;
		}
		
		sURL = newURL ;
		
		MvpImage image = MvpImageFactory.getImage(sURL);

		if (image == null)
		{
			String sId = Utils.formatLink(sURL)+".bin";
			taskCacheGetData = new TaskGetDataFromStorage(sId, DataType.Byte);
			executeSubTask(taskCacheGetData);
			return;
		}
		
		returnData = image;
		taskCompleted();
		
	
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */			
	@Override
	public Object getReturnData() {

		return returnData;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onResetData()
	{
		super.onResetData();
		
		taskCacheGetData = null;
		taskCachePutData = null;
		taskServerDataReq = null;
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onReturnFromTask(Task subTask, boolean canceled) 
	{
		if (DEBUG) Log.m(TAG,this,"onReturnFromTask");
		
		super.onReturnFromTask(subTask, canceled);

		if (canceled || isCanceled())
		{
			return;
		}
		
		//----------------------------------------
		if (subTask == taskCacheGetData) // return from getting  data from disk
		{	
						
			if (taskCacheGetData.isError())	// no cache
			{
				taskServerDataReq = new TaskDataDownload(sURL);
				executeSubTask(taskServerDataReq);
				return;
			}
			
			returnData = MvpImageFactory.cache(sURL, taskCacheGetData.getByteData());
			
			Date date = new Date() ;  
			long delta =  date.getTime() - taskCacheGetData.getFileLastUpdate();
			
			if (delta < dataLifeTime)
			{
				taskCompleted();
				return;
			}
			
			taskServerDataReq = new TaskDataDownload(sURL);
			executeSubTask(taskServerDataReq);
			return;
		}

		
		//----------------------------------------		
		if (subTask ==  taskServerDataReq) // return from getting data from server
		{		
			if (taskServerDataReq.isError())
			{
				taskCompleted(taskServerDataReq.getError());
				return;
			}
			
			byte[] byteDataFromServer = taskServerDataReq.getByteData();
			byte[] byteDataFromCache = null;

			boolean dataChanged = false;
			
			if (null != taskCacheGetData)
			{
				byteDataFromCache = taskCacheGetData.getByteData();				
			}
			
			if (null==byteDataFromCache)
			{
				dataChanged = true;
			}
			else
			{
				if (byteDataFromServer != null)
				{
					if (byteDataFromCache.length != byteDataFromServer.length)	
					{
						dataChanged = true;
					}
				}
			}
			
			if (dataChanged)
			{
				taskCachePutData = new TaskPutDataToStorage(taskCacheGetData.getFileId(), byteDataFromServer);
				executeSubTask(taskCachePutData);
				return;
			}
			
			taskCompleted();
			
			return;
		}
		
		//----------------------------------------	
		
		if (subTask == taskCachePutData) // return from saving data
		{
			returnData = MvpImageFactory.cache(sURL, taskServerDataReq.getByteData());
			taskCompleted();
			return;
		}
	}
	

	
}
