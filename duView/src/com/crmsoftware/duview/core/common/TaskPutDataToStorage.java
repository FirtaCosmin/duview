
package com.crmsoftware.duview.core.common;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.os.Environment;

import com.crmsoftware.duview.configuration.PlatformSettings;
import com.crmsoftware.duview.data.MvpError;
import com.crmsoftware.duview.data.MvpList;
import com.crmsoftware.duview.data.MvpMedia;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.data.MvpError.ErrorType;
import com.crmsoftware.duview.utils.AppSettings;
import com.crmsoftware.duview.utils.Log;
//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************
import com.crmsoftware.duview.utils.Utils;


public class TaskPutDataToStorage extends Task{

	public static boolean DEBUG = Log.DEBUG; 
	public final static String TAG = "TaskPutDataToStorage"; 

	public static String PATH = AppSettings.getInstance().getCacheDirectory();
	
	private String fileId;
	private String sData;
	
	private byte[] byteBuffer = null;
	
	public TaskPutDataToStorage(String fileId, String sData) 
	{
		if (DEBUG) Log.m(TAG,this,"ctor(String)");
		
		this.fileId = fileId;
		this.byteBuffer = null;
		this.sData = sData;
	}
	
	public TaskPutDataToStorage(String fileId,  byte[] byteBuffer) 
	{
		if (DEBUG) Log.m(TAG,this,"ctor(byte[])");
		
		this.fileId = fileId;
		this.byteBuffer = byteBuffer;
		this.sData = null;
	}

	
	@Override
	protected void onExecute() 
	{
		if (DEBUG) Log.m(TAG,this,"onExecute");

		String fullPath = PATH + "/" + fileId;
		
		FileOutputStream fOut = null;
		OutputStreamWriter osw = null;
		    
		try 
		{
			if (DEBUG) Log.d(TAG,"writing file: " + fullPath);
			
			
			File file = new File(fullPath);
			boolean bValidFile  = file.exists() && !file.isDirectory();
			
			
			fOut = new FileOutputStream(file);
			
			if (null != sData)
			{
				osw = new OutputStreamWriter(fOut);
				osw.write(sData);
				osw.flush();
				
				if (DEBUG) Log.d(TAG,"Data:\n " + sData);
			}
			else
			{
				fOut.write(byteBuffer);
				if (DEBUG) Log.d(TAG,"Data: []byte size=" + byteBuffer.length);
			}
			fOut.flush();			
			
		} 
		catch (FileNotFoundException e) 
		{
			if (DEBUG) Log.d(TAG,"error:  " + fullPath);
			setError(new MvpError(ErrorType.Processing, e));
		} catch (IOException e) 
		{
			if (DEBUG) Log.d(TAG,"error:  " + fullPath);
			e.printStackTrace();
		}
		finally 
		{

			if (osw != null) 
			{
				try 
				{
					osw.close();
				} 
				catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			if (fOut != null) 
			{
				try 
				{
					fOut.close();
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
				
		taskCompleted();
	}



}
