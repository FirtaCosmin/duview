package com.crmsoftware.duview.core.common;
//******************************************************************************************
//Description:  file created for duView - CRM project
//   Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import com.crmsoftware.duview.utils.Log;

public class TasksPoolExecutor extends ThreadPoolExecutor
{
	public static boolean DEBUG = Log.DEBUG; 
	public static final String TAG = TasksPoolExecutor.class.getSimpleName();	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	   private boolean isPaused;
	   
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	   private ReentrantLock pauseLock = new ReentrantLock();
	   
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	   private Condition unpaused = pauseLock.newCondition();

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	   private static TasksPoolExecutor instance = null;
	   
	   
	   private BlockingQueue<Task> workQueue;
	   
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	   public static TasksPoolExecutor getInstance()
	   {
			if (null == instance)
			{
				 int corePoolSize = 8;//Runtime.getRuntime().availableProcessors();
				 int maximumPoolSize = corePoolSize*16;
				 
				 long keepAliveTime = 1;	// amount of time an idle thread waits before terminating
				 TimeUnit unit = TimeUnit.SECONDS; // Time Unit to seconds
				 BlockingQueue<Task> workQueue = new LinkedBlockingQueue<Task>();	
				 RejectedExecutionHandler handler = new RejectedExecutionHandler() {
					
					@Override
					public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
						
						if (r instanceof Task)
						{
							Task worker = (Task) r;
							Log.e (TAG,"rejectedExecution: " + worker);
						}
						
					}
				};
				 
				instance = new TasksPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, handler);
				
			}
			return instance;
	   }
	   
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		   
	public TasksPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, java.util.concurrent.TimeUnit unit, BlockingQueue workersQueue, RejectedExecutionHandler handler)
	{		   
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workersQueue);
	}; 

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
   protected void beforeExecute(Thread t, Runnable r) {
	   
	 super.beforeExecute(t, r);
     pauseLock.lock();
     try 
     {
       while (isPaused) unpaused.await();
     } 
     catch (InterruptedException ie) {
       t.interrupt();
     } finally {
       pauseLock.unlock();
     }
   }

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	   public void pause() {
	     pauseLock.lock();
	     try {
	       isPaused = true;
	     } finally {
	       pauseLock.unlock();
	     }
	   }

	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	   public void resume() {
	     pauseLock.lock();
	     try {
	       isPaused = false;
	       unpaused.signalAll();
	     } finally {
	       pauseLock.unlock();
	     }
	   }

}
