package com.crmsoftware.duview.core.common;

//******************************************************************************************
//Description:  file created for duView - CRM project
//Author: Catalin Chitu (cata_chitu@yahoo.com)
//Creation date: 01/05/2014
//Modified data: 
//(c) 2014 CRM Software - all copy, distribution not allowed without CRM Software permission
//******************************************************************************************


import com.crmsoftware.duview.data.IJsonParser;
import com.crmsoftware.duview.data.MvpError;
import com.crmsoftware.duview.data.MvpObject;
import com.crmsoftware.duview.data.MvpServerError;
import com.crmsoftware.duview.data.MvpError.ErrorType;
import com.crmsoftware.duview.utils.Log;

public class TaskSingleReqParse<TemplateType> extends Task{

	public static boolean DEBUG = Log.DEBUG; 
	public final static String TAG = "TaskSingleReqCache"; 
	
	private Class classType;
	private String sURL;
	private HttpReqParams httpParams;
	public Object mvpData = null;
	
	private TaskJsonRequest taskServerDataReq = null;
	private TaskJsonParser taskServerDataParsing = null;
	
	public TaskSingleReqParse(ITaskReturn callback, Class classType, String sURL, HttpReqParams httpParams) 
	{
		super(callback);
		
		if (DEBUG) Log.m(TAG,this,"ctor()");
		
		this.classType = classType;

		this.sURL = sURL;
		this.httpParams = httpParams;	
	}

	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onExecute() 
	{
		if (DEBUG) Log.m(TAG,this,"onExecute");

		
		taskServerDataReq = new TaskJsonRequest(
				sURL, 
				httpParams);
		executeSubTask(taskServerDataReq);
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	protected void onResetData()
	{
		super.onResetData();
		
		taskServerDataReq = null;
		taskServerDataParsing = null;
	}
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */	
	
	public Object getReturnData()
	{
		return  mvpData;
	}
	
	
	/**
	 * 
	 *
	 * @param  
	 * @return      
	 * @see         
	 */		
	@Override
	public void onReturnFromTask(Task subTask, boolean canceled) 
	{
		if (DEBUG) Log.m(TAG,this,"onReturnFromTask");
		
		super.onReturnFromTask(subTask, canceled);

		if (canceled || isCanceled())
		{
			return;
		}
		
		//----------------------------------------		
		if (subTask ==  taskServerDataReq) // return from getting data from server
		{		
			if (taskServerDataReq.isError())
			{
				taskCompleted(taskServerDataReq.getError());
				return;
			}
			
			if (taskServerDataReq.getStringData().contains(MvpServerError.TAG_RESPONSECODE))
			{
				taskCompleted(new MvpServerError(taskServerDataReq.getStringData()));
				return;				
			}
			
			IJsonParser mvpData = null;
			try {
				mvpData= (IJsonParser) classType.newInstance();
			} 
			catch (InstantiationException e) 
			{
				setError(new MvpError(ErrorType.Processing, e));
				
			} 
			catch (IllegalAccessException e) 
			{
				setError(new MvpError(ErrorType.Processing, e));
			}
			
			
			taskServerDataParsing = new TaskJsonParser(mvpData, taskServerDataReq.getStringData());
			executeSubTask(taskServerDataParsing);	
			
			return;
		}
		
		//----------------------------------------	
		if (subTask == taskServerDataParsing) // return from parsing data from server
		{
			if (taskServerDataParsing.isError())
			{
				taskCompleted(new MvpError(ErrorType.Processing, taskServerDataParsing.getError()));
				return;
			}
			
			mvpData = taskServerDataParsing.getReturnData();
			taskCompleted();
				
			return;
		}
		
	}
	

	
}
